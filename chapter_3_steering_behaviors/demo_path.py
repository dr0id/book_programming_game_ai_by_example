#!/usr/bin/python
# -*- coding: utf-8 -*-

import random

import pygame

from vectors import Vec2 as Vec
import gameentity
from params import params
import mainloop
import baseworld
import waypointpath

# ------------------------------------------------------------------------------
class World(baseworld.BaseWorld):

    def init(self):
        self.title = "Steering Behaviors - follow path"

        #setup vehicle
        vehicle = gameentity.Vehicle(self, Vec(250, 250), 20, Vec(0, 0), params.MaxSpeed, Vec(1, 1), params.VehicleMass,
                                     params.VehicleScale, params.MaxTurnRatePerSecond, params.SteeringForce * 2.0,
                                     params)
        vehicle.steering.switch_on(vehicle.steering.FOLLOWPATH)
        self.border = 100
        vehicle.steering.path = waypointpath.WayPointPath.create_randomized_path(random.randint(4, 10), self.border, self.border, self.size[0] - self.border, self.size[1] - self.border, True)
        self.vehicles.append(vehicle)
        
        self.draw_path_on = True
        self.dt = 0

    def update(self, dt):
        self.dt = dt
        for vehicle in self.vehicles:
            vehicle.update(dt)
        self.check_keys(dt)

    def refresh_text(self):
        self.text_to_draw = [ (410, 477, "'R' to reset"),
                              (10, 477, "'U' to randomize path"),
                              (10, 10, "MaxSteeringForce (Ins/Del): " + str(self.vehicles[0].max_force / params.SteeringForceTweaker)),
                              (10, 30, "MaxSpeed (Home/End): " + str(self.vehicles[0].max_speed)),
                              (10, 50, "WaypointSeekDist (D/C): " + str(self.vehicles[0].steering.waypointseekdist)),
                              ]

    # --- controller --- #

    def check_keys(self, dt):
        # this pollutes the world class with pygame
        delta = 1.0 * dt
        keys = pygame.key.get_pressed()
        if keys[pygame.K_INSERT]:
            self.vehicles[0].max_force += delta
            self.refresh_text()
        if keys[pygame.K_DELETE]:
            self.vehicles[0].max_force -= delta
            self.refresh_text()
        if keys[pygame.K_HOME]:
            self.vehicles[0].max_speed += delta
            self.refresh_text()
        if keys[pygame.K_END]:
            self.vehicles[0].max_speed -= delta
            self.refresh_text()
        if keys[pygame.K_d]:
            self.vehicles[0].steering.waypointseekdist += delta
            self.refresh_text()
        if keys[pygame.K_c]:
            self.vehicles[0].steering.waypointseekdist -= delta
            if self.vehicles[0].steering.waypointseekdist < 1:
                self.vehicles[0].steering.waypointseekdist = 1
            self.refresh_text()

    def on_event(self, event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_u:
                self.vehicles[0].steering.path = waypointpath.WayPointPath.create_randomized_path(random.randint(4, 10), self.border, self.border, self.size[0] - self.border, self.size[1] - self.border, True)
                
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
if __name__ == "__main__":
    mainloop.MainLoop(World).run()

