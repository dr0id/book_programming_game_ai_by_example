#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame

from vectors import Vec2 as Vec
import gameentity
from params import params
import mainloop
import baseworld

# ------------------------------------------------------------------------------
class World(baseworld.BaseWorld):

    def init(self):
        self.title = "Steering Behaviors - interpose"
        self.draw_wander_info_on = True
        
        speed_tuner = 0.5
        
        #setup vehicle
        agent1 = gameentity.Vehicle(self, Vec(250, 250), 20, Vec(0, 0), params.MaxSpeed, Vec(1, 1), params.VehicleMass,
                                    params.VehicleScale, params.MaxTurnRatePerSecond, params.SteeringForce,
                                    params)
        agent1.steering.switch_on(agent1.steering.WANDER)
        self.vehicles.append(agent1)
        
        agent2 = gameentity.Vehicle(self, Vec(250, 250), 20, Vec(0, 0), params.MaxSpeed * speed_tuner, Vec(1, 1),
                                    params.VehicleMass, params.VehicleScale, params.MaxTurnRatePerSecond,
                                    params.SteeringForce, params)
        agent2.steering.switch_on(agent2.steering.WANDER)
        self.vehicles.append(agent2) 
        
        interposer = gameentity.Vehicle(self, Vec(100, 100), 20, Vec(0, 0), params.MaxSpeed, Vec(1, 1),
                                        params.VehicleMass, params.VehicleScale, params.MaxTurnRatePerSecond,
                                        params.SteeringForce, params)
        interposer.steering.switch_on(interposer.steering.INTERPOSE)
        interposer.color = (255, 0, 0)
        interposer.steering.target_agent1 = agent1
        interposer.steering.target_agent2 = agent2
        self.vehicles.append(interposer)
        
        self.draw_wander_info_on = False
        
    def update(self, dt):
        for vehicle in self.vehicles:
            vehicle.update(dt)
        self.crosshair = self.vehicles[2].steering.interpose_point
        self.check_keys(dt)
        
    def refresh_text(self):
        self.text_to_draw = [ (410, 477, "'R' to reset"),
                              (10, 10, "Use turn arround time (Space): " + str(self.vehicles[1].steering.use_turn_arround_time)),
                              ]

    def on_event(self, event):
        # this pollutes the world class with pygame
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                self.vehicles[1].steering.use_turn_arround_time = not self.vehicles[1].steering.use_turn_arround_time
        self.refresh_text()
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
if __name__ == "__main__":
    mainloop.MainLoop(World).run()

