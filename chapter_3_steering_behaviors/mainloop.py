# -*- coding: utf-8 -*-

import pygame
from params import params
# ------------------------------------------------------------------------------

class MainLoop(object):

    def __init__(self, world_impl, screen_size=(500, 500)):
        self.screen_size = screen_size
        self.world_impl = world_impl
        self.world = world_impl(self.screen_size)
        # font handling and speed up
        self._font = None
        self._text_cache = {} # {text:surf}
        
        self.schow_fps = False
        self.do_clear_screen = True
        self.show_debug_vectors = False

    def handle_events(self):
        for event in pygame.event.get():
            # print event
            if event.type == pygame.QUIT:
                self.running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                        self.running = False
                elif event.key == pygame.K_r:
                    # reset
                    print("reset")
                    self.world = self.world_impl(self.screen_size)
                elif event.key == pygame.K_F1:
                    self.schow_fps = not self.schow_fps
                elif event.key == pygame.K_F2:
                    self.do_clear_screen = not self.do_clear_screen
                elif event.key == pygame.K_F3:
                    self.show_debug_vectors = not self.show_debug_vectors
            self.world.on_event(event)

    def run(self):
        pygame.init()
        pygame.display.set_caption(self.world.title)
        self.screen = pygame.display.set_mode(self.screen_size)
        clock = pygame.time.Clock()

        self.running = True
        while self.running:
            self.handle_events()

            dt_seconds = clock.tick() / 1000.0
            # prevent too big timesteps, no good for simulaton
            # this happens when you drag the window
            if dt_seconds > 0.3:
                dt_seconds = 0
            self.world.update(dt_seconds)

            # draw everything
            if self.world.crosshair:
                self.draw_crosshair(self.world.crosshair)
            if self.world.draw_path_on:
                for vehicle in self.world.vehicles:
                    self.draw_path(vehicle)
            for vehicle in self.world.vehicles:
                self.draw_vehicle(vehicle)
            if self.world.draw_wander_info_on:
                for vehicle in self.world.vehicles:
                    self.draw_wander_info(vehicle)
            if self.show_debug_vectors:
                for vehicle in self.world.vehicles:
                    self.draw_debug_vectors(vehicle)
            if self.world.draw_obstacles_on:
                self.draw_obstacles()
            if self.world.draw_dbox_on:
                for vehicle in self.world.vehicles:
                    self.draw_dbox(vehicle)
            if self.world.draw_feelers_on:
                for vehicle in self.world.vehicles:
                    self.draw_feelers(vehicle)
            if self.world.draw_walls_on:
                self.draw_walls()

            if self.world.draw_text_overlay_on:
                for text in self.world.text_to_draw:
                    self.draw_text(*text)
                
            if self.world.draw_view_distance_on:
                for vehicle in self.world.vehicles:
                    self.draw_debug_view_radius(vehicle, params["viewdistance"], (255, 0, 0))
                    self.draw_debug_view_radius(vehicle, 100, (0, 255, 0))
            
            if self.world.draw_bounding_radius_on:
                for vehicle in self.world.vehicles:
                    self.draw_debug_view_radius(vehicle, vehicle.bounding_radius, (150, 0, 255))
                

            pygame.display.flip()
            if self.do_clear_screen:
                self.screen.fill((255, 255, 255))
            if self.schow_fps:
                self.draw_text(440, 10, str(clock.get_fps()))

    # ---- draw routines ---- #
                
    def draw_crosshair(self, pos):
        x, y = pos.as_xy_tuple()
        # r = 6
        # pygame.draw.circle(screen, (255, 0, 0), (x+1, y) , r, 1)
        r = 10
        pygame.draw.line(self.screen, (255, 0, 0), (x-r, y), (x+r, y), 1)
        pygame.draw.line(self.screen, (255, 0, 0), (x, y-r), (x, y+r), 1)

    def draw_vehicle(self, vehicle):
        pos = vehicle.position
        # p1 = (pos + vehicle.heading * 20).as_xy_tuple()
        p1 = (pos + vehicle.heading * 5 * vehicle.scale).as_xy_tuple()
        p2 = (pos - vehicle.heading * 5 * vehicle.scale + vehicle.side * 3 * vehicle.scale).as_xy_tuple()
        p3 = (pos - vehicle.heading * 5 * vehicle.scale - vehicle.side * 3 * vehicle.scale).as_xy_tuple()
        pygame.draw.aalines(self.screen, vehicle.color, True, [p1, p2, p3], 1)

    def draw_text(self, posx, posy, text, overwrite=False):
        if text in self._text_cache and not overwrite:
            surf = self._text_cache[text]
        else:
            if not self._font:
                self._font = pygame.font.Font(None, 21)
            if len(self._text_cache) > 2000:
                print("Warning, text cache has more than 2000 entries, reseting")
                self._text_cache = {}
            surf = self._font.render(text, 0, (100, 100, 100))
            surf.set_alpha(128)
            surf = surf.convert_alpha()
            self._text_cache[text] = surf

        self.screen.blit(surf, (posx, posy))

    def draw_wander_info(self, vehicle):
        # green circle
        pos = vehicle.position + vehicle.steering.wander_distance * vehicle.heading * vehicle.bounding_radius
        pygame.draw.circle(self.screen, (0, 255, 0), pos.as_xy_tuple(int), int(vehicle.steering.wander_radius * vehicle.bounding_radius), 1)
        self.screen.set_at(pos.as_xy_tuple(int), (0, 255, 0))

        # red circle
        import steeringbehaviors
        # _wander_target should not be used directly (its in vehicle local coordinates)
        target_world = steeringbehaviors.point_to_world_2d(vehicle.steering._wander_target, vehicle.heading, vehicle.side, vehicle.position)
        pos = vehicle.position + (target_world - vehicle.position) * vehicle.bounding_radius + \
                vehicle.steering.wander_distance * vehicle.heading * vehicle.bounding_radius
        pygame.draw.circle(self.screen, (255, 0, 0), pos.as_xy_tuple(int), 7, 1)

    def draw_debug_vectors(self, vehicle):
        # vehicle axis, (r, g, b) == (x_axis, y_axis, z_axis)
        pygame.draw.line(self.screen, (255, 0, 0), vehicle.position.as_xy_tuple(), (vehicle.position + vehicle.heading * 20).as_xy_tuple(), 1)
        pygame.draw.line(self.screen, (0, 255, 0), vehicle.position.as_xy_tuple(), (vehicle.position + vehicle.side * 20).as_xy_tuple(), 1)

        # pygame.draw.line(self.screen, (255, 150, 0), vehicle.position.as_xy_tuple(), (vehicle.position + vehicle.velocity).as_xy_tuple(), 1)

        # world axis, (r, g, b) == (x_axis, y_axis, z_axis)
        self.draw_text(160, 180, "(200, 200)")
        pygame.draw.line(self.screen, (255, 0, 0), (200, 200), (230, 200), 1)
        pygame.draw.line(self.screen, (0, 255, 0), (200, 200), (200, 230), 1)

        # steering force
        force_end = vehicle.position + vehicle.steering._steering_force
        pygame.draw.line(self.screen, (150, 0, 255), vehicle.position.as_xy_tuple(), force_end.as_xy_tuple(), 1)

    def draw_obstacles(self):
        for obstacle in self.world.obstacles:
            x, y = obstacle.position.as_xy_tuple()
            pygame.draw.circle(self.screen, (0, 0, 0), (x, y) , obstacle.bounding_radius, 1)

    def draw_dbox(self, vehicle):
        p1 = (vehicle.position + vehicle.steering.dboxlength * vehicle.heading + vehicle.bounding_radius * vehicle.side).as_xy_tuple()
        p2 = (vehicle.position + vehicle.steering.dboxlength * vehicle.heading - vehicle.bounding_radius * vehicle.side).as_xy_tuple()
        p3 = (vehicle.position - vehicle.bounding_radius * vehicle.side).as_xy_tuple()
        p4 = (vehicle.position + vehicle.bounding_radius * vehicle.side).as_xy_tuple()
        pygame.draw.lines(self.screen, (200, 200, 200), True, [p1, p2, p3, p4])

    def draw_feelers(self, vehicle):
        for feeler in vehicle.steering.feelers:
            pygame.draw.line(self.screen, (0, 150, 0), vehicle.position.as_xy_tuple(), feeler.as_xy_tuple(), 1)
        
    def draw_walls(self):
        for wall in self.world.walls:
            pygame.draw.line(self.screen, (100, 100, 100), wall.start.as_xy_tuple(), wall.end.as_xy_tuple(), 3)
            if self.world.draw_walls_normals_on:
                middle = (wall.start + wall.end) * 0.5
                pygame.draw.line(self.screen, (255, 0, 255), middle.as_xy_tuple(), (middle + wall.normal * 10).as_xy_tuple(), 1)
                
    def draw_path(self, vehicle):
        if vehicle.steering.path:
            prev_point = vehicle.steering.path.waypoints[-1]
            start_idx = 0 if vehicle.steering.path.is_closed else 1
            color = (200, 100, 100)
            for point in vehicle.steering.path.waypoints[start_idx:]:
                pygame.draw.line(self.screen, color, prev_point.as_xy_tuple(), point.as_xy_tuple(), 1)
                pygame.draw.circle(self.screen, (255, 0, 0), point.as_xy_tuple(int), 4, 0)
                prev_point = point
                
    def draw_debug_view_radius(self, vehicle, view_range, color=(255, 0, 0)):
        pygame.draw.circle(self.screen, color, vehicle.position.as_xy_tuple(int), view_range, 1)
