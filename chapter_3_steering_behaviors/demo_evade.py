#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame

from vectors import Vec2 as Vec
import gameentity
from params import params as _params
import mainloop
import baseworld

# ------------------------------------------------------------------------------
class World(baseworld.BaseWorld):

    def init(self):
        self.title = "Steering Behaviors - evade"
        self.draw_wander_info_on = True
        
        #setup vehicle
        predator = gameentity.Vehicle(self, Vec(250, 250), 20, Vec(0, 0), _params.MaxSpeed * 1.1, Vec(1, 1),
                                      _params.VehicleMass, 1.0, _params.MaxTurnRatePerSecond, _params.SteeringForce,
                                      _params)
        predator.steering.switch_on(predator.steering.WANDER)
        predator.color = (255, 0, 255)
        self.vehicles.append(predator)
        
        evader = gameentity.Vehicle(self, Vec(100, 100), 20, Vec(0, 0), _params.MaxSpeed, Vec(1, 1),
                                    _params.VehicleMass, _params.VehicleScale, _params.MaxTurnRatePerSecond,
                                    _params.SteeringForce, _params)
        evader.steering.switch_on(evader.steering.EVADE)
        evader.steering.target_agent1 = predator
        self.vehicles.append(evader)
        self.draw_wander_info_on = False
        
    def update(self, dt):
        for vehicle in self.vehicles:
            vehicle.update(dt)
        self.crosshair = self.vehicles[1].steering.targeted_evader_position
        self.check_keys(dt)
        
    def refresh_text(self):
        self.text_to_draw = [ (410, 477, "'R' to reset"),
                              (10, 10, "Use turn arround time (Space): " + str(self.vehicles[1].steering.use_turn_arround_time)),
                              ]

    def on_event(self, event):
        # this pollutes the world class with pygame
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                self.vehicles[1].steering.use_turn_arround_time = not self.vehicles[1].steering.use_turn_arround_time
        self.refresh_text()
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
if __name__ == "__main__":
    mainloop.MainLoop(World).run()

