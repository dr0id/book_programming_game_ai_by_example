#!/usr/bin/python
# -*- coding: utf-8 -*-

import random

import pygame

from vectors import Vec2 as Vec
import gameentity
from params import params
import mainloop
import baseworld

# ------------------------------------------------------------------------------
class World(baseworld.BaseWorld):

    def init(self):
        self.title = "Steering Behaviors - hide"
        self.draw_wander_info_on = True

        self.generate_obstacles()
        
        #setup vehicle
        other = gameentity.Vehicle(self, Vec(100, 100), 20, Vec(0, 0), params.MaxSpeed, Vec(1, 1), params.VehicleMass,
                                   params.VehicleScale, params.MaxTurnRatePerSecond, params.SteeringForce,
                                   params, self.obstacles)
        other.steering.switch_on(other.steering.WANDER)
        other.steering.switch_on(other.steering.OBSTACLEAVOIDANCE)
        other.color = (255, 0, 0)
        self.vehicles.append(other)
        
        
        agent1 = gameentity.Vehicle(self, Vec(300, 250), 20, Vec(0, 0), params.MaxSpeed, Vec(1, 1), params.VehicleMass,
                                    params.VehicleScale, params.MaxTurnRatePerSecond, params.SteeringForce,
                                    params, self.obstacles)
        agent1.steering.switch_on(agent1.steering.HIDE)
        agent1.steering.switch_on(other.steering.OBSTACLEAVOIDANCE)
        agent1.steering.target_agent1 = other
        self.vehicles.append(agent1)
        
        agent2 = gameentity.Vehicle(self, Vec(200, 250), 20, Vec(0, 0), params.MaxSpeed, Vec(1, 1), params.VehicleMass,
                                    params.VehicleScale, params.MaxTurnRatePerSecond, params.SteeringForce,
                                    params, self.obstacles)
        agent2.steering.switch_on(agent2.steering.HIDE)
        agent2.steering.switch_on(other.steering.OBSTACLEAVOIDANCE)
        agent2.steering.target_agent1 = other
        self.vehicles.append(agent2) 
        
        self.draw_obstacles_on = True
        self.draw_wander_info_on = False
       
    def generate_obstacles(self):
        border = params.MaxObstacleRadius
        while len(self.obstacles) < params.NumObstacles:
            position = Vec(random.randint(border, self.size[0] - border), random.randint(border, self.size[1] - border))
            radius = random.randint(params.MinObstacleRadius, params.MaxObstacleRadius)
            overlap = False
            for entity in self.obstacles:
                sum_radius = radius + entity.bounding_radius
                if entity.position.get_distance_sq(position) < sum_radius * sum_radius:
                    overlap = True
                    break
            if overlap:
                continue
            self.obstacles.append(gameentity.BaseGameEntity(position, 1.0, radius))
       
    def update(self, dt):
        for vehicle in self.vehicles:
            vehicle.update(dt)
        self.crosshair = self.vehicles[2].steering.interpose_point
        self.check_keys(dt)
        
    def refresh_text(self):
        self.text_to_draw = [ (410, 477, "'R' to reset"),
                              ]

# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
if __name__ == "__main__":
    mainloop.MainLoop(World).run()

