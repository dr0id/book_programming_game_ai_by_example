#!/usr/bin/python
# -*- coding: utf-8 -*-

import random
import math

import pygame

from vectors import Vec2 as Vec
import gameentity
from params import params
import mainloop
import baseworld
from wall import Wall

# ------------------------------------------------------------------------------
class World(baseworld.BaseWorld):

    def init(self):
        self.title = "Steering Behaviors - wall avoidance"
        self.generate_walls()

        #setup vehicle
        for i in range(3):
            vehicle = gameentity.Vehicle(self, Vec(250, 250), 12, Vec(0.0, 0.0), params.MaxSpeed, Vec(1, 1),
                                         params.VehicleMass, params.VehicleScale, params.MaxTurnRatePerSecond,
                                         params.SteeringForce * 2.0, params)

            vehicle.steering.switch_on(vehicle.steering.WANDER)
            vehicle.steering.switch_on(vehicle.steering.WALLAVOIDANCE)
            self.vehicles.append(vehicle)
            
        

        self.draw_walls_on = True
        self.draw_walls_normals_on = True
        self.draw_feelers_on = True
        self.draw_wander_info_on = False

    def generate_walls(self):
        width, height = self.size
        self.walls.append(Wall(Vec(20, 22), Vec(width-20, 22)))
        self.walls.append(Wall(Vec(22, 250), Vec(22, 20)))
        self.walls.append(Wall(Vec(width - 22, 20), Vec(width - 22, 250)))
        
        p = Vec(width / 3, height - height / 3)
        self.walls.append(Wall(Vec(width / 2, height-20), p))
        self.walls.append(Wall(p, Vec( 22, height / 2)))
        
        radius = 248 - 20
        HALFPI =  math.pi / 2.0
        num_steps = 10
        posx = 250
        posy = 250
        for step in range(1, num_steps+1):
            old_x = posx + radius * math.cos(HALFPI / num_steps * (step - 1))
            old_y = posy + radius * math.sin(HALFPI / num_steps * (step - 1))
            x = posx + radius * math.cos(HALFPI / num_steps * step)
            y = posy + radius * math.sin(HALFPI / num_steps * step)
            self.walls.append(Wall(Vec(old_x, old_y), Vec(x, y)))
            
        

    def update(self, dt):
        for vehicle in self.vehicles:
            vehicle.update(dt)
        self.check_keys(dt)

    def refresh_text(self):
        self.text_to_draw = [ (410, 477, "'R' to reset"),
                              (10, 10, "MaxSteeringForce (Ins/Del): " + str(self.vehicles[0].max_force / params.SteeringForceTweaker)),
                              (10, 30, "MaxSpeed (Home/End): " + str(self.vehicles[0].max_speed)),
                              (10, 50, "Draw feelers ('B'): " + str(self.draw_feelers_on)),
                              (10, 70, "Draw wander info ('V'): " + str(self.draw_wander_info_on)),
                              (10, 90, "Draw walls normal ('C'): " + str(self.draw_walls_normals_on)),
                              ]

    def check_keys(self, dt):
        # this pollutes the world class with pygame
        keys = pygame.key.get_pressed()
        if keys[pygame.K_INSERT]:
            for vehicle in self.vehicles:
                vehicle.max_force += 5 * dt
            self.refresh_text()
        if keys[pygame.K_DELETE]:
            for vehicle in self.vehicles:
                vehicle.max_force -= 5 * dt
            self.refresh_text()
        if keys[pygame.K_HOME]:
            for vehicle in self.vehicles:
                vehicle.max_speed += 1 * dt
            self.refresh_text()
        if keys[pygame.K_END]:
            for vehicle in self.vehicles:
                vehicle.max_speed -= 1 * dt
            self.refresh_text()

    def on_event(self, event):
        # this pollutes the world class with pygame
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_b:
                self.draw_feelers_on = not self.draw_feelers_on
            elif event.key == pygame.K_v:
                self.draw_wander_info_on = not self.draw_wander_info_on
            elif event.key == pygame.K_c:
                self.draw_walls_normals_on = not self.draw_walls_normals_on
            self.refresh_text()



# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
if __name__ == "__main__":
    mainloop.MainLoop(World).run()

