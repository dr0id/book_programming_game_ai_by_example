﻿#!/usr/bin/python
# -*- coding: utf-8 -*-
"""

see: http://en.wikipedia.org/wiki/Bin_%28computational_geometry%29

"""

__version__ = '$Id: binspace.py 353 2010-01-07 21:02:49Z dr0iddr0id@gmail.com $'

import collections


class BinSpace(object):

    def __init__(self, cell_size_x, cell_size_y):
        self.cell_size_x = int(cell_size_x)
        self.cell_size_y = int(cell_size_y)
        self._cells = collections.defaultdict(list) # {(x,y):[entities]}
        self._entities = collections.defaultdict(lambda: None) # {entitiy: (cellx, celly)}

    def add(self, entity):
        if self._entities[entity]:
            self._cells[self._entities[entity]].remove(entity)
        hash = (int(entity.position.x // self.cell_size_x), \
                int(entity.position.y // self.cell_size_y))
        # # TODO: faster hash as (this also changes the "get_neighbors()" method!!):
        # hash = int(entity.position.x // self.cell_size_x + (entity.position.y // self.cell_size_y) * self.cell_size_x)
        self._cells[hash].append(entity)
        self._entities[entity] = hash

    def remove(self, entity):
        hash = self._entities[entity]
        if hash:
            self._cells[hash].remove(entity)
            del self._entities[entity]

    def update_entity(self, entity):
        new_hash = (int(entity.position.x // self.cell_size_x), \
                    int(entity.position.y // self.cell_size_y))
        hash = self._entities[entity]
        if new_hash != hash:
            if hash:
                self._cells[hash].remove(entity)
            self._cells[new_hash].append(entity)
            self._entities[entity] = new_hash

    def update_entity_list(self, entity_list):
        for entity in entity_list:
            new_hash = (int(entity.position.x // self.cell_size_x), \
                        int(entity.position.y // self.cell_size_y))
            hash = self._entities[entity]
            if new_hash != hash:
                if hash:
                    self._cells[hash].remove(entity)
                self._cells[new_hash].append(entity)
                self._entities[entity] = new_hash

    def get_neighbors(self, pos, radius):
        """
        Calculates all entities that are in the circle
        with center at pos and radius.
        Returns a list of entities.
        """
        xmin = int((pos.x - radius) // self.cell_size_x)
        ymin = int((pos.y - radius) // self.cell_size_y)
        xmax = int((pos.x + radius) // self.cell_size_x)
        ymax = int((pos.y + radius) // self.cell_size_y)
        
        hits = []
        for cellx in range(xmin, xmax+1):
            for celly in range(ymin, ymax+1):
                hits.extend(self._cells[(cellx, celly)])
        
        return hits
    
# -----------------------------------------------------------------------------

import pygame
from pygame import Rect

class BinRectSpace(object):

    class _Cell(object):
    
        def __init__(self, topleft, size):
            self.entities = []
            self.rect = Rect(0, 0, *size)
            self.rect.topleft = topleft

    def __init__(self, width, height, num_cells_x, num_cells_y):
        self.num_cells_x = int(num_cells_x)
        self.num_cells_y = int(num_cells_y)
        self.cell_size_x = int(width // self.num_cells_x)
        self.cell_size_y = int(height // self.num_cells_y)
        self.space_width = self.cell_size_x * num_cells_x
        self.space_height = self.cell_size_y * num_cells_y
        self._cells = []
        
        # generate the cells
        for ycell in range(0, num_cells_y):
            for xcell in range(0, num_cells_x):
                self._cells.append( self._Cell((xcell * self.cell_size_x, ycell * self.cell_size_y), \
                                         (self.cell_size_x, self.cell_size_y)) )
    def _to_index(self, position):
        assert position.x < self.space_width
        assert position.y < self.space_height
        return int(position.x // self.cell_size_x) + \
               int(position.y // self.cell_size_y) * self.num_cells_x

    def add(self, entity):
        idx = self._to_index(entity.position)
        self._cells[idx].entities.append(entity)

    def remove(self, entity):
        idx = self._to_index(entity.position)
        self._cells[idx].entities.remove(entity)
        
    def update_entity(self, entity):
        old_idx = self._to_index(entity.old_position)
        idx = self._to_index(entity.position)
        if old_idx == idx: return
        self._cells[old_idx].entities.remove(entity)
        self._cells[idx].entities.append(entity)

    def update_entity_list(self, entity_list):
        for entitiy in entity_list:
            old_idx = self._to_index(entity.old_position)
            idx = self._to_index(entity.position)
            if old_idx == idx: return
            self._cells[old_idx].entities.remove(entity)
            self._cells[idx].entities.append(entity)

    def get_neighbors(self, pos, radius):
        w = int(2 * radius)
        rect = Rect(0, 0, w, w)
        rect.center = (pos.x, pos.y)
        hits = []
        for idx in rect.collidelistall(self._cells):
            hits.extend(self._cells[idx].entities)
        return hits

        
# ------------------------------------------------------------------------------

def run_unittests():
    import unittest
    from vectors import Vec2 as Vec
    
    class Entity(object):
        def __init__(self):
            self.position = Vec(0.0, 0.0)
            self.old_position = Vec(0.0, 0.0)
    
    class BinSpaceTests(unittest.TestCase):
        
        def setUp(self):
            self.cell_size_x = 10
            self.cell_size_y = 10
            self.binspace = BinSpace(self.cell_size_x, self.cell_size_y)
            self.entity = Entity()
            self.entity.position = Vec(25, 25)
            self.binspace.add(self.entity)

        def test_hit_in_same_cell(self):
            hits = self.binspace.get_neighbors(Vec(25, 25), 1)
            self.assertTrue(len(hits) > 0, "should have at least on hit")
            self.assertTrue(hits[0] is self.entity, "wrong entity hit")
            
        def test_hit_in_same_cell_offset(self):
            hits = self.binspace.get_neighbors(Vec(31, 31), 10)
            self.assertTrue(len(hits) > 0, "should have at least on hit")
            self.assertTrue(hits[0] is self.entity, "wrong entity hit")

        def test_no_hit_in_cell_offset(self):
            hits = self.binspace.get_neighbors(Vec(31, 31), 2)
            self.assertTrue(len(hits) == 0, "should not have any hit")
            
        def test_remove_entity(self):
            self.binspace.remove(self.entity)
            hits = self.binspace.get_neighbors(Vec(25, 25), 2)
            self.assertTrue(len(hits) == 0, "no hits should occure since entity has been removed")
            
        def test_hit_in_same_sell_after_update(self):
            self.entity.old_position.x = self.entity.position.x
            self.entity.old_position.y = self.entity.position.y
            self.entity.position.x = 33
            self.entity.position.y = 33
            self.binspace.update_entity(self.entity)
            
            hits = self.binspace.get_neighbors(Vec(33, 33), 1)
            self.assertTrue(len(hits) > 0, "should have at least on hit")
            self.assertTrue(hits[0] is self.entity, "wrong entity hit")
            
            hits = self.binspace.get_neighbors(Vec(28, 28), 10)
            self.assertTrue(len(hits) > 0, "should have at least on hit")
            self.assertTrue(hits[0] is self.entity, "wrong entity hit")
            
            hits = self.binspace.get_neighbors(Vec(3, 3), 2)
            self.assertTrue(len(hits) == 0, "should not have any hit")
            
    class BinRectSpaceTests(BinSpaceTests):
    
        def setUp(self):
            self.width = 100
            self.height = 100
            self.num_cells_x = 10
            self.num_cells_y = 10
            self.binspace = BinRectSpace(self.width, self.height, self.num_cells_x, self.num_cells_y)
            self.entity = Entity()
            self.entity.position = Vec(25, 25)
            self.binspace.add(self.entity)
        
    
    suite1 = unittest.TestLoader().loadTestsFromTestCase(BinSpaceTests)
    suite2 = unittest.TestLoader().loadTestsFromTestCase(BinRectSpaceTests)
    suite = unittest.TestSuite([suite1, suite2])
    unittest.TextTestRunner(verbosity=2).run(suite)
    

if __name__ == '__main__':
    run_unittests()