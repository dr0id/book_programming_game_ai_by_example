#!/usr/bin/python
# -*- coding: utf-8 -*-

import random

import pygame

from vectors import Vec2 as Vec
import gameentity
from params import params
import mainloop
import baseworld

# ------------------------------------------------------------------------------
class World(baseworld.BaseWorld):

    def init(self):
        self.title = "Steering Behaviors - obstacle avoidance"
        self.generate_obstacles()

        #setup vehicle
        for i in range(3):
            vehicle = gameentity.Vehicle(self, Vec(250, 250), 12, Vec(0, 0), params.MaxSpeed, Vec(1, 1),
                                         params.VehicleMass, params.VehicleScale, params.MaxTurnRatePerSecond,
                                         params.SteeringForce * 2, params, self.obstacles)
            vehicle.steering.switch_on(vehicle.steering.WANDER)
            vehicle.steering.switch_on(vehicle.steering.OBSTACLEAVOIDANCE)
            self.vehicles.append(vehicle)

        self.draw_obstacles_on = True
        self.draw_dbox_on = True
        self.draw_wander_info_on = False

    def generate_obstacles(self):
        border = params.MaxObstacleRadius
        while len(self.obstacles) < params.NumObstacles:
            position = Vec(random.randint(border, self.size[0] - border), random.randint(border, self.size[1] - border))
            radius = random.randint(params.MinObstacleRadius, params.MaxObstacleRadius)
            overlap = False
            for entity in self.obstacles:
                sum_radius = radius + entity.bounding_radius
                if entity.position.get_distance_sq(position) < sum_radius * sum_radius:
                    overlap = True
                    break
            if overlap:
                continue
            self.obstacles.append(gameentity.BaseGameEntity(position, 1.0, radius))

    def update(self, dt):
        for vehicle in self.vehicles:
            vehicle.update(dt)
        self.check_keys(dt)

    def refresh_text(self):
        self.text_to_draw = [ (410, 477, "'R' to reset"),
                              (10, 10, "MaxSteeringForce (Ins/Del): " + str(self.vehicles[0].max_force / params.SteeringForceTweaker)),
                              (10, 30, "MaxSpeed (Home/End): " + str(self.vehicles[0].max_speed)),
                              (10, 50, "Draw detection box ('B'): " + str(self.draw_dbox_on)),
                              (10, 70, "Draw wander info ('V'): " + str(self.draw_wander_info_on)),
                              ]

    def check_keys(self, dt):
        # this pollutes the world class with pygame
        keys = pygame.key.get_pressed()
        if keys[pygame.K_INSERT]:
            for vehicle in self.vehicles:
                vehicle.max_force += 5 * dt
            self.refresh_text()
        if keys[pygame.K_DELETE]:
            for vehicle in self.vehicles:
                vehicle.max_force -= 5 * dt
                if vehicle.max_force <= 0:
                    vehicle.max_force = 0
            self.refresh_text()
        if keys[pygame.K_HOME]:
            for vehicle in self.vehicles:
                vehicle.max_speed += 10 * dt
            self.refresh_text()
        if keys[pygame.K_END]:
            for vehicle in self.vehicles:
                vehicle.max_speed -= 10 * dt
                if vehicle.max_speed <= 0:
                    vehicle.max_speed = 0
            self.refresh_text()

    def on_event(self, event):
        # this pollutes the world class with pygame
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_b:
                self.draw_dbox_on = not self.draw_dbox_on
            elif event.key == pygame.K_v:
                self.draw_wander_info_on = not self.draw_wander_info_on
            self.refresh_text()



# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
if __name__ == "__main__":
    mainloop.MainLoop(World).run()

