#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame

from vectors import Vec2 as Vec
import gameentity
from params import params
import mainloop
import baseworld

# ------------------------------------------------------------------------------
class World(baseworld.BaseWorld):

    def init(self):
        self.title = "Steering Behaviors - arrive"
        self.crosshair = Vec(200, 150)
        
        #setup vehicle
        vehicle = gameentity.Vehicle(self, Vec(250, 250), 20, Vec(0, 0), params.MaxSpeed, Vec(1, 1), params.VehicleMass,
                                     params.VehicleScale, params.MaxTurnRatePerSecond, params.SteeringForce,
                                     params)
        vehicle.steering.switch_on(vehicle.steering.ARRIVE)
        vehicle.steering.target = self.crosshair
        self.vehicles.append(vehicle)
        
        self.deceleration_values = {vehicle.steering.SLOW : 'slow', vehicle.steering.NORMAL : 'normal', vehicle.steering.FAST : 'fast'}
        
    def update(self, dt):
        for vehicle in self.vehicles:
            vehicle.update(dt)
        self.check_keys(dt)
        
    def refresh_text(self):
        self.text_to_draw = [( 10, 477, "Click to move crosshair"),
                              (410, 477, "'R' to reset"),
                              (10, 10, "MaxSteeringForce (Ins/Del): " + str(self.vehicles[0].max_force / params.SteeringForceTweaker)),
                              (10, 30, "MaxSpeed (Home/End): " + str(self.vehicles[0].max_speed)),
                              (10, 50, "Deceleration (Space): " + str(self.deceleration_values[self.vehicles[0].steering.deceleration])),
                              ]

    # --- controller --- #
    
    def check_keys(self, dt):
        # this pollutes the world class with pygame
        keys = pygame.key.get_pressed()
        if keys[pygame.K_INSERT]:
            self.vehicles[0].max_force += 1.0 * dt
            self.refresh_text()
        if keys[pygame.K_DELETE]:
            self.vehicles[0].max_force -= 1.0 * dt
            self.refresh_text()
        if keys[pygame.K_HOME]:
            self.vehicles[0].max_speed += 10.0 * dt
            self.refresh_text()
        if keys[pygame.K_END]:
            self.vehicles[0].max_speed -= 10.0 * dt
            self.refresh_text()

    def on_event(self, event):
        # this pollutes the world class with pygame
        if event.type == pygame.MOUSEBUTTONDOWN:
            self.crosshair = Vec(event.pos[0], event.pos[1])
            for vehicle in self.vehicles:
                vehicle.steering.target = self.crosshair
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                self.vehicles[0].steering.deceleration = (self.vehicles[0].steering.deceleration + 1) % len(self.deceleration_values)
                self.vehicles[0].steering.deceleration += 1
                self.refresh_text()
        
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
if __name__ == "__main__":
    mainloop.MainLoop(World).run()

