# -*- coding: utf-8 -*-
from steeringbehaviors import SteeringParameters

try:
    import ConfigParser
except:
    import configparser as ConfigParser

_config_parser = ConfigParser.SafeConfigParser()

_config_parser.read("params.ini")

# this would read in alle options from all setctions
# _options = dict( (option, _config_parser.getfloat(section, option)) \
# for section in _config_parser.sections() \
# for option in _config_parser.options(section) )

section = "steeringbehaviors"
_options = dict((option, _config_parser.getfloat(section, option)) \
              for option in _config_parser.options(section))

params = SteeringParameters()
params.NumAgents = _options["numagents"]
params.NumObstacles = _options["numobstacles"]
params.MinObstacleRadius = _options["minobstacleradius"]
params.MaxObstacleRadius = _options["maxobstacleradius"]
params.NumCellsX = _options["numcellsx"]
params.NumCellsY = _options["numcellsy"]
params.NumSamplesForSmoothing = _options["numsamplesforsmoothing"]
params.SteeringForceTweaker = _options["steeringforcetweaker"]
params.DecelerationTweaker = _options["decelerationtweaker"]
params.SteeringForce = _options["steeringforce"]
params.MaxSpeed = _options["maxspeed"]
params.VehicleMass = _options["vehiclemass"]
params.VehicleScale = _options["vehiclescale"]
params.MaxTurnRatePerSecond = _options["maxturnratepersecond"]
params.SeparationWeight = _options["separationweight"]
params.AlignmentWeight = _options["alignmentweight"]
params.CohesionWeight = _options["cohesionweight"]
params.ObstacleAvoidanceWeight = _options["obstacleavoidanceweight"]
params.WallAvoidanceWeight = _options["wallavoidanceweight"]
params.WanderWeight = _options["wanderweight"]
params.SeekWeight = _options["seekweight"]
params.FleeWeight = _options["fleeweight"]
params.ArriveWeight = _options["arriveweight"]
params.PursuitWeight = _options["pursuitweight"]
params.OffsetPursuitWeight = _options["offsetpursuitweight"]
params.InterposeWeight = _options["interposeweight"]
params.HideWeight = _options["hideweight"]
params.EvadeWeight = _options["evadeweight"]
params.FollowPathWeight = _options["followpathweight"]
params.ViewDistance = _options["viewdistance"]
params.MinDetectionBoxLength = _options["mindetectionboxlength"]
params.WallDetectionFeelerLength = _options["walldetectionfeelerlength"]
params.prWallAvoidance = _options["prwallavoidance"]
params.prObstacleAvoidance = _options["probstacleavoidance"]
params.prSeparation = _options["prseparation"]
params.prAlignment = _options["pralignment"]
params.prCohesion = _options["prcohesion"]
params.prWander = _options["prwander"]
params.prSeek = _options["prseek"]
params.prFlee = _options["prflee"]
params.prEvade = _options["prevade"]
params.prHide = _options["prhide"]
params.prArrive = _options["prarrive"]
params.WanderRad = _options["wanderrad"]
params.WanderDist = _options["wanderdist"]
params.WanderJitterPerSec = _options["wanderjitterpersec"]
params.WaypointSeekDist = _options["waypointseekdist"]

# apply tweaking force
_steering_force_tweaker = params.SteeringForceTweaker
params.SteeringForce = params.SteeringForce * _steering_force_tweaker
params.SeparationWeight = params.SeparationWeight * _steering_force_tweaker
params.AlignmentWeight = params.AlignmentWeight * _steering_force_tweaker
params.CohesionWeight = params.CohesionWeight * _steering_force_tweaker
params.ObstacleAvoidanceWeight = params.ObstacleAvoidanceWeight * _steering_force_tweaker
params.WallAvoidanceWeight = params.WallAvoidanceWeight * _steering_force_tweaker
params.WanderWeight = params.WanderWeight * _steering_force_tweaker
params.SeekWeight = params.SeekWeight * _steering_force_tweaker
params.FleeWeight = params.FleeWeight * _steering_force_tweaker
params.ArriveWeight = params.ArriveWeight * _steering_force_tweaker
params.PursuitWeight = params.PursuitWeight * _steering_force_tweaker
params.OffsetPursuitWeight = params.OffsetPursuitWeight * _steering_force_tweaker
params.InterposeWeight = params.InterposeWeight * _steering_force_tweaker
params.HideWeight = params.HideWeight * _steering_force_tweaker
params.EvadeWeight = params.EvadeWeight * _steering_force_tweaker
params.FollowPathWeight = params.FollowPathWeight * _steering_force_tweaker
