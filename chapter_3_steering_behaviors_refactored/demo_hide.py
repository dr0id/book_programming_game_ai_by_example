#!/usr/bin/python
# -*- coding: utf-8 -*-

import random

import pygame

from vectors import Vec2 as Vec
import gameentity
from params import params
import mainloop
import baseworld

# ------------------------------------------------------------------------------
class World(baseworld.BaseWorld):

    def init(self):
        self.title = "Steering Behaviors - hide"
        self.draw_wander_info_on = True

        self.generate_obstacles()
        
        #setup vehicle
        other = gameentity.Vehicle(self,
                                     Vec(100, 100),
                                     20,
                                     Vec(0, 0),
                                     params["maxspeed"],
                                     Vec(1, 1),
                                     params["vehiclemass"],
                                     params["vehiclescale"],
                                     params["maxturnratepersecond"],
                                     params["steeringforce"],
                                     self.obstacles)
        other.steering.behavior_wander.activate()
        other.steering.behavior_obstacleavoidance.activate()
        other.color = (255, 0, 0)
        self.vehicles.append(other)
        
        
        agent1 = gameentity.Vehicle(self,
                                     Vec(300, 250),
                                     20,
                                     Vec(0, 0),
                                     params["maxspeed"],
                                     Vec(1, 1),
                                     params["vehiclemass"],
                                     params["vehiclescale"],
                                     params["maxturnratepersecond"],
                                     params["steeringforce"],
                                     self.obstacles)
        agent1.steering.behavior_obstacleavoidance.activate()
        agent1.steering.behavior_hide.activate(other)
        self.vehicles.append(agent1)
        
        agent2 = gameentity.Vehicle(self,
                                     Vec(200, 250),
                                     20,
                                     Vec(0, 0),
                                     params["maxspeed"],
                                     Vec(1, 1),
                                     params["vehiclemass"],
                                     params["vehiclescale"],
                                     params["maxturnratepersecond"],
                                     params["steeringforce"],
                                     self.obstacles)
        agent2.steering.behavior_obstacleavoidance.activate()
        agent2.steering.behavior_hide.activate(other)
        self.vehicles.append(agent2) 
        
        self.draw_obstacles_on = True
        self.draw_wander_info_on = False
       
    def generate_obstacles(self):
        border = params["maxobstacleradius"]
        while len(self.obstacles) < params["numobstacles"]:
            position = Vec(random.randint(border, self.size[0] - border), random.randint(border, self.size[1] - border))
            radius = random.randint(params["minobstacleradius"], params["maxobstacleradius"])
            overlap = False
            for entity in self.obstacles:
                sum_radius = radius + entity.bounding_radius
                if entity.position.get_distance_sq(position) < sum_radius * sum_radius:
                    overlap = True
                    break
            if overlap:
                continue
            self.obstacles.append(gameentity.BaseGameEntity(position, 1.0, radius))
       
    def update(self, dt):
        for vehicle in self.vehicles:
            vehicle.update(dt)
        self.check_keys(dt)
        
    def refresh_text(self):
        self.text_to_draw = [ (410, 477, "'R' to reset"),
                              ]

# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
if __name__ == "__main__":
    mainloop.MainLoop(World).run()

