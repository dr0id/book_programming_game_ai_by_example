#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame

from vectors import Vec2 as Vec
import gameentity
from params import params
import mainloop
import baseworld

# ------------------------------------------------------------------------------
class World(baseworld.BaseWorld):

    def init(self):
        self.title = "Steering Behaviors - flee"
        self.crosshair = Vec(200, 150)

        #setup vehicle
        vehicle = gameentity.Vehicle(self,
                                     Vec(250,250),
                                     20,
                                     Vec(0, 0),
                                     params["maxspeed"],
                                     Vec(1, 1),
                                     params["vehiclemass"],
                                     params["vehiclescale"],
                                     params["maxturnratepersecond"],
                                     params["steeringforce"])
        vehicle.steering.behavior_flee.activate(self.crosshair)
        self.vehicles.append(vehicle)

    def update(self, dt):
        for vehicle in self.vehicles:
            vehicle.update(dt)
        self.check_keys(dt)

    def refresh_text(self):
        self.text_to_draw = [( 10, 477, "Click to move crosshair"),
                              (410, 477, "'R' to reset"),
                              (10, 10, "MaxSteeringForce (Ins/Del): " + str(self.vehicles[0].max_force / params["steeringforcetweaker"])),
                              (10, 30, "MaxSpeed (Home/End): " + str(self.vehicles[0].max_speed)),
                              (10, 50, "Panic distance (Space): " + str(self.vehicles[0].steering.behavior_flee.flee_panic_distance)),
                              ]

    # --- controller --- #

    def check_keys(self, dt):
        # this pollutes the world class with pygame
        keys = pygame.key.get_pressed()
        if keys[pygame.K_INSERT]:
            self.vehicles[0].max_force += 1 * dt
            self.refresh_text()
        if keys[pygame.K_DELETE]:
            self.vehicles[0].max_force -= 1 * dt
            if self.vehicles[0].max_force <= 0:
                self.vehicles[0].max_force = 0
            self.refresh_text()
        if keys[pygame.K_HOME]:
            self.vehicles[0].max_speed += 1 * dt
            self.refresh_text()
        if keys[pygame.K_END]:
            self.vehicles[0].max_speed -= 1 * dt
            if self.vehicles[0].max_speed <= 0:
                self.vehicles[0].max_speed = 0
            self.refresh_text()

    def on_event(self, event):
        # this pollutes the world class with pygame
        if event.type == pygame.MOUSEBUTTONDOWN:
            self.crosshair = Vec(event.pos[0], event.pos[1])
            for vehicle in self.vehicles:
                # either will work:
                # vehicle.steering.behavior_flee.activate(self.crosshair)
                vehicle.steering.target = self.crosshair
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                if self.vehicles[0].steering.behavior_flee.flee_panic_distance:
                    self.vehicles[0].steering.behavior_flee.flee_panic_distance = None
                else:
                    self.vehicles[0].steering.behavior_flee.flee_panic_distance = 100
                self.refresh_text()

# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
if __name__ == "__main__":
    mainloop.MainLoop(World).run()

