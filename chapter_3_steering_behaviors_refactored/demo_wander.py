#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame

from vectors import Vec2 as Vec
import gameentity
from params import params
import mainloop
import baseworld

# ------------------------------------------------------------------------------
class World(baseworld.BaseWorld):

    def init(self):
        self.title = "Steering Behaviors - wander"
        self.draw_wander_info_on = True
        
        #setup vehicle
        for i in range(10):
            vehicle = gameentity.Vehicle(self,
                                         Vec(250.0,250.0),
                                         20.0,
                                         Vec(0, 0),
                                         params["maxspeed"],
                                         Vec(1.0, 1.0),
                                         params["vehiclemass"],
                                         params["vehiclescale"],
                                         params["maxturnratepersecond"],
                                         params["steeringforce"])
            vehicle.steering.behavior_wander.activate()
            self.vehicles.append(vehicle)
        
        
    def update(self, dt):
        for vehicle in self.vehicles:
            vehicle.update(dt)
        self.check_keys(dt)
        
    def refresh_text(self):
        self.text_to_draw = [ (410, 477, "'R' to reset"),
                              (10, 10, "MaxSteeringForce (Ins/Del): " + str(self.vehicles[0].max_force / params["steeringforcetweaker"])),
                              (10, 30, "MaxSpeed (Home/End): " + str(self.vehicles[0].max_speed)),
                              (10, 50, "Jitter (F/V): " + str(self.vehicles[0].steering.behavior_wander.jitter)),
                              (10, 70, "Distance (G/B): " + str(self.vehicles[0].steering.behavior_wander.distance)),
                              (10, 90, "Radius (H/N): " + str(self.vehicles[0].steering.behavior_wander.radius)),
                              (10, 477, "Debug circles on/off (Space)"),
                              ]

    # --- controller --- #
    
    def check_keys(self, dt):
        # this pollutes the world class with pygame
        keys = pygame.key.get_pressed()
        
        if keys[pygame.K_INSERT]:
            for vehicle in self.vehicles:
                vehicle.max_force += 500.0 * dt
            self.refresh_text()
        if keys[pygame.K_DELETE]:
            for vehicle in self.vehicles:
                vehicle.max_force -= 500.0 * dt
                if vehicle.max_force <= 0:
                    vehicle.max_force = 0
            self.refresh_text()
            
        if keys[pygame.K_HOME]:
            for vehicle in self.vehicles:
                vehicle.max_speed += 10.0 * dt
            self.refresh_text()
        if keys[pygame.K_END]:
            for vehicle in self.vehicles:
                vehicle.max_speed -= 10.0 * dt
                if vehicle.max_speed <= 0:
                    vehicle.max_speed = 0
            self.refresh_text()

        if keys[pygame.K_f]:
            for vehicle in self.vehicles:
                vehicle.steering.behavior_wander.jitter += 10.0 * dt
            self.refresh_text()
        if keys[pygame.K_v]:
            for vehicle in self.vehicles:
                vehicle.steering.behavior_wander.jitter -= 10.0 * dt
            self.refresh_text()

        if keys[pygame.K_g]:
            for vehicle in self.vehicles:
                vehicle.steering.behavior_wander.distance += 1 * dt
            self.refresh_text()
        if keys[pygame.K_b]:
            for vehicle in self.vehicles:
                vehicle.steering.behavior_wander.distance -= 1 * dt
            self.refresh_text()

        if keys[pygame.K_h]:
            for vehicle in self.vehicles:
                vehicle.steering.behavior_wander.radius += 1 * dt
            self.refresh_text()
        if keys[pygame.K_n]:
            for vehicle in self.vehicles:
                vehicle.steering.behavior_wander.radius -= 1 * dt
                if vehicle.steering.behavior_wander.radius <= 0.1:
                    vehicle.steering.behavior_wander.radius = 0.1
            self.refresh_text()


    def on_event(self, event):
        # this pollutes the world class with pygame
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                self.draw_wander_info_on = not self.draw_wander_info_on
        
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
if __name__ == "__main__":
    mainloop.MainLoop(World).run()

