# -*- coding: utf-8 -*-

import random
import math

from vectors import Vec2 as Vec


# ------------------------------------------------------------------------------

class WayPointPath(object):

    def __init__(self, waypoints, closed=False):
        self.waypoints = waypoints
        self._current_idx = 0
        self.current_waypoint = self.waypoints[self._current_idx]
        self.is_closed = closed
        self.is_finished = False

    def set_next_waypoint(self):
        self._current_idx += 1
        if self._current_idx >= len(self.waypoints):
            if self.is_closed:
                self._current_idx = 0
            else:
                self_current_idx = len(self.waypoints) - 1
                self.is_finished = True
                
        self.current_waypoint = self.waypoints[self._current_idx]
        
    @staticmethod
    def create_randomized_path(num_waypoints, xmin, ymin, xmax, ymax, closed=False):
        assert num_waypoints > 0
        assert xmin < xmax
        assert ymin < ymax
        
        midx = (xmin + xmax) * 0.5
        midy = (ymin + ymax) * 0.5
        
        smaller = min(midx, midy)
        waypoints = []
        spacing = math.pi * 2.0 / num_waypoints
        for idx in range(num_waypoints):
            radius = (0.8 * random.random()) * smaller
            point = Vec(midx + radius * math.cos(idx * spacing), midy + radius * math.sin(idx * spacing))
            waypoints.append(point)
            
        return WayPointPath(waypoints, closed)
            