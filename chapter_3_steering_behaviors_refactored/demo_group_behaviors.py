# -*- coding: utf-8 -*-


# ------------------------------------------------------------------------------

import random
import math

import pygame

from vectors import Vec2 as Vec
import gameentity
from params import params
import mainloop
import baseworld
from wall import Wall

# ------------------------------------------------------------------------------
class World(baseworld.BaseWorld):

    def init(self):
        self.title = "Steering Behaviors - group behaviors"
        self.draw_view_distance_on = False
        self.generate_walls()
        self.generate_obstacles()
        self.draw_bounding_radius_on = False
        self.is_non_penetration_on = True

        # shark, red
        shark = gameentity.Vehicle(self,
                                     Vec(250,250),
                                     20,
                                     Vec(0.0, 0.0),
                                     params["maxspeed"],
                                     Vec(1, 1),
                                     params["vehiclemass"],
                                     params["vehiclescale"] * 1.0,
                                     params["maxturnratepersecond"],
                                     params["steeringforce"],
                                     self.obstacles)
        shark.steering.behavior_wander.jitter = 160 * 2
        shark.steering.behavior_wander.activate()

        shark.color = (255, 0, 0)
        self.vehicles.append(shark)

        # first group, blue
        group_vehicles = []
        for i in range(30):
            vehicle = gameentity.Vehicle(self,
                                         Vec(250+random.random()*30,250+random.random()*30),
                                         7,
                                         Vec(0.0, 0.0),
                                         params["maxspeed"],
                                         Vec(random.random(), random.random()),
                                         params["vehiclemass"],
                                         params["vehiclescale"] * 0.3,
                                         params["maxturnratepersecond"],
                                         params["steeringforce"],
                                         self.obstacles)
            vehicle.steering.behavior_flock.activate(group_vehicles)
            vehicle.steering.behavior_evade.activate(shark, thread_range=100)
            self.vehicles.append(vehicle)
            group_vehicles.append(vehicle)
            vehicle.is_non_penetration_on = True
            vehicle.steering.summing_method = vehicle.steering.PRIORITIZED
            # vehicle.steering.summing_method = vehicle.steering.WEIGHTEDAVERAGE
            vehicle.steering.evade_thread_range = 100.0

        # second group, pink
        group_vehicles = []
        for i in range(30):
            vehicle = gameentity.Vehicle(self,
                                         Vec(250+random.random()*30,250+random.random()*30),
                                         7,
                                         Vec(0.0, 0.0),
                                         params["maxspeed"],
                                         Vec(random.random(), random.random()),
                                         params["vehiclemass"],
                                         params["vehiclescale"] * 0.3,
                                         params["maxturnratepersecond"],
                                         params["steeringforce"],
                                         self.obstacles)
            vehicle.steering.behavior_flock.activate(group_vehicles)
            vehicle.steering.behavior_evade.activate(shark, thread_range=100)
            self.vehicles.append(vehicle)
            group_vehicles.append(vehicle)
            vehicle.is_non_penetration_on = True
            vehicle.steering.summing_method = vehicle.steering.PRIORITIZED
            vehicle.steering.evade_thread_range = 100.0
            vehicle.color = (255, 0, 255)

    def generate_walls(self):
        width, height = self.size
        self.walls.append(Wall(Vec(20, 22), Vec(width-20, 22)))
        self.walls.append(Wall(Vec(22, 250), Vec(22, 20)))
        self.walls.append(Wall(Vec(width - 22, 20), Vec(width - 22, 250)))

        p = Vec(width / 3, height - height / 3)
        self.walls.append(Wall(Vec(width / 2, height-20), p))
        self.walls.append(Wall(p, Vec( 22, height / 2)))

        radius = 248 - 20
        HALFPI =  math.pi / 2.0
        num_steps = 10
        posx = 250
        posy = 250
        for step in range(1, num_steps+1):
            old_x = posx + radius * math.cos(HALFPI / num_steps * (step - 1))
            old_y = posy + radius * math.sin(HALFPI / num_steps * (step - 1))
            x = posx + radius * math.cos(HALFPI / num_steps * step)
            y = posy + radius * math.sin(HALFPI / num_steps * step)
            self.walls.append(Wall(Vec(old_x, old_y), Vec(x, y)))

    def generate_obstacles(self):
        border = params["maxobstacleradius"]
        while len(self.obstacles) < params["numobstacles"]:
            position = Vec(random.randint(border, self.size[0] - border), random.randint(border, self.size[1] - border))
            radius = random.randint(params["minobstacleradius"], params["maxobstacleradius"])
            overlap = False
            for entity in self.obstacles:
                sum_radius = radius + entity.bounding_radius
                if entity.position.get_distance_sq(position) < sum_radius * sum_radius:
                    overlap = True
                    break
            if overlap:
                continue
            self.obstacles.append(gameentity.BaseGameEntity(position, 1.0, radius))

    def refresh_text(self):
        self.text_to_draw = [ (410, 477, "'R' to reset"),
                              (10, 10, "MaxSteeringForce (Ins/Del): " + str(self.vehicles[0].max_force / params["steeringforcetweaker"])),
                              (10, 30, "MaxSpeed (Home/End): " + str(self.vehicles[0].max_speed)),
                              (10, 50, "Change summing method(S): " + self.get_current_summing_method_name()),
                              (10, 70, "Show view radius(V): " + str(self.draw_view_distance_on)),
                              (10, 90, "Show Bounding radius on/off(B): " + str(self.draw_bounding_radius_on)),

                              (10, 110, "Walls on/off(W): " + str(self.draw_walls_on)),
                              (10, 130, "Obstacles on/off(O): " + str(self.draw_obstacles_on)),
                              (10, 150, "Penetration constraint on/off(P): " + str(self.is_non_penetration_on)),
                              (10, 170, "info off (i)"),
                              ]

    def get_current_summing_method_name(self):
        vehicle = self.vehicles[-2]
        if vehicle.steering.WEIGHTEDAVERAGE == vehicle.steering.summing_method:
            return "weighted average"
        elif vehicle.steering.PRIORITIZED == vehicle.steering.summing_method:
            return "prioritized sum"
        elif vehicle.steering.DITHERED == vehicle.steering.summing_method:
            return "dithered"

    # --- controller --- #

    def check_keys(self, dt):
        # this pollutes the world class with pygame
        keys = pygame.key.get_pressed()
        if keys[pygame.K_INSERT]:
            for vehicle in self.vehicles:
                vehicle.max_force += 500.0 * dt
            self.refresh_text()
        if keys[pygame.K_DELETE]:
            for vehicle in self.vehicles:
                vehicle.max_force -= 500.0 * dt
                if vehicle.max_force <= 0:
                    vehicle.max_force = 0
            self.refresh_text()

        if keys[pygame.K_HOME]:
            for vehicle in self.vehicles:
                vehicle.max_speed += 10.0 * dt
            self.refresh_text()
        if keys[pygame.K_END]:
            for vehicle in self.vehicles:
                vehicle.max_speed -= 10.0 * dt
                if vehicle.max_speed <= 0:
                    vehicle.max_speed = 0
            self.refresh_text()

        # if keys[pygame.K_f]:
            # for vehicle in self.vehicles:
                # vehicle.steering.wander_jitter += 10.0 * dt
            # self.refresh_text()
        # if keys[pygame.K_v]:
            # for vehicle in self.vehicles:
                # vehicle.steering.wander_jitter -= 10.0 * dt
            # self.refresh_text()

        # if keys[pygame.K_g]:
            # for vehicle in self.vehicles:
                # vehicle.steering.wander_distance += 1 * dt
            # self.refresh_text()
        # if keys[pygame.K_b]:
            # for vehicle in self.vehicles:
                # vehicle.steering.wander_distance -= 1 * dt
            # self.refresh_text()

        # if keys[pygame.K_h]:
            # for vehicle in self.vehicles:
                # vehicle.steering.wander_radius += 1 * dt
            # self.refresh_text()
        # if keys[pygame.K_n]:
            # for vehicle in self.vehicles:
                # vehicle.steering.wander_radius -= 1 * dt
                # if vehicle.steering.wander_radius <= 0.1:
                    # vehicle.steering.wander_radius = 0.1
            # self.refresh_text()

    def on_event(self, event):
        # this pollutes the world class with pygame
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_F3:
                self.draw_wander_info_on = not self.draw_wander_info_on
            elif event.key == pygame.K_s:
                sum_method = None
                vehicle = self.vehicles[-2]
                if vehicle.steering.WEIGHTEDAVERAGE == vehicle.steering.summing_method:
                    sum_method = vehicle.steering.PRIORITIZED
                elif vehicle.steering.PRIORITIZED == vehicle.steering.summing_method:
                    sum_method = vehicle.steering.DITHERED
                elif vehicle.steering.DITHERED == vehicle.steering.summing_method:
                    sum_method = vehicle.steering.WEIGHTEDAVERAGE
                for vehicle in self.vehicles:
                    vehicle.steering.summing_method = sum_method
            elif event.key == pygame.K_v:
                self.draw_view_distance_on = not self.draw_view_distance_on
            elif event.key == pygame.K_i:
                self.draw_text_overlay_on = not self.draw_text_overlay_on
            elif event.key == pygame.K_w:
                self.draw_walls_on = not self.draw_walls_on
                if self.draw_walls_on:
                    for vehicle in self.vehicles:
                        vehicle.steering.behavior_wallavoidance.activate()
                else:
                    for vehicle in self.vehicles:
                        vehicle.steering.behavior_wallavoidance.deactivate()
            elif event.key == pygame.K_o:
                self.draw_obstacles_on = not self.draw_obstacles_on
                del self.obstacles[:]
                self.generate_obstacles()
                if self.draw_obstacles_on:
                    for vehicle in self.vehicles:
                        vehicle.steering.behavior_obstacleavoidance.activate()
                else:
                    for vehicle in self.vehicles:
                        vehicle.steering.behavior_obstacleavoidance.deactivate()
            elif event.key == pygame.K_b:
                self.draw_bounding_radius_on = not self.draw_bounding_radius_on
            elif event.key == pygame.K_p:
                self.is_non_penetration_on = not self.is_non_penetration_on
                for vehicle in self.vehicles:
                    vehicle.is_non_penetration_on = self.is_non_penetration_on

            self.refresh_text()


# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
if __name__ == "__main__":
    mainloop.MainLoop(World).run()



