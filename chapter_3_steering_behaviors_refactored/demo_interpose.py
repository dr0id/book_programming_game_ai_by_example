#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame

from vectors import Vec2 as Vec
import gameentity
from params import params
import mainloop
import baseworld

# ------------------------------------------------------------------------------
class World(baseworld.BaseWorld):

    def init(self):
        self.title = "Steering Behaviors - interpose"
        self.draw_wander_info_on = True
        
        speed_tuner = 0.5
        
        #setup vehicle
        agent1 = gameentity.Vehicle(self,
                                     Vec(250, 250),
                                     20,
                                     Vec(0, 0),
                                     params["maxspeed"] * speed_tuner,
                                     Vec(1, 1),
                                     params["vehiclemass"],
                                     params["vehiclescale"],
                                     params["maxturnratepersecond"],
                                     params["steeringforce"])
        agent1.steering.behavior_wander.activate()
        self.vehicles.append(agent1)
        
        agent2 = gameentity.Vehicle(self,
                                     Vec(250, 250),
                                     20,
                                     Vec(0, 0),
                                     params["maxspeed"] * speed_tuner,
                                     Vec(1, 1),
                                     params["vehiclemass"],
                                     params["vehiclescale"],
                                     params["maxturnratepersecond"],
                                     params["steeringforce"])
        agent2.steering.behavior_wander.activate()
        self.vehicles.append(agent2) 
        
        interposer = gameentity.Vehicle(self,
                                     Vec(100, 100),
                                     20,
                                     Vec(0, 0),
                                     params["maxspeed"],
                                     Vec(1, 1),
                                     params["vehiclemass"],
                                     params["vehiclescale"],
                                     params["maxturnratepersecond"],
                                     params["steeringforce"])
        interposer.color = (255, 0, 0)
        interposer.steering.behavior_interpose.activate(agent1, agent2)
        self.vehicles.append(interposer)
        
        self.draw_wander_info_on = False
        
    def update(self, dt):
        for vehicle in self.vehicles:
            vehicle.update(dt)
        self.crosshair = self.vehicles[2].steering.behavior_interpose.interpose_point
        self.check_keys(dt)
        
    def refresh_text(self):
        self.text_to_draw = [ (410, 477, "'R' to reset"),
                              (10, 10, "Use turn arround time (Space): " + str(self.vehicles[2].steering.behavior_interpose.turn_around_coefficient != 0.0)),
                              ]

    def on_event(self, event):
        # this pollutes the world class with pygame
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                if self.vehicles[2].steering.behavior_interpose.turn_around_coefficient == 0.0:
                    self.vehicles[2].steering.behavior_interpose.turn_around_coefficient = -1.0
                else:
                    self.vehicles[2].steering.behavior_interpose.turn_around_coefficient = 0.0
        self.refresh_text()
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
if __name__ == "__main__":
    mainloop.MainLoop(World).run()

