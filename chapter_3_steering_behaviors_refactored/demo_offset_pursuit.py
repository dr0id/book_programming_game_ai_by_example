#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame

from vectors import Vec2 as Vec
import gameentity
from params import params
import mainloop
import baseworld

# ------------------------------------------------------------------------------
class World(baseworld.BaseWorld):

    def init(self):
        self.title = "Steering Behaviors - offset pursuit"
        self.crosshair = Vec(200, 150)

        #setup vehicle
        leader = gameentity.Vehicle(self,
                                     Vec(250, 250),
                                     20,
                                     Vec(0, 0),
                                     params["maxspeed"],
                                     Vec(1, 1),
                                     params["vehiclemass"],
                                     params["vehiclescale"] * 1.5,
                                     params["maxturnratepersecond"],
                                     params["steeringforce"])
        leader.steering.behavior_arrive.activate(self.crosshair)
        leader.color = (255, 0, 255)
        self.vehicles.append(leader)
        self.leader = leader
        
        offsets = [Vec(-10, -20), Vec(-30, -15), Vec(-30, 15), Vec(-10, 20)]
        for i in range(4):
            evader = gameentity.Vehicle(self,
                                         Vec(100, 100),
                                         20,
                                         Vec(0, 0),
                                         params["maxspeed"],
                                         Vec(1, 1),
                                         params["vehiclemass"],
                                         1.0,
                                         params["maxturnratepersecond"],
                                         params["steeringforce"])
            evader.steering.behavior_offsetpursuit.activate(leader, offsets[i])
            self.vehicles.append(evader)
        
    def update(self, dt):
        for vehicle in self.vehicles:
            vehicle.update(dt)
        self.check_keys(dt)
        
    def refresh_text(self):
        self.text_to_draw = [ (410, 477, "'R' to reset"),
                              (10, 10, "Click to move crosshair"),
                              ]

    def on_event(self, event):
        # this pollutes the world class with pygame
        if event.type == pygame.MOUSEBUTTONDOWN:
            self.crosshair = Vec(event.pos[0], event.pos[1])
            # either will work:
            # self.leader.steering.behavior_arrive.activate(self.crosshair)
            self.leader.steering.target = self.crosshair

# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
if __name__ == "__main__":
    mainloop.MainLoop(World).run()

