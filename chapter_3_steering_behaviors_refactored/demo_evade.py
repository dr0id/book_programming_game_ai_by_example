#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame

from vectors import Vec2 as Vec
import gameentity
from params import params
import mainloop
import baseworld

# ------------------------------------------------------------------------------
class World(baseworld.BaseWorld):

    def init(self):
        self.title = "Steering Behaviors - evade"
        self.draw_wander_info_on = True
        
        #setup vehicle
        predator = gameentity.Vehicle(self,
                                     Vec(250, 250),
                                     20,
                                     Vec(0, 0),
                                     params["maxspeed"] * 1.1,
                                     Vec(1, 1),
                                     params["vehiclemass"],
                                     1.0,
                                     params["maxturnratepersecond"],
                                     params["steeringforce"])
        predator.steering.behavior_wander.activate()
        predator.color = (255, 0, 255)
        self.vehicles.append(predator)
        
        evader = gameentity.Vehicle(self,
                                     Vec(100, 100),
                                     20,
                                     Vec(0, 0),
                                     params["maxspeed"],
                                     Vec(1, 1),
                                     params["vehiclemass"],
                                     params["vehiclescale"],
                                     params["maxturnratepersecond"],
                                     params["steeringforce"])
        evader.steering.behavior_evade.activate(predator)
        self.vehicles.append(evader)
        self.draw_wander_info_on = False
        
    def update(self, dt):
        for vehicle in self.vehicles:
            vehicle.update(dt)
        self.crosshair = self.vehicles[1].steering.targeted_evader_position
        self.check_keys(dt)
        
    def refresh_text(self):
        self.text_to_draw = [ (410, 477, "'R' to reset"),
                              (10, 10, "Use turn arround time (Space): " + str(self.vehicles[1].steering.behavior_interpose.turn_around_coefficient != 0.0)),
                              ]

    def on_event(self, event):
        # this pollutes the world class with pygame
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                # self.vehicles[1].steering.use_turn_arround_time = not self.vehicles[1].steering.use_turn_arround_time
                if self.vehicles[1].steering.behavior_interpose.turn_around_coefficient == 0.0:
                    self.vehicles[1].steering.behavior_interpose.turn_around_coefficient = -0.5
                else:
                    self.vehicles[1].steering.behavior_interpose.turn_around_coefficient = 0.0
        self.refresh_text()
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
if __name__ == "__main__":
    mainloop.MainLoop(World).run()

