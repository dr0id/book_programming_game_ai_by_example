# -*- coding: utf-8 -*-

import random
from math import cos as math_cos
from math import hypot as math_hypot
from math import pi as math_pi
from math import sin as math_sin
from math import sqrt as math_sqrt
from random import random as rand_float
from sys import maxsize as sys_maxsize

from vectors import EPSILON
from vectors import Vec2 as Vec2d

_DEFAULT_VALUE = object()


class SteeringBehaviors(object):
    WEIGHTEDAVERAGE = 1 << 1
    PRIORITIZED = 1 << 2
    DITHERED = 1 << 3

    SLOW = 3
    NORMAL = 2
    FAST = 1

    def __init__(self, parameters):
        # ---- dependencies ---- #

        # ---- variables ---- #

        self.calculate = self._calculate_prioritized
        self._need_tagging = 0

        self._steering_force = Vec2d(0.0, 0.0)

        # TODO: is this really good to have a shared 'target' variable?
        self.target = None
        # self.target_agent1 = None
        # self.target_agent2 = None

        # ---- params group behaviors ---- #
        self.group_vehicles = None  # TODO: empty list might be better?? maybe not, None will raise an exceptionn...
        self.tagged_vehicles = []
        # ---- params evade ---- #
        self.targeted_evader_position = None  # set by _EvadeBehavior else None!
        # ---- params pursuit ---- #
        self.targeted_pursuit_position = None  # set by _PursuitBehavior else None!

        self.viewdistance = parameters["viewdistance"]

        # ---- internal dependencies ---- #
        self._active_behaviors = []
        self._update_list = []

        self.behavior_seek = _SeekBehavior(self, parameters["seekweight"], parameters["prseek"])
        self.behavior_flee = _FleeBehavior(self, parameters["fleeweight"], parameters["prflee"])
        self.behavior_arrive = _ArriveBehavior(self, parameters["arriveweight"], parameters["prarrive"],
                                               parameters['decelerationtweaker'])
        self.behavior_wander = _WanderBehavior(self, parameters["wanderweight"], parameters["prwander"],
                                               parameters["wanderdist"], parameters["wanderjitterpersec"],
                                               parameters["wanderrad"])
        self.behavior_evade = _EvadeBehavior(self, parameters["evadeweight"], parameters["prevade"])

        self.behavior_cohesion = _CohesionBehavior(self, parameters["cohesionweight"], parameters["prcohesion"])
        self.behavior_separation = _SeparationBehavior(self, parameters["separationweight"], parameters["prseparation"])
        self.behavior_alignment = _AlignmentBehavior(self, parameters["alignmentweight"], parameters["pralignment"])
        self.behavior_flock = _FlockBehavior(self, None, None)

        self.behavior_obstacleavoidance = _ObstacleAvoidanceBehavior(self, parameters["obstacleavoidanceweight"],
                                                                     parameters["probstacleavoidance"],
                                                                     parameters["mindetectionboxlength"],
                                                                     parameters["breakweight"])
        self.behavior_wallavoidance = _WallAvoidanceBehavior(self, parameters["wallavoidanceweight"],
                                                             parameters["prwallavoidance"])
        self.behavior_hide = _HideBehavior(self, parameters["hideweight"], parameters["prhide"],
                                           parameters['decelerationtweaker'])

        self.behavior_pursuit = _PursuitBehavior(self, parameters["pursuitweight"], parameters["prpursuit"])
        self.behavior_interpose = _InterposeBehavior(self, parameters["interposeweight"], parameters["printerpose"],
                                                     parameters['decelerationtweaker'])
        self.behavior_distance_interpose = _DistanceInterposeBehavior(self, parameters["interposeweight"],
                                                                      parameters["printerpose"],
                                                                      parameters['decelerationtweaker'])
        self.behavior_followpath = _FollowPathBehavior(self, parameters["followpathweight"],
                                                       parameters["prfollowpath"], parameters["waypointseekdist"],
                                                       parameters['decelerationtweaker'])
        self.behavior_offsetpursuit = _OffsetPursuitBehavior(self, parameters["offsetpursuitweight"],
                                                             parameters["proffsetpursuit"],
                                                             parameters['decelerationtweaker'])

    def __str__(self):
        names = ""
        for beh in self._active_behaviors:
            names += beh.__class__.__name__ + ", "
        return names

    # ---- public methods ---- #

    @property
    def summing_method(self):
        if self.calculate == self._calculate_weighted_sum:
            return self.WEIGHTEDAVERAGE
        elif self.calculate == self._calculate_prioritized:
            return self.PRIORITIZED
        elif self.calculate == self._calculate_dithered:
            return self.DITHERED

    @summing_method.setter
    def summing_method(self, summing_method):
        if summing_method == self.WEIGHTEDAVERAGE:
            self.calculate = self._calculate_weighted_sum
        elif summing_method == self.PRIORITIZED:
            self.calculate = self._calculate_prioritized
        elif summing_method == self.DITHERED:
            self.calculate = self._calculate_dithered
        else:
            raise ValueError("unknown summing_method for summing method")

    # this is a place holder dor documentation, self.calculate is set in set_summing_method
    def calculate(self, vehicle, world):
        """
        Calculates the new steering force and returns it.
        """
        pass

    def activate_behavior(self, behavior):
        # assert behavior.is_active(), "behavior '{0}' is not acitve!".format(behavior)
        if behavior.is_active():
            if behavior not in self._active_behaviors:
                self._active_behaviors.append(behavior)

                # sort for sum priority for the prioitized summation
                self._active_behaviors.sort(key=lambda x: x.sum_priority)

                self._create_update_list()
                self._check_behaviors_need_tagging()
            else:
                if __debug__:
                    raise UserWarning("behavior is already active: {0}".format(behavior))
        else:
            behavior.activate()  # this will activate the behavior and call this method again

    def deactivate_behavior(self, behavior):
        if behavior.is_active():
            behavior.deactivate()  # this will de-activate the behavior and call this method again
        else:
            if behavior in self._active_behaviors:
                self._active_behaviors.remove(behavior)

            self._create_update_list()
            self._check_behaviors_need_tagging()

    # ---- private methods ---- #

    def _check_behaviors_need_tagging(self):
        self._need_tagging = len([beh for beh in self._active_behaviors if beh.needs_tagging()])
        # just in case a behavior is using this but returns needs_tagging==True
        if not self._need_tagging:
            self.tagged_vehicles = []

    def _create_update_list(self):
        self._update_list = [beh.update for beh in self._active_behaviors]

    # ---- summing methods ---- #

    def _calculate_weighted_sum(self, vehicle, world):
        # self._steering_force.zero() # reset
        self._steering_force.x = 0.0  # reset
        self._steering_force.y = 0.0  # reset

        if self._need_tagging > 0:
            assert self.group_vehicles is not None
            self.tagged_vehicles = world.get_vehicles_within_view_range(vehicle, self.group_vehicles, self.viewdistance)

        for update in self._update_list:
            self._steering_force += update(vehicle, world)

        # truncate to max_force
        if self._steering_force.x ** 2 + self._steering_force.y ** 2 > vehicle.max_force ** 2:
            scaling = vehicle.max_force / math_hypot(self._steering_force.x, self._steering_force.y)
            self._steering_force.x *= scaling
            self._steering_force.y *= scaling
        return self._steering_force

    def _calculate_prioritized(self, vehicle, world):
        # self._steering_force.zero() # reset
        self._steering_force.x = 0.0  # reset
        self._steering_force.y = 0.0  # reset

        if self._need_tagging > 0:
            assert self.group_vehicles is not None
            self.tagged_vehicles = world.get_vehicles_within_view_range(vehicle, self.group_vehicles, self.viewdistance)

        force = Vec2d(0.0, 0.0)

        # NOTE: order of update methods is the prioritation, see sum_priority attribute of behavior classes
        for update in self._update_list:
            force = update(vehicle, world)
            if self._exceed_accumulate_force(force, vehicle):
                return self._steering_force
        return self._steering_force

    def _exceed_accumulate_force(self, force, vehicle):
        # //calculate how much steering force the vehicle has used so far
        # magnitude_so_far = self._steering_force.length
        magnitude_so_far = math_hypot(self._steering_force.x, self._steering_force.y)
        # //calculate how much steering force remains to be used by this vehicle
        magnitude_remaining = vehicle.max_force - magnitude_so_far
        # //return true if there is no more force left to use
        if magnitude_remaining <= 0.0:
            return True
        # //calculate the magnitude of the force we want to add
        # magnitude_to_add = force.length
        magnitude_to_add = math_hypot(force.x, force.y)
        # //if the magnitude of the sum of ForceToAdd and the running total
        # //does not exceed the maximum force available to this vehicle, just
        # //add together. Otherwise add as much of the ForceToAdd vector is
        # //possible without going over the max.
        if magnitude_to_add < magnitude_remaining:
            self._steering_force += force
        else:
            # //add it to the steering force
            # -- normalized -- #
            _length = math_hypot(force.x, force.y)
            if _length > 0.0:
                force.x /= _length
                force.y /= _length
            # -- normalized -- #
            self._steering_force += force * magnitude_remaining
        # return false;
        return False

    def _calculate_dithered(self, vehicle, world):
        # self._steering_force.zero() # reset
        self._steering_force.x = 0.0  # reset
        self._steering_force.y = 0.0  # reset

        if self._need_tagging > 0:
            assert self.group_vehicles is not None
            self.tagged_vehicles = world.get_vehicles_within_view_range(vehicle, self.group_vehicles, self.viewdistance)

        for behavior in self._active_behaviors:
            if rand_float() < behavior.probability:
                self._steering_force += behavior.update(vehicle, world) / behavior.probability
                # if not self._steering_force.is_zero():
                if not (self._steering_force.x < EPSILON and self._steering_force.y < EPSILON):
                    # tuncate to max_force
                    if self._steering_force.x ** 2 + self._steering_force.y ** 2 > vehicle.max_force ** 2:
                        scaling = vehicle.max_force / math_hypot(self._steering_force.x, self._steering_force.y)
                        self._steering_force.x *= scaling
                        self._steering_force.y *= scaling
                    return self._steering_force
        return self._steering_force


# ---- behaviors ----#

class _Behavior(object):
    sum_priority = 1000

    def __init__(self, steering, weight, probability):
        self.steering = steering
        self.weight = weight
        self.probability = probability
        self._is_active = False

    def update(self, vehicle, world):
        raise NotImplementedError("should be overridden!")

    def activate_behavior(self):
        # prevent adding a behavior twice
        if self._is_active:
            return
        self._is_active = True
        self.steering.activate_behavior(self)

    def deactivate(self):
        if not self._is_active:
            return
        self._is_active = False
        self.steering.deactivate_behavior(self)

    def is_active(self):
        return self._is_active

    def needs_tagging(self):
        return False


class _PositionTargetedBehavior(_Behavior):

    def update(self, vehicle, world):
        pass

    def activate(self, target_position):
        self.steering.target = target_position
        _Behavior.activate_behavior(self)

    @property
    def target_position(self):
        raise Exception()

    @target_position.setter
    def target_position(self, val):
        raise Exception()


class _SeekBehavior(_PositionTargetedBehavior):
    sum_priority = 107

    def update(self, vehicle, world):
        assert self.steering.target is not None
        return self.seek(vehicle, self.steering.target) * self.weight

    @staticmethod
    def seek(vehicle, target_pos):
        # desired_vel = (target_pos - vehicle.position).normalized * vehicle.max_speed
        # -- normalized -- #
        direction = target_pos - vehicle.position
        _length = math_hypot(direction.x, direction.y)
        if _length > 0.0:
            direction.x /= _length
            direction.y /= _length
        # -- normalized -- #
        desired_vel = direction * vehicle.max_speed
        return desired_vel - vehicle.velocity


class _FleeBehavior(_PositionTargetedBehavior):
    flee_panic_distance = None
    sum_priority = 103

    def activate(self, target_position, flee_panic_distance=_DEFAULT_VALUE):
        if flee_panic_distance is not _DEFAULT_VALUE:
            self.flee_panic_distance = flee_panic_distance
        _PositionTargetedBehavior.activate(self, target_position)

    def update(self, vehicle, world):
        assert self.steering.target is not None
        return self.flee(vehicle, self.steering.target, self.flee_panic_distance) * self.weight

    @staticmethod
    def flee(vehicle, target_pos, flee_panic_distance):
        if flee_panic_distance:
            if vehicle.position.get_distance_sq(target_pos) > flee_panic_distance * flee_panic_distance:
                return Vec2d(0.0, 0.0)
        # desired_vel = (vehicle.position - target_pos).normalized * vehicle.max_speed
        # -- normalized -- #
        direction = vehicle.position - target_pos
        _length = math_hypot(direction.x, direction.y)
        if _length > 0.0:
            direction.x /= _length
            direction.y /= _length
        # -- normalized -- #
        desired_vel = direction * vehicle.max_speed
        return desired_vel - vehicle.velocity


class _ArriveBehavior(_PositionTargetedBehavior):
    sum_priority = 108

    def __init__(self, steering, weight, probability, deceleration_tweaker):
        _PositionTargetedBehavior.__init__(self, steering, weight, probability)
        self.deceleration_tweaker = deceleration_tweaker
        self.deceleration = SteeringBehaviors.NORMAL

    def activate(self, target_position, deceleration=None):
        if deceleration is not None:
            self.deceleration = deceleration
        _PositionTargetedBehavior.activate(self, target_position)

    def update(self, vehicle, world):
        assert self.steering.target is not None
        return self.arrive(vehicle, self.steering.target, self.deceleration, self.deceleration_tweaker) * self.weight

    @staticmethod
    def arrive(vehicle, target_pos, deceleration, deceleration_tweaker):
        to_target = target_pos - vehicle.position
        dist = math_hypot(to_target.x, to_target.y)

        if dist > 0:
            speed = dist / (deceleration * deceleration_tweaker)
            # make sure velocity does not exceed max
            if speed > vehicle.max_speed:
                speed = vehicle.max_speed
            desired_velocity = to_target * speed / dist
            return desired_velocity - vehicle.velocity
        return Vec2d(0.0, 0.0)


class _DistanceInterposeBehavior(_Behavior):
    sum_priority = 112
    # target_position = None
    target_agent2 = None
    turn_around_coefficient = 0.0  # disabled

    def __init__(self, steering, weight, probability, deceleration_tweaker, deceleration=SteeringBehaviors.NORMAL):
        _Behavior.__init__(self, steering, weight, probability)
        self.dist_from_target = 0.0
        self.deceleration = deceleration
        self.interpose_point = None
        self.deceleration_tweaker = deceleration_tweaker

    def activate(self, target_position, target_agent2, dist_from_target1, turn_around_coefficient=None):
        self.steering.target = target_position
        self.target_agent2 = target_agent2
        self.dist_from_target = dist_from_target1
        if turn_around_coefficient is not None:
            self.turn_around_coefficient = turn_around_coefficient
        _Behavior.activate_behavior(self)

    def update(self, vehicle, world):
        midpoint = self.steering.target + (
                self.target_agent2.position - self.steering.target).normalized * self.dist_from_target
        time_to_reach_midpoint = vehicle.position.get_distance(midpoint) / vehicle.max_speed
        if self.turn_around_coefficient != 0.0:
            # turn_arround_time
            # dot = vehicle.heading.dot((midpoint - vehicle.position).normalized) - 1.0
            # -- normalized -- #
            direction = midpoint - vehicle.position
            _length = math_hypot(direction.x, direction.y)
            if _length > 0.0:
                direction.x /= _length
                direction.y /= _length
            # -- normalized -- #
            dot = vehicle.heading.dot(direction) - 1.0
            time_to_reach_midpoint += dot * self.turn_around_coefficient
        b_pos = self.target_agent2.position + self.target_agent2.velocity * time_to_reach_midpoint
        self.interpose_point = self.steering.target + (b_pos - self.steering.target).normalized * self.dist_from_target
        return self.weight * _ArriveBehavior.arrive(vehicle, self.interpose_point, self.deceleration,
                                                    self.deceleration_tweaker)


class _InterposeBehavior(_Behavior):
    sum_priority = 112
    target_agent1 = None
    target_agent2 = None
    turn_around_coefficient = 0.0  # disabled

    def __init__(self, steering, weight, probability, deceleration_tweaker, deceleration=SteeringBehaviors.FAST):
        _Behavior.__init__(self, steering, weight, probability)
        self.deceleration_tweaker = deceleration_tweaker
        self.deceleration = deceleration
        self.interpose_point = None

    def activate(self, target_agent1, target_agent2, turn_around_coefficient=None):
        self.target_agent1 = target_agent1
        self.target_agent2 = target_agent2
        if turn_around_coefficient is not None:
            self.turn_around_coefficient = turn_around_coefficient
        _Behavior.activate_behavior(self)

    def update(self, vehicle, world):
        midpoint = (self.target_agent1.position + self.target_agent2.position) * 0.5
        time_to_reach_midpoint = vehicle.position.get_distance(midpoint) / vehicle.max_speed
        if self.turn_around_coefficient != 0.0:
            # turn_arround_time
            # dot = vehicle.heading.dot((midpoint - vehicle.position).normalized) - 1.0
            # -- normalized -- #
            direction = midpoint - vehicle.position
            _length = math_hypot(direction.x, direction.y)
            if _length > 0.0:
                direction.x /= _length
                direction.y /= _length
            # -- normalized -- #
            dot = vehicle.heading.dot(direction) - 1.0
            time_to_reach_midpoint += dot * self.turn_around_coefficient

        a_pos = self.target_agent1.position + self.target_agent1.velocity * time_to_reach_midpoint
        b_pos = self.target_agent2.position + self.target_agent2.velocity * time_to_reach_midpoint

        self.interpose_point = (a_pos + b_pos) * 0.5

        return self.weight * _ArriveBehavior.arrive(vehicle, self.interpose_point,
                                                    self.deceleration, self.deceleration_tweaker)


class _WanderBehavior(_Behavior):
    sum_priority = 109
    _target_local = Vec2d(0.0, 0.0)

    def __init__(self, steering, weight, probability, distance, jitter, radius, theta=None):
        _Behavior.__init__(self, steering, weight, probability)
        self.distance = distance
        self.jitter = jitter
        self.radius = radius
        if theta is None:
            theta = random.random() * 2.0 * math_pi
        self.target = Vec2d(self.radius * math_sin(theta), self.radius * math_cos(theta))

    def activate(self):
        _Behavior.activate_behavior(self)

    def update(self, vehicle, world):
        self.target.x += (2 * random.random() - 1.0) * self.jitter * 0.001
        self.target.x += (2 * random.random() - 1.0) * self.jitter * 0.001
        # -- normalize --#
        target_len = math_hypot(self.target.x, self.target.y)
        if target_len:
            self.target.x /= target_len
            self.target.y /= target_len
        # -- normalize --#
        self.target.x *= self.radius
        self.target.y *= self.radius

        self._target_local.x = self.distance + self.target.x
        self._target_local.y = self.target.y
        target_world = point_to_world_2d(self._target_local, vehicle.heading, vehicle.side, vehicle.position)
        return (target_world - vehicle.position) * self.weight


class _EvadeBehavior(_Behavior):
    flee_panic_distance = None
    thread_range = None
    sum_priority = 102
    turn_around_coefficient = 0.0  # disabled
    pursuer = None

    def activate(self, pursuer, thread_range=None, turn_around_coefficient=None):
        assert pursuer is not None
        self.pursuer = pursuer
        if thread_range is not None:
            self.thread_range = thread_range
        if turn_around_coefficient is not None:
            self.turn_around_coefficient = turn_around_coefficient
        _Behavior.activate_behavior(self)

    def update(self, vehicle, world):
        return self.weight * self.evade(vehicle, self.pursuer, self.thread_range, self.flee_panic_distance,
                                        self.turn_around_coefficient)

    @staticmethod
    def evade(vehicle, pursuer, thread_range=None, flee_panic_distance=None, turn_around_coefficient=0.0):
        to_pursuer = pursuer.position - vehicle.position

        if thread_range and (to_pursuer.x ** 2 + to_pursuer.y ** 2) > thread_range * thread_range:
            return Vec2d(0.0, 0.0)

        look_ahead_time = math_hypot(to_pursuer.x, to_pursuer.y) / (vehicle.max_speed +
                                                                    math_hypot(pursuer.velocity.x, pursuer.velocity.y))

        if turn_around_coefficient != 0.0:
            # turn_arround_time
            # dot = vehicle.heading.dot((pursuer.position - vehicle.position).normalized) - 1.0
            # -- normalized -- #
            direction = pursuer.position - vehicle.position
            _length = math_hypot(direction.x, direction.y)
            if _length > 0.0:
                direction.x /= _length
                direction.y /= _length
            # -- normalized -- #
            dot = vehicle.heading.dot(direction) - 1.0
            look_ahead_time += dot * turn_around_coefficient

        vehicle.steering.targeted_evader_position = pursuer.position + pursuer.velocity * look_ahead_time
        return _FleeBehavior.flee(vehicle, vehicle.steering.targeted_evader_position, flee_panic_distance)


class _GroupBehavior(_Behavior):

    def activate(self, vehicle_group):
        self.steering.group_vehicles = vehicle_group
        _Behavior.activate_behavior(self)

    def needs_tagging(self):
        return True


class _CohesionBehavior(_GroupBehavior):
    sum_priority = 106
    center_of_mass = Vec2d(0.0, 0.0)

    def update(self, current_vehicle, world):
        # uses previously tagged vehicles
        self.center_of_mass.x = 0.0
        self.center_of_mass.y = 0.0
        count = 0.0
        for vehicle in self.steering.tagged_vehicles:
            self.center_of_mass.x += vehicle.position.x
            self.center_of_mass.y += vehicle.position.y
            count += 1

        if count > 0.0:
            self.center_of_mass.x /= count
            self.center_of_mass.y /= count
            return _SeekBehavior.seek(current_vehicle, self.center_of_mass) * self.weight
        return self.center_of_mass * self.weight


class _SeparationBehavior(_GroupBehavior):
    sum_priority = 104
    steering_force = Vec2d(0.0, 0.0)

    def update(self, current_vehicle, world):
        # uses previously tagged vehicles
        self.steering_force.x = 0.0
        self.steering_force.y = 0.0
        for vehicle in self.steering.tagged_vehicles:
            to_agent_x = current_vehicle.position.x - vehicle.position.x
            to_agent_y = current_vehicle.position.y - vehicle.position.y
            # scale force inversily proportional to the agents distance
            length_sq = float(to_agent_x ** 2 + to_agent_y ** 2)
            if length_sq > 0.0:
                self.steering_force.x += to_agent_x / length_sq
                self.steering_force.y += to_agent_y / length_sq
        return self.steering_force * self.weight


class _AlignmentBehavior(_GroupBehavior):
    sum_priority = 105
    average_heading = Vec2d(0.0, 0.0)

    def update(self, current_vehicle, world):
        # uses previously tagged vehicles
        self.average_heading.x = 0.0
        self.average_heading.y = 0.0
        count = 0.0
        for vehicle in self.steering.tagged_vehicles:
            self.average_heading.x += vehicle.heading.x
            self.average_heading.y += vehicle.heading.y
            count += 1.0

        if count > 0.0:
            self.average_heading.x /= count
            self.average_heading.y /= count
            # substract vehicles heading to get steering force
            return (self.average_heading - current_vehicle.heading) * self.weight
        return self.average_heading


class _FlockBehavior(_GroupBehavior):

    def activate(self, vehicle_group):
        # no need to add this 'container' behavior to the active list
        self.steering.behavior_separation.activate(vehicle_group)
        self.steering.behavior_alignment.activate(vehicle_group)
        self.steering.behavior_cohesion.activate(vehicle_group)
        self.steering.behavior_wander.activate()
        self._is_active = True

    def deactivate(self):
        self.steering.behavior_cohesion.deactivate()
        self.steering.behavior_separation.deactivate()
        self.steering.behavior_alignment.deactivate()
        self._is_active = False


class _ObstacleAvoidanceBehavior(_Behavior):
    sum_priority = 101

    def __init__(self, steering, weight, probability, min_dboxlength, break_weight):
        _Behavior.__init__(self, steering, weight, probability)
        self.min_dboxlength = min_dboxlength
        self.break_weight = break_weight
        self.dboxlength = min_dboxlength  # read-only

    def activate(self):
        _Behavior.activate_behavior(self)

    def update(self, vehicle, world):
        dboxlength = self.min_dboxlength + \
                     (math_hypot(vehicle.velocity.x, vehicle.velocity.y) / vehicle.max_speed) * self.min_dboxlength
        self.dboxlength = dboxlength
        # vehicle.world.tag_obstacles_within_view_range(vehicle, obstacles, dboxlength)
        _obstacles = world.get_obstacles_within_view_range(vehicle, vehicle.obstacles, dboxlength)

        closest_intersecting_obstacle = None
        dist_to_closest = sys_maxsize
        local_pos_of_closest_obstacle_x = 0.0
        local_pos_of_closest_obstacle_y = 0.0

        for obstacle in _obstacles:
            local_center = point_to_local_2d(obstacle.position, vehicle.heading, vehicle.side, vehicle.position)
            circle_x = local_center.x
            circle_y = local_center.y
            # obstacles behind the vehicle are ignored
            if circle_x >= 0:
                expanded_radius = vehicle.bounding_radius + obstacle.bounding_radius
                # intersecting the detection box
                if abs(circle_y) < expanded_radius:
                    # find cicle line intersection points

                    sqrt_part = math_sqrt(expanded_radius * expanded_radius + circle_y * circle_y)

                    dist_to_current = circle_x - sqrt_part

                    if dist_to_current <= 0:
                        dist_to_current = circle_x + sqrt_part

                    # see if this is the closes so far
                    if dist_to_current < dist_to_closest:
                        dist_to_closest = dist_to_current
                        closest_intersecting_obstacle = obstacle
                        local_pos_of_closest_obstacle_x = circle_x
                        local_pos_of_closest_obstacle_y = circle_y

        # calculte steering force away from obstacle
        if closest_intersecting_obstacle:
            # the closer, the stronger the steering force should be
            multiplier = 1.0 + (dboxlength - local_pos_of_closest_obstacle_x) / dboxlength
            # lateral force
            steering_force_y = multiplier * \
                               (closest_intersecting_obstacle.bounding_radius - local_pos_of_closest_obstacle_y)
            # break_weight = 0.2
            # apply a breaking force proportional to the distance to obstacle
            steering_force_x = self.break_weight * \
                               (closest_intersecting_obstacle.bounding_radius - local_pos_of_closest_obstacle_x)

            # convert the steering vector to global space
            return self.weight * \
                   vector_to_world_2d(Vec2d(steering_force_x, steering_force_y), vehicle.heading, vehicle.side)
        return Vec2d(0.0, 0.0)


class _AgentTargetedBehavior(_Behavior):
    target_agent = None

    def activate(self, agent):
        self.target_agent = agent
        _Behavior.activate_behavior(self)


class _PursuitBehavior(_AgentTargetedBehavior):
    sum_priority = 110
    turn_around_coefficient = 0.0  # disabled

    def activate(self, agent, turn_around_coefficient=None):
        _AgentTargetedBehavior.activate(self, agent)
        if turn_around_coefficient is not None:
            self.turn_around_coefficient = turn_around_coefficient

    def update(self, vehicle, world):
        evader = self.target_agent
        to_evader = evader.position - vehicle.position
        relative_heading = vehicle.heading.dot(evader.heading)
        if to_evader.dot(vehicle.heading) > 0 and relative_heading < -0.95:
            vehicle.steering.targeted_pursuit_position = evader.position
            return _SeekBehavior.seek(vehicle, evader.position) * self.weight

        look_ahead_time = math_hypot(to_evader.x, to_evader.y) / (
                vehicle.max_speed + math_hypot(evader.velocity.x, evader.velocity.y))
        if self.turn_around_coefficient != 0.0:
            # turn_arround_time
            # dot = vehicle.heading.dot((evader.position - vehicle.position).normalized) - 1.0
            # -- normalized -- #
            direction = evader.position - vehicle.position
            _length = math_hypot(direction.x, direction.y)
            if _length > 0.0:
                direction.x /= _length
                direction.y /= _length
            # -- normalized -- #
            dot = vehicle.heading.dot(direction) - 1.0
            look_ahead_time += dot * self.turn_around_coefficient

        vehicle.steering.targeted_pursuit_position = evader.position + evader.velocity * look_ahead_time
        return _SeekBehavior.seek(vehicle, vehicle.steering.targeted_pursuit_position) * self.weight


class _HideBehavior(_AgentTargetedBehavior):
    sum_priority = 113
    flee_panic_distance = None
    thread_range = None

    def __init__(self, steering, weight, probability, deceleration_tweaker, deceleration=SteeringBehaviors.FAST):
        _AgentTargetedBehavior.__init__(self, steering, weight, probability)
        self.deceleration_tweaker = deceleration_tweaker
        self.deceleration = deceleration

    def activate(self, agent, deceleration_tweaker=None, deceleration=None,
                 flee_panic_distance=_DEFAULT_VALUE, thread_range=_DEFAULT_VALUE):
        if deceleration_tweaker is not None:
            self.deceleration_tweaker = deceleration_tweaker
        if deceleration is not None:
            self.deceleration = deceleration
        if flee_panic_distance is not _DEFAULT_VALUE:
            self.flee_panic_distance = flee_panic_distance
        if thread_range is not _DEFAULT_VALUE:
            self.thread_range = thread_range
        _AgentTargetedBehavior.activate(self, agent)

    def update(self, vehicle, world):
        # def _hide(self, target, obstacles):
        dist_to_closest = sys_maxsize
        best_hiding_spot = Vec2d(0.0, 0.0)

        for obstacle in vehicle.obstacles:
            # getting hiding spot
            distance_from_boundary = 30.0
            dist_away = obstacle.bounding_radius + distance_from_boundary
            # to_ob = (obstacle.position - self.target_agent.position).normalized
            # -- normalized -- #
            to_ob = obstacle.position - self.target_agent.position
            _length = math_hypot(to_ob.x, to_ob.y)
            if _length > 0.0:
                to_ob.x /= _length
                to_ob.y /= _length
            # -- normalized -- #
            hiding_spot = (to_ob * dist_away) + obstacle.position

            dist = vehicle.position.get_distance(hiding_spot)
            if dist < dist_to_closest:
                dist_to_closest = dist
                best_hiding_spot = hiding_spot

        if dist_to_closest == sys_maxsize:
            return self.weight * \
                   _EvadeBehavior.evade(vehicle, self.target_agent, self.thread_range, self.flee_panic_distance)

        return self.weight * \
               _ArriveBehavior.arrive(vehicle, best_hiding_spot, self.deceleration, self.deceleration_tweaker)


class _WallAvoidanceBehavior(_Behavior):
    sum_priority = 100

    def activate(self):
        _Behavior.activate_behavior(self)

    def update(self, vehicle, world):
        dist_to_closest = sys_maxsize
        closest_wall = None
        closest_point = None
        steering_force = Vec2d(0.0, 0.0)

        for feeler_local in vehicle.feelers:
            feeler = point_to_world_2d(feeler_local, vehicle.heading, vehicle.side, vehicle.position)
            for wall in world.walls:
                intersection, dist_to_current, point = line_intersection_2d(vehicle.position, feeler, wall.start,
                                                                            wall.end)
                if intersection:
                    if dist_to_current < dist_to_closest:
                        dist_to_closest = dist_to_current
                        closest_wall = wall
                        closest_point = point

            if closest_wall:
                overshoot = feeler - closest_point
                # create force away from wall
                steering_force += closest_wall.normal * math_hypot(overshoot.x, overshoot.y)
                # makes a difference is last or first feeler wins?
                break

        return steering_force * self.weight


class _FollowPathBehavior(_Behavior):
    sum_priority = 114

    def __init__(self, steering, weight, probability, waypointseekdist, deceleration_tweaker):
        _Behavior.__init__(self, steering, weight, probability)
        self.path = None
        self.waypointseekdist = waypointseekdist
        self.deceleration = SteeringBehaviors.NORMAL
        self.deceleration_tweaker = deceleration_tweaker

    def activate(self, path, deceleration=None):
        _Behavior.activate_behavior(self)
        self.path = path
        if deceleration is not None:
            self.deceleration = deceleration

    def update(self, vehicle, world):
        if self.path.current_waypoint.get_distance_sq(vehicle.position) < self.waypointseekdist * self.waypointseekdist:
            self.path.set_next_waypoint()

        if self.path.is_finished:
            return self.weight * \
                   _ArriveBehavior.arrive(vehicle, self.path.current_waypoint, self.deceleration,
                                          self.deceleration_tweaker)
        else:
            return _SeekBehavior.seek(vehicle, self.path.current_waypoint) * self.weight


class _OffsetPursuitBehavior(_Behavior):
    sum_priority = 111
    turn_around_coefficient = 0.0  # disabled

    def __init__(self, steering, weight, probability, deceleration_tweaker):
        _Behavior.__init__(self, steering, weight, probability)
        self.deceleration = SteeringBehaviors.FAST
        self.deceleration_tweaker = deceleration_tweaker
        self.leader = None
        self.offset = Vec2d(0.0, 0.0)

    def activate(self, leader, offset, deceleration=None, turn_around_coefficient=None):
        _Behavior.activate_behavior(self)
        self.leader = leader
        self.offset = offset
        if deceleration is not None:
            self.deceleration = deceleration
        if turn_around_coefficient is not None:
            self.turn_around_coefficient = turn_around_coefficient

    def update(self, vehicle, world):
        world_offset_point = point_to_world_2d(self.offset, self.leader.heading, self.leader.side, self.leader.position)
        to_offset = world_offset_point - vehicle.position
        look_ahead_time = math_hypot(to_offset.x, to_offset.y) / (vehicle.max_speed +
                                                                  math_hypot(self.leader.velocity.x,
                                                                             self.leader.velocity.y))

        if self.turn_around_coefficient != 0.0:
            # turn_arround_time
            # dot = vehicle.heading.dot((self.leader.position - vehicle.position).normalized) - 1.0
            # -- normalized -- #
            direction = self.leader.position - vehicle.position
            _length = math_hypot(direction.x, direction.y)
            if _length > 0.0:
                direction.x /= _length
                direction.y /= _length
            # -- normalized -- #
            dot = vehicle.heading.dot(direction) - 1.0
            look_ahead_time += dot * self.turn_around_coefficient

        return _ArriveBehavior.arrive(vehicle, world_offset_point + self.leader.velocity * look_ahead_time,
                                      self.deceleration, self.deceleration_tweaker) * self.weight


# TODO: move this to 'geometry' module?
def line_intersection_2d(A, B, C, D):
    rTop = (A.y - C.y) * (D.x - C.x) - (A.x - C.x) * (D.y - C.y)
    rBot = (B.x - A.x) * (D.y - C.y) - (B.y - A.y) * (D.x - C.x)

    sTop = (A.y - C.y) * (B.x - A.x) - (A.x - C.x) * (B.y - A.y)
    sBot = (B.x - A.x) * (D.y - C.y) - (B.y - A.y) * (D.x - C.x)

    if (rBot == 0) or (sBot == 0):
        # lines are parallel
        return False, None, None

    r = rTop / rBot
    s = sTop / sBot

    if (r > 0) and (r < 1) and (s > 0) and (s < 1):
        dist = A.get_distance(B) * r
        point = A + r * (B - A)
        return True, dist, point
    else:
        return False, 0, None


# TODO: move this to a 'transform' module?
def point_to_world_2d(local_point, x_axis, y_axis, global_pos):
    # m11 m12 m13       r11 r12 t13       xax yax pox
    # m21 m22 m23  =>   r21 r22 t23  =>   xay yay poy
    # m31 m32 m33       m31 m32 1.0       m31 m32 1.0
    assert x_axis.length < 1.0 + EPSILON
    assert x_axis.length > 1.0 - EPSILON
    assert y_axis.length < 1.0 + EPSILON
    assert y_axis.length > 1.0 - EPSILON
    return Vec2d(x_axis.x * local_point.x + y_axis.x * local_point.y + global_pos.x,
                 x_axis.y * local_point.x + y_axis.y * local_point.y + global_pos.y)


def point_to_local_2d(global_point, x_axis, y_axis, position):
    # m11 m12 m13       r11 r12 t13       xax xay -pox
    # m21 m22 m23  =>   r21 r22 t23  =>   yax yay -poy
    # m31 m32 m33       m31 m32 1.0       m31 m32 1.0
    assert x_axis.length < 1.0 + EPSILON
    assert x_axis.length > 1.0 - EPSILON
    assert y_axis.length < 1.0 + EPSILON
    assert y_axis.length > 1.0 - EPSILON
    gx = global_point.x - position.x
    gy = global_point.y - position.y
    return Vec2d(x_axis.x * gx + x_axis.y * gy,
                 y_axis.x * gx + y_axis.y * gy)


def vector_to_world_2d(local_vector, x_axis, y_axis):
    # m11 m12 m13       r11 r12 t13       xax yax pox
    # m21 m22 m23  =>   r21 r22 t23  =>   xay yay poy
    # m31 m32 m33       m31 m32 1.0       m31 m32 1.0
    assert x_axis.length < 1.0 + EPSILON
    assert x_axis.length > 1.0 - EPSILON
    assert y_axis.length < 1.0 + EPSILON
    assert y_axis.length > 1.0 - EPSILON
    return Vec2d(x_axis.x * local_vector.x + y_axis.x * local_vector.y,
                 x_axis.y * local_vector.x + y_axis.y * local_vector.y)

# def vector_to_local_2d(global_vector, x_axis, y_axis):
# # m11 m12 m13       r11 r12 t13       xax yax pox
# # m21 m22 m23  =>   r21 r22 t23  =>   xay yay poy
# # m31 m32 m33       m31 m32 1.0       m31 m32 1.0
# assert x_axis.length < 1.0 + EPSILON and x_axis.length > 1.0 - EPSILON
# assert y_axis.length < 1.0 + EPSILON and y_axis.length > 1.0 - EPSILON
# return Vec2d(x_axis.x * global_vector.x + x_axis.y * global_vector.y,
# y_axis.x * global_vector.x + y_axis.y * global_vector.y)


# if __name__ == '__main__':
# x_axis = Vec2d(1, 1).normalized
# y_axis = Vec2d(-1, 1).normalized
# position = Vec2d(10, 0)
# local_point = Vec2d(45,77)
# print "local", local_point
# p = point_to_world_2d(local_point, x_axis, y_axis, position)
# # p = point_to_world_2d(local_point, x_axis, y_axis, Vec2d(0.0, 0.0))
# print "world", p
# # p = point_to_local_2d(Vec2d(0, 1.41421356237), x_axis, y_axis, Vec2d(0,0))
# p = point_to_local_2d(p, x_axis, y_axis, position)
# # p = point_to_local_2d(p, x_axis, y_axis, Vec2d(0.0, 0.0))
# print "local again", p
# print p == local_point

# global_point = Vec2d(20.0, 33.0)
# print "global", global_point
# p = point_to_local_2d(global_point, x_axis, y_axis, position)
# print "local", p
# p = point_to_world_2d(p, x_axis, y_axis, position)
# print "global again", p
# print p == global_point
