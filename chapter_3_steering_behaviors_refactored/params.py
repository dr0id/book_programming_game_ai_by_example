# -*- coding: utf-8 -*-

try:
    import ConfigParser
except:
    import configparser as ConfigParser

_config_parser = ConfigParser.SafeConfigParser()

_config_parser.read("params.ini")

# this would read in alle options from all setctions
# params = dict( (option, _config_parser.getfloat(section, option)) \
                # for section in _config_parser.sections() \
                # for option in _config_parser.options(section) )

section = "steeringbehaviors"
params = dict( (option, _config_parser.getfloat(section, option)) \
                for option in _config_parser.options(section) )

steeringforcetweaker = params["steeringforcetweaker"]

params["steeringforce"] = params["steeringforce"] * steeringforcetweaker
params["separationweight"] = params["separationweight"] * steeringforcetweaker
params["alignmentweight"] = params["alignmentweight"] * steeringforcetweaker
params["cohesionweight"] = params["cohesionweight"] * steeringforcetweaker
params["obstacleavoidanceweight"] = params["obstacleavoidanceweight"] * steeringforcetweaker
params["wallavoidanceweight"] = params["wallavoidanceweight"] * steeringforcetweaker
params["wanderweight"] = params["wanderweight"] * steeringforcetweaker
params["seekweight"] = params["seekweight"] * steeringforcetweaker
params["fleeweight"] = params["fleeweight"] * steeringforcetweaker
params["arriveweight"] = params["arriveweight"] * steeringforcetweaker
params["pursuitweight"] = params["pursuitweight"] * steeringforcetweaker
params["offsetpursuitweight"] = params["offsetpursuitweight"] * steeringforcetweaker
params["interposeweight"] = params["interposeweight"] * steeringforcetweaker
params["hideweight"] = params["hideweight"] * steeringforcetweaker
params["evadeweight"] = params["evadeweight"] * steeringforcetweaker
params["followpathweight"] = params["followpathweight"] * steeringforcetweaker

