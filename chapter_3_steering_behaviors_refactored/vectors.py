# -*- coding: utf-8 -*-

"""
Vector calsses.

:Variables:
    PI_DIV_180 : float
        Just pi/180.0 used for converting  between degrees and radians
        (negavite because of the rotation direction)

:Note:
    Make sure to know the difference between::

        u = Vec2()
        v = Vec2()
        w = Vec2()

        # only values are copied, same as u.x = w.x and u.y = w.y etc.
        u.values = w
        # reference, v is actually lost and points now to w
        v = w

    When using ``v = w`` any change to w will also change w and vice versa
    since both point to the same `Vec2` instance.

"""




__author__ = "dr0iddr0id {at} gmail [dot] com (C) 2010"

import logging
_LOGGER = logging.getLogger('pyknic.geometry.vectors')

if __debug__:
    _LOGGER.debug('%s loading ... \n' % (__name__))
    import time
    _START_TIME = time.time()

#import math
from math import hypot
#from math import radians
#from math import degrees
from math import sin
from math import cos
from math import acos
from math import pi
from math import atan2


EPSILON = 1e-9
PI_DIV_180 = pi / 180.0


def sign(value):
    """
    Signum function. Computes the sign of a value.

    :Parameters:
        value : numerical
            Any number.

    :rtype: numerical

    :Returns:
        -1 if value was negative,
        1 if value was positive,
        0 if value was equal zero
    """
    assert isinstance(value, int) or isinstance(value, float)
    if 0 < value:
        return 1
    elif 0 > value:
        return -1
    else:
        return 0

# -----------------------------------------------------------------------------

class Vec2(object):
    """
    See `vectors`

    2D vector class.

    :Ivariables:
        x : int, float
            x value
        y : int, float
            y value

    """

    __slots__ = tuple('xy')

    def __init__(self, x_coord, y_coord):
        """
        Constructor.

        :Parameters:
            x_coord : int, float
                x value
            y_coord : int, float
                y value
        """
        if __debug__:
            import decimal
            assert isinstance(x_coord,
                                   (int, complex, float, decimal.Decimal))
            assert isinstance(y_coord,
                                   (int, complex, float, decimal.Decimal))
        self.x = x_coord  # pylint: disable-msg=C0103
        self.y = y_coord  # pylint: disable-msg=C0103

    # -- properties -- #
    def set_values(self, other):
        """
        Copy the values from other to own, e.g.:
        self.x = other.x
        self.y = other.y
        """
        assert isinstance(other, self.__class__)
        self.x = other.x
        self.y = other.y
    values = property(None, set_values, doc='set only, value assignemt')

    def _set_from_iterable(self, other):
        """
        Sets the x and y values from other using an iterator on it.
        """
        oiter = iter(other)
        try:
            self.x = next(oiter)
            self.y = next(oiter)
        except StopIteration:
            pass
    from_iter = property(None, _set_from_iterable,
                         doc='''
                                set only, vlaue assignment from iterable,
                                if iterable is too short, old values will be
                                left in place, e.g. iterable is [1] then only x
                                will be set and y is unmodified''')

    def _length(self):
        """
        Calculates the length of the vector and returns it.
        """
        return hypot(self.x, self.y)
    def _set_length(self, value):
        """Scale the vector to have a given length"""
        self *= (value / self.length)
    length = property(_length, _set_length, doc="""
    returns length and change the length if set::

        v = Vec2(x, y)
        length = v.length
        v.length = 10 # vectors is scaled so length is 10 now

    """)

    def _length_sq(self):
        """
        Calculates the squared length of the vector and returns it.
        """
        return self.x * self.x + self.y * self.y
    length_sq = property(_length_sq, doc="""returns length squared""")

    def _normalized(self):
        """
        Returns a new vector with unit length.
        """
        leng = self.length
        if leng:
            return self.__class__(self.x / leng, self.y / leng)
        else:
            return self.__class__(0, 0)
    normalized = property(_normalized,
                                doc="""returns new vector with unit length""")

    def _normal_left(self):
        """returns the right normal (perpendicular), not unit length"""
        return self.__class__(-self.y, self.x)
    normal_left = property(_normal_left)

    def _normal_right(self):
        """returns the left normal (perpendicular), not unit length"""
        return self.__class__(self.y, -self.x)
    normal_right = property(_normal_right)

    perp = normal_left

    def _get_angle(self):
        """returns the angle in radians"""
        return atan2(self.y, self.x) / PI_DIV_180
    angle = property(_get_angle,
                            doc="read only, returns angle, 0 is at 3 o'clock")

    # -- repr methods -- #
    def __str__(self):
        return "<%s(%s, %s) at %s>" % (self.__class__.__name__,
                                        self.x,
                                        self.y,
                                        hex(id(self)))

#    def __str__(self):
#        return u"<%s(%s, %s)>" %(self.__class__.__name__, self.x, self.y)

    # -- math -- #
    def __add__(self, other):
        """+ operation"""
        assert isinstance(other, self.__class__)
        return self.__class__(self.x + other.x, self.y + other.y)

    def __iadd__(self, other):
        """+= operation"""
        assert isinstance(other, self.__class__)
        self.x += other.x
        self.y += other.y
        return self

    def __sub__(self, other):
        """- operation"""
        assert isinstance(other, self.__class__)
        return self.__class__(self.x - other.x, self.y - other.y)

    def __isub__(self, other):
        """-= operation"""
        assert isinstance(other, self.__class__)
        self.x -= other.x
        self.y -= other.y
        return self

    def __mul__(self, scalar):
        """* operator, only scalar multiplication, for other use the methods"""
        return self.__class__(self.x * scalar,  self.y * scalar)
    __rmul__ = __mul__

    def __imul__(self, scalar):
        """ \*= multiplication, scalar only"""
        self.x *= scalar
        self.y *= scalar
        return self

    def __len__(self):
        """returns always 2"""
        return 2

    def __div__(self, scalar):
        """/ operator, only scalar"""
        return self.__class__(self.x / scalar, self.y / scalar)
        
    __truediv__ = __div__

    def __idiv__(self, scalar):
        """/= operator, only scalar"""
        self.x /= scalar
        self.y /= scalar
        return self

    def __neg__(self):
        """- operator, same as -1 * v"""
        return self.__class__(-self.x, -self.y)

    def __pos__(self):
        """+ operator"""
        return self.__class__(abs(self.x), abs(self.y))

    # -- comparison -- #
    def __eq__(self, other):
        """ == operator, vectors are equal when components are equal, might not
        make much sense since using a tolerance would be better"""
        if isinstance(other, self.__class__):
            delta_x = self.x - other.x
            delta_y = self.y - other.y
            return delta_x * delta_x + delta_y * delta_y < EPSILON
            # return self.get_distance_sq(other) < EPSILON
        return False

    def __ne__(self, other):
        """same as 'not __eq__'"""
        return not self.__eq__(other)

    def __hash__(self):
        return id(self)

    # -- items access -- #
    def __getitem__(self, key):
        """[] operator, slow"""
        return (self.x, self.y)[key]

    def __iter__(self):
        """iterator, slow"""
        return iter((self.x, self.y))

    # -- additional methods -- #
    def as_tuple(self):
        """
        Returns a tuple containing the vector values.

        :rtype: tuple
        :Returns: (x, y)

        """
        return self.x, self.y

    as_xy_tuple = as_tuple

    def truncate(self, scalar):
        if self.length_sq > scalar * scalar:
            self.length = scalar

    def wrap_around(self, size):
        x_max, y_max = size
        if self.x > x_max:
            self.x = 0
        elif self.x < 0.0:
            self.x = x_max
        if self.y > y_max:
            self.y = 0
        elif self.y < 0.0:
            self.y = y_max

    def round(self, n_digits=3):
        """
        Round values to n_digits.

        :Parameters:
            n_digits : int
                Number of digits after the point.
        """
        self.x = round(self.x, n_digits)
        self.y = round(self.y, n_digits)

    def rounded(self, n_digits=3):
        """
        Get a rounded vector.

        :Parameters:
            n_digits : int
                Number of digits after the point.

        :rtype: `Vec2`
        :Returns: `Vec2` rounded to n_digits
        """
        return self.__class__(round(self.x, n_digits), round(self.y, n_digits))

    def normalize(self):
        """
        Make the vector unit length.

        :Returns: the length it has before normalizing.
        """
        leng = self.length
        if leng:
            self.x /= leng
            self.y /= leng
        return leng

    def clone(self):
        """
        Clone the vector.

        :rtype: `Vec2`
        :Returns: a copy of itself.
        """
        return self.__class__(self.x, self.y)

    def dot(self, other):
        """
        Dot product.
        """
        assert isinstance(other, self.__class__)
        return self.x * other.x + self.y * other.y

    def cross(self, other):
        """
        Cross product.

        :Parameters:
            other : `Vec2`
                The second vector for the cross product.

        :rtype: float
        :Returns: z value of the cross product (since x and y would be 0).
        :Note: a.cross(b) == - b.cross(a)
        """
        assert isinstance(other, self.__class__)
        return self.x * other.y - self.y * other.x

    def project_onto(self, other):
        """
        Project this vector onto another one.

        :Parameters:
            other : `Vec2`
                The other vector to project onto.

        :rtype: `Vec2`
        :Returns: The projected vector.
        """
        assert isinstance(other, self.__class__)
        return self.dot(other) / other.length_sq * other

    def reflect(self, normal):
        """normal should be normalized unitlength"""
        assert isinstance(normal, self.__class__)
        return self - 2 * self.dot(normal) * normal

    def reflect_tangent(self, tangent):
        """tangent should be normalized, unitlength"""
        assert isinstance(tangent, self.__class__)
        return 2 * tangent.dot(self) * tangent - self

    def rotate(self, degrees):
        """Rotates the vector bout the angle (degrees), + clockwise, - ccw"""
        rad = degrees * PI_DIV_180
        sin_val = sin(rad)
        cos_val = cos(rad)
        xcoord = self.x
        self.x = cos_val * xcoord - sin_val * self.y
        self.y = sin_val * xcoord + cos_val * self.y

    def rotated(self, degrees):
        """
        Returns a new vector, rotated about angle (degrees), + clockwise, - ccw
        """
        rad = degrees * PI_DIV_180
        sin_val = sin(rad)
        cos_val = cos(rad)
        return self.__class__(cos_val * self.x - sin_val * self.y,
                              sin_val * self.x + cos_val * self.y)

    def scaled(self, scale):
        """
        Returns a vector scaled to given length.

        :Returns: `Vec2` with lange scale
        """
        scale = scale / self.length
        return self.__class__(scale * self.x, scale * self.y)

    def rotate_to(self, angle_degrees):
        """rotates the vector to the given angle (degrees)."""
        self.rotate(angle_degrees - self.angle)

    def get_angle_between(self, other):
        """Returns the angle between the vectors."""
        assert isinstance(other, self.__class__)
        length = self.length
        olen = other.length
        if length and olen:
            return  acos(self.dot(other) / (length * olen)) / PI_DIV_180
        return 0

    def get_distance_sq(self, other):
        """
        Distance squared this and other point
        (represented as vector from origin).

        :Parameters:
            other : `Vec2`
                The second vector for the distance.
        :rtype: float
        """
        assert isinstance(other, self.__class__)
        delta_x = self.x - other.x
        delta_y = self.y - other.y
        return delta_x * delta_x + delta_y * delta_y

    def get_distance(self, other):
        """
        Distance this and other point (represented as vector from origin).

        :Parameters:
            other : `Vec2`
                The second vector for the distance.
        :rtype: float
        """
        assert isinstance(other, self.__class__)
        return hypot(self.x - other.x, self.y - other.y)

        
    def zero(self):
        self.x = 0.0
        self.y = 0.0

    def is_zero(self):
        return self.x * self.x + self.y * self.y < EPSILON
        


# ------------------------------------------------------------------------------

class Vec3(object):
    """
    See `vectors`

    3D Vector.


    :Ivariables:
        x : int, float
            x, value
        y : int, float
            y, value
        z : int, float
            z, value
    """

    __slots__ = tuple('xyz')

    def __init__(self, x_coord, y_coord, z_coord=0):
        """
        Constructor.

        :Parameters:
            x_coord : int, float
                x value
            y_coord : int, float
                y value
            z_coord : int, float
                z value, defaults to 0
        """
        if __debug__:
            import decimal
            assert isinstance(x_coord,
                                (int, complex, float, decimal.Decimal))
            assert isinstance(y_coord,
                                (int, complex, float, decimal.Decimal))
            assert isinstance(z_coord,
                                (int, complex, float, decimal.Decimal))
        self.x = x_coord # pylint: disable-msg=C0103
        self.y = y_coord # pylint: disable-msg=C0103
        self.z = z_coord # pylint: disable-msg=C0103

    # -- properties -- #
    def _set_values(self, other):
        """Copy the values from other, e.g.:
            self.x = other.x
            self.y = other.y
            self.z = other.z
        """
        assert isinstance(other, self.__class__)
        self.x = other.x
        self.y = other.y
        self.z = other.z
    values = property(None, _set_values, doc='set only, value assignemt')

    def _set_from_iterable(self, other):
        """Set x,y and z using an iterator on other"""
        oiter = iter(other)
        try:
            self.x = next(oiter)
            self.y = next(oiter)
            self.z = next(oiter)
        except StopIteration:
            pass
    from_iter = property(None, _set_from_iterable,
                         doc='''
                                set only, vlaue assignment from iterable,
                                if iterable is too short, old values will be
                                left in place, e.g. iterable is [1,2] then
                                only x and y will be set and z remains
                                unmodified''')

    def _length(self):
        """Returns the length of the vector"""
        # x ** 5 is faster than sqrt(x)
        return (self.x * self.x + self.y * self.y + self.z * self.z) ** 0.5
    def _set_length(self, value):
        """Scale the vector to have a given length"""
        self *= (value / self.length)
    length = property(_length, _set_length, doc="""
    get/set the length of vector
    returns length and change the length if set::

        v = Vec3(x, y, z)
        length = v.length
        v.length = 10 # vectors is scaled so length is 10 now

    """)

    def _length_sq(self):
        """Returns the squared length"""
        return self.x * self.x + self.y * self.y + self.z * self.z
    length_sq = property(_length_sq,
                                    doc="""returns length squared of vector""")

    def _normalized(self):
        """Returns a new, normalized vector"""
        leng = self.length
        if leng:
            return self.__class__(self.x / leng, self.y / leng,  self.z / leng)
        else:
            return self.__class__(0, 0, 0)
    normalized = property(_normalized,
                                doc="""returns new `Vec3` with unit length""")

    def _normal_right(self):
        """ not unit length"""
        return self.__class__(self.y, -self.x, self.z)
    normal_right = property(_normal_right,
          doc="""returns the right normal, not unitlength, z is just copied""")

    def _normal_left(self):
        """ not unit length"""
        return self.__class__(-self.y, self.x, self.z)
    normal_left = property(_normal_left,
              doc="""returns left normal, not unit length, z is just copied""")

    def _get_angle(self):
        """returns the angle for the x and y component"""
        return atan2(self.y, self.x) / PI_DIV_180
    angle = property(_get_angle,
                doc="read only, returns angle of vector, 0 is at 3 o'clock")

    # -- repr methods -- #
    def __str__(self):
        return "<%s(%s, %s, %s) at %s>" % (self.__class__.__name__,
                                            self.x,
                                            self.y,
                                            self.z,
                                            hex(id(self)))
    __repr__ = __str__

#    def __str__(self):
#        return u"<%s(%s, %s, %s)>" % (self.__class__.__name__,
#                                      self.x,
#                                      self.y,
#                                      self.z)

    # -- math -- #
    def __add__(self, other):
        """+ opertaor"""
        assert isinstance(other, self.__class__)
        return self.__class__(self.x + other.x,
                              self.y + other.y,
                              self.z + other.z)

    def __iadd__(self, other):
        """+= opertaor"""
        assert isinstance(other, self.__class__)
        self.x += other.x
        self.y += other.y
        self.z += other.z
        return self

    def __sub__(self, other):
        """- opertaor"""
        assert isinstance(other, self.__class__)
        return self.__class__(self.x - other.x,
                              self.y - other.y,
                              self.z - other.z)

    def __isub__(self, other):
        """-= opertaor"""
        assert isinstance(other, self.__class__)
        self.x -= other.x
        self.y -= other.y
        self.z -= other.z
        return self

    def __mul__(self, scalar):
        """\* opertaor with scalar only, for `dot` or `cross` see
        corresponding methods
        """
        return self.__class__(self.x * scalar, self.y * scalar, self.z * scalar)
    __rmul__ = __mul__

    def __imul__(self, scalar):
        """\*= opertaor with a scalar"""
        self.x *= scalar
        self.y *= scalar
        self.z *= scalar
        return self

    def __len__(self):
        """always 3"""
        return 3

    def __div__(self, scalar):
        """/ opertaor, scalar only"""
        return self.__class__(self.x / scalar, self.y / scalar, self.z / scalar)

    def __idiv__(self, scalar):
        """/= opertaor, scalar only"""
        self.x /= scalar
        self.y /= scalar
        self.z /= scalar
        return self

    def __neg__(self):
        """- opertaor, same as -1 \* vec"""
        return self.__class__(-self.x, -self.y, -self.z)

    def __pos__(self):
        return self.__class__(abs(self.x), abs(self.y), abs(self.z))

    # -- comparison -- #
    def __eq__(self, other):
        """ == operator, vectors are equal when components are equal, might
        not make much sense since using a tolerance would be better"""
        if isinstance(other, self.__class__):
            return self.x == other.x and self.y == other.y and self.z == other.z
        return False

    def __ne__(self, other):
        """same as not __eq__"""
        return not self.__eq__(other)

    def __hash__(self):
        return id(self)

    # -- items access -- #
    def __getitem__(self, key):
        """[] operator, slow"""
        return (self.x, self.y, self.z)[key]

    def __iter__(self):
        """returns an iterator"""
        return iter((self.x, self.y, self.z))

    # -- additional methods -- #
    def as_tuple(self):
        """returns a tuple, (x, y, z)"""
        return self.x, self.y, self.z

    def as_xy_tuple(self):
        """returns tuple (x, y), z is supressed"""
        return self.x, self.y

    def round(self, n_digits=3):
        """
        Round values to n_digits.

        :Parameters:
            n_digits : int
                Number of digits after the point.
        """
        self.x = round(self.x, n_digits)
        self.y = round(self.y, n_digits)
        self.z = round(self.z, n_digits)

    def rounded(self, n_digits=3):
        """
        Get a rounded vector.

        :Parameters:
            n_digits : int
                Number of digits after the point.

        :rtype: `Vec3`
        :Returns: `Vec3` rounded to n_digits
        """
        return self.__class__(round(self.x, n_digits),
                              round(self.y, n_digits),
                              round(self.z, n_digits))

    def normalize(self):
        """
        Make the vector unit length. If length is 0 then nothing is done.
        """
        leng = self.length
        if leng:
            self.x /= leng
            self.y /= leng
            self.z /= leng
        return leng

    def clone(self):
        """
        Clone the vector.

        :rtype: `Vec3`
        :Returns: a copy of itself.
        """
        return self.__class__(self.x, self.y, self.z)

    def dot(self, other):
        """
        Dot product.

        :Parameters:
            other : `Vec3`
                Other vector.

        :rtype: float

        :Returns:
            self.dot(other)
        """
        assert isinstance(other, self.__class__)
        return self.x * other.x + self.y * other.y + self.z * other.z

    def cross(self, other):
        """
        Cross product.

        :Parameters:
            other : `Vec3`
                The second vector for the cross product.

        :rtype: `Vec3`
        :Returns: vector resulting from the cross product.
        :Note: a.cross(b) == - b.cross(a)
        """
        assert isinstance(other, self.__class__)
        return self.__class__(self.y * other.z - self.z * other.y,
                              self.z * other.x - self.x * other.z,
                              self.x * other.y - self.y * other.x)

    def project_onto(self, other):
        """
        Project this vector onto another one.

        :Parameters:
            other : `Vec3`
                The other vector to project onto.

        :rtype: `Vec3`
        :Returns: The projected vector.
        """
        assert isinstance(other, self.__class__)
        return self.dot(other) / other.length_sq * other

    def reflect(self, normal):
        """
        Reflects this vector at a normal.

        :Parameters:
            normal : `Vec3`
                normal should be normalized unitlength

        :rtype: `Vec3`

        :Returns:
            Reflected vector.

        """
        assert isinstance(normal, self.__class__)
        return self - 2 * self.dot(normal) * normal

    def reflect_tangent(self, tangent):
        """
        Reflects vector at a tangent.

        :Parameters:
            tangent : `Vec3`
                tangent should be normalized, unitlength

        :rtype: `Vec3`

        :Returns:
            Reflected vector.
        """
        assert isinstance(tangent, self.__class__)
        return 2 * tangent.dot(self) * tangent - self

    def rotated(self, degrees, axis_vec3): # pylint: disable-msg=R0914
        """
        Rotates the vector around given axis and returns a new vector.

        :Parameters:
            degrees : float
                angle in degrees
            axis_vec3 : `Vec3`
                axis to rotate around

        :rtype: `Vec3`

        :Returns: Rotated vector.

        :Note: see: http://www.cprogramming.com/tutorial/3d/rotation.html

        """
        assert isinstance(axis_vec3, self.__class__)
        #// http://www.cprogramming.com/tutorial/3d/rotation.html
        #
        #//tXX + c  tXY + sZ  tXZ - sY  0
        #//tXY-sZ   tYY + c   tYZ + sX  0
        #//tXY + sY tYZ - sX  tZZ + c   0
        #//0        0         0         1
        #
        #//Where c = cos (theta), s = sin (theta), t = 1-cos (theta),
        #  and <X,Y,Z> is the unit vector representing the arbitary axis

        theta = -degrees * PI_DIV_180
        co_ = cos(theta)
        si_ = sin(theta)
        th_ = 1 - co_
        xcoord, ycoord, zcoord = axis_vec3.normalized.as_tuple()
        xy_ = xcoord * ycoord
        yz_ = ycoord * zcoord
        sx_ = si_ * xcoord
        sy_ = si_ * ycoord
        sz_ = si_ * zcoord

        _x_ = self.x
        _y_ = self.y
        _z_ = self.z

        return self.__class__((th_ * xcoord * xcoord + co_) * _x_ + (th_ * xy_    + sz_)          * _y_ + (th_ * xcoord  * zcoord - sy_) * _z_, # pylint: disable-msg=C0301
                              (th_ * xy_    - sz_)          * _x_ + (th_ * ycoord * ycoord + co_) * _y_ + (th_ * yz_     + sx_)          * _z_, # pylint: disable-msg=C0301
                              (th_ * xy_    + sy_)          * _x_ + (th_ * yz_    - sx_)          * _y_ + (th_ * zcoord  * zcoord + co_) * _z_) # pylint: disable-msg=C0301

    def scaled(self, scale):
        """
        Returns a vector scaled to given length.

        :Returns: `Vec3` with lange scale
        """
        scale = scale / self.length
        return self.__class__(scale * self.x, scale * self.y, scale * self.z)

    def get_angle_between(self, other):
        """
        Get angle between this and other vector.

        :Parameters:
            other : `Vec3`
                other vector

        :rtype: float

        :Returns:
            Angle in degrees.
        """
        assert isinstance(other, self.__class__)
        length = self.length
        olen = other.length
        if length and olen:
            return  acos(self.dot(other) / (length * olen)) / PI_DIV_180
        return 0

    def get_full_angle_between(self, other):
        """
        Get angle between this and other vector.

        :Parameters:
            other : `Vec3`
                other vector

        :rtype: float

        :Returns:
            Angle in degrees.
        """
        assert isinstance(other, self.__class__)
        length = self.length
        olen = other.length
        if length and olen:
            return \
                  (atan2(other.y, other.x) - atan2(self.y, self.x)) / PI_DIV_180
        return 0

    def get_distance_sq(self, other):
        """
        Distance squared from this and other point (represented as vector
        from origin).

        :Parameters:
            other : `Vec2`
                The second vector for the distance.
        :rtype: float
        """
        assert isinstance(other, self.__class__)
        delta_x = self.x - other.x
        delta_y = self.y - other.y
        delta_z = self.z - other.z
        return delta_x * delta_x + delta_y * delta_y + delta_z * delta_z

    def get_distance(self, other):
        """
        Distance this and other point (represented as vector from origin).

        :Parameters:
            other : `Vec2`
                The second vector for the distance.
        :rtype: float
        """
        assert isinstance(other, self.__class__)
        delta_x = self.x - other.x
        delta_y = self.y - other.y
        delta_z = self.z - other.z
        # x ** 0.5 is faster than sqrt(x)
        return (delta_x * delta_x + delta_y * delta_y + delta_z * delta_z)**0.5

Vec3.ZERO = Vec3(0.0, 0.0, 0.0)
Vec3.UNIT_X = Vec3(1.0, 0.0, 0.0)
Vec3.UNIT_Y = Vec3(0.0, 1.0, 0.0)
Vec3.UNIT_Z = Vec3(0.0, 0.0, 1.0)



# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------

if __debug__:
    _DELTA = time.time() - _START_TIME
    _LOGGER.debug('%s loaded: %fs \n' % (__name__, _DELTA))
