#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame

from vectors import Vec2 as Vec
import gameentity
from params import params
import mainloop
import baseworld

# ------------------------------------------------------------------------------
class World(baseworld.BaseWorld):

    def init(self):
        self.title = "Steering Behaviors - pursuit"
        self.draw_wander_info_on = True
        
        #setup vehicle
        prey = gameentity.Vehicle(self,
                                     Vec(250, 250),
                                     20,
                                     Vec(0, 0),
                                     params["maxspeed"] * 1.1,
                                     Vec(1, 1),
                                     params["vehiclemass"],
                                     1.0,
                                     params["maxturnratepersecond"],
                                     params["steeringforce"])
        prey.steering.behavior_wander.activate()
        prey.color = (255, 0, 255)
        self.vehicles.append(prey)
        
        predator = gameentity.Vehicle(self,
                                     Vec(100, 100),
                                     20,
                                     Vec(0, 0),
                                     params["maxspeed"],
                                     Vec(1, 1),
                                     params["vehiclemass"],
                                     params["vehiclescale"],
                                     params["maxturnratepersecond"] / 10,
                                     params["steeringforce"])
        predator.steering.behavior_pursuit.activate(prey)
        self.vehicles.append(predator)
        self.draw_wander_info_on = False
        
    def update(self, dt):
        for vehicle in self.vehicles:
            vehicle.update(dt)
        self.crosshair = self.vehicles[1].steering.targeted_pursuit_position
        self.check_keys(dt)
        
    def refresh_text(self):
        self.text_to_draw = [ (410, 477, "'R' to reset"),
                              (10, 10, "Use turn arround time (Space): " + str(self.vehicles[1].steering.behavior_pursuit.turn_around_coefficient != 0)),
                              ]

    def on_event(self, event):
        # this pollutes the world class with pygame
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                if self.vehicles[1].steering.behavior_pursuit.turn_around_coefficient != 0:
                    self.vehicles[1].steering.behavior_pursuit.turn_around_coefficient = 0
                else:
                    self.vehicles[1].steering.behavior_pursuit.turn_around_coefficient = -0.5
        self.refresh_text()
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
if __name__ == "__main__":
    mainloop.MainLoop(World).run()

