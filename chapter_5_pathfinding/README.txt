
This is about path finding algorithms and the graph implementation.

This application was written closely to the C++ original and might not be
that pythonic as it would be if written from scratch in python.

The implementations of the algorithm are not optimal to memory usage, since each 
run allocates as much memory as the graph itself if not even more. Therefore 
this implementation might not be usable for large graphs with many nodes. Probably
dictionaries could be used instead of the lists.

For simpler re-use graph.py contains all the path finding stuff.


The GUI is just a very simple implementation hacked together for this implementation.
Due to this the gui is only accessible by the mouse.

Run 'demo_pathfinder.py' and use the mouse for the fun.






