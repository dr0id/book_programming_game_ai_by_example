#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import pygame


from . import gui
from . import constants as consts
from . import pathfinderapp
from . import graph

# ------------------------------------------------------------------------------


class App(object):

    def __init__(self):
        self.running = True
        self.screen = None
        self.tile_area = None
        self.fill_color = (255, 0, 255)
        self.pathfinder = pathfinderapp.Pathfinder()
        self._map_extension = "*.map"
        self.caption = "Pathfinder"

        # gui, created in init()
        self.gui_root = None
        self.brush_bar = None
        self.search_bar = None
        self.status_bar = None
        self.tile_toggle_menu = None
        self.graph_toggle_menu = None
        
        # resources
        self.image_path = os.path.join(os.path.dirname(__file__), "images")
        self.map_path = os.path.join(os.path.dirname(__file__), "maps")

    def init_pygame(self):
        # TODO: make sure the window appears centered on screen
        # os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (x,y)
        os.environ['SDL_VIDEO_WINDOW_POS'] = 'center'
        pygame.init()
        pygame.display.set_caption(self.caption + " - New")
        icon = pygame.image.load(os.path.join(self.image_path, "icon1.bmp"))
        pygame.display.set_icon(icon)
        self.screen = pygame.display.set_mode((consts.screen_w, consts.screen_h))
        self.tile_area = pygame.Surface((consts.client_w, consts.client_h))

    def on_print(self, item):
        print('menu clicked')

    def init(self):
        self.init_pygame()
        self.init_gui()
        self.pathfinder.create_graph(consts.num_cells_x, consts.num_cells_y)


    def init_gui(self):
        self.gui_root = gui.GuiRoot((consts.screen_w, consts.screen_h))

        menu_bar = gui.MenuBar((0, 0), (consts.screen_w, consts.menu_bar_height))
        self.gui_root.append(menu_bar)

        file_menu = gui.MenuItem("File")
        file_menu.append(gui.MenuItem("Save as", self.on_save))
        file_menu.append(gui.MenuItem("Load", self.on_load))
        file_menu.append(gui.MenuItem("New", self.on_menu_new))
        file_menu.append(gui.MenuItem("Exit", self.on_exit))
        menu_bar.append(file_menu)

        view_menu = gui.MenuItem("View")
        self.graph_toggle_menu = gui.CheckableMenuItem("Graph", self.on_show_graph, False)
        view_menu.append(self.graph_toggle_menu)
        self.labels_toggle_menu = gui.CheckableMenuItem("Labels", self.on_show_labels, False)
        view_menu.append(self.labels_toggle_menu)
        self.tile_toggle_menu = gui.CheckableMenuItem("Tiles", self.on_show_tiles, True)
        view_menu.append(self.tile_toggle_menu)
        menu_bar.append(view_menu)

        # toolbars and its buttons
        # image_name = os.path.join(os.getcwd(), "pathfinder", "toolbar1.bmp")
        image_name = os.path.join(self.image_path, "toolbar1.bmp")
        toolbar_image = pygame.image.load(image_name)
        image_size = 16

        # brush types toolbar
        self.brush_bar = gui.Bar((0, consts.screen_h - consts.tool_bar_height), (consts.screen_w, consts.tool_bar_height))
        self.gui_root.append(self.brush_bar)

        brush_coords = (
                        (pathfinderapp.BrushType.TARGET,  (0 * image_size, 0, image_size, image_size)),
                        (pathfinderapp.BrushType.SOURCE,  (1 * image_size, 0, image_size, image_size)),
                        (pathfinderapp.BrushType.OBSTACLE,(2 * image_size, 0, image_size, image_size)),
                        (pathfinderapp.BrushType.MUD,     (3 * image_size, 0, image_size, image_size)),
                        (pathfinderapp.BrushType.WATER,   (4 * image_size, 0, image_size, image_size)),
                        (pathfinderapp.BrushType.NORMAL,  (5 * image_size, 0, image_size, image_size)),
                        )

        for id, coords in brush_coords:
            img = pygame.Surface((image_size, image_size - 1))
            img.blit(toolbar_image, (0, 0), pygame.Rect(coords))
            button = gui.ImageToggleButton((0, 0), img, self.on_brush_button_pressed)
            button.tag = id
            self.brush_bar.append(button)

        # search algorithms toolbar
        self.search_bar = gui.Bar((consts.client_w - 4 * 23 - 4, consts.screen_h - consts.tool_bar_height), (consts.screen_w, consts.tool_bar_height))
        self.gui_root.append(self.search_bar)

        search_coords = (
                        (pathfinderapp.AlgorithmType.SEARCH_DFS,      ( 6 * image_size, 0, image_size, image_size)),
                        (pathfinderapp.AlgorithmType.SEARCH_BFS,      ( 7 * image_size, 0, image_size, image_size)),
                        (pathfinderapp.AlgorithmType.SEARCH_DIJKSTRA, ( 8 * image_size, 0, image_size, image_size)),
                        (pathfinderapp.AlgorithmType.SEARCH_ASTAR,    ( 9 * image_size, 0, image_size, image_size)),
                        # (pathfinderapp.AlgorithmType.NONE,            (10 * image_size, 0, image_size, image_size)),
                        )

        for id, coords in search_coords:
            img = pygame.Surface((image_size, image_size - 1))
            img.blit(toolbar_image, (0, 0), pygame.Rect(coords))
            button = gui.ImageToggleButton((0, 0), img, self.on_search_button_pressed)
            button.tag = id
            self.search_bar.append(button)

        self.status_bar = gui.StatusBar((0, consts.screen_h - consts.tool_bar_height - consts.info_window_height), (consts.screen_w, consts.info_window_height))

        self.gui_root.append(self.status_bar)

        self.gui_root.init()
        
        gui.MessageBox("Info", "Use the mouse.").show()
        

    def run(self):

        self.init()

        while self.running:
            self.check_events()
            self.update()
            self.draw()

        pygame.quit()
        sys.exit()

    def check_events(self):
        events = [pygame.event.wait()]
        for event in events:

            if gui.HANDLED == self.gui_root.handle_event(event):
                continue

            if event.type == pygame.QUIT:
                self.running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self.running = False
                elif event.key == pygame.K_t:
                    self.pathfinder.show_tiles = not self.pathfinder.show_tiles
                    self.tile_toggle_menu.is_checked = self.pathfinder.show_tiles
                elif event.key == pygame.K_g:
                    self.pathfinder.show_graph = not self.pathfinder.show_graph
                    self.graph_toggle_menu.is_checked = self.pathfinder.show_graph
                elif event.key == pygame.K_l:
                    self.pathfinder.show_labels = not self.pathfinder.show_labels
                    self.labels_toggle_menu.is_checked = self.pathfinder.show_labels

            elif event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1: # left mouse button
                    x, y = event.pos
                    self.pathfinder.paint_terrain(x, y - consts.menu_bar_height)
            elif event.type == pygame.MOUSEMOTION:
                if event.buttons[0] == 1:
                    x, y = event.pos
                    self.pathfinder.paint_terrain(x, y - consts.menu_bar_height)

    def update(self):
        pass

    def draw(self):
        self.screen.fill(self.fill_color)
        # graph and tile stuff
        self.tile_area.fill(self.fill_color)
        self.pathfinder.draw(self.tile_area)
        self.screen.blit(self.tile_area, (0, consts.menu_bar_height))

        # if self.time_taken > 0:
        text = "Time elapsed for {0} is {1} ms      Cost is {2}".format(self.pathfinder.get_name_of_current_search_algorithm(), self.pathfinder.time_taken, self.pathfinder.cost_to_target)
        self.status_bar.set_text(text)

        # gui
        self.gui_root.draw(self.screen)

        pygame.display.flip()

    def on_exit(self, sender):
        self.running = False

    def on_menu_new(self, sender):
        pygame.display.set_caption(self.caption + " - New")
        self.pathfinder.create_graph(consts.num_cells_x, consts.num_cells_y)
        # uncheck any buttons for search
        for button in self.search_bar.children:
            button.reset()

    def on_brush_button_pressed(self, sender):
        for button in self.brush_bar.children:
            if button != sender:
                button.reset()
        self.pathfinder.change_brush(sender.tag)

    def on_search_button_pressed(self, sender):
        for button in self.search_bar.children:
            if button != sender:
                button.reset()
        if sender.tag == pathfinderapp.AlgorithmType.NONE:
            pass
        elif sender.tag == pathfinderapp.AlgorithmType.SEARCH_ASTAR:
            self.pathfinder.create_path_astar()
        elif sender.tag == pathfinderapp.AlgorithmType.SEARCH_BFS:
            self.pathfinder.create_path_BFS()
        elif sender.tag == pathfinderapp.AlgorithmType.SEARCH_DFS:
            self.pathfinder.create_path_DFS()
        elif sender.tag == pathfinderapp.AlgorithmType.SEARCH_DIJKSTRA:
            self.pathfinder.create_path_dijkstra()

    def on_show_tiles(self, sender):
        self.pathfinder.show_tiles = sender.is_checked

    def on_show_graph(self, sender):
        self.pathfinder.show_graph = sender.is_checked

    def on_show_labels(self, sender):
        self.pathfinder.show_labels = sender.is_checked

    def on_load(self, sender):
        open_dialog = gui.OpenDialog("Open file", self.map_path, self.on_load_file, self._map_extension)
        open_dialog.show()

    def on_load_file(self, sender):
        try:
            self.pathfinder.load(sender.filename)
            pygame.display.set_caption(self.caption + " - " + os.path.basename(sender.filename))
        except Exception as ex:
            print(('!!! ERROR loading:', ex))
            import traceback
            traceback.print_exc()
            msgb = gui.MessageBox("Error loading", str(ex))
            msgb.show()

    def on_save(self, sender):
        save_dialog = gui.SaveDialog("Save file", self.map_path, self.on_save_file, self._map_extension)
        save_dialog.show()

    def on_save_file(self, sender):
        try:
            self.pathfinder.save(sender.filename)
            pygame.display.set_caption(self.caption + " - " + os.path.basename(sender.filename))
        except Exception as ex:
            print(('!!! ERROR saving:', str(ex)))
            msgbox = gui.MessageBox("Error saving", str(ex))
            msgbox.show()

# ------------------------------------------------------------------------------
if __name__ == "__main__":
    App().run()

