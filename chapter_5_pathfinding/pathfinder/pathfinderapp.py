# -*- coding: utf-8 -*-

from math import hypot as math_hypot

import pygame

from . import constants
from . import graph


class BrushType(object):
    NORMAL = 0
    OBSTACLE = 1
    WATER = 2
    MUD = 3
    SOURCE = 4
    TARGET = 5


class AlgorithmType:
    NONE = 0
    SEARCH_ASTAR = 1
    SEARCH_BFS = 2
    SEARCH_DFS = 3
    SEARCH_DIJKSTRA = 4


class Pathfinder(object):

    def __init__(self):
        self.terrain_type = []
        self.path = []
        self.graph = None
        self.sub_tree = []
        self.cost_to_target = 0.0
        self.current_algorithm = AlgorithmType.NONE
        self.current_terrain_brush = BrushType.NORMAL

        self.cell_width = 0
        self.cell_height = 0

        self.num_cells_x = 0
        self.num_cells_y = 0

        self.source_cell = 0
        self.target_cell = 0

        self.has_start = False
        self.has_finish = False

        self.show_graph = False
        self.show_labels = False
        self.show_tiles = True

        self.time_taken = 0.0

    # //this calls the appropriate algorithm
    def update_algorithm(self):
        if self.current_algorithm == AlgorithmType.NONE:
            pass
        elif self.current_algorithm == AlgorithmType.SEARCH_ASTAR:
            self.create_path_astar()
        elif self.current_algorithm == AlgorithmType.SEARCH_BFS:
            self.create_path_BFS()
        elif self.current_algorithm == AlgorithmType.SEARCH_DFS:
            self.create_path_DFS()
        elif self.current_algorithm == AlgorithmType.SEARCH_DIJKSTRA:
            self.create_path_dijkstra()

    # //helper function for PaintTerrain (see below)
    def update_graph_from_brush(self, brush, cell_index):
        """
        //  given a brush and a node index, this method updates the graph appropriately
        //  (by removing/adding nodes or changing the costs of the node's edges)
        """
        # //set the terrain type in the terrain index
        self.terrain_type[cell_index] = brush

        # //if current brush is an obstacle then this node must be removed
        # //from the graph
        if brush == BrushType.OBSTACLE:
            self.graph.remove_node(cell_index)
        else:
            # //make the node active again if it is currently inactive
            if not self.graph.is_node_present(cell_index):
                y = cell_index // self.num_cells_y
                x = cell_index - (y * self.num_cells_y)

                node = graph.NavGraphNode(x * self.cell_width + self.cell_width / 2.0,
                                          y * self.cell_height + self.cell_height / 2.0, cell_index)
                self.graph.add_node(node)
                self.helper_add_all_neighbours_to_grid_node(self.graph, y, x, self.num_cells_x, self.num_cells_y)

            # //set the edge costs in the graph
            self.weight_nav_graph_node_edges(self.graph, cell_index, self.get_terrain_cost(brush))

    def weight_nav_graph_node_edges(self, _graph, node_idx, weight):
        # make sure the node is present
        assert node_idx < self.graph.num_nodes(), "node index invalid"

        # set the cost for each edge
        for edge in self.graph.edges[node_idx]:
            # calculate the distance between nodes
            node_from = self.graph.nodes[edge.idx_from]
            node_to = self.graph.nodes[edge.idx_to]
            dist = math_hypot(node_to.position_x - node_from.position_x, node_to.position_y - node_from.position_y)

            # set edge cost
            self.graph.set_edge_cost(edge.idx_from, edge.idx_to, dist * weight)

            # if not a digraph, set the cost of the parallel edge to the same
            if not self.graph.is_digraph:
                self.graph.set_edge_cost(edge.idx_to, edge.idx_from, dist * weight)

    def get_name_of_current_search_algorithm(self):
        names = {
            AlgorithmType.NONE: "",
            AlgorithmType.SEARCH_ASTAR: "A Star",
            AlgorithmType.SEARCH_BFS: "Breadth First",
            AlgorithmType.SEARCH_DFS: "Depth First",
            AlgorithmType.SEARCH_DIJKSTRA: "Dijkstra",
        }
        return names[self.current_algorithm] if self.current_algorithm in names else "UNKNOWN!"

    def create_graph(self, cells_across, cells_up):
        # TODO: use array class here?
        self.terrain_type = [BrushType.NORMAL] * cells_up * cells_across
        self.num_cells_x = cells_across
        self.num_cells_y = cells_up
        self.cell_width = constants.client_w // cells_across
        self.cell_height = constants.client_h // cells_up

        # //create the graph
        # del self.graph
        # self.graph = NavGraph(False)
        self.graph = graph.SparseGraph(graph.NavGraphNode, graph.NavGraphEdge, False)

        self.helper_create_grid(self.graph, constants.client_w, constants.client_h, self.num_cells_x, self.num_cells_y)

        # //initialize source and target indexes to mid top and bottom of grid
        self.target_cell = self.point_to_index(constants.client_w // 2, self.cell_height * 1)
        self.source_cell = self.point_to_index(constants.client_w // 2, constants.client_h - self.cell_height * 3)

        self.path = []
        self.sub_tree = []

        self.current_algorithm = AlgorithmType.NONE

        self.time_taken = 0.0

    def helper_create_grid(self, _graph, cy_size, cx_size, num_cells_x, num_cells_y):
        """
        //  creates a graph based on a grid layout. This function requires the
        //  dimensions of the environment and the number of cells required horizontally
        //  and vertically
        """
        cell_w = 1.0 * cx_size / num_cells_x
        cell_h = 1.0 * cy_size / num_cells_y

        # assert cell_w == constants.cell_size, "cell_w '{0}' is not the same as '{1}'".format(cell_w, constants.cell_size)
        # assert cell_h == constants.cell_size, "cell_h '{0}' is not the same as '{1}'".format(cell_h, constants.cell_size)

        mid_x = cell_w / 2.0
        mid_y = cell_h / 2.0

        for row in range(num_cells_y):
            for col in range(num_cells_x):
                n = graph.NavGraphNode(mid_x + col * cell_w, mid_y + row * cell_h, _graph.get_next_free_node_index())
                _graph.add_node(n)

        # //now to calculate the edges. (A position in a 2d array [x][y] is the
        # //same as [y*NumCellsX + x] in a 1d array). Each cell has up to eight
        # //neighbours.
        for row in range(num_cells_y):
            for col in range(num_cells_x):
                self.helper_add_all_neighbours_to_grid_node(_graph, row, col, num_cells_x, num_cells_y)

    def helper_add_all_neighbours_to_grid_node(self, _graph, row, col, num_cells_x, num_cells_y):
        for j, i in [(-1, -1), (0, -1), (1, -1), (-1, 0), (1, 0), (-1, 1), (0, 1), (1, 1)]:
            node_x = col + j
            node_y = row + i

            if self.is_valid_neighbour(node_x, node_y, num_cells_x, num_cells_y):
                node_pos_x, node_pos_y = _graph.get_node(row * num_cells_x + col).position
                neighbour_pos_x, neighbour_pos_y = _graph.get_node(node_y * num_cells_x + node_x).position

                # in this app this is 2D, otherwise the more general distance calculation should be used
                dist = math_hypot(neighbour_pos_x - node_pos_x, neighbour_pos_y - node_pos_y)

                new_edge = graph.NavGraphEdge(row * num_cells_x + col, node_y * num_cells_x + node_x, dist)

                _graph.add_edge(new_edge)

                # if graph is not a di-graph then an edge needs to be added going
                # in the other direction
                if not _graph.is_digraph:
                    new_edge = graph.NavGraphEdge(node_y * num_cells_x + node_x, row * num_cells_x + col, dist)
                    _graph.add_edge(new_edge)

    def is_valid_neighbour(self, node_x, node_y, num_cells_x, num_cells_y):
        return not (node_x < 0 or node_x >= num_cells_x or node_y < 0 or node_y >= num_cells_y)

    # //this will paint whatever cell the cursor is currently over in the
    # //currently selected terrain brush
    def paint_terrain(self, point_x, point_y):
        # //convert p to an index into the graph
        x = point_x // self.cell_width
        y = point_y // self.cell_height

        # //make sure the values are legal
        if x > self.num_cells_x or y > self.num_cells_y - 1:
            return

        # //reset path and tree records
        self.sub_tree = []
        self.path = []

        cell_index = y * self.num_cells_x + x

        # //if the current terrain brush is set to either source or target we
        # //should change the appropriate node
        if self.current_terrain_brush == BrushType.SOURCE:
            self.source_cell = cell_index
        elif self.current_terrain_brush == BrushType.TARGET:
            self.target_cell = cell_index
        else:
            # //otherwise, change the terrain at the current mouse position
            self.update_graph_from_brush(self.current_terrain_brush, cell_index)

        # //update any currently selected algorithm
        self.update_algorithm()

    # the algorithms
    def create_path_DFS(self):
        """
        //  uses DFS to find a path between the start and target cells.
        //  Stores the path as a series of node indexes in m_Path.
        """
        # //set current algorithm
        self.current_algorithm = AlgorithmType.SEARCH_DFS

        # clear any existing path
        self.path = []
        self.sub_tree = []

        # clear and start timer
        start_time = pygame.time.get_ticks()

        # do the search
        dfs = graph.GraphSearchDFS(self.graph, self.source_cell, self.target_cell)

        # record the time taken
        self.time_taken = pygame.time.get_ticks() - start_time  # ms

        # now grab the path (if one has been found)
        self.path = dfs.get_path_to_target()

        self.sub_tree = dfs.get_search_tree()

        self.cost_to_target = 0.0

    def create_path_BFS(self):
        """
        //  uses BFS to find a path between the start and target cells.
        //  Stores the path as a series of node indexes in m_Path.
        """
        # //set current algorithm
        self.current_algorithm = AlgorithmType.SEARCH_BFS

        # clear any existing path
        self.path = []
        self.sub_tree = []

        # clear and start timer
        start_time = pygame.time.get_ticks()

        # do the search
        bfs = graph.GraphSearchBFS(self.graph, self.source_cell, self.target_cell)

        # record the time taken
        self.time_taken = pygame.time.get_ticks() - start_time  # ms

        # now grab the path (if one has been found)
        self.path = bfs.get_path_to_target()

        self.sub_tree = bfs.get_search_tree()

        self.cost_to_target = 0.0

    def create_path_dijkstra(self):
        """
        //  creates a path from m_iSourceCell to m_iTargetCell using Dijkstra's algorithm
        """
        # //set current algorithm
        self.current_algorithm = AlgorithmType.SEARCH_DIJKSTRA

        # clear any existing path
        self.path = []
        self.sub_tree = []

        # clear and start timer
        start_time = pygame.time.get_ticks()

        # do the search
        djk = graph.GraphSearchDijkstra(self.graph, self.source_cell, self.target_cell)
        # self.graph = self.create_dikdstra_book_graph()
        # djk = graph.GraphSearchDijkstra(self.graph, 5, 3)

        # record the time taken
        self.time_taken = pygame.time.get_ticks() - start_time  # ms

        # now grab the path (if one has been found)
        self.path = djk.get_path_to_target()

        self.sub_tree = djk.get_SPT()

        self.cost_to_target = djk.get_cost_to_target()

    def create_dikdstra_book_graph(self):
        g = graph.SparseGraph(graph.NavGraphNode, graph.NavGraphEdge, True)
        g.add_node(graph.NavGraphNode(0, 0, g.get_next_free_node_index()))
        g.add_node(graph.NavGraphNode(200, 400, g.get_next_free_node_index()))
        g.add_node(graph.NavGraphNode(200, 200, g.get_next_free_node_index()))
        g.add_node(graph.NavGraphNode(300, 250, g.get_next_free_node_index()))
        g.add_node(graph.NavGraphNode(300, 350, g.get_next_free_node_index()))
        g.add_node(graph.NavGraphNode(200, 300, g.get_next_free_node_index()))
        g.add_node(graph.NavGraphNode(250, 400, g.get_next_free_node_index()))

        g.add_edge(graph.NavGraphEdge(1, 5, 2.9))
        g.add_edge(graph.NavGraphEdge(1, 6, 1.0))
        g.add_edge(graph.NavGraphEdge(2, 3, 3.1))
        g.add_edge(graph.NavGraphEdge(3, 5, 0.8))
        g.add_edge(graph.NavGraphEdge(4, 3, 3.7))
        g.add_edge(graph.NavGraphEdge(5, 2, 1.9))
        g.add_edge(graph.NavGraphEdge(5, 6, 3.0))
        g.add_edge(graph.NavGraphEdge(6, 4, 1.1))

        print('graph created:')
        print(('nodes: ', [str(n) for n in g.nodes]))
        print('edges: ')
        for edges in g.edges:
            print([str(e) for e in edges])

        return g

    def create_path_astar(self):
        """
        //  creates a path from m_iSourceCell to m_iTargetCell
        //  using AStar's algorithm using the euclidian heuristic
        """
        # //set current algorithm
        self.current_algorithm = AlgorithmType.SEARCH_ASTAR

        # clear any existing path
        self.path = []
        self.sub_tree = []

        # clear and start timer
        start_time = pygame.time.get_ticks()

        # do the search
        # //create an instance of the A* search using the Euclidean heuristic
        ast = graph.GraphSearchAStar(self.graph, self.source_cell, self.target_cell, graph.HeuristicEuclidian)

        # record the time taken
        self.time_taken = pygame.time.get_ticks() - start_time  # ms

        # now grab the path (if one has been found)
        self.path = ast.get_path_to_target()

        self.sub_tree = ast.get_SPT()

        self.cost_to_target = ast.get_cost_to_target()

    def min_spanning_tree(self):
        raise NotImplementedError()

    # helpers
    def change_brush(self, new_brush):
        self.current_terrain_brush = new_brush

    def change_source(self, cell):
        self.source_cell = cell

    def change_target(self, cell):
        self.target_cell = cell

    def point_to_index(self, point_x, point_y):
        """
        converts a POINTS into an index into the graph
        """
        x = int(1.0 * point_x / self.cell_width)
        y = int(1.0 * point_y / self.cell_height)

        if x > self.num_cells_x or y > self.num_cells_y:
            return graph.INVALID_NODE_INDEX
        return y * self.num_cells_x + x

    def get_terrain_cost(self, brush):
        cost = {
            BrushType.NORMAL: 1.0,
            BrushType.OBSTACLE: -1,
            BrushType.WATER: 3.0,
            BrushType.MUD: 2.0,
            BrushType.SOURCE: 0,
            BrushType.TARGET: -1,
        }
        # return cost.get(brush, -11)
        return cost[brush]

    def save(self, file_name):
        # file_ = open(file_name, 'wb')
        with open(file_name, 'w') as file_:
            # file_.writelines(map(str, [self.num_cells_x, self.num_cells_y]))
            # file_.writelines(["{0}\n".format(x) for x in self.terrain_type])
            file_.write(str(self.num_cells_x))
            file_.write("\n")
            file_.write(str(self.num_cells_y))
            file_.write("\n")
            for idx, terrain in enumerate(self.terrain_type):
                if idx == self.source_cell:
                    file_.write(str(BrushType.SOURCE))
                elif idx == self.target_cell:
                    file_.write(str(BrushType.TARGET))
                else:
                    file_.write(str(terrain))
                file_.write("\n")

    def load(self, file_name):
        file_ = open(file_name, 'r')
        lines = file_.readlines()

        # convert to integers
        lines = list(map(int, lines))

        # load the size of the grid
        self.num_cells_x = lines[0]
        self.num_cells_y = lines[1]

        # create a graph to the correct size
        self.create_graph(self.num_cells_x, self.num_cells_y)

        # load the terrain
        for idx, terrain in enumerate(lines[2:]):
            if terrain == BrushType.SOURCE:
                self.source_cell = idx
            elif terrain == BrushType.TARGET:
                self.target_cell = idx
            else:
                self.terrain_type[idx] = terrain
                self.update_graph_from_brush(terrain, idx)

    def draw(self, screen):
        self.draw_tiles(screen)
        self.draw_graph(screen)
        self.draw_paths(screen)

    def draw_tiles(self, screen):
        for node_index in range(len(self.graph.nodes)):

            x, y = self.graph.nodes[node_index].position
            rect = pygame.Rect(x - self.cell_width / 2, y - self.cell_height / 2, self.cell_width, self.cell_height)

            colors = {
                BrushType.NORMAL: (255, 255, 255),
                BrushType.OBSTACLE: (0, 0, 0),
                BrushType.WATER: (0, 255, 255),
                BrushType.MUD: (128, 0, 0),
                BrushType.SOURCE: (0, 255, 0),
                BrushType.TARGET: (255, 0, 0),
            }

            border = constants.cell_size // 4
            size = border * 3
            if node_index == self.source_cell:
                color = colors[BrushType.SOURCE]
                screen.fill(color, rect)
                # draw a rect
                pygame.draw.rect(screen, (0, 0, 0), rect.inflate(-size, -size), 2)
            elif node_index == self.target_cell:
                color = colors[BrushType.TARGET]
                screen.fill(color, rect)
                # draw cross
                pygame.draw.aaline(screen, (0, 0, 0), (rect.left + border, rect.top + border),
                                   (rect.right - border, rect.bottom - border), 1)
                pygame.draw.aaline(screen, (0, 0, 0), (rect.right - border, rect.top + border),
                                   (rect.left + border, rect.bottom - border), 1)
            else:
                # color = colors.get(self.terrain_type[node_index], (255, 255, 0))
                color = colors[self.terrain_type[node_index]]
                screen.fill(color, rect)
            screen.set_at(rect.topleft, (0, 0, 0, 0))
            screen.set_at(rect.topright, (0, 0, 0, 0))
            screen.set_at(rect.bottomleft, (0, 0, 0, 0))
            screen.set_at(rect.bottomright, (0, 0, 0, 0))

        if self.show_tiles:
            for x in range(constants.num_cells_x):
                pygame.draw.aaline(screen, (200, 200, 200, 128), (x * self.cell_width, 0),
                                   (x * self.cell_width, constants.client_h), 1)

            for y in range(constants.num_cells_y):
                pygame.draw.aaline(screen, (200, 200, 200, 128), (0, y * self.cell_height),
                                   (constants.client_w, y * self.cell_height), 1)

    def draw_graph(self, screen):
        if self.show_graph:
            # nothing to paint if there are no nodes
            if self.graph.num_nodes() == 0:
                return
            # draw nodes
            draw_circle = pygame.draw.circle
            draw_line = pygame.draw.line
            color = (200, 200, 200)
            font = pygame.font.Font(None, 12)
            for node in self.graph.nodes:
                if node.index != graph.INVALID_NODE_INDEX:
                    cx, cy = node.position
                    draw_circle(screen, color, (int(cx), int(cy)), 3)

                    for edge in self.graph.edges[node.index]:
                        sx, sy = node.position
                        ex, ey = self.graph.get_node(edge.idx_to).position
                        draw_line(screen, color, (sx, sy), (ex, ey), 1)

            if self.show_labels:
                for node in self.graph.nodes:
                    if node.index != graph.INVALID_NODE_INDEX:
                        text = font.render(str(node.index), 1, (0, 0, 0))
                        screen.blit(text, text.get_rect(center=node.position))

    def draw_paths(self, screen):
        draw_line = pygame.draw.line
        red = (255, 0, 0)
        blue = (0, 0, 255)
        for edge in self.sub_tree:
            if edge:
                from_x, from_y = self.graph.get_node(edge.idx_from).position
                to_x, to_y = self.graph.get_node(edge.idx_to).position
                draw_line(screen, red, (from_x, from_y), (to_x, to_y), 1)

        if len(self.path) > 1:
            # it = self.path[0]
            # for nxt in self.path[1:]:
            # draw_line(screen, blue, self.graph.get_node(it).position, self.graph.get_node(nxt).position, 2)
            # it = nxt
            pygame.draw.lines(screen, blue, 0, [self.graph.get_node(idx).position for idx in self.path], 2)
