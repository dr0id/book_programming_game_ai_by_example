# -*- coding: utf-8 -*-


import basegameentity
import locations
from minerownedstates import GoHomeAndSleepTilRested

#-------------------------------------------------------------------------------

# above this value a miner is thirsty
thirst_level = 5
# the amount of nuggets a miner can carry
max_nuggets = 3
# above this value a miner is sleepy
tiredness_threshold = 5
# the amount of gold a miner must have before he feels comfortable
comfort_level = 5

#-------------------------------------------------------------------------------


class Miner(basegameentity.BaseGameEntity):
    """
    The miner class implemented as a state machine.
    """

    def __init__(self, id):
        super(Miner, self).__init__(id)
        self._location = locations.SHACK

        self._gold_carried = 0
        self._money_in_bank = 0
        self._thirst = 0
        self._fatigue = 0
        
        self.current_state = None

        self.change_state(GoHomeAndSleepTilRested)
        
    def change_state(self, new_state):
        """
        Change to the next state. First exit is called on the current state,
        then the new state is made the current state and finally enter is called
        on the new state.
        
        :Parameters:
            new_state : object
                Can be any object that has following methods: enter(sm) and 
                exit(sm), can not be None (use a end state instead).
        
        """

        assert new_state is not None
        assert hasattr(new_state, "enter")
        assert hasattr(getattr(new_state, "enter"), '__call__')
        assert hasattr(new_state, "exit")
        assert hasattr(getattr(new_state, "exit"), '__call__')

        if self.current_state is not None:
            self.current_state.exit(self)
        self.current_state = new_state
        self.current_state.enter(self)

    def update(self):
        self._thirst += 1
        self.current_state.execute(self)
        
    def dump(self):
        print(self._gold_carried, self._money_in_bank, self._thirst, \
        self._fatigue, self.location, self.current_state.__name__)

    # location
    def _get_location(self):
        return self._location
    def _set_location(self, new_loc):
        self._location = new_loc
    location = property(_get_location, _set_location)

    # gold carried
    def add_to_gold_carried(self, amount):
        self._gold_carried += amount
    def _get_gold_carried(self):
        return self._gold_carried
    def _set_gold_carried(self, val):
        self._gold_carried = val
    gold_carried = property(_get_gold_carried, _set_gold_carried)
    def pockets_full(self):
        return self._gold_carried >= max_nuggets

    # fatigue
    def _get_fatigue(self):
        return self._fatigue
    fatigue = property(_get_fatigue)
    def increase_fatigue(self):
        self._fatigue += 1
    def decrease_fatigue(self):
        self._fatigue -= 1
    def is_fatigued(self):
        return self.fatigue > tiredness_threshold

    # thirst
    def is_thirsty(self):
        return self._thirst >= thirst_level
    def buy_and_drink_a_whisky(self):
        self._thirst = 0
        self._money_in_bank -= 2

    # wealth
    def _get_wealth(self):
        return self._money_in_bank
    def _set_wealth(self, val):
        self._money_in_bank = val
    wealth = property(_get_wealth, _set_wealth)
    def add_to_wealth(self, val):
        self._money_in_bank += val
    def is_wealthy(self):
        return self.wealth >= comfort_level
#-------------------------------------------------------------------------------



