# -*- coding: utf-8 -*-


import random
import time

import messagedispatcher
import entitynames
import messagetypes
import locations
from state import State


#-------------------------------------------------------------------------------

class WifesGlobalState(State):

    @staticmethod
    def enter(wife):
        pass

    @staticmethod
    def exit(wife):
        pass

    @staticmethod
    def execute(wife):
        if random.random() < 0.1 and not wife.statemachine.is_in_state(VisitBathroom):
            wife.statemachine.change_state(VisitBathroom)
        
    @staticmethod
    def on_message(wife, message):
        if message.message == messagetypes.msg_hi_honey_im_home:
            print("Message handled by %s at time: %s" % (wife.ID, time.time()))
            print("%s: Hi honey. Let me make you some of mah fine country stew" % wife.ID)
            wife.statemachine.change_state(CookStew)
            return True
        return False


#-------------------------------------------------------------------------------

class DoHouseWork(State):

    @staticmethod
    def enter(wife):
        print("%s: Time to do some more housework!" % wife.ID)

    @staticmethod
    def exit(wife):
        pass

    @staticmethod
    def execute(wife):
        doing = random.randint(0,3)
        if doing == 0:
            print("%s: Moppin' the floor" % wife.ID)
        elif doing == 1:
            print("%s: Washin' the dishes" % wife.ID)
        elif doing == 2:
            print("%s: Makin' the bed" % wife.ID)
        
    @staticmethod
    def on_message(wife, message):
        return False

#-------------------------------------------------------------------------------

class VisitBathroom(State):

    @staticmethod
    def enter(wife):
        print("%s: Walkin' to the can. Need to powda mah pretty li'lle nose" % wife.ID)

    @staticmethod
    def exit(wife):
        print("%s: Leavin' the Jon" % wife.ID)

    @staticmethod
    def execute(wife):
        print("%s: Ahhhhhh! Sweet relief!" % wife.ID)
        wife.statemachine.revert_to_previous_state()
        
    @staticmethod
    def on_message(wife, message):
        return False

#-------------------------------------------------------------------------------

class CookStew(State):

    @staticmethod
    def enter(wife):
        if not wife.is_cooking():
            print("%s: Putting the stew in the oven" % wife.ID)
            messagedispatcher.dispatch_message(1.5, 
                                               wife.ID, 
                                               wife.ID, 
                                               messagetypes.msg_stew_ready)
            wife.set_cooking(True)
                                               

    @staticmethod
    def exit(wife):
        print("%s: Puttin' the stew on the table" % wife.ID)

    @staticmethod
    def execute(wife):
        print("%s: Fussin' over food" % wife.ID)
        
    @staticmethod
    def on_message(wife, message):
        if message.message == messagetypes.msg_stew_ready:
            print("Message handled by %s at time: %s" % (wife.ID, time.time()))
            print("%s: StewReady! Lets eat" % wife.ID)
            messagedispatcher.dispatch_message(0.0,
                                               wife.ID,
                                               entitynames.bob_id,
                                               messagetypes.msg_stew_ready)
            wife.set_cooking(False)
            wife.statemachine.change_state(DoHouseWork)
            return True
        return False

#-------------------------------------------------------------------------------


