# -*- coding: utf-8 -*-


_entities = {} # {ID: entity}

def register_entity(entity):
    """
    Registers an entity.
    The entity needs to have an ID attribute (should be unique).
    
    :Parameters:
        entity : object
            Needs to have an attribute named ID.
    """
    _entities[entity.ID] = entity

def remove_entity(entity):
    """
    Removes entity from registry.
    
    :Parameters:
        entity : object
            Needs to have an attribute named ID.
    """
    if entity.ID in _entities:
        del _entities[entity.ID]

def get_entity_from_ID(id):
    """
    Returns the object registered by the given id.
    
    :raises:
        AssertionError if no such id is registered.
    
    :Parameters:
        id : object
            Unique id.
            
    :Returns:
        The registered object.
    :rtype:
        object

    """
    assert id in _entities and "invalid id: " + str(id)
    return _entities[id]


