# -*- coding: utf-8 -*-

"""
TODO
"""

import time

import messagedispatcher
import entitynames
import messagetypes
import locations
from state import State


#-------------------------------------------------------------------------------

class EnterMineAndDigForNugget(State):

    @staticmethod
    def enter(miner):
        if miner.location != locations.GOLDMINE:
            print("%s: Walkin' to the goldmine" % miner.ID)
            miner.location = locations.GOLDMINE

    @staticmethod
    def exit(miner):
        print("%s: Ah'm leavin' the goldmine with mah pockets full o' sweet gold" % miner.ID)

    @staticmethod
    def execute(miner):
        miner.add_to_gold_carried(1)
        miner.increase_fatigue()
        print("%s: Pickin' up a nugget" % miner.ID)

        if miner.pockets_full():
            miner.statemachine.change_state(VisitBankAndDepositGold)

        if miner.is_thirsty():
            miner.statemachine.change_state(QuenchThirst)

    @staticmethod
    def on_message(agent, message):
        # send message to global message handler
        return False

#-------------------------------------------------------------------------------

class VisitBankAndDepositGold(State):
    @staticmethod
    def enter(miner):
        if miner.location != locations.BANK:
            print("%s: Goin' to the bank. Yes siree" % miner.ID)
            miner.location = locations.BANK

    @staticmethod
    def exit(miner):
        print("%s: Leavin' the bank" % miner.ID)

    @staticmethod
    def execute(miner):
        miner.add_to_wealth(miner.gold_carried)
        miner.gold_carried = 0
        print("%s: Depositing gold. Total savings now: %s" % (miner.ID, miner.wealth))
        # wealthy enough to have a well earned rest?
        if miner.is_wealthy():
            print("%s: WooHoo! Rich enough for now. Back home to mah li'lle lady" % miner.ID)
            miner.statemachine.change_state(GoHomeAndSleepTilRested)
        else:
            miner.statemachine.change_state(EnterMineAndDigForNugget)

    @staticmethod
    def on_message(agent, message):
        # send message to global message handler
        return False
#-------------------------------------------------------------------------------
class GoHomeAndSleepTilRested(State):
    @staticmethod
    def enter(miner):
        if miner.location != locations.SHACK:
            print("%s: Walkin' home" % miner.ID)
            miner.location = locations.SHACK

            messagedispatcher.dispatch_message(0.0, # immediatly
                                               miner.ID,
                                               entitynames.elsa_id,
                                               messagetypes.msg_hi_honey_im_home)
           
    @staticmethod
    def exit(miner):
        pass
        # print "%s: Leaving the house" % miner.ID

    @staticmethod
    def execute(miner):
        if miner.is_fatigued():
            miner.decrease_fatigue()
            print("%s: ZZZZ..." % miner.ID)
        else:
            print("%s: What a God darn fantastic nap! Time to find more gold" % miner.ID)
            miner.statemachine.change_state(EnterMineAndDigForNugget)
            
    @staticmethod
    def on_message(miner, message):
        if message.message == messagetypes.msg_stew_ready:
            print("Message handled by %s at time: %s" % (miner.ID, time.time()))
            print("%s: Okay Hun, ahm a comin'!" % miner.ID)

            miner.statemachine.change_state(EatStew)
      
            return True
        return False

#-------------------------------------------------------------------------------
class QuenchThirst(State):
    @staticmethod
    def enter(miner):
        if miner.location != locations.SALOON:
            print("%s: Boy, ah sure is thusty! Walking to the saloon" % miner.ID)
            miner.location = locations.SALOON

    @staticmethod
    def exit(miner):
        print("%s: Leaving the saloon, feelin' good" % miner.ID)

    @staticmethod
    def execute(miner):
        if miner.is_thirsty():
            miner.buy_and_drink_a_whisky()
            print("%s: That's mighty fine sippin liquer" % miner.ID)
            miner.statemachine.change_state(EnterMineAndDigForNugget)
        else:
            print("%s: ERROR ERROR" % miner.ID)
        
    @staticmethod
    def on_message(agent, message):
        # send message to global message handler
        return False

#-------------------------------------------------------------------------------


class EatStew(State):

    @staticmethod
    def enter(agent):
        print("%s: Smells Reaaal goood Elsa!" % agent.ID)

    @staticmethod
    def exit(agent):
        print("%s: Thankya li'lle lady. Ah better get back to whatever ah wuz doin'" % agent.ID)

    @staticmethod
    def execute(agent):
        print("%s: Tastes real good too!" % agent.ID)
        agent.statemachine.revert_to_previous_state()
        
    @staticmethod
    def on_message(agent, message):
        return False


#-------------------------------------------------------------------------------


