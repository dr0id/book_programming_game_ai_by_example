# -*- coding: utf-8 -*-

class Telegram(object):

    def __init__(self, sender_id, receiver_id, msg, extra_info=None):
        self.sender_id = sender_id
        self.receiver_id = receiver_id
        self.message = msg # this is actually the messagtype
        self.extra_info = extra_info

