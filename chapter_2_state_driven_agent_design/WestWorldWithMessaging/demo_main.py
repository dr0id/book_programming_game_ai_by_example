#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
TODO
"""
import time

import miner
import minerswife
import entityregistry
import messagedispatcher
import entitynames

#-------------------------------------------------------------------------------
def main():

    # create the entities
    bob = miner.Miner(entitynames.bob_id)
    elsa = minerswife.MinersWife(entitynames.elsa_id)
    # register them
    entityregistry.register_entity(bob)
    entityregistry.register_entity(elsa)
    # main loop
    for i in range(30):
        bob.update()
        elsa.update()
        # pump the messages (delivers the delayed messages)
        messagedispatcher.dispatch_delayed_messages()
        
        # m.dump()
        time.sleep(0.8)

    input('>>press Enter to finish....')
    
#-------------------------------------------------------------------------------
if __name__ == "__main__":
    main()

