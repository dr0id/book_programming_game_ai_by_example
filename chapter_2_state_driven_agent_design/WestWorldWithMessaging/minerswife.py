# -*- coding: utf-8 -*-


import basegameentity
import locations
import statemachines
from minerswifeownedstates import WifesGlobalState, DoHouseWork, CookStew

#-------------------------------------------------------------------------------

# above this value a miner is thirsty
thirst_level = 5
# the amount of nuggets a miner can carry
max_nuggets = 3
# above this value a miner is sleepy
tiredness_threshold = 5
# the amount of gold a miner must have before he feels comfortable
comfort_level = 5

#-------------------------------------------------------------------------------


class MinersWife(basegameentity.BaseGameEntity):
    """
    The miner class implemented as a state machine.
    """

    def __init__(self, id):
        super(MinersWife, self).__init__(id)
        
        
        self._location = locations.SHACK
        self._cooking = False

        self.statemachine = statemachines.StateMachine(self)
        self.statemachine.set_global_state(WifesGlobalState)
        self.statemachine.set_current_state(DoHouseWork)

    def handle_message(self, message):
        return self.statemachine.handle_message(message)

    def update(self):
        self.statemachine.update()

    # location
    def _get_location(self):
        return self._location
    def _set_location(self, new_loc):
        self._location = new_loc
    location = property(_get_location, _set_location)

    def is_cooking(self):
        return self._cooking
    def set_cooking(self, val):
        self._cooking = val
#-------------------------------------------------------------------------------



