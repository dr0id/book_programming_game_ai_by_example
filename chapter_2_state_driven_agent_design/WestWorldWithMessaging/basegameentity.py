# -*- coding: utf-8 -*-

"""
TODO
"""

#-------------------------------------------------------------------------------
_next_id = 0
def _gen_next_id():
    yield _next_id
    _next_id += 1
    
def get_id():
    return next(_gen_next_id)
#-------------------------------------------------------------------------------


class BaseGameEntity(object):

    def __init__(self, id):
        self._id = id
        
    ID = property(lambda self: self._id, "unique id")
    
    def update(self):
        pass
        
    def handle_message(self, message):
        return False
#-------------------------------------------------------------------------------


#-------------------------------------------------------------------------------


