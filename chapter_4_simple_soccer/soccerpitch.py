# -*- coding: utf-8 -*-

import pygame

import soccerteam
import teamstates
from wall import Wall
from vectors import Vec2 as Vec
from goal import Goal
from params import Parameters
from soccerball import SoccerBall

# ------------------------------------------------------------------------------

num_regions_horizontal = 6
num_regions_vertical = 3
border = 20


class SoccerPitch(object):

    def __init__(self, size):
        self.playing_area = pygame.Rect(border, border, size[0] - 2 * border, size[1] - 2 * border)
        self.red_goal = Goal(   Vec(self.playing_area.left, self.playing_area.centery - Parameters.goal_width // 2),
                                Vec(self.playing_area.left, self.playing_area.centery + Parameters.goal_width // 2),
                                Vec(1, 0))
        self.blue_goal = Goal(  Vec(self.playing_area.right - 1, self.playing_area.centery - Parameters.goal_width // 2),
                                Vec(self.playing_area.right - 1, self.playing_area.centery + Parameters.goal_width // 2),
                                Vec(-1, 0))
        self.walls = []
        self.regions = []
        self.goal_keeper_has_ball = False
        self.is_game_on = True
        self.is_game_paused = True
        self.size = size

        self.create_regions(self.playing_area.size)
        self.create_walls(self.playing_area)
        self.ball = SoccerBall(Vec(*self.playing_area.center), Parameters.ball_size, Parameters.ball_mass, self.walls)

        self.red_team = soccerteam.SoccerTeam(self.red_goal, self.blue_goal,
                                                self, soccerteam.TeamColor.RED)
        self.blue_team = soccerteam.SoccerTeam(self.blue_goal, self.red_goal,
                                                self, soccerteam.TeamColor.BLUE)
        
        self.red_team.set_opponents(self.blue_team)
        self.blue_team.set_opponents(self.red_team)
        
        self.set_players_group()

    def update(self, dt):

        if self.is_game_paused:
            return


        self.ball.update(dt)

        self.blue_team.update(dt)
        self.red_team.update(dt)

        # if a goal has been detected reset the pitch ready kickoff
        if self.blue_goal.scored(self.ball) or self.red_goal.scored(self.ball):
            # print('>>>>>>>> goal')
            self.is_game_on = False

            # reset the ball
            self.ball.place_at_position(Vec(*self.playing_area.center))


            # get the teams ready for kickoff
            self.red_team.statemachine.change_state(teamstates.PrepareForKickOff)
            self.blue_team.statemachine.change_state(teamstates.PrepareForKickOff)

        # raise NotImplemented

    def create_regions(self, size):
        rw, rh = size
        rw //= num_regions_horizontal
        rh //= num_regions_vertical
        rect = pygame.Rect(border, border, rw, rh)
        for col in range(num_regions_horizontal-1, -1, -1):
            for row in range(num_regions_vertical-1, -1, -1):
                self.regions.append(rect.move(col * rw, row * rh))

    def create_walls(self, playing_area):
        topleft = Vec(*playing_area.topleft)
        topright = Vec(*playing_area.topright)
        bottomright = Vec(*playing_area.bottomright)
        bottomleft = Vec(*playing_area.bottomleft)

        self.walls.append(Wall(topleft, topright))
        self.walls.append(Wall(topright, self.blue_goal.left_post))
        self.walls.append(Wall(self.blue_goal.right_post, bottomright))

        self.walls.append(Wall(bottomright, bottomleft))
        self.walls.append(Wall(bottomleft, self.red_goal.right_post))
        self.walls.append(Wall(self.red_goal.left_post, topleft))

    def toggle_pause(self):
        self.is_game_paused = not self.is_game_paused

    def get_region_from_index(self, index):
        assert index >=0 and index < len(self.regions)
        return self.regions[index]

    def get_all_players(self):
        return self.red_team.players + self.blue_team.players

    def set_players_group(self):
        players = self.get_all_players()
        for player in players:
            # player.steering.behavior_separation.vehicle_group = players
            player.steering.group_vehicles = players

    @staticmethod
    def tag_vehicles_within_view_range(current_vehicle, vehicles, view_range):
        # if current_vehicle.world.use_bin_space:
            # for vehicle in current_vehicle.world.bin_space.get_neighbors(current_vehicle.position, view_range):
                # if vehicle is not current_vehicle and vehicle is not vehicles[0]:
                    # vehicle.tag = True
                # else:
                    # vehicle.tag = False

        # else:
            cur_v_pos_dist_sq = current_vehicle.position.get_distance_sq
            for vehicle in vehicles:
                vehicle.tag = False
                if cur_v_pos_dist_sq(vehicle.position) < (view_range + vehicle.bounding_radius)**2:
                    vehicle.tag = True

            current_vehicle.tag = False
            # vehicles[0].tag = False

    @staticmethod
    def get_vehicles_within_view_range(current_vehicle, vehicles, view_range):
        # if current_vehicle.world.use_bin_space:
            # # for vehicle in current_vehicle.world.bin_space.get_neighbors(current_vehicle.position, view_range):
                # # if vehicle is not current_vehicle and vehicle is not vehicles[0]:
                    # # vehicle.tag = True
                # # else:
                    # # vehicle.tag = False
            # raise NotImplementedError()
        # else:
            tagged = [veh for veh in vehicles if current_vehicle.position.get_distance_sq(veh.position) < (view_range + veh.bounding_radius) ** 2]
            try:
                tagged.remove(current_vehicle)
            except:
                pass
            return tagged
