# -*- coding: utf-8 -*-

try:
    from sys import maxint
except:
    # python 3
    from sys import maxsize as maxint
from math import sqrt as math_sqrt
from math import pi as PI
from random import random

import gameentity
from vectors import Vec2 as Vec
from vectors import EPSILON
from params import Parameters
from steeringbehaviors import line_intersection_2d

# ------------------------------------------------------------------------------

class SoccerBall(gameentity.MovingEntity):

    def __init__(self, position, ball_size, mass, walls):
        super(SoccerBall, self).__init__(position,
                                      ball_size,
                                      Vec(0.0, 0.0), # velocity
                                      -1.0, # mas speed, unused
                                      Vec(0, 1), # heading
                                      mass,
                                      Vec(1.0, 1.0), # scale, unused
                                      0, # max_turn_rate, unused
                                      0) #max_force, unused
        # keeps a record of the balls last position
        self.old_position = Vec(position.x, position.y)
        # TODO: book and code missmatch?? => soccerteam.ControllingPlayer
        # reference to the player/goalkeeper that posses the ball
        # self.owner = None
        # the wall of the pitch used for collision detection
        self.walls = walls

    def handle_message(self, msg):
        return False

    def add_noise_to_kick(self, ball_pos, ball_target):
        # //returns a random double in the range -1 < n < 1
        rnd_clamped = random() - random()
        displacement = (PI - PI * Parameters.player_kicking_accuracy) * rnd_clamped
        to_target = ball_target - ball_pos
        to_target.rotate(displacement)
        return to_target + ball_pos

    def kick(self, direction, force):
        """
        applys a force to the ball in the direction of heading. Truncates
        the new velocity to make sure it doesn't exceed the max allowable.
        """
        # print('???? ball kicked!', direction, force)
        # //ensure direction is normalized
        direction.normalize()

        # //calculate the acceleration
        acceleration = (direction * force) / self.mass

        # //update the velocity
        self.velocity = acceleration

    def trap(self):
        """
        this is used by players and goalkeepers to 'trap' a ball -- to stop
        it dead. That player is then assumed to be in possession of the ball
        and m_pOwner is adjusted accordingly
        """
        self.velocity.x = 0.0
        self.velocity.y = 0.0

    def update(self, dt):
        """
        updates the ball physics, tests for any collisions and adjusts
        the ball's velocity accordingly
        """
        # //keep a record of the old position so the goal::scored method
        # //can utilize it for goal testing
        self.old_position.values = self.position

        self.test_collision_with_walls(self.walls)

        if self.velocity.length_sq > Parameters.friction ** 2:
            self.velocity += self.velocity.normalized * Parameters.friction

            self.position += self.velocity

            self.heading = self.velocity.normalized

    def future_position(self, time):
        # //using the equation s = ut + 1/2at^2, where s = distance, a = friction
        # //u=start velocity

        # //calculate the ut term, which is a vector
        ut = self.velocity * time
        # //calculate the 1/2at^2 term, which is scalar
        half_a_t_sq = 0.5 * Parameters.friction * time * time
        # //turn the scalar quantity into a vector by multiplying the value with
        # //the normalized velocity vector (because that gives the direction)
        scalar_to_vector = half_a_t_sq * self.velocity.normalized
        # //the predicted position is the balls position plus these two terms
        return self.position + ut + scalar_to_vector

    def time_to_cover_distance(self, A, B, force):
        # //this will be the velocity of the ball in the next time step *if*
        # //the player was to make the pass.
        speed = force / self.mass

        # //calculate the velocity at B using the equation
        # //
        # //  v^2 = u^2 + 2as
        # //

        # //first calculate s (the distance between the two positions)
        dist_to_cover = A.get_distance(B)

        term = speed ** 2 + 2.0 * dist_to_cover * Parameters.friction

        # //if  (u^2 + 2as) is negative it means the ball cannot reach point B.
        if term <= 0.0:
            return -1.0

        v = math_sqrt(term)

        # //it IS possible for the ball to reach B and we know its speed when it
        # //gets there, so now it's easy to calculate the time using the equation
        # //
        # //    t = v-u
        # //        ---
        # //         a
        # //
        return (v - speed) / Parameters.friction

    def place_at_position(self, new_pos):
        """positions the ball at the desired location and sets the ball's velocity to zero"""
        self.position.values = new_pos
        self.old_position.values = self.position
        self.velocity.x = 0.0
        self.velocity.y = 0.0

    def test_collision_with_walls(self, walls):
        # //test ball against each wall, find out which is closest
        vel_normal = self.velocity.normalized
        intersection_point = None
        
        collision_point = None
        closest = None
        dist_to_intersection = maxint

        # //iterate through each wall and calculate if the ball intersects.
        # //If it does then store the index into the closest intersecting wall
        for wall in walls:
            # //assuming a collision if the ball continued on its current heading
            # //calculate the point on the ball that would hit the wall. This is
            # //simply the wall's normal(inversed) multiplied by the ball's radius
            # //and added to the balls center (its position)
            this_collision_point = self.position - (wall.normal * self.bounding_radius)

            # //calculate exactly where the collision point will hit the plane
            if self.where_is_point(this_collision_point, wall.start, wall.normal) == 1: # backside
                dist_to_wall = self.distance_to_ray_plane_intersection(this_collision_point,
                                                                    wall.normal,
                                                                    wall.start,
                                                                    wall.normal)
                intersection_point = this_collision_point + (dist_to_wall * wall.normal)
            else:
                dist_to_wall = self.distance_to_ray_plane_intersection(this_collision_point,
                                                                    vel_normal,
                                                                    wall.end,
                                                                    wall.normal)
                intersection_point = this_collision_point + (dist_to_wall * vel_normal)

            # //check to make sure the intersection point is actually on the line
            # //segment
            on_line_segment = False
            intersection, xx, yy = line_intersection_2d(wall.start,
                                    wall.end,
                                    this_collision_point - wall.normal * 20,
                                    this_collision_point + wall.normal * 20)
            if intersection:
                on_line_segment = True

            # //Note, there is no test for collision with the end of a line segment

            # //now check to see if the collision point is within range of the
            # //velocity vector. [work in distance squared to avoid sqrt] and if it
            # //is the closest hit found so far.
            # //If it is that means the ball will collide with the wall sometime
            # //between this time step and the next one.
            dist_sq = this_collision_point.get_distance_sq(intersection_point)
            if dist_sq <= self.velocity.length_sq and (dist_sq < dist_to_intersection) and on_line_segment:
              dist_to_intersection = dist_sq
              closest = wall
              collision_point = intersection_point

        # //to prevent having to calculate the exact time of collision we
        # //can just check if the velocity is opposite to the wall normal
        # //before reflecting it. This prevents the case where there is overshoot
        # //and the ball gets reflected back over the line before it has completely
        # //reentered the playing area.
        if closest is not None and self.velocity.normalized.dot(closest.normal) < 0:
            self.velocity = self.velocity.reflect(closest.normal)

    def where_is_point(self, point, point_on_plane, plane_normal):
        dir = point_on_plane - point
        d = dir.dot(plane_normal)

        if d < -EPSILON:
            return -1 # fron
        elif d > EPSILON:
            return 1 # backside

        return 0 # on plane

    def distance_to_ray_plane_intersection(self, origin, heading, plane_point, plane_normal):
        d = -plane_normal.dot(plane_point)
        number = plane_normal.dot(origin) + d
        denom = plane_normal.dot(heading)

        if (denom < EPSILON) and (denom > -EPSILON):
            return -1.0
        return -(number / denom)