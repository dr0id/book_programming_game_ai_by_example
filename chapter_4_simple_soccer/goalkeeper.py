# -*- coding: utf-8 -*-


import playerbase
from params import Parameters
from vectors import Vec2 as Vec
from playerbase import PlayerBase
from statemachine import StateMachine
from goalkeeperstates import GlobalKeeperState

# ------------------------------------------------------------------------------

class GoalKeeper(PlayerBase):

    def __init__(self, 
                    home_team, 
                    home_region,
                    start_state,
                    heading,
                    velocity,
                    mass,
                    max_force,
                    max_speed,
                    max_turn_rate,
                    scale
                    ):
        """
        """
        PlayerBase.__init__(self,
                    home_team, 
                    home_region,
                    heading,
                    velocity,
                    mass,
                    max_force,
                    max_speed,
                    max_turn_rate,
                    scale,
                    playerbase.Role.GOAL_KEEPER)
        self.statemachine = StateMachine(self)
        
        self.statemachine.set_previous_state(start_state)
        self.statemachine.set_global_state(GlobalKeeperState)
        self.statemachine.change_state(start_state)
        
        # //this vector is updated to point towards the ball and is used when
        # //rendering the goalkeeper (instead of the underlaying vehicle's heading)
        # //to ensure he always appears to be watching the ball
        self.look_at = Vec(0, 0)
        
    def update(self, dt):
        # //run the logic for the current state
        self.statemachine.update()

        # //calculate the combined force from each steering behavior 
        steering_force = self.steering.calculate(self, None)

        # //Acceleration = Force/Mass
        acceleration = steering_force / self.mass

        # //update velocity
        self.velocity += acceleration

        # //make sure player does not exceed maximum velocity
        self.velocity.truncate(self.max_speed)

        # //update the position
        self.position += self.velocity

        # //enforce a non-penetration constraint if desired
        if Parameters.non_penetration_constraint:
            self.enforce_non_penetration(self.team.pitch.get_all_players())

        # //update the heading if the player has a non zero velocity
        if not self.velocity.is_zero():
            self.heading = self.velocity.normalized
            self.side = self.heading.perp

        # //look-at vector always points toward the ball
        if not self.team.pitch.goal_keeper_has_ball:
            self.look_at = (self.team.pitch.ball.position - self.position).normalized
        
    def handle_message(self, msg):
        self.statemachine.handle_message(msg)
        
    def get_rear_interpose_target(self):
        xpos = self.team.home_goal.center.x
        ypos = self.team.pitch.playing_area.centery - \
                Parameters.goal_width * 0.5 + (self.team.pitch.ball.position.y * Parameters.goal_width) / \
                self.team.pitch.playing_area.height

        return Vec(xpos, ypos)
        
    def ball_within_range_for_intercept(self):
        return (self.team.home_goal.center.get_distance_sq(self.team.pitch.ball.position) <= Parameters.goal_keeper_intercept_range_sq)

    def too_far_from_goal_mouth(self):
        return (self.position.get_distance_sq(self.get_rear_interpose_target()) > Parameters.goal_keeper_intercept_range_sq)
        