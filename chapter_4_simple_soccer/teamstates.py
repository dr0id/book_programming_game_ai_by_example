# -*- coding: utf-8 -*-

import state
from soccerteam import TeamColor

# ------------------------------------------------------------------------------

def change_player_home_region(team, new_regions):
    for idx, player in enumerate(team.players):
        team.set_player_home_region(player, new_regions[idx])

# ------------------------------------------------------------------------------

# //these define the home regions for this state of each of the players
blue_attacking_regions = [1, 12, 14, 6, 4]
red_attacking_regions = [16, 3, 5, 9, 13]

class Attacking(state.State):

    @staticmethod
    def enter(team):

        # //set up the player's home regions
        if team.color == TeamColor.BLUE:
            change_player_home_region(team, blue_attacking_regions)
        else:
            change_player_home_region(team, red_attacking_regions)

        # //if a player is in either the Wait or ReturnToHomeRegion states, its
        # //steering target must be updated to that of its new home region to enable
        # //it to move into the correct position.
        team.update_targets_of_waiting_players()

    @staticmethod
    def exit(team):
        # //there is no supporting player for defense
        team.supporting_player = None

    @staticmethod
    def execute(team):
        # //if this team is no longer in control change states
        # if (!team->InControl())
        # {
        # team->GetFSM()->ChangeState(Defending::Instance()); return;
        # }
        if not team.is_in_control():
            team.statemachine.change_state(Defending)
            return

        # //calculate the best position for any supporting attacker to move to
        # team->DetermineBestSupportingPosition();
        team.determine_best_supporting_position()


    @staticmethod
    def on_message(team, message):
        return False

# ------------------------------------------------------------------------------

# //these define the home regions for this state of each of the players
blue_defending_regions = [1, 6, 8, 3, 5]
red_defending_regions = [16, 9, 11, 12, 14]

class Defending(state.State):

    @staticmethod
    def enter(team):
        # //set up the player's home regions
        if team.color == TeamColor.BLUE:
            change_player_home_region(team, blue_defending_regions)
        else:
            change_player_home_region(team, red_defending_regions)

        # //if a player is in either the Wait or ReturnToHomeRegion states, its
        # //steering target must be updated to that of its new home region
        team.update_targets_of_waiting_players()

    @staticmethod
    def exit(team):
        pass

    @staticmethod
    def execute(team):
        # //if in control change states
        if team.is_in_control():
            team.statemachine.change_state(Attacking)
            return

    @staticmethod
    def on_message(team, message):
        return False

# ------------------------------------------------------------------------------

class PrepareForKickOff(state.State):

    @staticmethod
    def enter(team):
        # //reset key player pointers
        team.controlling_player = None
        team.supporting_player = None
        team.receiving_player = None
        team.player_closest_to_ball = None

        # //send Msg_GoHome to each player.
        team.return_all_field_players_to_home()
        # print('PrepareForKickOff -> enter', team.color, 'return all field players to home!!')

    @staticmethod
    def exit(team):
        team.pitch.is_game_on = True
        # print('PrepareForKickOff -> exit', team.color)

    @staticmethod
    def execute(team):
        # //if both teams in position, start the game
        if team.all_players_at_home() and team.opponents.all_players_at_home():
            # print('PrepareForKickOff -> exec: all players at home!')
            team.statemachine.change_state(Defending)

    @staticmethod
    def on_message(team, message):
        return False

# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------









