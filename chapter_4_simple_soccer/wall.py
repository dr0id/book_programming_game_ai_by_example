# -*- coding: utf-8 -*-


# ------------------------------------------------------------------------------

class Wall(object):

    def __init__(self, start_position, end_position, normal=None):
        """
        All public attributes should be treated as read-only!
        """
        self.start = start_position
        self.end = end_position
        if normal:
            self.normal = normal
        else:
            self.normal = (self.end - self.start).normalized.perp
        
        
