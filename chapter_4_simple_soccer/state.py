# -*- coding: utf-8 -*-



class State(object):

    @staticmethod
    def enter(agent):
        raise NotImplementedError()

    @staticmethod
    def exit(agent):
        raise NotImplementedError()

    @staticmethod
    def execute(agent):
        raise NotImplementedError()
        
    @staticmethod
    def on_message(agent, message):
        raise NotImplementedError()

