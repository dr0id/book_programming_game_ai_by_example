# -*- coding: utf-8 -*-

import steeringbehaviors
from steeringbehaviors import line_intersection_2d

# ------------------------------------------------------------------------------

class Goal(object):

    def __init__(self, left, right, facing):
        self.left_post = left
        self.right_post = right
        self.facing = facing
        self.center = (left + right) / 2.0
        self.num_goals_scored = 0

    def reset_goals_scored(self):
        self.num_goals_scored = 0
        
    def scored(self, ball):
        intersect, xx, yy = line_intersection_2d(ball.position, ball.old_position, self.left_post, self.right_post)
        if intersect:
            # print('XXX goal')
            self.num_goals_scored += 1
            return True
        return False

