# -*- coding: utf-8 -*-

# import binspace

# ------------------------------------------------------------------------------
class BaseWorld(object):

    def __init__(self, size):
        self.size = size
        self.crosshair = None
        self.draw_wander_info_on = False
        self.draw_obstacles_on = False
        self.draw_dbox_on = False
        self.draw_walls_on = False
        self.draw_walls_normals_on = True
        self.draw_feelers_on = False
        self.draw_path_on = False
        self.draw_view_distance_on = False
        self.draw_text_overlay_on = False
        self.draw_bounding_radius_on = False
        # self.use_bin_space = False

        self.vehicles = []
        self.obstacles = []
        self.walls = []
        # self.bin_space = binspace.BinSpace(50, 50)


        self.init()
        self.refresh_text()

    def init(self):
        pass

    def update(self, dt):
        for vehicle in self.vehicles:
            vehicle.update(dt)
        # if self.use_bin_space:
            # self.bin_space.update_entity_list(self.vehicles)
        self.check_keys(dt)
        
    def refresh_text(self):
        pass

    # --- controller --- #

    def check_keys(self, dt):
        pass

    def on_event(self, event):
        pass

    # --- collision helper --- #

    @staticmethod
    def tag_obstacles_within_view_range(vehicle, obstacles, view_range):
        for obstacle in obstacles:
            obstacle.tag = False
            radii = view_range + obstacle.bounding_radius
            if vehicle.position.get_distance_sq(obstacle.position) < radii * radii:
                obstacle.tag = True

    @staticmethod
    def get_obstacles_within_view_range(vehicle, obstacles, view_range):
        found = []
        for obstacle in obstacles:
            radii = view_range + obstacle.bounding_radius
            if vehicle.position.get_distance_sq(obstacle.position) < radii * radii:
                found.append(obstacle)
        return found

    @staticmethod
    def tag_vehicles_within_view_range(current_vehicle, vehicles, view_range):
        # if current_vehicle.world.use_bin_space:
            # for vehicle in current_vehicle.world.bin_space.get_neighbors(current_vehicle.position, view_range):
                # if vehicle is not current_vehicle and vehicle is not vehicles[0]:
                    # vehicle.tag = True
                # else:
                    # vehicle.tag = False

        # else:
            cur_v_pos_dist_sq = current_vehicle.position.get_distance_sq
            for vehicle in vehicles:
                vehicle.tag = False
                if cur_v_pos_dist_sq(vehicle.position) < (view_range + vehicle.bounding_radius)**2:
                    vehicle.tag = True

            current_vehicle.tag = False
            # vehicles[0].tag = False



    @staticmethod
    def get_vehicles_within_view_range(current_vehicle, vehicles, view_range):
        # if current_vehicle.world.use_bin_space:
            # # for vehicle in current_vehicle.world.bin_space.get_neighbors(current_vehicle.position, view_range):
                # # if vehicle is not current_vehicle and vehicle is not vehicles[0]:
                    # # vehicle.tag = True
                # # else:
                    # # vehicle.tag = False
            # raise NotImplementedError()
        # else:
            tagged = [veh for veh in vehicles if current_vehicle.position.get_distance_sq(veh.position) < (view_range + veh.bounding_radius) ** 2]
            tagged.remove(current_vehicle)
            return tagged


# ------------------------------------------------------------------------------
