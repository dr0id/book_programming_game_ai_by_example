# -*- coding: utf-8 -*-

from sys import maxsize as sys_maxsize
from random import random as random_random
from random import randint as random_randint
from math import sqrt as math_sqrt

import messagetypes
import messagedispatcher
import entityregistry
import statemachine
from vectors import Vec2 as Vec
class TeamColor(object):
    RED = "Red"
    BLUE = "Blue"
import teamstates
from supportspotcalculator import SupportSpotCalculator
from params import Parameters
from goalkeeper import GoalKeeper
from fieldplayer import FieldPlayer
from playerbase import Role
from steeringbehaviors import point_to_local_2d as steeringbehaviors_point_to_local_2d
import goalkeeperstates
import fieldplayerstates

# ------------------------------------------------------------------------------


class SoccerTeam(object):

    def __init__(self, home_goal, opponents_goal, pitch, color):
        self.ID = color # TODO: hack
        self.color = color
        self.pitch = pitch
        self.home_goal = home_goal
        self.opponents_goal = opponents_goal

        self.statemachine = statemachine.StateMachine(self)

        self.statemachine.set_current_state(teamstates.Defending)
        self.statemachine.set_previous_state(teamstates.Defending)

        self.players = []
        self.opponents = None

        self.controlling_player = None
        self.supporting_player = None
        self.receiving_player = None
        self.player_closest_to_ball = None

        self.dist_sq_to_ball_of_closest_player = 0.0

        self.support_spot_calc = SupportSpotCalculator(Parameters.num_support_spots_x, Parameters.num_support_spots_y, self)

        self._create_players()

        for player in self.players:
            entityregistry.register_entity(player)

    def _create_players(self):
        if self.color == TeamColor.BLUE:
            self.players.append(self._create_goal_keeper(1, Vec(0.0, 1.0)))
            self.players.append(self._create_field_player(6, Vec(0.0, 1.0), Role.ATTACKER))
            self.players.append(self._create_field_player(8, Vec(0.0, 1.0), Role.ATTACKER))
            self.players.append(self._create_field_player(3, Vec(0.0, 1.0), Role.DEFENDER))
            self.players.append(self._create_field_player(5, Vec(0.0, 1.0), Role.DEFENDER))
        else:
            self.players.append(self._create_goal_keeper(16, Vec(0.0, -1.0)))
            self.players.append(self._create_field_player(9, Vec(0.0, -1.0), Role.ATTACKER))
            self.players.append(self._create_field_player(11, Vec(0.0, -1.0), Role.ATTACKER))
            self.players.append(self._create_field_player(12, Vec(0.0, -1.0), Role.DEFENDER))
            self.players.append(self._create_field_player(14, Vec(0.0, -1.0), Role.DEFENDER))

    def _create_goal_keeper(self, home_region, heading):
        return GoalKeeper(self,
                        home_region,
                        goalkeeperstates.TendGoal,
                        heading,
                        Vec(0.0, 0.0),
                        Parameters.player_mass,
                        Parameters.player_max_force,
                        Parameters.player_max_speed_without_ball,
                        Parameters.player_max_turn_rate,
                        Parameters.player_scale)

    def _create_field_player(self, home_region, heading, role):
        return FieldPlayer(self,
                        home_region,
                        fieldplayerstates.Wait,
                        heading,
                        Vec(0.0, 0.0),
                        Parameters.player_mass,
                        Parameters.player_max_force,
                        Parameters.player_max_speed_without_ball,
                        Parameters.player_max_turn_rate,
                        Parameters.player_scale,
                        role)



    def _calculate_closest_player_to_ball(self):
        """
        called each frame. Sets self.player_closest_to_ball to point to the player closest to the ball.

        """
        closest = sys_maxsize
        for player in self.players:
            dist_sq = player.position.get_distance_sq(self.pitch.ball.position)
            player.dist_sq_to_ball = dist_sq

            if dist_sq < closest:
                closest = dist_sq
                self.player_closest_to_ball = player

        self.dist_sq_to_ball_of_closest_player = closest

    def update(self, dt):
        # //this information is used frequently so it's more efficient to
        # //calculate it just once each frame
        self._calculate_closest_player_to_ball()

        # //the team state machine switches between attack/defense behavior. It
        # //also handles the 'kick off' state where a team must return to their
        # //kick off positions before the whistle is blown
        self.statemachine.update()

        # //now update each player
        for player in self.players:
            player.update(dt)


    def return_all_field_players_to_home(self):
        """
        calling this changes the state of all field players to that of
        ReturnToHomeRegion. Mainly used when a goal keeper has
        possession
        """
        # //  sends a message to all players to return to their home areas forthwith
        for player in self.players:
                if player.role != Role.GOAL_KEEPER:
                    messagedispatcher.dispatch_message(messagedispatcher.SEND_MSG_IMMEDIATELY, -1, player.ID, messagetypes.GO_HOME)

    def can_shoot(self, ball_pos, power):
        """
        returns true if player has a clean shot at the goal and sets ShotTarget
        to a normalized vector pointing in the direction the shot should be
        made. Else returns false and sets heading to a zero vector
        """
        # //the number of randomly created shot targets this method will test
        # int NumAttempts = Prm.NumAttemptsToFindValidStrike;
        num_attemps = Parameters.num_attemps_to_find_valid_strike

        # while (NumAttempts--)
        while num_attemps > 0:
            # //choose a random position along the opponent's goal mouth. (making
            # //sure the ball's radius is taken into account)
            shot_target = self.opponents_goal.center

            # //the y value of the shot position should lay somewhere between two
            # //goalposts (taking into consideration the ball diameter)
            min_y_val = (int)(self.opponents_goal.left_post.y + self.pitch.ball.bounding_radius)
            max_y_val = (int)(self.opponents_goal.right_post.y - self.pitch.ball.bounding_radius)

            shot_target.y = float(random_randint(min_y_val, max_y_val))

            # //make sure striking the ball with the given power is enough to drive
            # //the ball over the goal line.
            time = self.pitch.ball.time_to_cover_distance(ball_pos, shot_target, power)

            # //if it is, this shot is then tested to see if any of the opponents
            # //can intercept it.
            if time >= 0:
                if self.is_pass_safe_from_all_opponents(ball_pos, shot_target, None, power):
                    return True, shot_target
            num_attemps -= 1

        return False, shot_target

    def find_pass(self, passer, power, min_passing_dist):
        """
        The best pass is considered to be the pass that cannot be intercepted
        by an opponent and that is as far forward of the receiver as possible
        If a pass is found, the receiver's address is returned in the
        reference, 'receiver' and the position the pass will be made to is
        returned in the  reference 'PassTarget'
        """
        # //  The best pass is considered to be the pass that cannot be intercepted
        # //  by an opponent and that is as far forward of the receiver as possible
        clossest_to_goal_so_far = sys_maxsize
        target = Vec(0.0, 0.0)
        receiver = None
        pass_target = Vec(0.0, 0.0)

        for player in self.players:
            # //make sure the potential receiver being examined is not this player
            # //and that it is further away than the minimum pass distance
            # print('?? find pass:', self.color, player is not passer, passer.position.get_distance_sq(player.position) > min_passing_dist * min_passing_dist, passer.position.as_xy_tuple(), player.position.as_xy_tuple())
            if player is not passer and \
                        passer.position.get_distance_sq(player.position) > min_passing_dist * min_passing_dist:
                target = self.get_best_pass_to_receiver(passer, player, power)
                # print('?? find pass target:', self.color, target, power)
                if target:
                    # //if the pass target is the closest to the opponent's goal line found
                    # // so far, keep a record of it
                    dist_to_goal = abs(target.x - self.opponents_goal.center.x)

                    if dist_to_goal < clossest_to_goal_so_far:
                        clossest_to_goal_so_far = dist_to_goal
                        receiver = player
                        pass_target = target.clone()

        return receiver, pass_target

    def get_best_pass_to_receiver(self, passer, receiver, power):
        """
        Three potential passes are calculated. One directly toward the receiver's
        current position and two that are the tangents from the ball position
        to the circle of radius 'range' from the receiver.
        These passes are then tested to see if they can be intercepted by an
        opponent and to make sure they terminate within the playing area. If
        all the passes are invalidated the function returns false. Otherwise
        the function returns the pass that takes the ball closest to the
        opponent's goal area.
        """
        # //first, calculate how much time it will take for the ball to reach
        # //this receiver, if the receiver was to remain motionless
        ball = self.pitch.ball
        time = ball.time_to_cover_distance(ball.position, receiver.position, power)

        # //return false if ball cannot reach the receiver after having been
        # //kicked with the given power
        if time < 0:
            # print('!! ball cannot reach receiver', self.color, passer, receiver)
            return False

        # //the maximum distance the receiver can cover in this time
        intercept_range = time * receiver.max_speed
        # //Scale the intercept range
        scaling_factor = 0.3
        intercept_range *= scaling_factor

        # //now calculate the pass targets which are positioned at the intercepts
        # //of the tangents from the ball to the receiver's range circle.
        ip1, ip2 = get_tangent_points(receiver.position, intercept_range, ball.position)

        # num_pass_to_try = 3
        passes = [ip1, receiver.position, ip2]

        # // this pass is the best found so far if it is:
        # //
        # //  1. Further upfield than the closest valid pass for this receiver
        # //     found so far
        # //  2. Within the playing area
        # //  3. Cannot be intercepted by any opponents
        closest_so_far = sys_maxsize
        pass_target = False

        for cur_pass in passes:
            dist = abs(cur_pass.x - self.opponents_goal.center.x)

            if dist < closest_so_far and \
                self.pitch.playing_area.collidepoint(cur_pass.as_xy_tuple()) and \
                self.is_pass_safe_from_all_opponents(ball.position,
                                                    cur_pass,
                                                    receiver,
                                                    power):
                closest_so_far = dist
                pass_target = cur_pass.clone()

        return pass_target


    def is_pass_safe_from_opponent(self, from_pos, target, receiver, opp, passing_force):
        """
        test if a pass from positions 'from' to 'target' kicked with force
        'PassingForce'can be intercepted by an opposing player
        """
        # //move the opponent into local space.
        to_target = target - from_pos
        to_target_norm = to_target.normalized

        local_pos_opp = steeringbehaviors_point_to_local_2d(opp.position, \
                                                    to_target_norm, \
                                                    to_target_norm.perp, \
                                                    from_pos)

        # //if opponent is behind the kicker then pass is considered okay(this is
        # //based on the assumption that the ball is going to be kicked with a
        # //velocity greater than the opponent's max velocity)
        if local_pos_opp.x < 0:
            return True

        # //if the opponent is further away than the target we need to consider if
        # //the opponent can reach the position before the receiver.
        if from_pos.get_distance_sq(target) < opp.position.get_distance_sq(from_pos):
            if receiver:
                if target.get_distance_sq(opp.position) > target.get_distance_sq(receiver.position):
                    return True
                else:
                    return False
            else:
                return True

        # //calculate how long it takes the ball to cover the distance to the
        # //position orthogonal to the opponents position
        time_for_ball = self.pitch.ball.time_to_cover_distance(Vec(0.0, 0.0), \
                                                                Vec(local_pos_opp.x, 0.0),\
                                                                passing_force)

        # //now calculate how far the opponent can run in this time
        reach = opp.max_speed * time_for_ball + self.pitch.ball.bounding_radius + opp.bounding_radius

        # //if the distance to the opponent's y position is less than his running
        # //range plus the radius of the ball and the opponents radius then the
        # //ball can be intercepted
        if abs(local_pos_opp.y) < reach:
            return False
        else:
            return True

    def is_pass_safe_from_all_opponents(self, from_pos, target, receiver, passing_force):
        """
        tests a pass from position 'from' to position 'target' against each member
        of the opposing team. Returns true if the pass can be made without
        getting intercepted
        """
        assert self.opponents is not None
        for opp in self.opponents.players:
            if not self.is_pass_safe_from_opponent(from_pos, target, receiver, opp, passing_force):

                return False
        return True

    def is_opponent_within_radius(self, position, radius):
        """
        returns true if there is an opponent within radius of position
        """
        radius_sq = radius * radius
        assert self.opponents is not None
        for opp in self.opponents.players:
            if position.get_distance_sq(opp.position) < radius_sq:
                return True
        return False

    def request_pass(self, requester):
        """
        this tests to see if a pass is possible between the requester and
        the controlling player. If it is possible a message is sent to the
        controlling player to pass the ball asap.
        """
        # //maybe put a restriction here
        if random_random() > 0.1:
            return

        if (not requester.is_controlling_player()):
        
            is_save_from_opps = self.is_pass_safe_from_all_opponents(self.controlling_player.position, \
                                                    requester.position, \
                                                    requester, \
                                                    Parameters.max_passing_force)
            
            if is_save_from_opps:
                # //tell the player to make the pass
                # //let the receiver know a pass is coming
                print("request pass message: controlling: {0}, requester: {1}, req is controlling: {2}".format(self.controlling_player.ID, requester.ID, requester.is_controlling_player()))
                messagedispatcher.dispatch_message(messagedispatcher.SEND_MSG_IMMEDIATELY,\
                                                requester.ID, \
                                                self.controlling_player.ID, \
                                                messagetypes.PASS_TO_ME, \
                                                requester)
        # else:
            # print("controlling: {0}, requester: {1}, req is controlling: {2}".format(self.controlling_player.ID, requester.ID, requester.is_controlling_player()))



    def determine_best_supporting_attacker(self):
        """
        calculates the best supporting position and finds the most appropriate
        attacker to travel to the spot
        """
        closest_so_far = sys_maxsize
        best_player = None

        for player in self.players:
            # //only attackers utilize the BestSupportingSpot
            if player.role == Role.ATTACKER and player != self.controlling_player:
              # //calculate the dist. Use the squared value to avoid sqrt
              dist_sq = player.position.get_distance_sq(self.support_spot_calc.get_best_supporting_spot())

              # //if the distance is the closest so far and the player is not a
              # //goalkeeper and the player is not the one currently controlling
              # //the ball, keep a record of this player
              if dist_sq < closest_so_far:
                closest_so_far = dist_sq
                best_player = player

        return best_player


    @property
    def members(self):
        return self.players

    def set_opponents(self, opponents):
        self.opponents = opponents
        assert self.opponents is not None

    def get_support_spot(self):
        return self.support_spot_calc.get_best_supporting_spot()

    def get_controlling_player(self):
        return self.controlling_player

    def set_controlling_player(self, player):
        # print('??? set controlling player', self.color, player.ID)
        self.controlling_player = player
        assert self.opponents is not None
        self.opponents.lost_control()

    def lost_control(self):
        # print('??? lost controlling player', self.color)
        self.controlling_player = None

    def is_in_control(self):
        return (self.controlling_player is not None)

    def get_player_from_id(self, id):
        for player in self.players:
            if player.ID == id:
                return player
        return None

    def set_player_home_region(self, player, region):
        player.set_home_region(region)

    def determine_best_supporting_position(self):
        self.support_spot_calc.determine_supporting_position()

    def update_targets_of_waiting_players(self):
        for player in self.players:
            if player.role != Role.GOAL_KEEPER:
                # player is a fieldplayer
                if player.statemachine.is_in_state(fieldplayerstates.Wait) or \
                    player.statemachine.is_in_state(fieldplayerstates.ReturnToHomeRegion):
                    cx, cy = player.get_home_region().center
                    # player.steering.target = Vec(cx, cy)
                    player.steering.target = Vec(cx, cy)

    def all_players_at_home(self):
        for player in self.players:
            if not player.is_in_home_region():
                return False
        return True



# //------------------------------------------------------------------------
# //  Given a point P and a circle of radius R centered at C this function
# //  determines the two points on the circle that intersect with the
# //  tangents from P to the circle. Returns false if P is within the circle.
# //
# //  thanks to Dave Eberly for this one.
# //------------------------------------------------------------------------
def get_tangent_points(C, R, P):
# inline bool GetTangentPoints (Vector2D C, double R, Vector2D P, Vector2D& T1, Vector2D& T2)
# {
  # Vector2D PmC = P - C;
  pmc = P - C
  # double SqrLen = PmC.LengthSq();
  sqr_len = pmc.length_sq
  # double RSqr = R*R;
  r_sq = R * R
  # if ( SqrLen <= RSqr )
  # {
  if sqr_len <= r_sq:
      # // P is inside or on the circle
      # return false;
      return False, False
  # }

  # double InvSqrLen = 1/SqrLen;
  inv_sqr_len = 1.0 / sqr_len
  # double Root = sqrt(fabs(SqrLen - RSqr));
  root = math_sqrt(abs(sqr_len - r_sq))

  # T1.x = C.x + R*(R*PmC.x - PmC.y*Root)*InvSqrLen;
  # T1.y = C.y + R*(R*PmC.y + PmC.x*Root)*InvSqrLen;
  T1 = Vec(C.x + R * (R * pmc.x - pmc.y * root) * inv_sqr_len,
           C.y + R * (R * pmc.y + pmc.x * root) * inv_sqr_len)

  # T2.x = C.x + R*(R*PmC.x + PmC.y*Root)*InvSqrLen;
  # T2.y = C.y + R*(R*PmC.y - PmC.x*Root)*InvSqrLen;
  T2 = Vec(C.x + R * (R * pmc.x + pmc.y * root) * inv_sqr_len,
           C.y + R * (R * pmc.y - pmc.x * root) * inv_sqr_len)

  # return true;
  return T1, T2
# }
