#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame

from vectors import Vec2 as Vec
import gameentity
from params import params
import mainloop
import baseworld
from soccerpitch import SoccerPitch

# ------------------------------------------------------------------------------
class World(baseworld.BaseWorld):

    def init(self):
        self.title = "Simple Soccer"
        self.icon = pygame.image.load("icon1.png")
        self.soccer_pitch = SoccerPitch(self.size)
          
        self._disable_aids()
        self.draw_text_overlay_on = True
        
    def update(self, dt):
        # TODO: update?
        self.soccer_pitch.update(dt)
        self.check_keys(dt)
        
    def refresh_text(self):
        # TODO: text to display
        x, y = 250, 30
        self.text_to_draw = [   ( x,  y, "(P) PAUSED"),
                                ( x,  y+20, "(1) No Aids"),
                                ( x,  y+40, "(2) Show IDs"),
                                ( x,  y+60, "(3) Show States"),
                                ( x,  y+80, "(4) Show Regions"),
                                ( x, y+100, "(5) Show Support Spots"),
                                ( x, y+120, "(6) Show Targets"),
                                ( x, y+140, "(7) Highlight if Threatened"),
                                ( x, y+160, "(8) Show steering behaviors"),
                                ( x, y+180, "(r) reset"),
                            ]

    # --- controller --- #
    
    def check_keys(self, dt):
        # this pollutes the world class with pygame
        keys = pygame.key.get_pressed()

    def _disable_aids(self):
        self.is_show_ID_on = False
        self.is_show_states_on = False
        self.is_show_regions_on = False
        self.is_show_supportspots_on = False
        self.is_show_targets_on = False
        self.is_highlight_if_threatened_on = False
        self.is_show_steering_behaviors_on = False
        
    def on_event(self, event):
        # this pollutes the world class with pygame
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_1:
                self._disable_aids()
                
            elif event.key == pygame.K_2:
                self.is_show_ID_on = not self.is_show_ID_on
            elif event.key == pygame.K_3:
                self.is_show_states_on = not self.is_show_states_on
            elif event.key == pygame.K_4:
                self.is_show_regions_on = not self.is_show_regions_on
            elif event.key == pygame.K_5:
                self.is_show_supportspots_on = not self.is_show_supportspots_on
            elif event.key == pygame.K_6:
                self.is_show_targets_on = not self.is_show_targets_on
            elif event.key == pygame.K_7:
                self.is_highlight_if_threatened_on = not self.is_highlight_if_threatened_on
            elif event.key == pygame.K_8:
                self.is_show_steering_behaviors_on = not self.is_show_steering_behaviors_on
            elif event.key == pygame.K_9:
                pass
               
     
            elif event.key == pygame.K_SPACE or event.key == pygame.K_p:
                self.draw_text_overlay_on = not self.draw_text_overlay_on
                self.soccer_pitch.is_game_paused = self.draw_text_overlay_on
                self.refresh_text()
                return True
                
        return False  # not handled
        
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
if __name__ == "__main__":
    mainloop.MainLoop(World, (700, 350)).run()

