# -*- coding: utf-8 -*-

import messagedispatcher
import messagetypes

from state import State
from params import Parameters
from vectors import Vec2 as Vec


# ------------------------------------------------------------------------------

class GlobalKeeperState(State):

    @staticmethod
    def enter(keeper):
        pass

    @staticmethod
    def exit(keeper):
        pass

    @staticmethod
    def execute(keeper):
        pass
        
    @staticmethod
    def on_message(keeper, telegram):
        if telegram.message == messagetypes.GO_HOME:
            keeper.set_default_home_region()
            keeper.statemachine.change_state(ReturnHome)
        elif telegram.message == messagetypes.RECEIVE_BALL:
            keeper.statemachine.change_state(InterceptBall)
        # else:
            # print('goalkeeperstates global state received unknown message!', keeper.ID, telegram.message, telegram.sender_id)

        return False

# ------------------------------------------------------------------------------
        
class TendGoal(State):
    """
    //  This is the main state for the goalkeeper. When in this state he will
    //  move left to right across the goalmouth using the 'interpose' steering
    //  behavior to put himself between the ball and the back of the net.
    //
    //  If the ball comes within the 'goalkeeper range' he moves out of the
    //  goalmouth to attempt to intercept it. (see next state)
    """
    
    @staticmethod
    def enter(keeper):
        # //turn interpose on
        # //interpose will position the agent between the ball position and a target
        # //position situated along the goal mouth. This call sets the target
        keeper.steering.behavior_distance_interpose.activate(keeper.get_rear_interpose_target(), keeper.team.pitch.ball, Parameters.goal_keeper_tending_distance)

    @staticmethod
    def exit(keeper):
        keeper.steering.behavior_distance_interpose.deactivate()

    @staticmethod
    def execute(keeper):
        # //the rear interpose target will change as the ball's position changes
        # //so it must be updated each update-step 
        keeper.steering.target = keeper.get_rear_interpose_target()
        

        # //if the ball comes in range the keeper traps it and then changes state
        # //to put the ball back in play
        if keeper.is_ball_within_keeper_range():
            keeper.team.pitch.ball.trap()
            keeper.team.pitch.goal_keeper_has_ball = True
            keeper.statemachine.change_state(PutBallBackInPlay)
            return

        # //if ball is within a predefined distance, the keeper moves out from
        # //position to try and intercept it.
        if keeper.ball_within_range_for_intercept() and not keeper.team.is_in_control():
            keeper.statemachine.change_state(InterceptBall)
            # return

        # //if the keeper has ventured too far away from the goal-line and there
        # //is no threat from the opponents he should move back towards it
        if keeper.too_far_from_goal_mouth() and keeper.team.is_in_control():
            keeper.statemachine.change_state(ReturnHome)
            return
        
        
    @staticmethod
    def on_message(keeper, message):
        return False
        
# ------------------------------------------------------------------------------

class InterceptBall(State):
    """
    //  In this state the GP will attempt to intercept the ball using the
    //  pursuit steering behavior, but he only does so so long as he remains
    //  within his home region.
    """

    @staticmethod
    def enter(keeper):
        keeper.steering.behavior_pursuit.activate(keeper.team.pitch.ball)

    @staticmethod
    def exit(keeper):
        keeper.steering.behavior_pursuit.deactivate()

    @staticmethod
    def execute(keeper):
        # //if the goalkeeper moves to far away from the goal he should return to his
        # //home region UNLESS he is the closest player to the ball, in which case,
        # //he should keep trying to intercept it.
        if keeper.too_far_from_goal_mouth() and not keeper.is_closest_player_on_pitch_to_ball():
            keeper.statemachine.change_state(ReturnHome)
            return

        # //if the ball becomes in range of the goalkeeper's hands he traps the 
        # //ball and puts it back in play
        if keeper.is_ball_within_keeper_range():
            keeper.team.pitch.ball.trap()
            keeper.team.pitch.goal_keeper_has_ball = True
            keeper.statemachine.change_state(PutBallBackInPlay)
            return
        
    @staticmethod
    def on_message(keeper, message):
        return False

# ------------------------------------------------------------------------------

class ReturnHome(State):
    """
    //  In this state the goalkeeper simply returns back to the center of
    //  the goal region before changing state back to TendGoal
    """
    
    @staticmethod
    def enter(keeper):
        cx, cy = keeper.get_home_region().center
        keeper.steering.behavior_arrive.activate(Vec(cx, cy))

    @staticmethod
    def exit(keeper):
        keeper.steering.behavior_arrive.deactivate()

    @staticmethod
    def execute(keeper):
        cx, cy = keeper.get_home_region().center
        keeper.steering.behavior_arrive.activate(Vec(cx, cy))
        
        # //if close enough to home or the opponents get control over the ball,
        # //change state to tend goal
        if keeper.is_in_home_region() or not keeper.team.is_in_control():
            keeper.statemachine.change_state(TendGoal)
        
    @staticmethod
    def on_message(keeper, message):
        return False

# ------------------------------------------------------------------------------

class PutBallBackInPlay(State):

    @staticmethod
    def enter(keeper):
        # //let the team know that the keeper is in control
        keeper.team.set_controlling_player(keeper)

        # //send all the players home
        keeper.team.opponents.return_all_field_players_to_home()
        keeper.team.return_all_field_players_to_home()

    @staticmethod
    def exit(keeper):
        pass

    @staticmethod
    def execute(keeper):
        # //test if there are players further forward on the field we might
        # //be able to pass to. If so, make a pass.
        receiver, ball_target = keeper.team.find_pass(keeper, Parameters.max_passing_force, Parameters.min_pass_distance)
        if receiver:
            # //make the pass   
            keeper.team.pitch.ball.kick((ball_target - keeper.team.pitch.ball.position).normalized, Parameters.max_passing_force)

            # //goalkeeper no longer has ball 
            keeper.team.pitch.goal_keeper_has_ball = False

            # //let the receiving player know the ball's comin' at him
            messagedispatcher.dispatch_message(messagedispatcher.SEND_MSG_IMMEDIATELY, \
                        keeper.ID, \
                        receiver.ID, \
                        messagetypes.RECEIVE_BALL, \
                        ball_target.clone())


            # //go back to tending the goal   
            keeper.statemachine.change_state(TendGoal)

            return
            
        keeper.velocity = Vec(0.0, 0.0)
        
    @staticmethod
    def on_message(keeper, message):
        return False

# ------------------------------------------------------------------------------
