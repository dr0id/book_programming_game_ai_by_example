

This is the python implementation of the simple soocer demo 
from the chapter 4 of the book 'Programming AI by example'.

The simple soocer is all about how to implement team 
behavior using statemachines. The important thing to understand
is, that there is a statemachine for the entire team and each player 
is controlled by another statemachine. The interaction between those
two statemachines makes the team behavior. This is also reusing the 
statemachine implementation and message passing system from 
chapter 2 to coordinate. Also the steering behaviors from chapter 3 
are reused for the players movement. But of course you know that 
all already from the book!


This implementation follows the original as close as possible, but
there are differences. The reason is simply because python is different 
than C++. Python method calls are a bit slow in python so many parts would
have been written differently if using python from a beginning.


If you start the game then you see a menu like this:

        (P) PAUSED
        (1) No Aids
        (2) Show IDs
        (3) Show States
        (4) Show Regions
        (5) Show Support Spots
        (6) Show Targets
        (7) Highlight if Threatened
        (8) Show steering behaviors
        (r) reset

By pressing the corresponding key you activate the menu entry.        
        