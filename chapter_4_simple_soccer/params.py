# -*- coding: utf-8 -*-

try:
    import ConfigParser
except:
    import configparser as ConfigParser

_config_parser = ConfigParser.SafeConfigParser()

_config_parser.read("params.ini")

# this would read in all options from all sections
# params = dict( (option, _config_parser.getfloat(section, option)) \
                # for section in _config_parser.sections() \
                # for option in _config_parser.options(section) )

section = "steeringbehaviors"
params = dict( (option, _config_parser.getfloat(section, option)) \
                for option in _config_parser.options(section) )

steeringforcetweaker = params["steeringforcetweaker"]

params["steeringforce"] = params["steeringforce"] * steeringforcetweaker
params["separationweight"] = params["separationweight"] * steeringforcetweaker
params["alignmentweight"] = params["alignmentweight"] * steeringforcetweaker
params["cohesionweight"] = params["cohesionweight"] * steeringforcetweaker
params["obstacleavoidanceweight"] = params["obstacleavoidanceweight"] * steeringforcetweaker
params["wallavoidanceweight"] = params["wallavoidanceweight"] * steeringforcetweaker
params["wanderweight"] = params["wanderweight"] * steeringforcetweaker
params["seekweight"] = params["seekweight"] * steeringforcetweaker
params["fleeweight"] = params["fleeweight"] * steeringforcetweaker
params["arriveweight"] = params["arriveweight"] * steeringforcetweaker
params["pursuitweight"] = params["pursuitweight"] * steeringforcetweaker
params["offsetpursuitweight"] = params["offsetpursuitweight"] * steeringforcetweaker
params["interposeweight"] = params["interposeweight"] * steeringforcetweaker
params["hideweight"] = params["hideweight"] * steeringforcetweaker
params["evadeweight"] = params["evadeweight"] * steeringforcetweaker
params["followpathweight"] = params["followpathweight"] * steeringforcetweaker


class Parameters(object):

    params = params

    show_debug_walls = False

    goal_width = 100

    # //use to set up the sweet spot calculator
    num_support_spots_x = 13
    num_support_spots_y = 6

    # //these values tweak the various rules used to calculate the support spots
    spot_can_pass_score = 2.0
    spot_can_score_from_position_score = 1.0
    spot_dist_from_controlling_player_score = 2.0
    spot_closeness_to_supporting_player_score = 0.0
    spot_ahead_of_attacker_score = 0.0

    # //how many times per second the support spots will be calculated
    support_spot_update_freq = 1.0

    # //the chance a player might take a random pot shot at the goal
    chance_player_attemps_pot_shot = 0.005

    # //this is the chance that a player will receive a pass using the arrive
    # //steering behavior, rather than Pursuit
    chance_of_using_arrive_type_receive_behavior = 0.5

    ball_size = 5.0
    ball_mass = 1.0
    friction = -0.015

    # //the goalkeeper has to be this close to the ball to be able to interact with it
    keeper_in_ball_range = 10.0
    keeper_in_ball_range_sq = keeper_in_ball_range * keeper_in_ball_range
    player_in_target_range = 10.0
    player_in_target_range_sq = player_in_target_range * player_in_target_range

    # //player has to be this close to the ball to be able to kick it. The higher
    # //the value this gets, the easier it gets to tackle.
    player_kicking_distance = 6.0
    player_kicking_distance_sq = player_kicking_distance * player_kicking_distance

    # //the number of times a player can kick the ball per second
    player_kick_frequency = 8

    player_mass = 3.0
    player_max_force = 1.0
    player_max_speed_with_ball = 1.2
    player_max_speed_without_ball = 1.6
    player_max_turn_rate = 0.4
    player_scale = 1.0

    # //when an opponents comes within this range the player will attempt to pass
    # //the ball. Players tend to pass more often, the higher the value
    player_comfort_zone = 60.0
    player_comfort_zone_sq = player_comfort_zone * player_comfort_zone

    # //in the range zero to 1.0. adjusts the amount of noise added to a kick,
    # //the lower the value the worse the players get.
    player_kicking_accuracy = 0.99

    # //the number of times the SoccerTeam::CanShoot method attempts to find
    # //a valid shot
    num_attemps_to_find_valid_strike = 5

    max_dribble_force = 1.5
    max_shooting_force = 6.0
    max_passing_force = 3.0

    # //the distance away from the center of its home region a player
    # //must be to be considered at home
    # # within_range_of_home = 15.0

    # //how close a player must get to a sweet spot before he can change state
    # # within_range_of_sweet_spot = 15.0

    # //the minimum distance a receiving player must be from the passing player
    min_pass_distance = 120.0

    # //the minimum distance a player must be from the goalkeeper before it will
    # //pass the ball
    # # goal_keeper_min_pass_distance = 50.0

    # //this is the distance the keeper puts between the back of the net
    # //and the ball when using the interpose steering behavior
    goal_keeper_tending_distance = 20.0

    # //when the ball becomes within this distance of the goalkeeper he
    # //changes state to intercept the ball
    goal_keeper_intercept_range = 100.0
    goal_keeper_intercept_range_sq = goal_keeper_intercept_range * goal_keeper_intercept_range

    # //how close the ball must be to a receiver before he starts chasing it
    ball_within_receiving_range = 10.0




    # breaking rate of player if steering did not procude a steering force
    player_breaking_rate = 0.8

    non_penetration_constraint = False



