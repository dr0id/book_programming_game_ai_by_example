# -*- coding: utf-8 -*-


from math import pi
from random import random as random_random

QUARTER_PI = pi / 4.0

import messagedispatcher
import messagetypes

from vectors import Vec2 as Vec
from vectors import PI_DIV_180
from state import State
from params import Parameters

# ------------------------------------------------------------------------------

class GlobalPlayerState(State):

    @staticmethod
    def enter(player):
        pass

    @staticmethod
    def exit(player):
        pass

    @staticmethod
    def execute(player):
        # //if a player is in possession and close to the ball reduce his max speed
        if player.is_ball_within_receive_range() and player.is_controlling_player():
            player.max_speed = Parameters.player_max_speed_with_ball
        else:
            player.max_speed = Parameters.player_max_speed_without_ball
        
    @staticmethod
    def on_message(player, telegram):
        if telegram.message == messagetypes.RECEIVE_BALL:
            # //set the target
            player.steering.target = telegram.extra_info

            # //change state 
            player.statemachine.change_state(ReceiveBall)

            return True

        elif telegram.message == messagetypes.SUPPORT_ATTACKER:
            # //if already supporting just return
            if player.statemachine.is_in_state(SupportAttacker):
                return True

            # //set the target to be the best supporting position
            player.steering.target = player.team.get_support_spot()
            
            # //change the state
            player.statemachine.change_state(SupportAttacker)
            return True
            
        elif telegram.message == messagetypes.WAIT:
            # //change the state
            player.statemachine.change_state(Wait)
            return True

        elif telegram.message == messagetypes.GO_HOME:
            player.set_default_home_region()
            player.statemachine.change_state(ReturnToHomeRegion)
            return True
            
        elif telegram.message == messagetypes.PASS_TO_ME:
            # //get the position of the player requesting the pass 
            receiver = telegram.extra_info

            # //if the ball is not within kicking range or their is already a 
            # //receiving player, this player cannot pass the ball to the player
            # //making the request.
            if player.team.receiving_player is not None or \
                    not player.is_ball_within_kicking_range():
                # TODO: make this print optional
                # print("Player {0} cannot make requested pass <cannot kick ball>".format(player.ID))
                return True

            # //make the pass   
            ball = player.team.pitch.ball
            ball.kick(receiver.position - ball.position, Parameters.max_passing_force)

            # print("Player {0} passed ball to requesting player {1}".format(player.ID, receiver.ID))

            # //let the receiver know a pass is coming 
            messagedispatcher.dispatch_message(messagedispatcher.SEND_MSG_IMMEDIATELY,
                                            player.ID,
                                            receiver.ID,
                                            messagetypes.RECEIVE_BALL,
                                            receiver.position.clone())

            # //change state   
            player.statemachine.change_state(Wait)
            
            player.find_support()

            return True
        else:
            print('fieldpalyerstates global state received unknown message!', player.ID, telegram.message, telegram.sender_id)
        

        return False
        
# ------------------------------------------------------------------------------

class ChaseBall(State):

    @staticmethod
    def enter(player):
        player.steering.behavior_seek.activate(player.team.pitch.ball.position)

    @staticmethod
    def exit(player):
        player.steering.behavior_seek.deactivate()

    @staticmethod
    def execute(player):
        # //if the ball is within kicking range the player changes state to KickBall.
        if player.is_ball_within_kicking_range():
            player.statemachine.change_state(KickBall)
            return
                                                                                  
        # //if the player is the closest player to the ball then he should keep
        # //chasing it
        if player.is_closest_team_member_to_ball():
            # TODO: player.steering.target is already set to the ball
            player.steering.target = player.team.pitch.ball.position
            return

        # //if the player is not closest to the ball anymore, he should return back
        # //to his home region and wait for another opportunity
        player.statemachine.change_state(ReturnToHomeRegion)
        return
        
        
    @staticmethod
    def on_message(player, telegram):
        return False

# ------------------------------------------------------------------------------

class Dribble(State):

    @staticmethod
    def enter(player):
        # print('?? set control Dribble enter', player.team.color)
        player.team.set_controlling_player(player)

    @staticmethod
    def exit(player):
        pass

    @staticmethod
    def execute(player):
        dot = player.team.home_goal.facing.dot(player.heading)

        # //if the ball is between the player and the home goal, it needs to swivel
        # // the ball around by doing multiple small kicks and turns until the player 
        # //is facing in the correct direction
        if dot < 0:
            # //the player's heading is going to be rotated by a small amount (Pi/4) 
            # //and then the ball will be kicked in that direction
            direction = player.heading.clone()

            # //calculate the sign (+/-) of the angle between the player heading and the 
            # //facing direction of the goal so that the player rotates around in the 
            # //correct direction
            angle = QUARTER_PI / PI_DIV_180 * -1 * player.team.home_goal.facing.sign(player.heading)
            direction.rotate(angle)

            # //this value works well whjen the player is attempting to control the
            # //ball and turn at the same time
            kicking_force = 0.8
            player.team.pitch.ball.kick(direction, kicking_force)
        else:
            # //kick the ball down the field
            player.team.pitch.ball.kick(player.team.home_goal.facing, Parameters.max_dribble_force)

        # //the player has kicked the ball so he must now change state to follow it
        player.statemachine.change_state(ChaseBall)
        return

    @staticmethod
    def on_message(player, telegram):
        return False

# ------------------------------------------------------------------------------

class ReturnToHomeRegion(State):

    @staticmethod
    def enter(player):
        region = player.get_home_region()
        position = player.steering.target
        small_region = region.inflate(-int(region.width * 0.25), -int(region.height * 0.25))
        if not small_region.collidepoint(position.as_xy_tuple(int)):
            cx, cy = region.center
            player.steering.behavior_arrive.activate(Vec(cx, cy))
        else:
            player.steering.behavior_arrive.activate(position.clone())

    @staticmethod
    def exit(player):
        player.steering.behavior_arrive.deactivate()

    @staticmethod
    def execute(player):
        if player.team.pitch.is_game_on:
            # //if the ball is nearer this player than any other team member  &&
            # //there is not an assigned receiver && the goalkeeper does not gave
            # //the ball, go chase it
            if player.is_closest_team_member_to_ball() and \
                    player.team.receiving_player is not None and \
                    not player.team.pitch.goal_keeper_has_ball:
                player.statemachine.change_state(ChaseBall)
                return

        # //if game is on and close enough to home, change state to wait and set the 
        # //player target to his current position.(so that if he gets jostled out of 
        # //position he can move back to it)
        region = player.get_home_region()
        small_region = region.inflate(-int(region.width * 0.25), -int(region.height * 0.25))
        if player.team.pitch.is_game_on and \
                    small_region.collidepoint(player.position.as_xy_tuple(int)):
            player.steering.target = player.position.clone()
            player.statemachine.change_state(Wait)
            return
        
        # //if game is not on the player must return much closer to the center of his
        # //home region
        elif not player.team.pitch.is_game_on and player.is_at_target():
            player.statemachine.change_state(Wait)
            return
        
    @staticmethod
    def on_message(player, telegram):
        return False

# ------------------------------------------------------------------------------
        
class Wait(State):

    @staticmethod
    def enter(player):
        # //if the game is not on make sure the target is the center of the player's
        # //home region. This is ensure all the players are in the correct positions
        # //ready for kick off
        if not player.team.pitch.is_game_on:
            cx, cy = player.get_home_region().center
            player.steering.target = Vec(cx, cy)
        else:
            player.steering.target = player.position.clone()

    @staticmethod
    def exit(player):
        pass

    @staticmethod
    def execute(player):
        
        # //if the player has been jostled out of position, get back in position  
        if not player.is_at_target():
            # TODO: ???
            assert player.steering.target is not None
            player.steering.behavior_arrive.activate(player.steering.target)
            return
        else:
            player.steering.behavior_arrive.deactivate()
            player.velocity = Vec(0.0, 0.0)
            # //the player should keep his eyes on the ball!
            player.track_ball()
            
        # //if this player's team is controlling AND this player is not the attacker
        # //AND is further up the field than the attacker he should request a pass.
        if player.team.is_in_control() and \
                not player.is_controlling_player() and \
                player.is_ahead_of_attacker():
            # print("player {0} requesting pass WAIT".format(player.ID))
            player.team.request_pass(player)
            return

        if player.team.pitch.is_game_on:
            # //if the ball is nearer this player than any other team member  AND
            # //there is not an assigned receiver AND neither goalkeeper has
            # //the ball, go chase it
            if player.is_closest_team_member_to_ball() and \
                    player.team.receiving_player is None and \
                    not player.team.pitch.goal_keeper_has_ball:
                player.statemachine.change_state(ChaseBall)
                return
        
    @staticmethod
    def on_message(player, telegram):
        return False
        
# ------------------------------------------------------------------------------
        
class KickBall(State):

    @staticmethod
    def enter(player):
        # //let the team know this player is controlling
        # print('?? set control KickBall enter', player.team.color)
        player.team.set_controlling_player(player)
        # //the player can only make so many kick attempts per second.
        if not player.is_ready_for_next_kick():
            # print('! player not ready for kick!', player, player.team.color)
            player.statemachine.change_state(ChaseBall)
            return


    @staticmethod
    def exit(player):
        pass

    @staticmethod
    def execute(player):
        # print('? exec KickBall')
        # //calculate the dot product of the vector pointing to the ball
        # //and the player's heading
        ball = player.team.pitch.ball
        to_ball = ball.position - player.position
        dot = player.heading.dot(to_ball.normalized)

        # //cannot kick the ball if the goalkeeper is in possession or if it is 
        # //behind the player or if there is already an assigned receiver. So just
        # //continue chasing the ball
        if player.team.receiving_player is not None or \
                player.team.pitch.goal_keeper_has_ball or \
                dot < 0:
            player.statemachine.change_state(ChaseBall)
            return

        # /* Attempt a shot at the goal */

        # //if a shot is possible, this vector will hold the position along the 
        # //opponent's goal line the player should aim for.

        # //the dot product is used to adjust the shooting force. The more
        # //directly the ball is ahead, the more forceful the kick
        power = Parameters.max_shooting_force * dot

        # //if it is determined that the player could score a goal from this position
        # //OR if he should just kick the ball anyway, the player will attempt
        # //to make the shot
        do_shoot, ball_target = player.team.can_shoot(ball.position, power)
        if do_shoot or (random_random() < Parameters.chance_player_attemps_pot_shot):

            # //add some noise to the kick. We don't want players who are 
            # //too accurate! The amount of noise can be adjusted by altering
            # //Prm.PlayerKickingAccuracy
            ball_target = ball.add_noise_to_kick(ball.position, ball_target)

            # //this is the direction the ball will be kicked in
            kick_direction = ball_target - ball.position
            ball.kick(kick_direction, power)

            # //change state   
            player.statemachine.change_state(Wait)
            player.find_support()
            return

        # /* Attempt a pass to a player */
        power = Parameters.max_passing_force * dot

        # //if a receiver is found this will point to it
        # //test if there are any potential candidates available to receive a pass
        receiver, ball_target = player.team.find_pass(player, power, Parameters.min_pass_distance)
        # print('?? find pass:', receiver, player.team.color, player.is_threadened(), player)
        # print('???? --> control:', player.team.color, player.team.controlling_player)
        if player.is_threadened() and receiver is not None:
            # //add some noise to the kick
            ball_target = ball.add_noise_to_kick(ball.position, ball_target)
            kick_direction = ball_target - ball.position
            ball.kick(kick_direction, power)

            # //let the receiver know a pass is coming 
            messagedispatcher.dispatch_message(messagedispatcher.SEND_MSG_IMMEDIATELY, \
                                    player.ID, \
                                    receiver.ID, \
                                    messagetypes.RECEIVE_BALL, \
                                    ball_target.clone())

            # //the player should wait at his current position unless instruced
            # //otherwise  
            player.statemachine.change_state(Wait)

            player.find_support()
            return

        else:
            # print('?? find support for team', player.team.color)
            # //cannot shoot or pass, so dribble the ball upfield
            # CHANGE FIRST TO STATE DRIBBLE SO THE CONTROLLING TEAM IS SET
            # AGAIN, BECAUSE FIND SUPPORT NEEDS A CONTROLLING PLAYER!
            player.statemachine.change_state(Dribble)
            player.find_support()

    @staticmethod
    def on_message(player, telegram):
        return False

# ------------------------------------------------------------------------------
        
class ReceiveBall(State):

    @staticmethod
    def enter(player):
        # //let the team know this player is receiving the ball
        player.team.receiving_player = player

        # //this player is also now the controlling player
        # print('?? set control ReceiveBall enter', player.team.color)
        player.team.set_controlling_player(player)

        # //there are two types of receive behavior. One uses arrive to direct
        # //the receiver to the position sent by the passer in its telegram. The
        # //other uses the pursuit behavior to pursue the ball. 
        # //This statement selects between them dependent on the probability
        # //ChanceOfUsingArriveTypeReceiveBehavior, whether or not an opposing
        # //player is close to the receiving player, and whether or not the receiving
        # //player is in the opponents 'hot region' (the third of the pitch closest
        # //to the opponent's goal
        pass_thread_radius = 70.0

        if player.is_in_hot_region() or \
                random_random() < Parameters.chance_of_using_arrive_type_receive_behavior and \
                not player.team.is_opponent_within_radius(player.position, pass_thread_radius):
            player.steering.behavior_arrive.activate(player.team.pitch.ball.position)
        else:
            player.steering.behavior_pursuit.activate(player.team.pitch.ball)

    @staticmethod
    def exit(player):
        player.steering.behavior_arrive.deactivate()
        player.steering.behavior_pursuit.deactivate()
        player.team.receiving_player = None

    @staticmethod
    def execute(player):
        # //if the ball comes close enough to the player or if his team lose control
        # //he should change state to chase the ball
        if player.is_ball_within_receive_range() or not player.team.is_in_control():
            player.statemachine.change_state(ChaseBall)
            return

        if player.steering.behavior_pursuit.is_active():
            player.steering.behavior_pursuit.target_agent = player.team.pitch.ball

        # //if the player has 'arrived' at the steering target he should wait and
        # //turn to face the ball
        if player.is_at_target():
            player.steering.behavior_arrive.deactivate()
            player.steering.behavior_pursuit.deactivate()
            player.track_ball()
            player.velocity = Vec(0.0, 0.0)

    @staticmethod
    def on_message(player, telegram):
        return False

# ------------------------------------------------------------------------------
        
class SupportAttacker(State):

    @staticmethod
    def enter(player):
        # player->Steering()->ArriveOn();
        # player->Steering()->SetTarget(player->Team()->GetSupportSpot());
        player.steering.behavior_arrive.activate(player.team.get_support_spot())

    @staticmethod
    def exit(player):
        # //set supporting player to null so that the team knows it has to 
        # //determine a new one.
        player.team.supporting_player = None
        player.steering.behavior_arrive.deactivate()

    @staticmethod
    def execute(player):
        # //if his team loses control go back home
        if not player.team.is_in_control():
            player.statemachine.change_state(ReturnToHomeRegion)
            return

        # //if the best supporting spot changes, change the steering target
        current_support_spot = player.team.get_support_spot()
        if current_support_spot != player.steering.target:
            player.steering.behavior_arrive.activate(current_support_spot)

        # //if this player has a shot at the goal AND the attacker can pass
        # //the ball to him the attacker should pass the ball to this player
        can_shoot, shot_target = player.team.can_shoot(player.position, Parameters.max_shooting_force)
        if can_shoot:
            # print("player {0} requesting pass 'can shoot' SupportAttacker".format(player.ID))
            player.team.request_pass(player)

        # //if this player is located at the support spot and his team still have
        # //possession, he should remain still and turn to face the ball
        if player.is_at_target():
            player.steering.behavior_arrive.deactivate()
            
            # //the player should keep his eyes on the ball!
            player.track_ball() 
            player.velocity = Vec(0.0, 0.0)
            
            # //if not threatened by another player request a pass
            if not player.is_threadened():
                # print("player {0} requesting pass 'is threadened' SupportAttacker".format(player.ID))
                player.team.request_pass(player)
        
    @staticmethod
    def on_message(player, telegram):
        return False

