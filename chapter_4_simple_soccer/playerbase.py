# -*- coding: utf-8 -*-

import params
import messagedispatcher
import messagetypes
from params import Parameters
from gameentity import MovingEntity
from steeringbehaviors import SteeringBehaviors
from vectors import Vec2 as Vec

# ------------------------------------------------------------------------------

class Role(object):
    # TODO: use int to be faster
    GOAL_KEEPER = "Gaol keeper"
    ATTACKER = "Attacker"
    DEFENDER = "Defender"

class PlayerBase(MovingEntity):

    def __init__(self,
                    home_team,
                    home_region,
                    heading,
                    velocity,
                    mass,
                    max_force,
                    max_speed,
                    max_turn_rate,
                    scale,
                    role
                    ):
        """
        """
        posx, posy = home_team.pitch.get_region_from_index(home_region).center
        MovingEntity.__init__(self,
                 Vec(posx, posy),
                 scale * 1.0,
                 velocity,
                 max_speed,
                 heading,
                 mass,
                 Vec(scale, scale),
                 max_turn_rate,
                 max_force)

        # //this player's role in the team
        self.role = role

        # //a pointer to this player's team
        self.team = home_team

        # //the steering behaviors
        # SteeringBehaviors*      m_pSteering;
        self.steering = SteeringBehaviors(params.params)
        # TODO: use pitch as world, ball as target, self as vehicle

        # //the region that this player is assigned to.
        self._home_region = home_region

        # //the region this player moves to before kickoff
        self._default_region = home_region

        # //the distance to the ball (in squared-space). This value is queried
        # //a lot so it's calculated once each time-step and stored here.
        # double                   m_dDistSqToBall;
        self.dist_sq_to_ball = 0.0

        # //a player's start target is its start position (because it's just waiting)
        cx, cy = self.team.pitch.get_region_from_index(home_region).center
        self.steering.target = Vec(cx, cy)
        # self.steering.behavior_arrive.target_position = Vec(cx, cy)

    # //returns true if there is an opponent within this player's
    # //comfort zone
    def is_threadened(self):
        # //check against all opponents to make sure non are within this
        # //player's comfort zone
        for opp in self.team.opponents.players:
            # //calculate distance to the player. if dist is less than our
            # //comfort zone, and the opponent is infront of the player, return true
            if self.is_position_in_front_of_player(opp.position) and \
                        self.position.get_distance_sq(opp.position) < Parameters.player_comfort_zone_sq:
                return True
        return False

    # //rotates the player to face the ball or the player's current target
    def track_ball(self):
        self.rotate_heading_to_face_position(self.team.pitch.ball.position)

    def track_target(self):
        self.set_heading(self.steering.target - self.position)

    # //this messages the player that is closest to the supporting spot to
    # //change state to support the attacking player
    def find_support(self):
        """
        determines the player who is closest to the SupportSpot and messages him
        to tell him to change state to SupportAttacker
        """    
        # //if there is no support we need to find a suitable player.
        if self.team.supporting_player is None and self.team.is_in_control():
            best_support_player = self.team.determine_best_supporting_attacker()
            self.team.supporting_player = best_support_player
            assert best_support_player is not None
            messagedispatcher.dispatch_message(messagedispatcher.SEND_MSG_IMMEDIATELY,
                                            self.ID,
                                            self.team.supporting_player.ID,
                                            messagetypes.SUPPORT_ATTACKER)
            
        best_support_player = self.team.determine_best_supporting_attacker()

        # //if the best player available to support the attacker changes, update
        # //the pointers and send messages to the relevant players to update their
        # //states
        if best_support_player is not None and \
                    (self.team.supporting_player is not best_support_player):
            if self.team.supporting_player is not None:
                messagedispatcher.dispatch_message(messagedispatcher.SEND_MSG_IMMEDIATELY,
                                                self.ID,
                                                self.team.supporting_player.ID,
                                                messagetypes.GO_HOME)

            self.team.supporting_player = best_support_player
            messagedispatcher.dispatch_message(messagedispatcher.SEND_MSG_IMMEDIATELY,
                                            self.ID,
                                            self.team.supporting_player.ID,
                                            messagetypes.SUPPORT_ATTACKER)
        
    # //returns true if the ball can be grabbed by the goalkeeper
    def is_ball_within_keeper_range(self):
        return self.position.get_distance_sq(self.team.pitch.ball.position) < Parameters.keeper_in_ball_range_sq

    # //returns true if the ball is within kicking range
    def is_ball_within_kicking_range(self):
        return self.team.pitch.ball.position.get_distance_sq(self.position) < Parameters.player_kicking_distance_sq

    # //returns true if a ball comes within range of a receiver
    def is_ball_within_receive_range(self):
        return self.position.get_distance(self.team.pitch.ball.position) < Parameters.ball_within_receiving_range

    # //returns true if the player is located within the boundaries
    # //of his home region
    def is_in_home_region(self):
        region = self.team.pitch.get_region_from_index(self._home_region)
        if self.role == Role.GOAL_KEEPER:
            return region.collidepoint(self.position.as_xy_tuple(int))
        else:
            small_region = region.inflate(-int(region.width * 0.25), -int(region.height * 0.25))
            return small_region.collidepoint(self.position.as_xy_tuple(int))

    # //returns true if this player is ahead of the attacker
    def is_ahead_of_attacker(self):
        return abs(self.position.x - self.team.opponents_goal.center.x) < \
                abs(self.team.controlling_player.position.x - self.team.opponents_goal.center.x)

    # //returns true if a player is located at the designated support spot
    def is_at_support_spot(self):
        # TODO: implement
        raise NotImplementedError("not used in c++ code!?")

    # //returns true if the player is located at his steering target
    def is_at_target(self):
        return self.position.get_distance_sq(self.steering.target) < Parameters.player_in_target_range_sq

    # //returns true if the player is the closest player in his team to
    # //the ball
    def is_closest_team_member_to_ball(self):
        return (self.team.player_closest_to_ball == self)

    # //returns true if the point specified by 'position' is located in
    # //front of the player
    def is_position_in_front_of_player(self, position):
        to_subject = position - self.position
        if to_subject.dot(self.heading) > 0:
            return True
        return False

    # //returns true if the player is the closest player on the pitch to the ball
    def is_closest_player_on_pitch_to_ball(self):
        return self.is_closest_team_member_to_ball() and \
                (self.dist_sq_to_ball < self.team.opponents.dist_sq_to_ball_of_closest_player)

    # //returns true if this player is the controlling player
    def is_controlling_player(self):
        return (self.team.get_controlling_player() == self)

    # //returns true if the player is located in the designated 'hot region' --
    # //the area close to the opponent's goal
    def is_in_hot_region(self):
        # TODO: bug in source, should x coord be sued!?
        # return fabs(Pos().y - Team()->OpponentsGoal()->Center().y ) <
             # Pitch()->PlayingArea()->Length()/3.0;
        return abs(self.position.x - self.team.opponents_goal.center.x) < \
                    self.team.pitch.playing_area.width / 3.0

    # player_role Role()const{return m_PlayerRole;}

    # # double       DistSqToBall()const{return m_dDistSqToBall;}
    # # void        SetDistSqToBall(double val){m_dDistSqToBall = val;}

    # //calculate distance to opponent's/home goal. Used frequently by the passing
    # //methods
    def dist_to_opp_goal(self):
        # //calculate distance to opponent's goal. Used frequently by the passing//methods
        # return fabs(Pos().x - Team()->OpponentsGoal()->Center().x);
        return abs(self.position.x - self.team.opponents_goal.center.x)

    def dist_to_home_goal(self):
        # return fabs(Pos().x - Team()->HomeGoal()->Center().x);
        return abs(self.position.x - self.team.home_goal.center.x)

    def set_default_home_region(self):
        self._home_region = self._default_region

    # # SoccerBall* const        Ball()const;
    # # SoccerPitch* const       Pitch()const;
    # # SteeringBehaviors*const  Steering()const{return m_pSteering;}
    def get_home_region(self):
        return self.team.pitch.get_region_from_index(self._home_region)
    # # const Region* const      HomeRegion()const;
    def set_home_region(self, new_region):
        self._home_region = new_region
    # # SoccerTeam*const         Team()const{return m_pTeam;}




