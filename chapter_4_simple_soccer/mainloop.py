# -*- coding: utf-8 -*-

import pygame
from params import params
from params import Parameters
from playerbase import Role

# ------------------------------------------------------------------------------

GREEN = (0, 100, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
BLUE = (0, 0, 255)
BLACK = (0, 0, 0)
GREY = (180, 180, 180, 128)
YELLOW = (255, 255, 0, 128)
PINK = (255, 0, 255)
BROWN = (100, 50, 0)

class MainLoop(object):

    def __init__(self, world_impl, screen_size=(500, 400)):
        self.screen_size = screen_size
        self.world_impl = world_impl
        self.world = world_impl(self.screen_size)
        # font handling and speed up
        self._font = None
        self._font_small = None
        self._text_cache = {} # {text:surf}

        self.schow_fps = False
        self.do_clear_screen = True
        self.show_debug_vectors = False

    def handle_events(self):
        events = []
        if self.world.draw_text_overlay_on:
            events.append(pygame.event.wait())
        else:
            events.extend(pygame.event.get())
    
        for event in events:
            if not self.world.on_event(event):
                # if world did not handle it
                if event.type == pygame.QUIT:
                    self.running = False
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                            self.running = False
                    elif event.key == pygame.K_r:
                        # reset
                        print("reset")
                        self.world = self.world_impl(self.screen_size)
                    elif event.key == pygame.K_F1:
                        self.schow_fps = not self.schow_fps
                    elif event.key == pygame.K_F2:
                        self.do_clear_screen = not self.do_clear_screen
                    elif event.key == pygame.K_F3:
                        self.show_debug_vectors = not self.show_debug_vectors

    def run(self):
        pygame.init()
        pygame.display.set_caption(self.world.title)
        if self.world.icon:
            pygame.display.set_icon(self.world.icon)
        self.screen = pygame.display.set_mode(self.screen_size, pygame.SRCALPHA)
        clock = pygame.time.Clock()

        # fonts are initialzed here because this can only happen after pygame init
        self._font_small = pygame.font.Font(None, 14)
        self._font = pygame.font.Font(None, 21)

        self.running = True
        
        start_time = pygame.time.get_ticks()
        try:
            while self.running:
                self.handle_events()

                dt_seconds = clock.tick(60) / 1000.0
                # prevent too big timesteps, no good for simulaton
                # this happens when you drag the window
                if dt_seconds > 0.3:
                    dt_seconds = 0
                self.world.update(dt_seconds)


                # draw everything
                # if self.world.crosshair:
                    # self.draw_crosshair(self.world.crosshair)
                # if self.world.draw_path_on:
                    # for vehicle in self.world.vehicles:
                        # self.draw_path(vehicle)

                self.draw_soccer_pitch()


                self.draw_goal(self.world.soccer_pitch.red_goal, RED)
                self.draw_goal(self.world.soccer_pitch.blue_goal, BLUE)

                self.draw_soccerball()
                
                self.draw_players(self.world.soccer_pitch.red_team, RED)
                self.draw_players(self.world.soccer_pitch.blue_team, BLUE)


                # if self.world.draw_wander_info_on:
                    # for vehicle in self.world.vehicles:
                        # self.draw_wander_info(vehicle)
                # if self.show_debug_vectors:
                    # for vehicle in self.world.vehicles:
                        # self.draw_debug_vectors(vehicle)
                # if self.world.draw_obstacles_on:
                    # self.draw_obstacles()
                # if self.world.draw_dbox_on:
                    # for vehicle in self.world.vehicles:
                        # self.draw_dbox(vehicle)
                # if self.world.draw_feelers_on:
                    # for vehicle in self.world.vehicles:
                        # self.draw_feelers(vehicle)
                # if self.world.draw_walls_on:
                    # self.draw_walls()

                if self.world.draw_text_overlay_on:
                    if self.world.text_to_draw:
                        text_width = 0
                        for text in self.world.text_to_draw:
                            surf = self.draw_text(*text, color=WHITE)
                            sw, sh = surf.get_size()
                            if sw > text_width:
                                text_width = sw
                        tposx, tposy, text = self.world.text_to_draw[0]
                        text_height = self.world.text_to_draw[-1][1] - tposy + sh
                        border = 10
                        rect = pygame.Rect(tposx-border, tposy-border, text_width + 2 * border, text_height + 2 * border)
                        # pygame.draw.rect(self.screen, GREY, rect, 0)
                        self.screen.fill(GREY, rect)
                        # draw over the rect
                        for text in self.world.text_to_draw:
                            self.draw_text(*text, color=WHITE)

                # if self.world.draw_view_distance_on:
                    # for vehicle in self.world.vehicles:
                        # self.draw_debug_view_radius(vehicle, params["viewdistance"], (255, 0, 0))
                        # self.draw_debug_view_radius(vehicle, 100, (0, 255, 0))

                # if self.world.draw_bounding_radius_on:
                    # for vehicle in self.world.vehicles:
                        # self.draw_debug_view_radius(vehicle, vehicle.bounding_radius, (150, 0, 255))


                pygame.display.flip()
                if self.do_clear_screen:
                    self.screen.fill(GREEN)
                if self.schow_fps:
                    sw, sh = self.screen_size
                    self.draw_text(sw // 2,  5, "fps: {0}".format(str(clock.get_fps())))
        # except Exception as e:
            # pass
            
        finally:
            end_time = pygame.time.get_ticks()
            print("running for {0} seconds".format((end_time - start_time) / 1000.0))

    # ---- draw routines ---- #

    def draw_soccer_pitch(self):
        # if Parameters.show_debug_regions:
        if self.world.is_show_regions_on:
            neon_green = (0, 255, 0)
            for idx, region in enumerate(self.world.soccer_pitch.regions):
                pygame.draw.rect(self.screen, neon_green, region, 1)
                self.draw_text(region.centerx, region.centery, str(idx), color=neon_green)

        playing_area = self.world.soccer_pitch.playing_area
        pygame.draw.rect(self.screen, WHITE, playing_area, 1)
        pygame.draw.line(self.screen, WHITE, playing_area.midtop, playing_area.midbottom)
        pygame.draw.circle(self.screen, WHITE, playing_area.center, 82, 1)
        pygame.draw.circle(self.screen, WHITE, playing_area.center, 3, 0)

        if Parameters.show_debug_walls:
            self.draw_walls(self.world.soccer_pitch.walls)

        self.draw_support_spots(self.world.soccer_pitch.red_team)
        self.draw_support_spots(self.world.soccer_pitch.blue_team)
        
        dist = 25
        self.draw_text(playing_area.centerx - dist - 50, playing_area.midbottom[1] + 5, "Red: {0}".format(self.world.soccer_pitch.blue_goal.num_goals_scored), color=RED)
        self.draw_text(playing_area.centerx + dist, playing_area.midbottom[1] + 5, "Blue: {0}".format(self.world.soccer_pitch.red_goal.num_goals_scored), color=BLUE)
        
        text = "None"
        controlling_player = None
        if self.world.soccer_pitch.red_team.is_in_control():
            text = "Red"
            controlling_player = self.world.soccer_pitch.red_team.controlling_player.ID
        if self.world.soccer_pitch.blue_team.is_in_control():
            text = "Blue"
            controlling_player = self.world.soccer_pitch.blue_team.controlling_player.ID
        self.draw_text(5, 5, "{0} in control".format(text), color = WHITE)
        self.draw_text(500, 5, "Controlling player: {0}".format(controlling_player), color = WHITE)
        
    def draw_support_spots(self, team):
        if self.world.is_show_supportspots_on:
            if team.is_in_control():
                for spot in team.support_spot_calc.spots:
                    if spot.score > 0:
                        pygame.draw.circle(self.screen, (128, 128, 128), spot.position.as_xy_tuple(int), int(spot.score) + 1, 1)

                if team.support_spot_calc.best_supporting_spot is not None:
                    spot = team.support_spot_calc.best_supporting_spot
                    pygame.draw.circle(self.screen, PINK , spot.position.as_xy_tuple(int), int(spot.score) + 1, 0)

    def draw_goal(self, goal, color):
        lpx = goal.left_post.x
        lpy = goal.left_post.y
        rect = pygame.Rect(lpx, lpy, goal.facing.x * 40, goal.right_post.y - lpy)
        pygame.draw.rect(self.screen, color, rect, 1)

    def draw_soccerball(self):
        ball = self.world.soccer_pitch.ball
        posx, posy = ball.position.as_xy_tuple()
        pygame.draw.circle(self.screen, BLACK, (int(posx), int(posy)), int(ball.bounding_radius), 0)
        pygame.draw.circle(self.screen, WHITE, (int(posx), int(posy)), int(ball.bounding_radius), 1)

    def draw_players(self, team, color):
        for player in team.players:
            px, py = player.position.as_xy_tuple(int)
            if player.is_threadened() and self.world.is_highlight_if_threatened_on:
                pygame.draw.circle(self.screen, YELLOW, (px, py), 10, 0)
                
            if player.role == Role.GOAL_KEEPER:
                heading = player.look_at
            else:
                heading = player.heading
                
            side = heading.perp
            
            points = []
            points.append((player.position + 3 * heading - 10 * side).as_xy_tuple(int))
            points.append((player.position + 3 * heading + 10 * side).as_xy_tuple(int))
            points.append((player.position - 3 * heading +  8 * side).as_xy_tuple(int))
            points.append((player.position - 3 * heading -  8 * side).as_xy_tuple(int))

            pygame.draw.aalines(self.screen, color, True, points, 2)
            pygame.draw.circle(self.screen, color, (px, py), 7, 0)
            pygame.draw.circle(self.screen, BROWN, (px, py), 6, 0)

            if self.world.is_show_steering_behaviors_on:
                hx, hy = (player.heading * 10).as_xy_tuple(int)
                pygame.draw.line(self.screen, WHITE, (px, py), (px + hx, py + hy), 1)

                vx, vy = (player.velocity * 10).as_xy_tuple(int)
                pygame.draw.line(self.screen, RED, (px, py), (px + vx, py + vy), 1)
                vx, vy = (player.velocity.normalized * 10 * player.max_speed).as_xy_tuple(int)
                pygame.draw.circle(self.screen, RED, (px + vx, py + vy), 1, 0)

                for idx, text in enumerate(str(player.steering).split(', ')):
                    self.draw_text(px + 10, py + 5 + 10 * idx, text, False, (0, 150, 200), font=self._font_small)
                if player.role == Role.GOAL_KEEPER:
                    lx, ly = (10 * player.look_at).as_xy_tuple(int)
                    pygame.draw.line(self.screen, PINK, (px, py), (px + lx, py + ly), 1)

            if self.world.is_show_ID_on:
                self.draw_text(px - 10, py - 25, str(player.ID), False, (0, 200, 150))
            if self.world.is_show_states_on:
                self.draw_text(px + 10, py - 10, player.statemachine.get_name_of_current_state(), False, (0, 200, 150))
                
            if self.world.is_show_targets_on:
                pygame.draw.circle(self.screen, RED, player.steering.target.as_xy_tuple(int), 3, 0)


    def draw_crosshair(self, pos):
        x, y = pos.as_xy_tuple()
        # r = 6
        # pygame.draw.circle(screen, (255, 0, 0), (x+1, y) , r, 1)
        r = 10
        pygame.draw.line(self.screen, (255, 0, 0), (x-r, y), (x+r, y), 1)
        pygame.draw.line(self.screen, (255, 0, 0), (x, y-r), (x, y+r), 1)

    def draw_vehicle(self, vehicle):
        pos = vehicle.position
        # p1 = (pos + vehicle.heading * 20).as_xy_tuple()
        p1 = (pos + vehicle.heading * 5 * vehicle.scale).as_xy_tuple()
        p2 = (pos - vehicle.heading * 5 * vehicle.scale + vehicle.side * 3 * vehicle.scale).as_xy_tuple()
        p3 = (pos - vehicle.heading * 5 * vehicle.scale - vehicle.side * 3 * vehicle.scale).as_xy_tuple()
        pygame.draw.aalines(self.screen, vehicle.color, True, [p1, p2, p3], 1)

    def draw_text(self, posx, posy, text, overwrite=False, color=(100, 100, 100), font=None):
        if (text, color) in self._text_cache and not overwrite:
            surf = self._text_cache[(text, color)]
        else:
            _font = self._font
            if font:
                _font = font
            else:
                if not self._font:
                    self._font = pygame.font.Font(None, 21)
                    self._font_small = pygame.font.Font(None, 11)
                    _font = self._font
            if len(self._text_cache) > 2000:
                print("Warning, text cache has more than 2000 entries, reseting")
                self._text_cache = {}
            surf = _font.render(text, 0, color)
            # surf.set_alpha(128)
            # surf = surf.convert_alpha()
            self._text_cache[(text, color)] = surf

        self.screen.blit(surf, (posx, posy))
        return surf

    def draw_wander_info(self, vehicle):
        # green circle
        pos = vehicle.position + vehicle.steering.behavior_wander.distance * vehicle.heading * vehicle.bounding_radius
        rx, ry = pos.as_xy_tuple()
        pygame.draw.circle(self.screen, (0, 255, 0), (int(rx), int(ry)) , int(vehicle.steering.behavior_wander.radius * vehicle.bounding_radius), 1)
        vx, vy = pos.as_xy_tuple()
        self.screen.set_at((int(vx), int(vy)), (0, 255, 0))

        # red circle
        import steeringbehaviors
        # _wander_target should not be used directly (its in vehicle local coordinates)
        target_world = steeringbehaviors.point_to_world_2d(vehicle.steering.behavior_wander.target, vehicle.heading, vehicle.side, vehicle.position)
        pos = vehicle.position + (target_world - vehicle.position) * vehicle.bounding_radius + \
                vehicle.steering.behavior_wander.distance * vehicle.heading * vehicle.bounding_radius
        rx, ry = pos.as_xy_tuple()
        pygame.draw.circle(self.screen, (255, 0, 0), (int(rx), int(ry)), 7, 1)

    def draw_debug_vectors(self, vehicle):
        # vehicle axis, (r, g, b) == (x_axis, y_axis, z_axis)
        pygame.draw.line(self.screen, (255, 0, 0), vehicle.position.as_xy_tuple(), (vehicle.position + vehicle.heading * 20).as_xy_tuple(), 1)
        pygame.draw.line(self.screen, (0, 255, 0), vehicle.position.as_xy_tuple(), (vehicle.position + vehicle.side * 20).as_xy_tuple(), 1)

        # pygame.draw.line(self.screen, (255, 150, 0), vehicle.position.as_xy_tuple(), (vehicle.position + vehicle.velocity).as_xy_tuple(), 1)

        # world axis, (r, g, b) == (x_axis, y_axis, z_axis)
        self.draw_text(160, 180, "(200, 200)")
        pygame.draw.line(self.screen, (255, 0, 0), (200, 200), (230, 200), 1)
        pygame.draw.line(self.screen, (0, 255, 0), (200, 200), (200, 230), 1)

        # steering force
        force_end = vehicle.position + vehicle.steering._steering_force
        pygame.draw.line(self.screen, (150, 0, 255), vehicle.position.as_xy_tuple(), force_end.as_xy_tuple(), 1)

    def draw_obstacles(self):
        for obstacle in self.world.obstacles:
            x, y = obstacle.position.as_xy_tuple()
            pygame.draw.circle(self.screen, (0, 0, 0), (x, y) , obstacle.bounding_radius, 1)

    def draw_dbox(self, vehicle):
        p1 = (vehicle.position + vehicle.steering.behavior_obstacleavoidance.dboxlength * vehicle.heading + vehicle.bounding_radius * vehicle.side).as_xy_tuple()
        p2 = (vehicle.position + vehicle.steering.behavior_obstacleavoidance.dboxlength * vehicle.heading - vehicle.bounding_radius * vehicle.side).as_xy_tuple()
        p3 = (vehicle.position - vehicle.bounding_radius * vehicle.side).as_xy_tuple()
        p4 = (vehicle.position + vehicle.bounding_radius * vehicle.side).as_xy_tuple()
        pygame.draw.lines(self.screen, (200, 200, 200), True, [p1, p2, p3, p4])

    def draw_feelers(self, vehicle):
        #TODO: move conversion methods to world?
        import steeringbehaviors
        for feeler_local in vehicle.feelers:
            feeler = steeringbehaviors.point_to_world_2d(feeler_local, vehicle.heading, vehicle.side, vehicle.position)
            pygame.draw.line(self.screen, (0, 150, 0), vehicle.position.as_xy_tuple(), feeler.as_xy_tuple(), 1)

    def draw_walls(self, walls):
        for wall in walls:
            pygame.draw.line(self.screen, (100, 100, 100), wall.start.as_xy_tuple(), wall.end.as_xy_tuple(), 3)
            if self.world.draw_walls_normals_on:
                middle = (wall.start + wall.end) * 0.5
                pygame.draw.line(self.screen, (255, 0, 255), middle.as_xy_tuple(), (middle + wall.normal * 10).as_xy_tuple(), 1)

    def draw_path(self, vehicle):
        if vehicle.steering.behavior_followpath.path:
            prev_point = vehicle.steering.behavior_followpath.path.waypoints[-1]
            start_idx = 0 if vehicle.steering.behavior_followpath.path.is_closed else 1
            color = (200, 100, 100)
            for point in vehicle.steering.behavior_followpath.path.waypoints[start_idx:]:
                pygame.draw.line(self.screen, color, prev_point.as_xy_tuple(), point.as_xy_tuple(), 1)
                rx, ry = point.as_xy_tuple()
                pygame.draw.circle(self.screen, (255, 0, 0), (int(rx), int(ry)), 4, 0)
                prev_point = point

    def draw_debug_view_radius(self, vehicle, view_range, color=(255, 0, 0)):
        rx, ry = vehicle.position.as_xy_tuple()
        pygame.draw.circle(self.screen, color, (int(rx), int(ry)), int(view_range), 1)
