# -*- coding: utf-8 -*-

from soccerteam import TeamColor
from vectors import Vec2 as Vec
from regulator import Regulator
from params import Parameters

# ------------------------------------------------------------------------------
class SupportSpot(object):
    def __init__(self, pos, score=1.0):
        self.position = pos
        self.score = score

# ------------------------------------------------------------------------------

class SupportSpotCalculator(object):

    def __init__(self, num_x, num_y, team):
        self.team = team
        self.best_supporting_spot = None
        self.spots = []

        play_field = team.pitch.playing_area
        width, height = play_field.size
        width *= 0.9
        height *= 0.8
        slice_x = width / num_x
        slice_y = height / num_y

        left = play_field.left + 0.5 * ((play_field.width - width) + slice_x)
        right = play_field.right - 0.5 * ((play_field.width - width) + slice_x)
        top = play_field.top + 0.5 * ((play_field.height - height) + slice_y)

        for x in range(num_x // 2 - 1):
            for y in range(num_y):
                if team.color == TeamColor.BLUE:
                    v = Vec(left + x * slice_x, top + y * slice_y)
                    ss = SupportSpot(v)
                    self.spots.append(ss)
                else:
                    v = Vec(right - x * slice_x, top + y * slice_y)
                    ss = SupportSpot(v)
                    self.spots.append(ss)

        self.regulator = Regulator(Parameters.support_spot_update_freq)

    def get_best_supporting_spot(self):
        if self.best_supporting_spot is not None:
            return self.best_supporting_spot.position
        else:
            return self.determine_supporting_position()

    def determine_supporting_position(self):
        # //only update the spots every few frames                              
        if not self.regulator.is_ready() and self.best_supporting_spot is not None:
            return self.best_supporting_spot.position

        # //reset the best supporting spot
        self.best_supporting_spot = None

        best_score_so_far = 0.0

        for cur_spot in self.spots:
            # //first remove any previous score. (the score is set to one so that
            # //the viewer can see the positions of all the spots if he has the 
            # //aids turned on)
            cur_spot.score = 1.0

            # //Test 1. is it possible to make a safe pass from the ball's position 
            # //to this position?
            if self.team.is_pass_safe_from_all_opponents(self.team.controlling_player.position, \
                                                        cur_spot.position, \
                                                        None, \
                                                        Parameters.max_passing_force):
                cur_spot.score += Parameters.spot_can_pass_score

            # //Test 2. Determine if a goal can be scored from this position.  
            if self.team.can_shoot(cur_spot.position, Parameters.max_shooting_force):
                cur_spot.score += Parameters.spot_can_score_from_position_score

            # //Test 3. calculate how far this spot is away from the controlling
            # //player. The further away, the higher the score. Any distances further
            # //away than OptimalDistance pixels do not receive a score.
            if self.team.supporting_player is not None:
                optimal_distance = 200.0
                
                dist = self.team.controlling_player.position.get_distance(cur_spot.position)
              
                temp = abs(optimal_distance - dist)

                if temp < optimal_distance:
                    # //normalize the distance and add it to the score
                    cur_spot.score += Parameters.spot_dist_from_controlling_player_score * (optimal_distance - temp) / optimal_distance

            # //check to see if this spot has the highest score so far
            if cur_spot.score > best_score_so_far:
                best_score_so_far = cur_spot.score
                self.best_supporting_spot = cur_spot
        return self.best_supporting_spot.position


