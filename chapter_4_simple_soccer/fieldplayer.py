# -*- coding: utf-8 -*-

from playerbase import PlayerBase
from statemachine import StateMachine
from regulator import Regulator
from params import Parameters
from fieldplayerstates import GlobalPlayerState
from vectors import PI_DIV_180

# ------------------------------------------------------------------------------

class FieldPlayer(PlayerBase):

    def __init__(self,
                    home_team,
                    home_region,
                    start_state,
                    heading,
                    velocity,
                    mass,
                    max_force,
                    max_speed,
                    max_turn_rate,
                    scale,
                    role
                    ):
        """
        """
        PlayerBase.__init__(self,
                    home_team,
                    home_region,
                    heading,
                    velocity,
                    mass,
                    max_force,
                    max_speed,
                    max_turn_rate,
                    scale,
                    role)
        self.statemachine = StateMachine(self)
        if start_state:
            self.statemachine.set_previous_state(start_state)
            self.statemachine.set_global_state(GlobalPlayerState)
            self.statemachine.change_state(start_state)

        self.kick_limiter = Regulator(Parameters.player_kick_frequency)

        # the group is set in soocerpitch.set_players_group()
        self.steering.behavior_separation.activate([])

    def handle_message(self, msg):
        return self.statemachine.handle_message(msg)

    def is_ready_for_next_kick(self):
        return self.kick_limiter.is_ready()

    def update(self, dt):
        # //run the logic for the current state
        # print('?? player update:', self.team.color, self.statemachine.get_name_of_current_state(), self.statemachine, self)
        self.statemachine.update()

        # //calculate the combined steering force
        force = self.steering.calculate(self, self.team.pitch)

        # //if no steering force is produced decelerate the player by applying a
        # //braking force
        if force.is_zero():
            self.velocity *= Parameters.player_breaking_rate

        # //the steering force's side component is a force that rotates the
        # //player about its axis. We must limit the rotation so that a player
        # //can only turn by PlayerMaxTurnRate rads per update.
        # double TurningForce =   m_pSteering->SideComponent();
        # return m_pPlayer->Side().Dot(m_vSteeringForce) * m_pPlayer->MaxTurnRate();
        turning_force = self.side.dot(force) * self.max_turn_rate

        # Clamp(TurningForce, -Prm.PlayerMaxTurnRate, Prm.PlayerMaxTurnRate);
        # clamp, using if is faster than using min(..., max(...))
        turning_force = turning_force if turning_force < self.max_turn_rate else self.max_turn_rate
        turning_force = turning_force if turning_force > -self.max_turn_rate else -self.max_turn_rate

        # //rotate the heading vector (convert to degrees)
        self.heading.rotate(turning_force / PI_DIV_180)

        # //make sure the velocity vector points in the same direction as
        # //the heading vector
        # m_vVelocity = m_vHeading * m_vVelocity.Length();
        self.velocity = self.heading * self.velocity.length

        # //and recreate m_vSide
        self.side = self.heading.perp

        # //now to calculate the acceleration due to the force exerted by
        # //the forward component of the steering force in the direction
        # //of the player's heading
        forward_component = self.heading.dot(force)
        acceleration = self.heading * forward_component / self.mass

        self.velocity += acceleration

        # //make sure player does not exceed maximum velocity
        self.velocity.truncate(self.max_speed)

        # //update the position
        self.position += self.velocity

        # //enforce a non-penetration constraint if desired
        if Parameters.non_penetration_constraint:
            self.enforce_non_penetration(self.team.pitch.get_all_players())

