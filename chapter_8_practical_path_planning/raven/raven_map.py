# -*- coding: utf-8 -*-
from __future__ import print_function, division
import random
import logging

import params
from common import entityregistry
from common.binspace import BinRectSpace
from common.gameentity import BaseGameEntity
from common.graph import SparseGraph, NavGraphEdge, NavGraphNode, INVALID_NODE_INDEX, HandyGraphFunctions
from common.space_2d.vectors import Vec2 as Vec
from common.space_2d.wall import Wall
from common.triggers import TriggerSystem
from raven import object_enumerations, constants, raven_messages
from raven.raven_door import RavenDoor
from raven.raven_triggers import TriggerOnButtonSendMsg, TriggerHealthGiver, TriggerWeaponGiver, TriggerSoundNotify

logger = logging.getLogger(__name__)


class RavenMap(object):

    def __init__(self):
        self.walls = []
        self.trigger_system = TriggerSystem()
        self.spawn_points = []
        self.doors = []
        self.nav_graph = None
        self.cell_space = None
        self.cell_space_neighbourhood_range = 0
        self.size_x = 0
        self.size_y = 0
        self.path_costs = None

    def partition_nav_graph(self):
        logger.info("partitioning new graph")
        if self.cell_space:
            del self.cell_space
        self.cell_space = BinRectSpace(self.size_x, self.size_y, params.NumCellsX, params.NumCellsY)
        for node in self.nav_graph.nodes:
            if node.index == INVALID_NODE_INDEX:  # do not add invalid nodes
                continue
            self.cell_space.add(node)
        return self.cell_space

    def load_map(self, map_name):
        with open(map_name) as f:

            self.clear()

            BaseGameEntity.reset_next_valid_id()

            self.nav_graph = SparseGraph(NavGraphNode, NavGraphEdge, False)

            # load navigation graph
            try:
                self.nav_graph.clear()

                #  get the number of nodes and read them in
                node_count = int(self.read_next_line(f, "node count"))
                for n in range(node_count):
                    __, idx, __, pos_x, __, pos_y = self.read_next_line(f, "node").split()
                    new_node = NavGraphNode(int(pos_x), int(pos_y), int(idx))

                    # when editing graphs it's possible to end up with a situation where some
                    # of the nodes have been invalidated (their id's set to invalid_node_index). Therefore
                    # when a node of index invalid_node_index is encountered, it must still be added.
                    if new_node.index != INVALID_NODE_INDEX:
                        self.nav_graph.add_node(new_node)
                    else:
                        self.nav_graph.nodes.append(new_node)

                        # make sure an edge list is added for each node
                        self.nav_graph.edges.append([])
                        self.nav_graph._next_node_index += 1

                # now add the edges
                edge_count = int(self.read_next_line(f, "edge count"))
                for e in range(edge_count):
                    __, from_node_idx, __, to_node_idx, __, cost, __, flags, __, edge_id = self.read_next_line(f, "edge").split()
                    # logger.debug("add edge %s from %s to %s, cost %s, flags %s", edge_id, from_node_idx, to_node_idx, cost, flags)  # replace print?
                    new_edge = NavGraphEdge(int(from_node_idx), int(to_node_idx), float(cost), int(flags),
                                            int(edge_id))
                    self.nav_graph.add_edge(new_edge)

                # determine the average distance between graph nodes so that we can partition them efficiently
                self.cell_space_neighbourhood_range = HandyGraphFunctions.calculate_average_graph_edge_length(
                    self.nav_graph) + 1

                #  //load in the map size and adjust the client window accordingly
                #     in >> m_iSizeX >> m_iSizeY;
                size_x, size_y = self.read_next_line(f, "map size").split()
                self.size_x = int(size_x)
                self.size_y = int(size_y)

                # partition the graph nodes
                self.partition_nav_graph()

                logger.info("other lines")
                for line in f.readlines():
                    line = line.strip('\n')
                    if line.isspace() or len(line) == 0:
                        logger.info("skipping empty line..")
                        continue
                    logger.info("other %s", line)
                    # line: 0  20 130  20 10  1 0
                    junks = line.split()
                    entity_type = int(junks.pop(0))

                    if entity_type == object_enumerations.type_wall:
                        self._add_wall(junks)
                    elif entity_type == object_enumerations.type_sliding_door:
                        self._add_door(junks)
                    elif entity_type == object_enumerations.type_door_trigger:
                        self._add_door_trigger(junks)
                    elif entity_type == object_enumerations.type_spawn_point:
                        self._add_spawn_point(junks)
                    elif entity_type == object_enumerations.type_health:
                        self._add_health_giver(junks)
                    elif entity_type == object_enumerations.type_shotgun:
                        self._add_weapon_giver(object_enumerations.type_shotgun, junks)
                    elif entity_type == object_enumerations.type_rail_gun:
                        self._add_weapon_giver(object_enumerations.type_rail_gun, junks)
                    elif entity_type == object_enumerations.type_rocket_launcher:
                        self._add_weapon_giver(object_enumerations.type_rocket_launcher, junks)
                    else:
                        raise Exception("unknown entity type: {0}".format(entity_type))

                self.path_costs = HandyGraphFunctions.create_all_pairs_costs_table(self.nav_graph)

                logger.info("cell_space_neighbourhood_range: %s", self.cell_space_neighbourhood_range)
                logger.info("cell size: %s, %s", self.cell_space.cell_size_x, self.cell_space.cell_size_y)
                return True

            except Exception as e:
                logger.info("Exception %s", e)
                return False

    def read_next_line(self, f, info):
        next_line = f.readline()
        next_line = next_line.strip('\n')
        while next_line.isspace() or len(next_line) == 0:
            logger.info("skipping empty line")
            next_line = f.readline()
            next_line = next_line.strip('\n')

        logger.info("%s, %s", info, next_line)
        return next_line

    def clear(self):
        self.trigger_system.clear()
        self.doors = []
        self.walls = []
        self.spawn_points = []
        del self.nav_graph
        self.nav_graph = None
        del self.cell_space
        self.cell_space = None

    def _add_wall(self, junks):
        from_x, from_y, to_x, to_y, n_x, n_y = [float(j) for j in junks]
        nv = Vec(n_x, n_y)
        normal = nv.normalized
        if round(nv.length, 4) != round(normal.length, 4):
            logger.warning("Normalizing wall normal from %s to %s", nv, normal)

        wall = Wall(Vec(from_x, from_y), Vec(to_x, to_y), normal)
        if from_x < 0 or from_x > self.size_x \
            or from_y < 0 or from_y > self.size_y \
            or to_x < 0 or to_x > self.size_x \
            or to_y < 0 or to_y > self.size_y:
            logger.warning("Wall skipped because one coordinate was outside of the screen: %s, %s, %s, %s", from_x, from_y, to_x, to_y)
            return
        self.walls.append(wall)

    def add_sound_trigger(self, source_bot, sound_range):
        self.trigger_system.register(TriggerSoundNotify(source_bot, sound_range))

    def _add_weapon_giver(self, type_of_weapon, junks):
        tr_id, x, y, r, graph_node_index = [float(j) for j in junks]
        pos = Vec(x, y)

        wg = TriggerWeaponGiver(tr_id, pos, r, type_of_weapon)
        graph_node_index = int(graph_node_index)
        wg.graph_node_index = graph_node_index

        wg.add_circular_trigger_region(pos, params.DefaultGiverTriggerRange)
        wg.set_respawn_delay(params.Weapon_RespawnDelay * constants.frame_rate)

        # add it ot the appropriate vectors
        self.trigger_system.register(wg)

        #   //let the corresponding navgraph node point to this object
        node = self.nav_graph.nodes[graph_node_index]
        node.extra_info = wg

        #   //register the entity
        entityregistry.register_entity(wg)

    def _add_door(self, junks):
        new_id = int(junks.pop(0))
        p1_x = float(junks.pop(0))
        p1_y = float(junks.pop(0))
        p2_x = float(junks.pop(0))
        p2_y = float(junks.pop(0))
        # noinspection PyUnusedLocal
        num_trig = int(junks.pop(0))  # not used
        triggers = [int(j) for j in junks]
        door = RavenDoor(self, Vec(p1_x, p1_y), Vec(p2_x, p2_y), triggers, new_id)
        self.doors.append(door)
        entityregistry.register_entity(door)

    def _add_spawn_point(self, junks):
        dummy, x, y, dummy, dummy = [float(j) for j in junks]  # dummy values are artifacts from the map editor
        self.spawn_points.append(Vec(x, y))

    def _add_door_trigger(self, junks):
        new_id = int(junks.pop(0))
        receiver = int(junks.pop(0))
        message_to_send = int(junks.pop(0))
        x = float(junks.pop(0))
        y = float(junks.pop(0))
        r = float(junks.pop(0))
        pos = Vec(x, y)
        assert message_to_send == raven_messages.msg_open_sesame
        tr = TriggerOnButtonSendMsg(new_id, receiver, message_to_send, pos, r)

        self.trigger_system.register(tr)
        entityregistry.register_entity(tr)

    def _add_health_giver(self, junks):
        tr_id, x, y, r, health_given, graph_node_index = [float(j) for j in junks]
        pos = Vec(x, y)
        hg = TriggerHealthGiver(health_given, pos, 1.0, r, int(tr_id))
        hg.set_respawn_delay(params.Health_RespawnDelay * constants.frame_rate)
        graph_node_index = int(graph_node_index)
        hg.graph_node_index = graph_node_index

        self.trigger_system.register(hg)

        #   //let the corresponding navgraph node point to this object
        node = self.nav_graph.nodes[graph_node_index]
        node.extra_info = hg

        entityregistry.register_entity(hg)

    def get_random_spawn_point(self):
        point = random.choice(self.spawn_points)
        return Vec(point.x, point.y)  # make a copy

    def get_random_node_location(self):
        idx = random.randint(0, self.nav_graph.num_active_nodes() - 1)
        return self.nav_graph.nodes[idx].position.clone()

    def calculate_cost_to_travel_between_nodes(self, node_idx1, node_idx2):
        assert 0 <= node_idx1 < len(self.nav_graph.nodes), "invalid index node_idx1 " + str(node_idx1)
        assert 0 <= node_idx2 < len(self.nav_graph.nodes), "invalid index node_idx2 " + str(node_idx2)
        return self.path_costs[node_idx1][node_idx2]
