# -*- coding: utf-8 -*-
import pygame


class GraveMarkers(object):

    class GraveRecord(object):

        def __init__(self, pos, time_created, *args):
            self.position = pos
            self.time_created = time_created
            self.args = args

    def __init__(self, lifetime_in_s):
        self.lifetime = lifetime_in_s * 1000  # convert to ms
        self.grave_list = []

    def update(self):
        current_time = self.get_time()
        self.grave_list = [_g for _g in self.grave_list if current_time - _g.time_created <= self.lifetime]

    @staticmethod
    def get_time():
        return pygame.time.get_ticks()

    def add_grave(self, pos, *args):
        self.grave_list.append(GraveMarkers.GraveRecord(pos, self.get_time(), *args))
