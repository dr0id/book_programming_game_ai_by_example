# -*- coding: utf-8 -*-
from __future__ import print_function, division
import sys
import logging

logger = logging.getLogger(__name__)


class TargetingSystem(object):

    def __init__(self, owner, do_attack):
        self._do_attack = do_attack
        self._owner = owner
        # returns a pointer to the target, None if no current target
        self.target = None

    @property
    def is_target_present(self):
        """returns true if there is a currently assigned target"""
        return self.target is not None

    @property
    def is_target_within_fov(self):
        """returns true if the target is within the field of view of the owner"""
        return self._owner.sensory_memory.is_opponent_within_FOV(self.target)

    @property
    def is_target_shootable(self):
        """returns true if there is a unobstructed line of sight between the target and the owner"""
        return self._owner.sensory_memory.is_opponent_shootable(self.target)

    def update(self):
        """
        Each time this method is called the opponents in the owners sensory
        memory are examined and the closest is assigned to current_target.
        If there no opponents that have had theirs memory records updated
        within the memory span of the owner then the current target is set to None
        """
        closest_dist = sys.maxsize
        self.clear_target()
        if not self._do_attack:
            return

        # grab a list of all the opponents the owner can sense
        sensed_bots = self._owner.sensory_memory.get_list_of_recently_sensed_opponents()

        for cur_bot in sensed_bots:
            # make sure that the bot is alive and that it is not the owner
            if cur_bot.is_alive and cur_bot != self._owner:
                dist = cur_bot.position.get_distance_sq(self._owner.position)

                if dist < closest_dist:
                    closest_dist = dist
                    self.target = cur_bot
                    logger.info("set target %s of bot %s", cur_bot.ID, self._owner.ID)

    def get_last_recorded_position(self):
        """
        return the position the target was las seen. Throws aan exception if
        there is no target currently assigned
        """
        return self._owner.sensory_memory.get_last_recorded_position_of_opponent(self.target)

    def get_time_target_has_been_visible(self):
        """Returns the amount of time the target has been in the field of view"""
        return self._owner.sensory_memory.get_time_opponent_has_been_visible(self.target)

    def get_time_target_has_been_out_of_view(self):
        """Returns the amount of time the target has been out of view"""
        return self._owner.sensory_memory.get_time_opponent_has_been_out_of_view(self.target)

    def clear_target(self):
        """sets the target pointer to None"""
        logger.info("clear target %s of bot %s", self.target.ID if self.target else None, self._owner.ID)
        self.target = None
