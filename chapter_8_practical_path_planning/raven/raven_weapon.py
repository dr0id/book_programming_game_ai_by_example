# -*- coding: utf-8 -*-
from __future__ import print_function, division
import math
import random

import pygame

import params
from common.fuzzy import FuzzyModule, FuzzyOperators, FuzzyHedges
from raven import object_enumerations


class _RavenWeapon(object):

    def __init__(self, weapon_type,
                 default_num_rounds,
                 max_round_carried,
                 rate_of_fire_in_s,
                 ideal_range,
                 projectile_speed,
                 owner):
        # a weapon is always (in this game) carried by a bot
        self._owner = owner

        # an enumeration indicating the type of weapon
        self.weapon_type = weapon_type

        # amount of ammo carried for this weapon
        self.num_rounds_left = default_num_rounds

        #   //fuzzy logic is used to determine the desirability of a weapon. Each weapon
        #   //owns its own instance of a fuzzy module because each has a different rule
        #   //set for inferring desirability.
        self._fuzzy_module = FuzzyModule()

        #   //maximum number of rounds a bot can carry for this weapon
        self._max_round_carried = max_round_carried

        #   //the number of times this weapon can be fired per second
        self._rate_of_fire_in_ms = rate_of_fire_in_s / 1000.0  # convert to ms

        #   //the earliest time the next shot can be taken
        self._time_next_available = pygame.time.get_ticks()

        #   //this is used to keep a local copy of the previous desirability score
        #   //so that we can give some feedback for debugging
        # """returns the desirability score calculated in the last call to
        # get_desirability (just used for debugging)."""
        self.last_desirability_score = 0.0

        #   //this is the preferred distance from the enemy when using this weapon
        self._idea_range = ideal_range

        #   //the max speed of the projectile this weapon fires
        self.max_projectile_speed = projectile_speed

    @property
    def is_ready_for_next_shot(self):
        return True if pygame.time.get_ticks() > self._time_next_available else False

    def update_time_weapon_is_next_available(self):
        """this is called when a shot is fired to update m_dTimeNextAvailable"""
        self._time_next_available = pygame.time.get_ticks() + 1.0 / self._rate_of_fire_in_ms

    def _initialize_fuzzy_module(self):
        """
        this method initializes the fuzzy module with the appropriate fuzzy
        variables and rule base.
        """
        raise NotImplementedError()

    def aim_at(self, target):
        """this method aims the weapon at the given target by rotating the weapons
        owners facing direction (constrained by the bots turning rate). It returns True
        if the weapon is directly facing the target."""
        return self._owner.rotate_facing_toward_position(target)

    def shoot_at(self, target):
        """this discharges a projectile from the weapon at the given target position
        (provided the weapon is ready to be discharged... every weapon has its
        own rate of fire"""
        raise NotImplementedError()

    def get_desirability(self, dist_to_target):
        """this method returns a value representing the esirability of using the
        weapon. This is used by the I to select the most suitable weapon for
        a bots current situation. This value is calculated using fuzzy logic."""
        raise NotImplementedError()

    def decrement_rounds(self):
        if self.num_rounds_left > 0:
            self.num_rounds_left -= 1

    def increment_rounds(self, num):
        self.num_rounds_left += num
        self.num_rounds_left = self.num_rounds_left if self.num_rounds_left <= self._max_round_carried else self._max_round_carried
        self.num_rounds_left = self.num_rounds_left if self.num_rounds_left > 0 else 0


class RailGun(_RavenWeapon):

    def __init__(self, owner):
        _RavenWeapon.__init__(self, object_enumerations.type_rail_gun,
                              params.RailGun_DefaultRounds,
                              params.RailGun_MaxRoundsCarried,
                              params.RailGun_FiringFreq,
                              params.RailGun_IdealRange,
                              params.Slug_MaxSpeed,
                              owner)
        # setup fuzzy module
        self._initialize_fuzzy_module()

    def _initialize_fuzzy_module(self):
        distance_to_target = self._fuzzy_module.create_FLV("DistanceToTarget")

        target_close = distance_to_target.add_left_shoulder_set("Target_Close", 0, 25, 150)
        target_medium = distance_to_target.add_triangular_set("Target_Medium", 25, 150, 300)
        target_far = distance_to_target.add_right_shoulder_set("Target_Far", 150, 300, 1000)

        desirability = self._fuzzy_module.create_FLV("Desirability")

        very_desirable = desirability.add_right_shoulder_set("VeryDesirable", 50, 75, 100)
        desirable = desirability.add_triangular_set("Desirable", 25, 50, 75)
        undesirable = desirability.add_left_shoulder_set("Undesirable", 0, 25, 50)

        ammo_status = self._fuzzy_module.create_FLV("AmmoStatus")
        ammo_loads = ammo_status.add_right_shoulder_set("Ammo_Loads", 15, 30, 100)
        ammo_okay = ammo_status.add_triangular_set("Ammo_Okay", 0, 15, 30)
        ammo_low = ammo_status.add_triangular_set("Ammo_Low", 0, 0, 15)

        self._fuzzy_module.add_rule(FuzzyOperators.FzAnd(target_close, ammo_loads), FuzzyHedges.FzFairly(desirable))
        self._fuzzy_module.add_rule(FuzzyOperators.FzAnd(target_close, ammo_okay), FuzzyHedges.FzFairly(desirable))
        self._fuzzy_module.add_rule(FuzzyOperators.FzAnd(target_close, ammo_low), undesirable)

        self._fuzzy_module.add_rule(FuzzyOperators.FzAnd(target_medium, ammo_loads), very_desirable)
        self._fuzzy_module.add_rule(FuzzyOperators.FzAnd(target_medium, ammo_okay), desirable)
        self._fuzzy_module.add_rule(FuzzyOperators.FzAnd(target_medium, ammo_low), desirable)

        self._fuzzy_module.add_rule(FuzzyOperators.FzAnd(target_far, ammo_low), FuzzyHedges.FzVery(very_desirable))
        self._fuzzy_module.add_rule(FuzzyOperators.FzAnd(target_far, ammo_okay), FuzzyHedges.FzVery(very_desirable))
        self._fuzzy_module.add_rule(FuzzyOperators.FzAnd(target_far, ammo_low), very_desirable)

    def shoot_at(self, target):
        if self.num_rounds_left > 0 and self.is_ready_for_next_shot:
            # fire round
            self._owner.world.add_rail_gun_slug(self._owner, target)

            self.update_time_weapon_is_next_available()
            self.num_rounds_left -= 1

            # add a trigger to the game so that the other bots can hear  this shot
            # (provided they are within range)
            self._owner.world.raven_map.add_sound_trigger(self._owner, params.RailGun_SoundRange)

    def get_desirability(self, dist_to_target):
        if self.num_rounds_left == 0:
            self.last_desirability_score = 0.0
        else:
            # fuzzify distance and amount of ammo
            self._fuzzy_module.fuzzify("DistanceToTarget", dist_to_target)
            self._fuzzy_module.fuzzify("AmmoStatus", self.num_rounds_left)
            self.last_desirability_score = self._fuzzy_module.de_fuzzify("Desirability",
                                                                         FuzzyModule.DeFuzzifyMethod.max_av)

        return self.last_desirability_score


class ShotGun(_RavenWeapon):
    def __init__(self, owner):
        _RavenWeapon.__init__(self,
                              object_enumerations.type_shotgun,
                              params.ShotGun_DefaultRounds,
                              params.ShotGun_MaxRoundsCarried,
                              params.ShotGun_FiringFreq,
                              params.ShotGun_IdealRange,
                              params.Pellet_MaxSpeed,
                              owner)

        # how much shot the each shell contains
        self._num_balls_in_shell = params.ShotGun_NumBallsInShell
        # how much the shot spreads out when a cartridge is discharged
        self._spread = params.ShotGun_Spread

        self._initialize_fuzzy_module()

    def _initialize_fuzzy_module(self):
        distance_to_target = self._fuzzy_module.create_FLV("DistanceToTarget")

        target_close = distance_to_target.add_left_shoulder_set("Target_Close", 0, 25, 150)
        target_medium = distance_to_target.add_triangular_set("Target_Medium", 25, 150, 300)
        target_far = distance_to_target.add_right_shoulder_set("Target_Far", 150, 300, 1000)

        desirability = self._fuzzy_module.create_FLV("Desirability")

        very_desirable = desirability.add_right_shoulder_set("VeryDesirable", 50, 75, 100)
        desirable = desirability.add_triangular_set("Desirable", 25, 50, 75)
        undesirable = desirability.add_left_shoulder_set("Undesirable", 0, 25, 50)

        ammo_status = self._fuzzy_module.create_FLV("AmmoStatus")
        ammo_loads = ammo_status.add_right_shoulder_set("Ammo_Loads", 30, 60, 100)
        ammo_okay = ammo_status.add_triangular_set("Ammo_Okay", 0, 30, 60)
        ammo_low = ammo_status.add_triangular_set("Ammo_Low", 0, 0, 30)

        self._fuzzy_module.add_rule(FuzzyOperators.FzAnd(target_close, ammo_loads), very_desirable)
        self._fuzzy_module.add_rule(FuzzyOperators.FzAnd(target_close, ammo_okay), very_desirable)
        self._fuzzy_module.add_rule(FuzzyOperators.FzAnd(target_close, ammo_low), very_desirable)

        self._fuzzy_module.add_rule(FuzzyOperators.FzAnd(target_medium, ammo_loads), very_desirable)
        self._fuzzy_module.add_rule(FuzzyOperators.FzAnd(target_medium, ammo_okay), desirable)
        self._fuzzy_module.add_rule(FuzzyOperators.FzAnd(target_medium, ammo_low), undesirable)

        self._fuzzy_module.add_rule(FuzzyOperators.FzAnd(target_far, ammo_loads), desirable)
        self._fuzzy_module.add_rule(FuzzyOperators.FzAnd(target_far, ammo_okay), undesirable)
        self._fuzzy_module.add_rule(FuzzyOperators.FzAnd(target_far, ammo_low), undesirable)

    def shoot_at(self, target):
        if self.num_rounds_left > 0 and self.is_ready_for_next_shot:
            # a shotgun cartridge contains lots of tiny metal balls called pellets.
            # Therefore, every time the shotgun is discharged we have to calculated
            # the spread of the pellets and add one for each trajectory
            for b in range(self._num_balls_in_shell):
                # determine deviation from target using a bell curve type distribution
                deviation = (random.random() * 2 * self._spread) - self._spread
                adjusted_target = target - self._owner.position

                # rotate the target vector by the deviation
                adjusted_target.rotate(math.degrees(deviation))

                self._owner.world.add_shot_gun_pellet(self._owner, adjusted_target + self._owner.position)

            self.num_rounds_left -= 1
            self.update_time_weapon_is_next_available()

            # add a trigger to the game so that the other bots can hear this shot (provided they are within range)
            self._owner.world.raven_map.add_sound_trigger(self._owner, params.ShotGun_SoundRange)

    def get_desirability(self, dist_to_target):
        if self.num_rounds_left == 0:
            return 0.0
        else:
            # fuzzify distance and amount of ammo
            self._fuzzy_module.fuzzify("DistanceToTarget", dist_to_target)
            self._fuzzy_module.fuzzify("AmmoStatus", self.num_rounds_left)
            self.last_desirability_score = self._fuzzy_module.de_fuzzify("Desirability",
                                                                         FuzzyModule.DeFuzzifyMethod.max_av)

        return self.last_desirability_score


class RocketLauncher(_RavenWeapon):
    def __init__(self, owner):
        _RavenWeapon.__init__(self,
                              object_enumerations.type_rocket_launcher,
                              params.RocketLauncher_DefaultRounds,
                              params.RocketLauncher_MaxRoundsCarried,
                              params.RocketLauncher_FiringFreq,
                              params.RocketLauncher_IdealRange,
                              params.Rocket_MaxSpeed,
                              owner)

        self._initialize_fuzzy_module()

    def _initialize_fuzzy_module(self):
        dist_to_target = self._fuzzy_module.create_FLV("DistToTarget")

        target_close = dist_to_target.add_left_shoulder_set("Target_Close", 0, 25, 150)
        target_medium = dist_to_target.add_triangular_set("Target_Medium", 25, 150, 300)
        target_far = dist_to_target.add_right_shoulder_set("Target_Far", 150, 300, 1000)

        desirability = self._fuzzy_module.create_FLV("Desirability")
        very_desirable = desirability.add_right_shoulder_set("VeryDesirable", 50, 75, 100)
        desirable = desirability.add_triangular_set("Desirable", 25, 50, 75)
        undesirable = desirability.add_left_shoulder_set("Undesirable", 0, 25, 50)

        ammo_status = self._fuzzy_module.create_FLV("AmmoStatus")
        ammo_loads = ammo_status.add_right_shoulder_set("Ammo_loads", 10, 30, 100)
        ammo_okay = ammo_status.add_triangular_set("Ammo_Okay", 0, 10, 30)
        ammo_low = ammo_status.add_triangular_set("Ammo_Low", 0, 0, 10)

        self._fuzzy_module.add_rule(FuzzyOperators.FzAnd(target_close, ammo_loads), undesirable)
        self._fuzzy_module.add_rule(FuzzyOperators.FzAnd(target_close, ammo_okay), undesirable)
        self._fuzzy_module.add_rule(FuzzyOperators.FzAnd(target_close, ammo_low), undesirable)

        self._fuzzy_module.add_rule(FuzzyOperators.FzAnd(target_medium, ammo_loads), very_desirable)
        self._fuzzy_module.add_rule(FuzzyOperators.FzAnd(target_medium, ammo_okay), very_desirable)
        self._fuzzy_module.add_rule(FuzzyOperators.FzAnd(target_medium, ammo_low), desirable)

        self._fuzzy_module.add_rule(FuzzyOperators.FzAnd(target_far, ammo_loads), desirable)
        self._fuzzy_module.add_rule(FuzzyOperators.FzAnd(target_far, ammo_okay), undesirable)
        self._fuzzy_module.add_rule(FuzzyOperators.FzAnd(target_far, ammo_low), undesirable)

    def shoot_at(self, target):
        if self.num_rounds_left > 0 and self.is_ready_for_next_shot:
            # fire off a rocket!
            self._owner.world.add_rocket(self._owner, target)
            self.num_rounds_left -= 1
            self.update_time_weapon_is_next_available()

            # add a trigger to the game so that the other bots can hear this shot (provided they are within range)
            self._owner.world.raven_map.add_sound_trigger(self._owner, params.RocketLauncher_SoundRange)

    def get_desirability(self, dist_to_target):
        if self.num_rounds_left == 0:
            return 0
        else:
            # fuzzify distance and amount of ammo
            self._fuzzy_module.fuzzify("DistToTarget", dist_to_target)
            self._fuzzy_module.fuzzify("AmmoStatus", self.num_rounds_left)

            self.last_desirability_score = self._fuzzy_module.de_fuzzify("Desirability",
                                                                         FuzzyModule.DeFuzzifyMethod.max_av)

        return self.last_desirability_score


class Blaster(_RavenWeapon):

    def __init__(self, owner):
        _RavenWeapon.__init__(self,
                              object_enumerations.type_blaster,
                              params.Blaster_DefaultRounds,
                              params.Blaster_MaxRoundsCarried,
                              params.Blaster_FiringFreq,
                              params.Blaster_IdealRange,
                              params.Bolt_MaxSpeed,
                              owner)
        # setup the fuzzy module
        self._initialize_fuzzy_module()

    def _initialize_fuzzy_module(self):
        """set up some fuzzy variables and rules"""
        dist_to_target = self._fuzzy_module.create_FLV("DistToTarget")
        target_close = dist_to_target.add_left_shoulder_set("Target_Close", 0, 25, 150)
        target_medium = dist_to_target.add_triangular_set("Target", 25, 150, 300)
        target_far = dist_to_target.add_right_shoulder_set("Target_Far", 150, 300, 1000)

        desirability = self._fuzzy_module.create_FLV("Desirability")
        very_desirable = desirability.add_right_shoulder_set("VeryDesirable", 50, 75, 100)
        desirable = desirability.add_triangular_set("Desirable", 25, 50, 75)
        undesirable = desirability.add_left_shoulder_set("Undesirable", 0, 25, 50)

        self._fuzzy_module.add_rule(target_close, desirable)
        self._fuzzy_module.add_rule(target_medium, FuzzyHedges.FzVery(undesirable))
        self._fuzzy_module.add_rule(target_far, FuzzyHedges.FzVery(undesirable))

    def shoot_at(self, target):
        if self.is_ready_for_next_shot:
            # fire
            self._owner.world.add_bolt(self._owner, target)

            self.update_time_weapon_is_next_available()

            # add a trigger to the game so that the other bots can hear this shot
            # (provided they are within range)
            self._owner.world.raven_map.add_sound_trigger(self._owner, params.Blaster_SoundRange)

    def get_desirability(self, dist_to_target):
        # fuzzify distance and amount of ammo
        self._fuzzy_module.fuzzify("DistToTarget", dist_to_target)
        self.last_desirability_score = self._fuzzy_module.de_fuzzify("Desirability", FuzzyModule.DeFuzzifyMethod.max_av)
        return self.last_desirability_score
