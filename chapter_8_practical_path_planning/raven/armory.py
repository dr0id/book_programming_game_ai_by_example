# -*- coding: utf-8 -*-
from __future__ import print_function, division
import sys

import pygame

import params
from common import messagedispatcher
from common.gameentity import MovingEntity
from common.space_2d.geometry import get_line_segment_circle_closest_intersection_point, dist_to_line_segment_sq, \
    find_closest_point_of_intersection_with_walls
from common.space_2d.vectors import Vec2
from raven import raven_messages


class RavenProjectile(MovingEntity):

    def __init__(self, target,
                 world,
                 shooter_id,
                 origin,
                 heading,
                 damage,
                 radius,
                 max_speed,
                 mass,
                 max_force):
        max_turn_rate = 0  # irrelevant here, all shots go straight
        MovingEntity.__init__(self, origin.clone(), radius, Vec2(0, 0), max_speed, heading, mass, Vec2(radius, radius),
                              max_turn_rate, max_force)

        #  the ID of the entity that fired this
        self.shooter_id = shooter_id

        #  the place the projectile is aimed at
        self.target = target

        #  a pointer to the world data
        self.world = world

        #  where the projectile was fired from
        self.origin = origin.clone()

        #  how much damage the projectile inflicts
        self.damage = damage

        #  is it dead? A dead projectile is one that has come to the end of its
        #  trajectory and cycled through any explosion sequence. A dead projectile
        #  can be removed from the world environment and deleted.
        self.is_dead = False

        #  this is set to true as soon as a projectile hits something
        self.has_impacted = False

        #  the position where this projectile impacts an object
        self.impact_point = None

        #  this is stamped with the time this projectile was instantiated. This is
        #  to enable the shot to be rendered for a specific length of time
        self.time_of_creation = pygame.time.get_ticks()

    def update(self, dt=0.0):
        raise NotImplementedError()

    def get_closest_intersecting_bot(self, pos_from, pos_to):
        closest_bot = None
        closest_so_far = sys.maxsize

        # iterate through all entities checking against the line segment FromTo
        for _bot in self.world.bots:
            # make sure we don't check against the shooter of the projectile
            if _bot.ID != self.shooter_id:
                # if the distance to the FromTo is less than the entity bounding radius
                # then there is an intersection
                if dist_to_line_segment_sq(pos_from, pos_to, _bot.position) < _bot.bounding_radius:
                    # test to see if this is the closest so far
                    dist = _bot.position.get_distance_sq(self.origin)

                    if dist < closest_so_far:
                        closest_so_far = dist
                        closest_bot = _bot

        return closest_bot

    def get_list_of_intersecting_bots(self, pos_from, pos_to):
        # this will hold any bots that are intersecting with the line segment
        hits = []

        # iterate through all entities checking against the line segment FromTo
        for _bot in self.world.bots:
            # make sure we don't check against the shooter of the projectile
            if _bot.ID != self.shooter_id:
                # if the distance to the FromTo is less than the entity bounding radius
                # then there is an intersection
                if dist_to_line_segment_sq(pos_from, pos_to, _bot.position) < _bot.bounding_radius:
                    hits.append(_bot)

        return hits


class Bolt(RavenProjectile):

    def __init__(self, shooter_bot, target):
        RavenProjectile.__init__(self, target,
                                 shooter_bot.world,
                                 shooter_bot.ID,
                                 shooter_bot.position,
                                 shooter_bot.facing,
                                 params.Bolt_Damage,
                                 params.Bolt_Scale,
                                 params.Bolt_MaxSpeed,
                                 params.Bolt_Mass,
                                 params.Bolt_MaxForce)

    def update(self, dt=0.0):
        if not self.has_impacted:
            self.velocity = self.max_speed * self.heading

            # make sure vehicle does not exceed maximum speed
            self.velocity.truncate(self.max_speed)

            # update position
            self.position += self.velocity

            #     //if the projectile has reached the target position or it hits an entity
            #     //or wall it should explode/inflict damage/whatever and then mark itself
            #     //as dead

            #     //test to see if the line segment connecting the bolt's current position
            #     //and previous position intersects with any bots.
            hit = self.get_closest_intersecting_bot(self.position - self.velocity, self.position)

            if hit:
                self.has_impacted = True
                self.is_dead = True

                # send a message to the bot to let him know it has been hit and who the shot came from
                messagedispatcher.dispatch_message(messagedispatcher.SEND_MSG_IMMEDIATELY,
                                                   self.shooter_id,
                                                   hit.ID,
                                                   raven_messages.msg_take_that_MF,
                                                   self.damage)

            # test for impact with a
            is_intersecting, _point, _dist = find_closest_point_of_intersection_with_walls(
                self.position - self.velocity,
                self.position,
                self.world.raven_map.walls)
            if is_intersecting:
                self.is_dead = True
                self.has_impacted = True
                self.position = _point
                return


class Pellet(RavenProjectile):

    def __init__(self, shooter_bot, target):
        RavenProjectile.__init__(self, target,
                                 shooter_bot.world,
                                 shooter_bot.ID,
                                 shooter_bot.position,
                                 shooter_bot.facing,
                                 params.Pellet_Damage,
                                 params.Pellet_Scale,
                                 params.Pellet_MaxSpeed,
                                 params.Pellet_Mass,
                                 params.Pellet_MaxForce)

        self._time_shot_is_visible_to_player = params.Pellet_Persistence * 1000  # to ms
        self.impact_point = None

    def test_for_impact(self):
        """Tests the trajectory of the pellet for an impact"""
        # a shot gun shell is an instantaneous projectile so it only gets the change to update once
        self.has_impacted = True

        # first find the closest wall that this ray intersects with.
        # Then we can test against all entities within this range
        is_intersecting, _point, _dist = find_closest_point_of_intersection_with_walls(
            self.origin,
            self.position,
            self.world.raven_map.walls
        )

        self.impact_point = _point.clone()

        #  test to see if the ray between the current position of the shell and
        #  the start position intersects with any bots.
        hit = self.get_closest_intersecting_bot(self.origin, _point)

        # if no bots hit just return
        if not hit:
            return

        # determine the impact point with the bots bounding circle so that the
        # shell can be rendered properly
        is_intersecting, _point = get_line_segment_circle_closest_intersection_point(self.origin,
                                                                                     _point,
                                                                                     hit.position,
                                                                                     hit.bounding_radius)
        self.impact_point = _point.clone()

        # send a message to the bot to let him know it has been hit and who the shot came from
        messagedispatcher.dispatch_message(messagedispatcher.SEND_MSG_IMMEDIATELY,
                                           self.shooter_id,
                                           hit.ID,
                                           raven_messages.msg_take_that_MF,
                                           self.damage)

    def update(self, dt=0.0):
        if not self.has_impacted:
            # calculate the steering force
            desired_velocity = (self.target - self.position).normalized * self.max_speed

            sf = desired_velocity - self.velocity

            # update the position
            acceleration = sf / self.mass

            self.velocity += acceleration

            # make sure vehicle does not exceed maximum velocity
            self.velocity.truncate(self.max_speed)

            # update position
            self.position += self.velocity

            self.test_for_impact()
        elif not self.is_visible_to_player():
            self.is_dead = True

    def is_visible_to_player(self):
        """returns true if the shot is still to be rendered"""
        return pygame.time.get_ticks() < self.time_of_creation + self._time_shot_is_visible_to_player


class Rocket(RavenProjectile):

    def __init__(self, shooter, target):
        RavenProjectile.__init__(self,
                                 target,
                                 shooter.world,
                                 shooter.ID,
                                 shooter.position,
                                 shooter.facing,
                                 params.Rocket_Damage,
                                 params.Rocket_Scale,
                                 params.Rocket_MaxSpeed,
                                 params.Rocket_Mass,
                                 params.Rocket_MaxForce)

        # the radius of damage once the rocket has impacted
        self._blast_radius = params.Rocket_BlastRadius

        # this is used to render the splash when the rocket impacts
        self.current_blast_radius = 0.0

    def update(self, dt=0.0):
        if not self.has_impacted:
            self.velocity = self.max_speed * self.heading
            self.velocity.truncate(self.max_speed)

            self.position += self.velocity

            self.test_for_impact()
        else:
            self.current_blast_radius += params.Rocket_ExplosionDecayRate

            # when the rendered blast circle becomes equal in size of the blast radius
            # the rocket can be removed from the game
            if self.current_blast_radius >= self._blast_radius:
                self.is_dead = True

    def test_for_impact(self):
        #     //if the projectile has reached the target position or it hits an entity
        #     //or wall it should explode/inflict damage/whatever and then mark itself
        #     //as dead
        #
        #
        #     //test to see if the line segment connecting the rocket's current position
        #     //and previous position intersects with any bots.
        hit = self.get_closest_intersecting_bot(self.position - self.velocity, self.position)

        if hit is not None:
            self.has_impacted = True

            # send message to the bot to let him know its been hit, and who the the came from
            messagedispatcher.dispatch_message(messagedispatcher.SEND_MSG_IMMEDIATELY,
                                               self.shooter_id,
                                               hit.ID,
                                               raven_messages.msg_take_that_MF,
                                               self.damage)
            # test for bots within blast radius and inflict damage
            self.inflict_damage_on_bots_within_blast_radius()

        # test for impact with wall
        is_intersecting, _point, _dist = find_closest_point_of_intersection_with_walls(self.position - self.velocity,
                                                                                       self.position,
                                                                                       self.world.raven_map.walls)
        if is_intersecting:
            self.has_impacted = True
            # test for bots within the blast radius and inflict damage
            self.inflict_damage_on_bots_within_blast_radius()

            self.position = _point.clone()
            return

        # test to see if rocket has reached target position. If so, test for
        # all bots in vicinity
        tolerance = 0.5
        if self.position.get_distance_sq(self.target) <= tolerance * tolerance:
            self.has_impacted = True
            self.inflict_damage_on_bots_within_blast_radius()

    def inflict_damage_on_bots_within_blast_radius(self):
        """ If the rocket has impacted we test all bots to see if they are within the
            blast radius and reduce their health accordingly
        """
        for cur_bot in self.world.bots:
            if self.position.get_distance(cur_bot.position) < self._blast_radius + cur_bot.bounding_radius:
                # send a message to the bot to let it know its been hit, and who the shot came from
                messagedispatcher.dispatch_message(messagedispatcher.SEND_MSG_IMMEDIATELY,
                                                   self.shooter_id,
                                                   cur_bot.ID,
                                                   raven_messages.msg_take_that_MF,
                                                   self.damage)


class Slug(RavenProjectile):

    def __init__(self, shooter, target):
        RavenProjectile.__init__(self,
                                 target,
                                 shooter.world,
                                 shooter.ID,
                                 shooter.position,
                                 shooter.facing,
                                 params.Slug_Damage,
                                 params.Slug_Scale,
                                 params.Slug_MaxSpeed,
                                 params.Slug_Mass,
                                 params.Slug_MaxForce)
        self.time_shot_is_visible = params.Slug_Persistence * 1000  # to ms
        self.impact_point = None

    def is_visible_to_player(self):
        return pygame.time.get_ticks() < self.time_of_creation + self.time_shot_is_visible

    def update(self, dt=0.0):
        if not self.has_impacted:
            # calculate the steering force
            desired_velocity = (self.target - self.position).normalized * self.max_speed

            sf = desired_velocity - self.velocity

            # update the position
            acceleration = sf / self.mass

            self.velocity += acceleration

            # make sure the lug does not exceed maximum velocity
            self.velocity.truncate(self.max_speed)

            # update position
            self.position += self.velocity

            self.test_for_impact()
        elif not self.is_visible_to_player():
            self.is_dead = True

    def test_for_impact(self):
        # a rail gun slug travels VERY fast. It only gets the chance to update once
        self.has_impacted = True

        # first find the closest wall that this ray intersects with.
        # Then we can test against all entities within this range.
        is_intersecting, _point, _dist = find_closest_point_of_intersection_with_walls(self.origin,
                                                                                       self.position,
                                                                                       self.world.raven_map.walls)
        self.impact_point = _point.clone()

        # test to see if the ray between the current position of the slug and
        # the start position intersects with any bots.
        hits = self.get_list_of_intersecting_bots(self.origin, self.position)
        # if no bots hit just return
        if not hits:
            return

        # give some damage
        for bot in hits:
            messagedispatcher.dispatch_message(messagedispatcher.SEND_MSG_IMMEDIATELY,
                                               self.shooter_id,
                                               bot.ID,
                                               raven_messages.msg_take_that_MF,
                                               self.damage)
