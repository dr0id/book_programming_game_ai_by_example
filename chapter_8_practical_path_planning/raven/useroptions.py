# -*- coding: utf-8 -*-
from __future__ import print_function, division


class UserOptions(object):
    showGraph = False

    showNodeIndices = False

    showPathOfSelectedBot = False

    showTargetOfSelectedBot = False

    showOpponentsSensedBySelectedBot = False

    showOnlyShowBotsInTargetsFOV = False

    showGoalsOfSelectedBot = False

    showGoalAppraisals = False

    showWeaponAppraisals = False

    smoothPathsQuick = False

    smoothPathsPrecise = False

    showBotIDs = False

    showBotHealth = False

    showScore = False

    showDebugInfo = False

    doAttack = True

    showNextNode = False

    showNumSearches = False


