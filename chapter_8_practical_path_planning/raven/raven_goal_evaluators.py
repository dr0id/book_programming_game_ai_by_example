# -*- coding: utf-8 -*-
from __future__ import print_function, division

import params
from raven import raven_feature, object_enumerations


class GoalEvaluator(object):
    def __init__(self, character_bias):
        #   when the desirability score for a goal has been evaluated it is multiplied
        #   by this value. It can be used to create bots with preferences based upon
        #   their personality
        self.character_bias = character_bias
        self.last_desirability = 0.0

    def calculate_desirability(self, bot):
        """
        returns a score between 0 and 1 representing the desirability of the
        strategy the concrete subclass represents
        """
        raise NotImplementedError()

    def set_goal(self, bot):
        """adds the appropriate goal to the given bot's brain"""
        raise NotImplementedError()


class GetHealthGoalEvaluator(GoalEvaluator):

    def __init__(self, bias):
        GoalEvaluator.__init__(self, bias)

    def set_goal(self, owner):
        owner.brain.add_goal_get_item(object_enumerations.type_health)

    def calculate_desirability(self, bot):
        #   //first grab the distance to the closest instance of a health item
        #   double Distance = Raven_Feature::DistanceToItem(pBot, type_health);
        distance = raven_feature.distance_to_item(bot, object_enumerations.type_health)

        #   //if the distance feature is rated with a value of 1 it means that the
        #   //item is either not present on the map or too far away to be worth
        #   //considering, therefore the desirability is zero
        if distance == 1:
            self.last_desirability = 0.0
        else:
            #     //value used to tweak the desirability
            tweaker = params.Bot_HealthGoalTweaker

            #     //the desirability of finding a health item is proportional to the amount
            #     //of health remaining and inversely proportional to the distance from the
            #     //nearest instance of a health item.
            desirability = tweaker * (1 - raven_feature.health(bot)) / (
                raven_feature.distance_to_item(bot, object_enumerations.type_health))

            #     //ensure the value is in the range 0 to 1
            desirability = 0 if desirability < 0 else (1 if desirability > 1 else desirability)

            #     //bias the value according to the personality of the bot
            desirability *= self.character_bias
            self.last_desirability = desirability
        return self.last_desirability


class ExploreGoalEvaluator(GoalEvaluator):

    def __init__(self, bias):
        GoalEvaluator.__init__(self, bias)

    def calculate_desirability(self, bot):
        des = 0.05
        des *= self.character_bias
        self.last_desirability = des
        return des

    def set_goal(self, bot):
        bot.brain.add_goal_explore()


class AttackTargetGoalEvaluator(GoalEvaluator):

    def __init__(self, bias):
        GoalEvaluator.__init__(self, bias)

    def calculate_desirability(self, bot):
        des = 0.0

        #   //only do the calculation if there is a target present
        if bot.targeting_sys.is_target_present:
            tweaker = params.Bot_AggroGoalTweaker
            des = tweaker * raven_feature.health(bot) * raven_feature.total_weapon_strength(bot)

            #      //bias the value according to the personality of the bot
            des *= self.character_bias

        self.last_desirability = des
        return des

    def set_goal(self, bot):
        bot.brain.add_goal_attack_target()


class GetWeaponGoalEvaluator(GoalEvaluator):

    def __init__(self, bias, weapon_type, tweaker):
        GoalEvaluator.__init__(self, bias)
        self.tweaker = tweaker
        self.weapon_type = weapon_type

    def calculate_desirability(self, bot):
        #   //grab the distance to the closest instance of the weapon type
        dist = raven_feature.distance_to_item(bot, self.weapon_type)

        #   //if the distance feature is rated with a value of 1 it means that the
        #   //item is either not present on the map or too far away to be worth
        #   //considering, therefore the desirability is zero
        des = 0.0
        if dist == 1:
            pass
        else:
            #    //value used to tweak the desirability
            tweaker = self.tweaker

            health = raven_feature.health(bot)
            weapon_strength = raven_feature.individual_weapon_strength(bot, self.weapon_type)

            des = (tweaker * health * (1 - weapon_strength)) / dist

            #     //ensure the value is in the range 0 to 1
            des = 0 if des < 0 else (1 if des > 1 else des)

            des *= self.character_bias

        self.last_desirability = des
        return des

    def set_goal(self, bot):
        bot.brain.add_goal_get_item(self.weapon_type)
