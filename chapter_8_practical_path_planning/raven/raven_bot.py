# -*- coding: utf-8 -*-
from __future__ import print_function, division
import math

import params
from common import messagedispatcher
from common.gameentity import MovingEntity
from common.regulator import Regulator
from common.space_2d.vectors import Vec2
from common.steeringbehaviors import SteeringBehavior, SteeringParameters
from raven import constants, raven_messages
from raven.object_enumerations import type_bot
from raven.raven_goals import GoalThink
from raven.raven_graphtimesliced import PathPlanner
from raven.raven_sensorymemory import SensoryMemory
from raven.raven_targetingsystem import TargetingSystem
from raven.raven_weapon_sys import WeaponSystem


class _BotStatus(object):
    alive = 0
    dead = 1
    spawning = 2


class RavenBot(MovingEntity):
    Status = _BotStatus

    def __init__(self, world, position, do_attack):
        radius = params.Bot_Radius
        velocity = Vec2(0.0, 0.0)
        max_speed = params.Bot_MaxSpeed
        heading = Vec2(1.0, 0.0)
        mass = params.Bot_Mass
        scale = Vec2(params.Bot_Scale, params.Bot_Scale)
        max_turn_rate = params.Bot_MaxHeadTurnRate
        max_force = params.Bot_MaxForce
        MovingEntity.__init__(self, position, radius, velocity, max_speed,
                              heading, mass, scale, max_turn_rate, max_force)

        self.max_health = params.Bot_MaxHealth
        self.health = params.Bot_MaxHealth
        self.world = world
        self.num_updates_hit_persistent = constants.frame_rate * params.HitFlashTime
        self.hit = False
        self.score = 0
        self.status = RavenBot.Status.spawning
        self.is_possessed = False
        self.field_of_view = math.radians(params.Bot_FOV)

        self.entity_type = type_bot

        #   //a bot starts off facing in the direction it is heading
        self.facing = Vec2(self.heading.x, self.heading.y)

        self.path_planner = PathPlanner(self)
        steering_parameters = SteeringParameters()
        # hardcoded values for the bots
        steering_parameters.WanderRad = 1.2  # radius of the constraining circle for the wander behavior
        steering_parameters.WanderDist = 2.0  # distance the wander circle is projected in fron of the agent
        steering_parameters.WanderJitterPerSec = 40.0  # the maximum amount of displacement along the circle each frame

        steering_parameters.SeparationWeight = params.SeparationWeight
        steering_parameters.WanderWeight = params.WanderWeight
        steering_parameters.WallAvoidanceWeight = params.WallAvoidanceWeight
        steering_parameters.ViewDistance = params.ViewDistance
        steering_parameters.WallDetectionFeelerLength = params.WallDetectionFeelerLength
        self.WallDetectionFeelerLength = params.WallDetectionFeelerLength
        steering_parameters.SeekWeight = params.SeekWeight
        steering_parameters.ArriveWeight = params.ArriveWeight

        self.steering = SteeringBehavior(self, world, steering_parameters)
        self.steering.summing_method = self.steering.PRIORITIZED
        self.steering.deceleration = self.steering.NORMAL

        self.brain = GoalThink(self, None if do_attack else 0.0)

        self.targeting_sys = TargetingSystem(self, do_attack)
        self.weapon_sys = WeaponSystem(self, params.Bot_ReactionTime, params.Bot_AimAccuracy, params.Bot_AimPersistence)
        self.sensory_memory = SensoryMemory(self, params.Bot_MemorySpan)

        # create the regulators
        self._weapons_selection_regulator = Regulator(params.Bot_WeaponSelectionFrequency)
        self._goal_arbitration_regulator = Regulator(params.Bot_GoalAppraisalUpdateFreq)
        self._target_selection_regulator = Regulator(params.Bot_TargetingUpdateFreq)
        self._trigger_test_regulator = Regulator(params.Bot_TriggerUpdateFreq)
        self._vision_update_regulator = Regulator(params.Bot_VisionUpdateFreq)

    @property
    def is_read_for_trigger_update(self):
        return self._trigger_test_regulator.is_ready()

    @property
    def is_alive(self):
        return self.status == self.Status.alive

    @property
    def is_dead(self):
        return self.status == self.Status.dead

    @property
    def is_spawning(self):
        return self.status == self.Status.spawning

    def spawn(self, position):
        self.status = self.Status.alive
        self.brain.remove_all_sub_goals()
        self.targeting_sys.clear_target()
        self.position = position
        self.weapon_sys.initialize()
        self.health = self.max_health  # restore health to max

    def update(self):
        #   //process the currently active goal. Note this is required even if the bot
        #   //is under user control. This is because a goal is created whenever a user
        #   //clicks on an area of the map that necessitates a path planning request.
        self.brain.process()

        #   //Calculate the steering force and update the bot's velocity and position
        self.update_movement()

        #   //if the bot is under AI control but not scripted
        if not self.is_possessed:
            #     //examine all the opponents in the bots sensory memory and select one
            #     //to be the current target
            if self._target_selection_regulator.is_ready():
                self.targeting_sys.update()
            #     //appraise and arbitrate between all possible high level goals
            if self._goal_arbitration_regulator.is_ready():
                self.brain.arbitrate()

            #     //update the sensory memory with any visual stimulus
            if self._vision_update_regulator.is_ready():
                self.sensory_memory.update_vision()

            #     //select the appropriate weapon to use from the weapons currently in
            #     //the inventory
            if self._weapons_selection_regulator.is_ready():
                self.weapon_sys.select_weapon()

            #     //this method aims the bot's current weapon at the current target
            #     //and takes a shot if a shot is possible
            self.weapon_sys.take_aim_and_shoot()

    def update_movement(self):
        #   //calculate the combined steering force
        #   Vector2D force = m_pSteering->Calculate();
        force = self.steering.calculate()

        #   //if no steering force is produced decelerate the player by applying a
        #   //braking force
        if force.is_zero():
            self.velocity *= 0.8

        #   //calculate the acceleration
        acceleration = force / self.mass

        #   //update the velocity
        self.velocity += acceleration

        #   //make sure vehicle does not exceed maximum velocity
        self.velocity.truncate(self.max_speed)

        #   //update the position
        self.position += self.velocity

        #   //if the vehicle has a non zero velocity the heading and side vectors must
        #   //be updated
        if not self.velocity.is_zero():
            self.heading = self.velocity.normalized
            self.side = self.heading.perp

    def create_feelers(self):
        """
        Returns feelers in local coordinates.
        """
        feelers = []
        feeler_length = self.WallDetectionFeelerLength
        feelers.append(self.position + self.heading * feeler_length)
        feelers.append(self.position + (self.heading + self.side) * feeler_length * 0.5 * 0.70710678118654752440084436210485)
        feelers.append(self.position + (self.heading - self.side) * feeler_length * 0.5 * 0.70710678118654752440084436210485)
        return feelers

    def handle_message(self, msg):
        # print("bot", self.ID, "received message", msg.sender_id, msg.receiver_id, msg.message, msg.extra_info)

        # first see if the current goal accepts the message
        if self.brain.handle_message(msg):
            return True

        # handle any messages not handled by the goals
        if msg.message == raven_messages.msg_take_that_MF:
            # just return if already dead or spawning
            if self.is_dead or self.is_spawning:
                return True
            # the extra info field of the telegram carries the amount of damage
            self.reduce_health(msg.extra_info)

            # if this bot is now dead let the shooter know
            if self.is_dead:
                messagedispatcher.dispatch_message(messagedispatcher.SEND_MSG_IMMEDIATELY,
                                                   self.ID,
                                                   msg.sender_id,
                                                   raven_messages.msg_you_got_me,
                                                   None)
            return True
        elif msg.message == raven_messages.msg_you_got_me:
            self.score += 1

            # the bot this bot has just killed should be removed as the target
            self.targeting_sys.clear_target()

            return True

        elif msg.message == raven_messages.msg_gun_shot_sound:
            # add the source of this sound to the bots percepts
            other_bot = msg.extra_info
            self.sensory_memory.update_with_sound_source(other_bot)
            return True

        elif msg.message == raven_messages.msg_user_has_removed_bot:
            bot = msg.extra_info
            self.sensory_memory.remove_bot_from_memory(bot)

            # if the removed bot is the target, make sure the target is cleared
            if bot == self.targeting_sys.target:
                self.targeting_sys.clear_target()
            return True

        return False

    def increase_health(self, health_given):
        self.health += health_given
        if self.health > self.max_health:
            self.health = self.max_health

    def reduce_health(self, amount):
        self.health -= amount

        if self.health <= 0:
            self.status = self.Status.dead

        self.hit = True
        self.num_updates_hit_persistent = constants.frame_rate * params.HitFlashTime

    def take_possession(self):
        """this is called to allow a human player to control the bot"""
        if not self.is_spawning or self.is_dead:
            self.is_possessed = True
            print("Player possesses bot", self.ID)

    def exorcise(self):
        """called when a human is exorcised from this bot and the AI takes control"""
        self.is_possessed = False

        # whe the player is exorcised then the bot should resume normal service
        self.brain.add_goal_explore()

        print("Player is exorcised from bot", self.ID)

    def calculate_time_to_reach_position(self, pos):
        """
        returns a value indicating the time in seconds it will take the bot
        to reach the given position at its current speed.
        """
        time_to_reach_position = self.position.get_distance(pos) / (self.max_speed * constants.frame_rate)
        return time_to_reach_position * params.time_to_reach_position_tweaker

    def is_at_position(self, pos):
        """
        returns true if the bot is close to the given position
        """
        tolerance = 10.0
        return self.position.get_distance_sq(pos) < tolerance * tolerance

    def has_los_to(self, pos):
        """returns true if the bot has line of sight to the given position."""
        return self.world.is_los_okay(self.position, pos)

    def can_step_left(self):
        """
        returns true if there is space enough to step in the indicated direction
        If true PositionOfStep will be assigned the offset position
        """
        step_distance = self.bounding_radius * 2

        pos_of_step = self.position - self.facing.perp * step_distance - self.facing.perp * self.bounding_radius

        return self.can_walk_to(pos_of_step), pos_of_step

    def can_step_right(self):
        """
        returns true if there is space enough to step in the indicated direction
        If true PositionOfStep will be assigned the offset position
        """
        step_distance = self.bounding_radius * 2

        pos_of_step = self.position + self.facing.perp * step_distance + self.facing.perp * self.bounding_radius

        return self.can_walk_to(pos_of_step), pos_of_step

    def can_walk_to(self, to_pos):
        return not self.world.is_path_obstructed(self.position, to_pos, self.bounding_radius)

    def rotate_facing_toward_position(self, target):
        to_target = (target - self.position).normalized

        dot = self.facing.dot(to_target)

        # clamp to rectify an rounding errors
        dot = -1 if dot < -1 else (1 if dot > 1 else dot)

        # determine the angle between the heading vector and the target
        angle = math.acos(dot)

        # return true if the bots facing is withing weapon_aim_tolerance degs of facing the target
        weapon_aim_tolerance = 0.01  # approx 2 deg

        if angle < weapon_aim_tolerance:
            self.facing = to_target
            return True

        # clamp the amount to turn to the max turn rate
        if angle > self.max_turn_rate:
            angle = self.max_turn_rate

        # notice how the direction of rotation has to be determined  when
        # creating the rotation matrix
        self.facing.rotate(math.degrees(angle * self.facing.sign(to_target)))

        return False

    def get_target_bot(self):
        return self.targeting_sys.target

    def can_walk_between(self, from_pos, to_pos):
        return not self.world.is_path_obstructed(from_pos, to_pos, self.bounding_radius)
