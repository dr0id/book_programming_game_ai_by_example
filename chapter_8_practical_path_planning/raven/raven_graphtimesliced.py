# -*- coding: utf-8 -*-
from __future__ import print_function, division
import sys
import logging
from heapq import heappush, heappop, heapify

from common import messagedispatcher
from common.graph import NavGraphEdge, HeuristicEuclidian, INVALID_NODE_INDEX
from common.triggers import TriggerRespawning
from raven import raven_messages
from raven.object_enumerations import name_of_type
from raven.raven_triggers import TriggerWeaponGiver
from raven.useroptions import UserOptions

logger = logging.getLogger(__name__)


class PathEdge(object):

    def __init__(self, source, destination, behavior, door_id=0):
        self.door_id = door_id
        self.behavior = behavior
        self.destination = destination
        self.source = source


TARGET_NOT_FOUND = 0
TARGET_FOUND = 1
SEARCH_INCOMPLETE = 2


class _GraphSearchTimeSliced(object):

    def cycle_once(self):
        """When called, this method runs the algorithm through one search cycle. The
        method returns an enumerated value (target_found, target_not_found, search_incomplete)
        indicating the status of the search."""
        raise NotImplementedError()

    def get_SPT(self):
        """Returns the vector of edges that the algorithm has examined."""
        raise NotImplementedError()

    def get_cost_to_target(self):
        """Returns the total cost to the target."""
        raise NotImplementedError()

    def get_path_to_target(self):
        """Returns a list of nodes indexes that comprise the shortest path from the source to the target."""
        raise NotImplementedError()

    def get_path_as_path_edges(self):
        """Returns the path as a list of PathEdges."""
        raise NotImplementedError()


class GraphSearchAStarTimeSliced(_GraphSearchTimeSliced):

    def __init__(self, graph, source, target, heuristic=None):
        self._graph = graph

        # //indexed into my node. Contains the 'real' accumulative cost to that node
        self._g_costs = [0] * graph.num_nodes()

        # //indexed into by node. Contains the cost from adding m_GCosts[n] to
        # //the heuristic cost from n to the target node. This is the vector the
        # //iPQ indexes into.
        self._f_costs = [0] * graph.num_nodes()

        # //this vector contains the edges that comprise the shortest path tree -
        # //a directed subtree of the graph that encapsulates the best paths from
        # //every node on the SPT to the source node.
        self._shortest_path_tree = [None] * graph.num_nodes()  # edges

        # //this is an indexed (by node) vector of 'parent' edges leading to nodes
        # //connected to the SPT but that have not been added to the SPT yet. This is
        # //a little like the stack or queue used in BST and DST searches.
        self._search_frontier = [None] * graph.num_nodes()  # edges

        self._heuristic = heuristic
        if self._heuristic is None:
            self._heuristic = graph.HeuristicDijkstra

        # the source and target node indices
        self._source_idx = source
        self._target_idx = target

        # //create an indexed priority queue of nodes. The nodes with the
        # //lowest overall F cost (G+H) are positioned at the front.
        self._pq = []

        # put the source node on the queue
        heappush(self._pq, [self._f_costs[self._source_idx], self._source_idx])

    def cycle_once(self):
        # if the PQ is empty the target has not been found
        if not self._pq:
            return TARGET_NOT_FOUND

        # get lowest cost node from the queue
        cost, next_closest_node_index = heappop(self._pq)

        # put the node on the SPT
        self._shortest_path_tree[next_closest_node_index] = self._search_frontier[next_closest_node_index]

        # if the target has been found exit
        if next_closest_node_index == self._target_idx:
            return TARGET_FOUND

        # now to test all the edges attached to this node
        for edge in self._graph.edges[next_closest_node_index]:
            # //calculate the heuristic cost from this node to the target (H)
            h_cost = self._heuristic.calculate(self._graph, self._target_idx, edge.idx_to)

            # //calculate the 'real' cost to this node from the source (G)
            g_cost = self._g_costs[next_closest_node_index] + edge.cost

            # //if the node has not been added to the frontier, add it and update
            # //the G and F costs
            if self._search_frontier[edge.idx_to] is None:
                self._f_costs[edge.idx_to] = g_cost + h_cost
                self._g_costs[edge.idx_to] = g_cost

                heappush(self._pq, [self._f_costs[edge.idx_to], edge.idx_to])

                self._search_frontier[edge.idx_to] = edge

            # //if this node is already on the frontier but the cost to get here
            # //is cheaper than has been found previously, update the node
            # //costs and frontier accordingly.
            elif (g_cost < self._g_costs[edge.idx_to]) and (self._shortest_path_tree[edge.idx_to] is None):

                old_cost = self._f_costs[edge.idx_to]
                self._f_costs[edge.idx_to] = g_cost + h_cost
                self._g_costs[edge.idx_to] = g_cost

                idx = self._pq.index([old_cost, edge.idx_to])
                self._pq[idx][0] = self._f_costs[edge.idx_to]

                # //because the cost is less than it was previously, the PQ must be
                # //re-sorted to account for this.
                heapify(self._pq)
                # this is faster than heapify and dos the same thing
                # _siftup(pq, idx)

                self._search_frontier[edge.idx_to] = edge

        # there are still nodes to explore
        return SEARCH_INCOMPLETE

    def get_SPT(self):
        """Returns the vector of edges that the algorithm has examined"""
        return list(self._shortest_path_tree)

    def get_cost_to_target(self):
        """Returns the total cost to the target."""
        return self._g_costs[self._target_idx]

    def get_path_to_target(self):
        path = []

        # just return an empty path if no target or no path found
        if self._target_idx < 0:
            return path

        nd = self._target_idx

        path.append(nd)

        while (nd != self._source_idx) and (self._shortest_path_tree[nd] is not None):
            nd = self._shortest_path_tree[nd].idx_from
            path.append(nd)

        path.reverse()
        return path

    def get_path_as_path_edges(self):
        path = []

        # just return an empty path if no target or no path found
        if self._target_idx < 0:
            return path

        nd = self._target_idx

        while (nd != self._source_idx) and (self._shortest_path_tree[nd] is not None):
            edge = self._shortest_path_tree[nd]
            pe = PathEdge(self._graph.nodes[edge.idx_from].position,
                          self._graph.nodes[edge.idx_to].position,
                          edge.flags,
                          edge.do_intersecting_entity)
            pe.idx_from = edge.idx_from
            pe.idx_to = edge.idx_to
            path.append(pe)
            nd = edge.idx_from

        path.reverse()
        return path


class GraphSearchDijTimeSliced(_GraphSearchTimeSliced):

    def __init__(self, graph, source, target, terminal_condition):

        self._terminal_condition = terminal_condition

        # //a reference to the graph to be searched
        self._graph = graph

        # //this vector contains the edges that comprise the shortest path tree -
        # //a directed subtree of the graph that encapsulates the best paths from
        # //every node on the SPT to the source node.
        self._shortest_path_tree = [None] * graph.num_nodes()  # edges

        # //this is indexed into by node index and holds the total cost of the best
        # //path found so far to the given node. For example, m_CostToThisNode[5]
        # //will hold the total cost of all the edges that comprise the best path
        # //to node 5, found so far in the search (if node 5 is present and has
        # //been visited)
        self._cost_to_this_node = [0] * graph.num_nodes()  # floats

        # //this is an indexed (by node) vector of 'parent' edges leading to nodes
        # //connected to the SPT but that have not been added to the SPT yet. This is
        # //a little like the stack or queue used in BST and DST searches.
        self._search_frontier = [None] * graph.num_nodes()  # edges

        # the source and target node indices
        self._source = source
        self._target = target

        self._pq = []

        # put the source node on the queue
        heappush(self._pq, [self._cost_to_this_node[self._source], self._source])

    def cycle_once(self):
        # if the PQ is empty the target has not been found
        if not self._pq:
            return TARGET_NOT_FOUND

        # get lowest cost node from the queue
        next_cost, next_closest_node_index = heappop(self._pq)

        # move this node from the frontier to the spanning tree
        self._shortest_path_tree[next_closest_node_index] = self._search_frontier[next_closest_node_index]

        if self._terminal_condition.is_satisfied(self._graph, self._target, next_closest_node_index):
            # make a note of the node index that has satisfied the condition. This
            # is so we can work backwards from the index to extract the path from the
            # shortest path tree.
            self._target = next_closest_node_index
            return TARGET_FOUND

        # now to test all the edges attached to this node
        for edge in self._graph.edges[next_closest_node_index]:
            # the total cost to the node this edge points to is the cost to the
            # current node plus the cost of the edge connecting them
            # new_cost = next_cost + edge.cost
            new_cost = self._cost_to_this_node[next_closest_node_index] + edge.cost

            # //if this edge has never been on the frontier make a note of the cost
            # //to get to the node it points to, then add the edge to the frontier
            # //and the destination node to the PQ.
            if self._search_frontier[edge.idx_to] is None:
                self._cost_to_this_node[edge.idx_to] = new_cost
                heappush(self._pq, [new_cost, edge.idx_to])
                self._search_frontier[edge.idx_to] = edge

                # //else test to see if the cost to reach the destination node via the
                # //current node is cheaper than the cheapest cost found so far. If
                # //this path is cheaper, we assign the new cost to the destination
                # //node, update its entry in the PQ to reflect the change and add the
                # //edge to the frontier
            elif new_cost < self._cost_to_this_node[edge.idx_to] and \
                    self._shortest_path_tree[edge.idx_to] is None:

                cost = self._cost_to_this_node[edge.idx_to]
                # pq.remove([cost, edge.idx_to])
                pos = self._pq.index([cost, edge.idx_to])
                self._pq[pos][0] = new_cost
                self._cost_to_this_node[edge.idx_to] = new_cost

                # //because the cost is less than it was previously, the PQ must be
                # //re-sorted to account for this.
                heapify(self._pq)
                # this is faster than heapify and dos the same thing
                # _siftup(pq, pos)

                self._search_frontier[edge.idx_to] = edge

        # there are still nodes to explore
        return SEARCH_INCOMPLETE

    def get_SPT(self):
        pass

    def get_cost_to_target(self):
        pass

    def get_path_to_target(self):
        path = []
        # //just return an empty path if no path to target found or if
        # //no target has been specified
        if self._target < 0:
            return path

        nd = self._target
        path.append(nd)
        while (nd != self._source) and (self._shortest_path_tree[nd] is not None):
            nd = self._shortest_path_tree[nd].idx_from
            path.append(nd)

        path.reverse()
        return path

    def get_path_as_path_edges(self):
        path = []
        # //just return an empty path if no path to target found or if
        # //no target has been specified
        if self._target < 0:
            return path

        nd = self._target
        # path.append(nd)
        while (nd != self._source) and (self._shortest_path_tree[nd] is not None):
            edge = self._shortest_path_tree[nd]
            pe = PathEdge(self._graph.nodes[edge.idx_from].position,
                          self._graph.nodes[edge.idx_to].position,
                          edge.flags,
                          edge.do_intersecting_entity)
            path.append(pe)
            nd = edge.idx_from

        path.reverse()
        return path


class _TerminalCondition(object):
    def is_satisfied(self, graph, target_idx, current_node_idx):
        raise NotImplementedError()


class TerminalConditionFindActiveTrigger(_TerminalCondition):
    """The search will terminate when the currently examined graph node is the same as the target node."""

    def is_satisfied(self, graph, target_entity_type, current_node_idx):
        # get a reference to the node at the given node index
        node = graph.nodes[current_node_idx]

        # if the extra_info field is pointing to a giver trigger, test to make sure that
        # it is active and that it is of the correct type
        if node.extra_info and node.extra_info.active \
                and node.extra_info.entity_type == target_entity_type:
            return True

        return False


class TerminalConditionFindNodeIndex(_TerminalCondition):
    """
    The search will terminate when the currently examined  graph node is the same as the target node.
    """

    def is_satisfied(self, graph, target_idx, current_node_idx):
        return current_node_idx == target_idx


class PathPlanner(object):
    NO_CLOSEST_NODE_FOUND = -1

    def __init__(self, owner):
        # a pointer to the owner of this instance
        self._owner = owner
        # a reference to the nav graph
        self._nav_graph = owner.world.raven_map.nav_graph
        # a pointer to an instance of the current graph search
        self._current_search = None
        # this is the position the bot wishes to plan a path to reach
        self._destination = None

    def _get_closest_node_to_position(self, pos):
        """returns the index of the closest visible and unobstructed graph node to the given position"""
        closest_so_far = sys.maxsize
        closest_node_idx = PathPlanner.NO_CLOSEST_NODE_FOUND

        # when the cell space is queried this the the range searched for neighboring
        # graph nodes. This value is inversely proportional to the density of a
        # navigation graph (less dense = bigger values)
        cell_range = self._owner.world.raven_map.cell_space_neighbourhood_range

        # calculate the graph nodes that are neighboring this position
        neighbours = self._owner.world.raven_map.cell_space.get_neighbors(pos, cell_range)
        logger.debug("neighbours %s found for position %s: %s", len(neighbours), pos, [(_n.index, str(_n.position)) for _n in neighbours])
        # print("neighbours:", len(neighbours))
        for pn in neighbours:
            # if the path between this node and pos is unobstructed calculate the distance
            # if self._owner.can_walk_between(pos, pn.position):  # todo: why is this a performance eater?
                dist = pos.get_distance_sq(pn.position)

                # keep a record of the closest so far
                if dist < closest_so_far:
                    closest_so_far = dist
                    closest_node_idx = pn.index

        logger.debug("closest node index: %s", closest_node_idx)
        assert closest_node_idx != INVALID_NODE_INDEX
        return closest_node_idx

    def _smooth_path_edges_quick(self, path):
        """smooths a path by removing extraneous edges. (may not remove all extraneous edges)"""
        # create a couple of iterators and point them at the front of the path
        print("smoothing path quick for owner", self._owner.ID)
        e_idx1 = 0
        e_idx2 = 1

        # while e2 is not the last edge in the path, step through the edges checking
        # to see if the agent can move without obstruction from the source node of
        # e1 to the destination node of e2. If the agent can move between those
        # positions then the two edges are replaced with a single edge.
        while e_idx2 < len(path):
            e2 = path[e_idx2]
            # check for obstruction, adjust and remove the edges accordingly
            if e2.behavior == NavGraphEdge.Flags.NORMAL and \
                    self._owner.can_walk_between(path[e_idx1].source, e2.destination):
                (path[e_idx1]).destination = e2.destination
                path.remove(e2)
            else:
                e_idx1 = e_idx2
                e_idx2 += 1

    def _smooth_path_edges_precise(self, path):
        """smooths a path by removing extranous edges (removes *all* extranous edges)"""
        print("smoothing path precise for owner", self._owner.ID)
        # create a couple of iterators
        # point e1 to the beginning of the path
        e_idx1 = 0
        e_idx2 = 0

        while e_idx1 < len(path):
            # point e2 to the edge immediately following e1
            e_idx2 = e_idx1 + 1

            # while e2 is not the last edge in the path, step through the edges
            # checking to see if the agent can move without obstruction from the
            # source node of e1 to the destination node of e2. If the agent can move
            # between those positions then the any edges between e1 and e2 are
            # replaced with a single edge.
            while e_idx2 < len(path):
                # check for obstruction, adjust and remove the edges accordingly
                e1 = path[e_idx1]
                e2 = path[e_idx2]
                if e2.behavior == NavGraphEdge.Flags.NORMAL and self._owner.can_walk_between(e1.source, e2.destination):
                    e1.destination = e2.destination
                    del path[e_idx1 + 1: e_idx2 + 1]
                    e_idx2 = e_idx1 + 1
                else:
                    e_idx2 += 1

            e_idx1 += 1

    def _get_ready_for_new_search(self):
        """called at the commencement of a new search request. It clears up the
        appropriate lists and memory in preparation for a new search request."""
        # unregister any existing search with the path manager
        logger.debug("bot %s: initiating new path search", self._owner.ID)
        self._owner.world.path_manager.unregister(self)

        # clean up memory used by any existing search
        self._current_search = None

    def request_path_to_item(self, item_type):
        """creates an instance of the Dijkstras time-sliced search and registers it with the path manager

        given an item type, this method determines the closest reachable graph node
        to the bots position and then creates a instance of the time sliced
        Dijkstra's algorithm, which it registers with the search manager
        """

        logger.info("bot %s requested path to item %s", self._owner.ID, name_of_type[item_type])
        # clear the waypoint list and delete any active search
        self._get_ready_for_new_search()

        # find closest visible node to the bots position
        closest_node_to_bot = self._get_closest_node_to_position(self._owner.position)
        logger.debug("bot %s: node index nearest to bot: %s", self._owner.ID, closest_node_to_bot)

        # remove the destination node from the list and return false if no visible
        # node found. This will occur if the navgraph is badly designed or if the bot
        # has managed to get itself *inside* the geometry (surrounded by walls)
        # or an obstacle
        if closest_node_to_bot == PathPlanner.NO_CLOSEST_NODE_FOUND:
            logger.debug("bot %s: no node found near bot, aborting path search for item!", self._owner.ID)
            return False

        logger.info("bot %s: starting Dijkstra search for item %s from %s", self._owner.ID, name_of_type[item_type], closest_node_to_bot)
        # create an instance of the search algorithm
        self._current_search = GraphSearchDijTimeSliced(self._nav_graph, closest_node_to_bot, item_type,
                                                        TerminalConditionFindActiveTrigger())

        # register the search with the path manager
        self._owner.world.path_manager.register(self)

        return True

    def request_path_to_position(self, target_pos):
        """creates an instance of an A* time-sliced search and registers it with the path manager

        Given a target, this method first determines if nodes can be reached from
        the bots current position and the target position. If either end point is unreachable the
        method returns false.

        If nodes are reachable from both positions then an instance of the time
        sliced A* search is created and registered with the search manager. The method
        then returns true.
        """
        logger.info("bot %s at %s requested path to position %s", self._owner.ID, self._owner.position, target_pos)
        self._get_ready_for_new_search()

        # make a note of the target position
        self._destination = target_pos.clone()  # fixmed: clone not needed?

        # if the target is walkable from the bots position a path does not need to
        # be calculated, the bot can go straight to the position by ARRIVING at
        # the current waypoint
        if self._owner.can_walk_to(target_pos):
            logger.info("bot %s can walk to %s", self._owner.ID, target_pos)
            return True

        # find the closest visible node to the bots position
        closest_node_to_bot_idx = self._get_closest_node_to_position(self._owner.position)
        logger.debug("bot %s: node index nearest to bot: %s", self._owner.ID, closest_node_to_bot_idx)

        # remove the destination node from the list and return false if no visible
        # node found. This will occur if the navgraph is badly designed or if the bot
        # has managed to get itself *inside* the geometry (surrounded by walls)
        # or an obstacle.
        if closest_node_to_bot_idx == PathPlanner.NO_CLOSEST_NODE_FOUND:
            logger.debug("bot %s: no node found near bot, aborting path search!", self._owner.ID)
            return False

        # find the closest visible node to the target position
        closest_node_to_target_idx = self._get_closest_node_to_position(target_pos)
        logger.debug("bot %s: node index nearest to target: %s", self._owner.ID, closest_node_to_target_idx)

        # return false if there is a problem locating a visible node from the target.
        # This sort of thing occurs much more frequently than tha above. For
        # example, if the user clicks inside an area bounded by walls or inside an object.
        if closest_node_to_target_idx == PathPlanner.NO_CLOSEST_NODE_FOUND:
            logger.debug("bot %s: no node found near target, aborting path search!", self._owner.ID)
            return False
        # todo check path-cache here?
        logger.info("bot %s: starting A* search from %s to %s", self._owner.ID, closest_node_to_bot_idx, closest_node_to_target_idx)
        # create an instance of a distributed A* search class
        self._current_search = GraphSearchAStarTimeSliced(self._nav_graph, closest_node_to_bot_idx,
                                                          closest_node_to_target_idx, HeuristicEuclidian)

        # and register the search with the path manager
        self._owner.world.path_manager.register(self)

        return True

    def get_path(self):
        """
        called by an agent after it has been notified that a search has terminated
        successfully. The method extracts the path from m_pCurrentSearch, adds
        additional edges appropriate to the search type and returns it as a list of
        PathEdges.
        """
        assert self._current_search is not None, "no current search"
        # todo fill path-cache in this method?

        path = self._current_search.get_path_as_path_edges()

        closest = self._get_closest_node_to_position(self._owner.position)
        closest_pos = self.get_node_position(closest)

        pe = PathEdge(self._owner.position, closest_pos, NavGraphEdge.Flags.NORMAL)
        path.insert(0, pe)

        # if the bot requested a path to a location then an edge leading to the destination must be added
        if isinstance(self._current_search, GraphSearchAStarTimeSliced):
            path.append(PathEdge(path[-1].destination, self._destination, NavGraphEdge.Flags.NORMAL))

        # smooth paths if required
        if UserOptions.smoothPathsQuick:
            self._smooth_path_edges_quick(path)

        if UserOptions.smoothPathsPrecise:
            self._smooth_path_edges_precise(path)

        return path

    def get_cost_to_node(self, node_idx):
        """
        returns the cost to travel from the bot's current position to a specific
        graph node. This method makes use of the pre-calculated lookup table
        created by Raven_Game
        """
        # find the closest visible node to the bots position
        nd = self._get_closest_node_to_position(self._owner.position)

        # add the cost to this node
        cost = self._owner.get_distance(self._nav_graph.nodes[nd].position)

        # add the cost to the target node and return
        return cost + self._owner.world.raven_map.calculate_cost_to_travel_between_nodes(nd, node_idx)

    def get_cost_to_closest_item(self, giver_type):
        """
        returns the cost to the closest instance of the GiverType. This method
        also makes use of the pre-calculated lookup table. Returns -1 if no active
        trigger found
        """
        # find closest visible node to the bots position
        nd = self._get_closest_node_to_position(self._owner.position)

        # if no closest node found return failure
        if nd == -1:
            return -1

        closest_so_far = sys.maxsize

        # iterate through all the triggers to find the closest *active* trigger of type GiverTrigger
        for it in self._owner.world.raven_map.trigger_system.triggers:
            if it.active and it.entity_type == giver_type:
                cost = self._owner.world.raven_map.calculate_cost_to_travel_between_nodes(nd, it.graph_node_index)

                if cost < closest_so_far:
                    closest_so_far = cost

        # return a negative value if no active trigger of the type found
        if closest_so_far == sys.maxsize:
            return -1

        return closest_so_far

    def cycle_once(self):
        """
        the path manager calls this to iterate once though the search cycle
        of the currently assigned search algorithm. When a search is terminated
        the method messages the owner with either the msg_NoPathAvailable or
        msg_PathReady messages
        """
        result = self._current_search.cycle_once()

        # let the bot know of the failure to find a path
        if result == TARGET_NOT_FOUND:
            messagedispatcher.dispatch_message(messagedispatcher.SEND_MSG_IMMEDIATELY,
                                               messagedispatcher.SENDER_ID_IRRELEVANT,
                                               self._owner.ID,
                                               raven_messages.msg_no_path_available)

        elif result == TARGET_FOUND:
            # let the bot know a path has been found
            # if the search was for an item type then the final node in the path will
            # represent a giver trigger. Consequently, it's worth passing the pointer
            # to the trigger in the extra info field of the message. (The pointer
            # will just be None if no trigger)
            trigger = self._nav_graph.nodes[self._current_search.get_path_to_target()[-1]].extra_info

            messagedispatcher.dispatch_message(messagedispatcher.SEND_MSG_IMMEDIATELY,
                                               messagedispatcher.SENDER_ID_IRRELEVANT,
                                               self._owner.ID,
                                               raven_messages.msg_path_ready,
                                               trigger)
        return result

    def get_destination(self):
        return self._destination.clone()

    def set_destination(self, new_pos):
        self._destination = new_pos.clone()

    def get_node_position(self, idx):
        """
        used to retrieve the position of a graph node from its index. (takes
        into account the enumerations 'non_graph_source_node' and
        'non_graph_target_node'
        """
        return self._nav_graph.nodes[idx].position.clone()
