# -*- coding: utf-8 -*-
from __future__ import print_function, division
from common.gameentity import BaseGameEntity
from common.space_2d.wall import Wall
from raven import raven_messages


class DoorStates:
    open = 0
    opening = 1
    closed = 2
    closing = 3


class RavenDoor(BaseGameEntity):

    # noinspection SpellCheckingInspection
    def __init__(self, the_map, p1, p2, trigger_ids, new_id):
        BaseGameEntity.__init__(self, (p1 + p2) / 2.0, 1.0, 0, new_id)

        self.status = DoorStates.closed

        #  a sliding door is created from two walls, back to back.These walls must
        #  be added to a map's geometry in order for an agent to detect them
        self.wall1 = None
        self.wall2 = None

        #  a container of the id's of the triggers able to open this door
        self.switches = set(trigger_ids)

        #  how long the door remains open before it starts to shut again
        self._num_ticks_stay_open = 60

        #  how long the door has been open (0 if status is not open)
        self._num_ticks_currently_open = 0

        #  the door's position and size when in the open position
        self._p1 = p1
        self._p2 = p2
        self._size = p2.get_distance(p1)

        #  a normalized vector facing along the door. This is used frequently
        #  by the other methods so we might as well just calculate it once in the ctor
        self._to_p2_norm = (p2 - p1).normalized

        #  the door's current size
        self._current_size = self._size

        #  create the walls that make up the door's geometry
        _perp = self._to_p2_norm.perp
        self.wall1 = Wall(self._p1 + _perp, self._p2 + _perp)
        self.wall2 = Wall(self._p2 - _perp, self._p1 - _perp)
        the_map.walls.append(self.wall1)
        the_map.walls.append(self.wall2)

    def add_switch(self, switch_id):
        self.switches.add(switch_id)

    def update(self):
        if self.status == DoorStates.opening:
            self.open()
        elif self.status == DoorStates.closing:
            self.close()
        elif self.status == DoorStates.open:
            self._num_ticks_currently_open -= 1
            if self._num_ticks_currently_open < 0:
                self.status = DoorStates.closing

    def change_position(self, new_p1, new_p2):
        self._p1 = new_p1
        self._p2 = new_p2

        _perp = self._to_p2_norm.perp
        self.wall1.start = self._p1 + _perp
        self.wall1.end = self._p2 + _perp

        self.wall2.start = self._p2 - _perp
        self.wall2.end = self._p1 - _perp

    def open(self):
        if self.status == DoorStates.opening:
            if self._current_size < 2:
                self.status = DoorStates.open
                self._num_ticks_currently_open = self._num_ticks_stay_open
                return

        # reduce the current size
        self._current_size -= 1
        # clamp 0 <= current_size <= size
        self._current_size = 0 if self._current_size < 0 else (self._size if self._current_size > self._size else self._current_size)

        self.change_position(self._p1, self._p1 + self._to_p2_norm * self._current_size)

    def close(self):
        if self.status == DoorStates.closing:
            if self._current_size == self._size:
                self.status = DoorStates.closed
                return

        # reduce the current size
        self._current_size += 1
        # clamp 0 <= current_size <= size
        self._current_size = 0 if self._current_size < 0 else (self._size if self._current_size > self._size else self._current_size)

        self.change_position(self._p1, self._p1 + self._to_p2_norm * self._current_size)

    def handle_message(self, message):
        if message.message == raven_messages.msg_open_sesame:
            if self.status != DoorStates.open:
                self.status = DoorStates.opening
            return True
        return False
