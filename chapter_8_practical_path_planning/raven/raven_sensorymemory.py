# -*- coding: utf-8 -*-
from __future__ import print_function, division

import logging
import sys

import pygame

from common.space_2d.vectors import is_second_in_fov_of_first

logger = logging.getLogger(__name__)


class _MemoryRecord(object):

    def __init__(self):
        """records the time the opponent was last sensed (seen or heard). This
          is used to determine if a bot can 'remember' this record or not. 
          (if CurrentTime() - m_dTimeLastSensed is greater than the bot's
          memory span, the data in this record is made unavailable to clients)
        """
        self.time_last_sensed = -999999

        # it can be useful to know how long an opponent has been visible. This
        # variable is tagged with the current time whenever an opponent first becomes
        # visible. It's then a simple matter to calculate how long the opponent has
        # been in view (CurrentTime - fTimeBecameVisible)
        self.time_became_visible = -999999

        # it can also be useful to know the last time an opponent was seen
        self.time_last_visible = 0

        # a vector marking the position where the opponent was last sensed. This can
        # be used to help hunt down an opponent if it goes out of view
        self.last_sensed_position = None

        # set to true if opponent is within the field of view of the owner
        self.within_fov = False

        # set to true if there is no obstruction between the opponent and the owner, permitting a shot
        self.shootable = False


class SensoryMemory(object):

    def __init__(self, owner, memory_span_seconds):
        self._memory_span_in_ms = memory_span_seconds * 1000  # need in ms
        self._owner = owner
        self._memory_map = {}  # {bot, _MemoryRecord}

    def update_with_sound_source(self, noise_maker):
        """This method is used to update the memory map whenever an opponent makes a noise."""
        # make sure the bot being examined is not this bot
        if self._owner != noise_maker:
            # if the bot is already part of the memory then update its data, else
            # create new memory record and add it to the memory
            info = self._memory_map.setdefault(noise_maker, _MemoryRecord())

            # test if there is LOS between bots
            if self._owner.world.is_los_okay(self._owner.position, noise_maker.position):
                info.shootable = True
                # record the position of the bot
                # print("last sensed position noise_maker", noise_maker, noise_maker.position)
                info.last_sensed_position = noise_maker.position.clone()
            else:
                info.shootable = False

            # record the time it was sensed
            info.time_last_sensed = pygame.time.get_ticks()  # ms

    def remove_bot_from_memory(self, bot):
        """this removes a bots record from memory"""
        if bot in self._memory_map:
            del self._memory_map[bot]

    def update_vision(self):
        """this method iterates through all the opponents in the game world and
        updates the records of those that are in the owners FOV"""
        # for each bot in the world test to see if it is visible to the owner
        for bot in self._owner.world.bots:
            # make sure the bot being examined is not this bot
            if self._owner != bot:
                # make sure it is part of the memory map
                # get a reference to this bots data
                info = self._memory_map.setdefault(bot, _MemoryRecord())

                # test if there is LOS between bots
                if self._owner.world.is_los_okay(self._owner.position, bot.position):
                    info.shootable = True

                    # test if the bot is within FOV
                    if is_second_in_fov_of_first(self._owner.position, self._owner.facing, bot.position,
                                                 self._owner.field_of_view):
                        info.time_last_sensed = pygame.time.get_ticks()
                        # print("last sensed position", bot, bot.position)
                        info.last_sensed_position = bot.position.clone()
                        info.time_last_visible = pygame.time.get_ticks()

                        if not info.within_fov:
                            info.within_fov = True
                            info.time_became_visible = info.time_last_sensed
                    else:
                        info.within_fov = False
                else:
                    info.shootable = False
                    info.within_fov = False

    def is_opponent_shootable(self, opponent):
        info = self._memory_map.get(opponent, None)
        if info:
            return info.shootable
        return False

    def is_opponent_within_FOV(self, opponent):
        info = self._memory_map.get(opponent, None)
        if info:
            return info.within_fov
        return False

    def get_last_recorded_position_of_opponent(self, opponent):
        info = self._memory_map.get(opponent, None)
        if info:
            if info.last_sensed_position is None:
                print("no last sensed position for", opponent)
            return info.last_sensed_position
        raise Exception("Attempting to get position of unrecorded bot")

    def get_time_opponent_has_been_visible(self, opponent):
        """Returns the time that the opponent has been visible (in ms)."""
        info = self._memory_map.get(opponent, None)
        if info and info.within_fov:
            return pygame.time.get_ticks() - info.time_became_visible
        return 0

    def get_time_opponent_has_been_out_of_view(self, opponent):
        info = self._memory_map.get(opponent, None)
        if info and info.within_fov:
            return pygame.time.get_ticks() - info.time_last_visible
        return sys.maxsize

    def get_time_since_last_sensed(self, opponent):
        info = self._memory_map.get(opponent, None)
        if info and info.within_fov:
            return pygame.time.get_ticks() - info.time_last_sensed
        return 0

    def get_list_of_recently_sensed_opponents(self):
        """this method returns a list of all the opponents that have had their
        records updated within the last memory span seconds."""
        current_time = pygame.time.get_ticks()
        # this will store all the opponents the bot can remember
        # opponents = [_bot for _bot, _info in self._memory_map.items()
        #              if current_time - _info.time_last_sensed < self._memory_span_in_ms]
        opponents = []
        for _bot, _info in self._memory_map.items():
            # print(_bot, "cur", current_time, "last", _info.time_last_sensed, "div", current_time - _info.time_last_sensed, "span", self._memory_span_in_ms)
            if current_time - _info.time_last_sensed < self._memory_span_in_ms:
                opponents.append(_bot)

        return opponents
