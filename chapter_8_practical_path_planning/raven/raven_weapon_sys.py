# -*- coding: utf-8 -*-
from __future__ import print_function, division

import random
import sys

from raven import object_enumerations
from raven.raven_weapon import RailGun, ShotGun, RocketLauncher, Blaster


class WeaponSystem(object):

    def __init__(self, owner, reaction_time_in_s, aim_accuracy_in_rad, aim_persistence_in_s):
        self._owner = owner
        # this is the minimum amount of time a bot needs to see an opponent before
        # it can react to it. This variable is used to prevent a bot shooting at
        # an opponent the instant it becomes visible.
        self._reaction_time_in_ms = reaction_time_in_s * 1000
        # each time the current weapon is fired a certain amount of random moise is
        # added to the angle of the shot. This prevents the bots from hitting
        # their opponents 100% of the time. The lower this value the more accurate
        # a bots aim will be. Recommended values are between 0 and 0.2 (the value
        # represents the max deviation in radians that can be added to each shot).
        self._aim_accuracy = aim_accuracy_in_rad
        # the amount of time a bot will continue aiming at the position of the target
        # event if the target disappears from view
        self._aim_persistence_in_ms = aim_persistence_in_s * 1000

        # pointers to the weapons the bot is  carrying (a bot may only carry one instance of each weapon)
        self._weapon_map = {}  # {weapon_type: weapon}
        self.current_weapon = None  # a pointer to the weapon the bot is currently holding

    def _predict_future_position_of_target(self):
        """Predict where the target will be by the time it takes the current weapons
        projectile type to reach it. Used by take_aim_and_shoot."""
        max_speed = self.current_weapon.max_projectile_speed

        # if the target is ahead and facing the agent shoot at its current position
        target_bot = self._owner.targeting_sys.target
        to_enemy = target_bot.position - self._owner.position

        # the lookahead time is proportional to the distance between the enemy
        # and the pursuer; and is inversely proportional to the sum of the agent's velocities
        look_ahead_time = to_enemy.length / (max_speed + target_bot.max_speed)

        # return the predicted future position of the enemy
        return target_bot.position + target_bot.velocity * look_ahead_time

    def _add_noise_to_aim(self, aiming_pos):
        """Adds a random deviation to the firing angle not greater than self._aim_accuracy"""
        to_pos = aiming_pos - self._owner.position
        # random.random() returns a value in range [0, 1)
        # stretch that value to 2 * aim_accuracy
        # then subtract aim_accuracy to get the range [-aim_accuracy, aim_accuracy)
        rot = random.random() * 2.0 * self._aim_accuracy - self._aim_accuracy
        to_pos.rotate(rot)
        aiming_pos.values = to_pos + self._owner.position  # copy values

    def initialize(self):
        """Sets up the weapon map with just one weapon: the blaster"""
        self._weapon_map.clear()
        self.current_weapon = Blaster(self._owner)
        self._weapon_map[object_enumerations.type_blaster] = self.current_weapon
        self._weapon_map[object_enumerations.type_shotgun] = None
        self._weapon_map[object_enumerations.type_rail_gun] = None
        self._weapon_map[object_enumerations.type_rocket_launcher] = None

    def take_aim_and_shoot(self):
        """This method aims the bots current weapon at the target (if there s a target)
        and, if aimed correctly, fires a round. Called each update step from RavenBot.update"""
        # aim the weapon only if the current target is shootable or if it has only
        # very recently gone out of view ( this latter condition is to ensure the
        # weapon is aimed at the target event if it temporarily dodges behind a wll
        # or other cover)
        if self._owner.targeting_sys.is_target_shootable or \
                self._owner.targeting_sys.get_time_target_has_been_out_of_view() < self._aim_persistence_in_ms:
            # the position the weapon will be aimed at
            aiming_pos = self._owner.targeting_sys.target.position

            # if the current weapon is not an instant hit type gun the target position
            # must be adjusted to take into account the predicted movement of the target
            if self.current_weapon.weapon_type == object_enumerations.type_rocket_launcher or \
                    self.current_weapon.weapon_type == object_enumerations.type_blaster:
                aiming_pos = self._predict_future_position_of_target()

                # if the weapon is aimed correctly, there is line of sight between the
                # bot and the aiming position and it has been in view for a period longer
                # than the bots reaction time, shoot the weapon
                is_facing_target = self._owner.rotate_facing_toward_position(aiming_pos)
                is_visible_longer_than_reaction_time = (
                            self._owner.targeting_sys.get_time_target_has_been_visible() > self._reaction_time_in_ms)
                has_los_to_target = self._owner.has_los_to(aiming_pos)
                if is_facing_target and \
                        is_visible_longer_than_reaction_time and \
                        has_los_to_target:
                    self._add_noise_to_aim(aiming_pos)
                    self.current_weapon.shoot_at(aiming_pos)
            else:
                # no need to predict movement, aim directly at target
                # if the weapon is aimed correctly and it has been in view for a period
                # longer than the bots reaction time, shoot the weapon
                if self._owner.rotate_facing_toward_position(aiming_pos) and \
                        (self._owner.targeting_sys.get_time_target_has_been_visible() > self._reaction_time_in_ms):
                    self._add_noise_to_aim(aiming_pos)
                    self.current_weapon.shoot_at(aiming_pos)
        else:
            # no target to shoot at so rotate facing to the parallel with the bots heading direction
            self._owner.rotate_facing_toward_position(self._owner.position + self._owner.heading)

    def select_weapon(self):
        """This method determines the most appropriate weapon to use given the current
        game state. (called every n update steps from RavenBot.update)"""
        # if a target is present use fuzzy logic to determine the most desirable weapon
        if self._owner.targeting_sys.is_target_present:

            # calculate the distance to the target
            dist_to_target = self._owner.position.get_distance(self._owner.targeting_sys.target.position)

            # for each weapon in th inventory calculate its desirability given the
            # current situation. The most desirable weapon is elected
            best_so_far = -sys.maxsize
            for cur_weapon in self._weapon_map.values():
                # grab the desirability of this weapon (desirability is base upon
                # distance to target and ammo remaining)
                if cur_weapon:
                    score = cur_weapon.get_desirability(dist_to_target)

                    # if it is the most desirable so far select it
                    if score > best_so_far:
                        best_so_far = score

                        # place the weapon in the bots hand
                        self.current_weapon = cur_weapon
        else:
            self.current_weapon = self._weapon_map[object_enumerations.type_blaster]

    def add_weapon(self, weapon_type):
        """This will add a weapon of the specified type to the bots infentory.
        If the bot already has a weapon of this type only the ammo is added.
        (called by the weapon giver triggers to give a bot a weapon)."""
        w = None
        if weapon_type == object_enumerations.type_rail_gun:
            w = RailGun(self._owner)
        elif weapon_type == object_enumerations.type_shotgun:
            w = ShotGun(self._owner)
        elif weapon_type == object_enumerations.type_rocket_launcher:
            w = RocketLauncher(self._owner)

        # if the bot already holds a weapon of this type, just add its ammo
        present = self.get_weapon_from_inventory(weapon_type)

        if present:
            present.increment_rounds(w.num_rounds_left)
        else:
            # if not already holding, add th inventory
            self._weapon_map[weapon_type] = w

    def change_weapon(self, weapon_type):
        """Changes the current weapon to one of the specified type (provided that type
        is in the bots possession)"""
        w = self.get_weapon_from_inventory(weapon_type)
        if w:
            self.current_weapon = w

    def shoot_at(self, pos):
        """Shoots the current weapon at the given position."""
        self.current_weapon.shoot_at(pos)

    def get_weapon_from_inventory(self, weapon_type):
        """Returns a pointer to the specified weapon type (if in inventory, None if not)."""
        return self._weapon_map.get(weapon_type, None)

    def get_ammo_remaining_for_weapon(self, weapon_type):
        """Returns the amount of ammo remaining for the specified weapon."""
        w = self._weapon_map.get(weapon_type, None)
        if w:
            return w.num_rounds_left
        return 0
