# -*- coding: utf-8 -*-
from __future__ import division
import params
from raven import object_enumerations


def health(bot):
    return bot.health / bot.max_health


def distance_to_item(bot, item_type):
    #   determine the distance to the closest instance of the item type
    dist_to_item = bot.path_planner.get_cost_to_closest_item(item_type)

    #   if the previous method returns a negative value then there is no item of
    #   the specified type present in the game world at this time.
    if dist_to_item < 0:
        return 1

    #   these values represent cutoffs. Any distance over MaxDistance results in
    #   a value of 0, and value below MinDistance results in a value of 1
    max_distance = 500.0
    min_distance = 50.0
    dist_to_item = min_distance if dist_to_item < min_distance else (
        max_distance if dist_to_item > max_distance else dist_to_item)

    return dist_to_item / max_distance


def get_max_rounds_bot_can_carry_for_weapon(weapon_type):
    """
    //  helper function to tidy up IndividualWeapon method
    //  returns the maximum rounds of ammo a bot can carry for the given weapon
    """
    if weapon_type == object_enumerations.type_rail_gun:
        return params.RailGun_MaxRoundsCarried
    elif weapon_type == object_enumerations.type_rocket_launcher:
        return params.RocketLauncher_MaxRoundsCarried
    elif weapon_type == object_enumerations.type_shotgun:
        return params.ShotGun_MaxRoundsCarried
    else:
        raise Exception("trying to calculate for unknown weapon type {0}".format(weapon_type))


def individual_weapon_strength(bot, weapon_type):
    wp = bot.weapon_sys.get_weapon_from_inventory(weapon_type)

    if wp:
        return wp.num_rounds_left / get_max_rounds_bot_can_carry_for_weapon(weapon_type)
    else:
        return 0.0


def total_weapon_strength(bot):
    max_rounds_for_shotgun = get_max_rounds_bot_can_carry_for_weapon(object_enumerations.type_shotgun)
    max_rounds_for_railgun = get_max_rounds_bot_can_carry_for_weapon(object_enumerations.type_rail_gun)
    max_rounds_for_rocket_launcher = get_max_rounds_bot_can_carry_for_weapon(object_enumerations.type_rocket_launcher)
    total_rounds_carryable = max_rounds_for_railgun + max_rounds_for_shotgun + max_rounds_for_rocket_launcher

    num_slugs = bot.weapon_sys.get_ammo_remaining_for_weapon(object_enumerations.type_rail_gun)
    num_cartridges = bot.weapon_sys.get_ammo_remaining_for_weapon(object_enumerations.type_shotgun)
    num_rockets = bot.weapon_sys.get_ammo_remaining_for_weapon(object_enumerations.type_rocket_launcher)
    total_ammo_remaining = num_slugs + num_cartridges + num_rockets

    # the value of the tweaker (must be in the range 0-1) indicates how much
    # desirability value is returned even if a bot has not picked up any weapons.
    # (it basically adds in an amount for a bot's persistent weapon -- the blaster)
    tweaker = 0.1

    return tweaker + (1.0 - tweaker) * (total_ammo_remaining / total_rounds_carryable)
