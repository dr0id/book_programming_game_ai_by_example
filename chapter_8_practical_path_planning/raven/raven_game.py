# -*- coding: utf-8 -*-
from __future__ import print_function, division

import logging
import sys

import pygame

import params
from common import entityregistry, graph, messagedispatcher
from common.goals import CompositeGoal
from common.graph import NavGraphEdge
from common.graphtimesliced import PathManager
from common.messagedispatcher import SEND_MSG_IMMEDIATELY
from common.regulator import Regulator
from common.space_2d.geometry import do_lines_intersect_circle, line_intersection_2d
from common.space_2d.vectors import Vec2, is_second_in_fov_of_first
from raven import object_enumerations, raven_goals, armory, raven_goal_evaluators
from raven.grave_markers import GraveMarkers
from raven.object_enumerations import name_of_type
from raven.raven_bot import RavenBot
from raven.raven_map import RavenMap
from raven.raven_messages import msg_user_has_removed_bot
from raven.raven_triggers import TriggerHealthGiver, TriggerWeaponGiver, TriggerOnButtonSendMsg
from raven.useroptions import UserOptions

logger = logging.getLogger(__name__)


class RavenGame(object):

    def __init__(self):
        self.params = params
        self.raven_map = None

        self.bots = []
        self._projectiles = []
        self._selected_bot = None
        self.is_paused = False
        self._bot_font = None
        self._rip_font = None
        self.path_manager = None
        self.grave_markers = None
        every_second = 1.0
        self._print_info_regulator = Regulator(every_second)

    @property
    def walls(self):
        return self.raven_map.walls

    def load_map(self, map_name):
        # clear any current bots and projectiles
        self.clear()

        # make sure entity manager is reset
        entityregistry.reset()

        # in with new
        self.grave_markers = GraveMarkers(params.GraveLifetime)
        self.raven_map = RavenMap()
        self.path_manager = PathManager(params.MaxSearchCyclesPerUpdateStep)

        # make sure the entity manager is reset
        entityregistry.reset()

        if self.raven_map.load_map(map_name):
            self.add_bots(self.params.NumBots)
            return True

        return False

    def clear(self):
        self.bots = []
        self._projectiles = []
        self._selected_bot = None
        entityregistry.reset()

    def add_bots(self, num_bots):
        for i in range(num_bots):
            rb = RavenBot(self, Vec2(0, 0), UserOptions.doAttack)

            rb.steering.switch_on(rb.steering.WALLAVOIDANCE)
            rb.steering.switch_on(rb.steering.SEPARATION)

            self.bots.append(rb)

            entityregistry.register_entity(rb)

        for bot in self.bots:
            bot.steering.group_vehicles = self.bots

    def tag_vehicles_within_view_range(self, current_vehicle, vehicles, view_range):
        # todo: remove tag and return a list of vehicles instead...? What is faster? Unittest to ensure the same results!
        cur_v_pos_dist_sq = current_vehicle.position.get_distance_sq
        for vehicle in vehicles:
            vehicle.tag = False
            if cur_v_pos_dist_sq(vehicle.position) < (view_range + vehicle.bounding_radius) ** 2:
                vehicle.tag = True

        current_vehicle.tag = False

    def update(self):

        if self.is_paused:
            return

        self.grave_markers.update()

        # update all the queued searches in the path manager
        self.path_manager.update_searches()

        for door in self.raven_map.doors:
            door.update()

        # update any current projectiles
        self._projectiles = [_p for _p in self._projectiles if not _p.is_dead]  # remove the dead
        for projectile in self._projectiles:
            projectile.update()

        # update the bots
        spawn_possible = True
        for bot in self.bots:
            if bot.is_spawning and spawn_possible:
                spawn_possible = self.attempt_to_add_bot(bot)
            elif bot.is_dead:
                self.grave_markers.add_grave(bot.position, int(bot.ID))
                bot.status = RavenBot.Status.spawning
            elif bot.is_alive:
                bot.update()

        self.raven_map.trigger_system.update(self.bots)

    def remove_bot(self):
        if not self.bots:
            return
        bot = self.bots.pop(-1)
        entityregistry.remove_entity(bot)
        self.path_manager.unregister(bot.path_planner)
        if bot == self._selected_bot:
            self._selected_bot = None
        bot.reduce_health(200)  # make sure that this bot isn't evaluated anymore
        self.notify_all_bots_of_removal(bot)

    def attempt_to_add_bot(self, bot):
        # make sure there are some spawn points available
        if not self.raven_map.spawn_points:
            raise Exception("Map has not spawn points!")

        # we'll make the same number of attempts to spawn a bot this update as there are spawn points
        for attempts in range(len(self.raven_map.spawn_points)):
            # select a random spawn point
            pos = self.raven_map.get_random_spawn_point()
            available = True
            for _bot in self.bots:
                if pos.get_distance(_bot.position) < _bot.bounding_radius:
                    available = False

            if available:
                bot.spawn(pos)
                return True

        return False

    def add_shot_gun_pellet(self, shooter, target):
        p = armory.Pellet(shooter, target)
        self._projectiles.append(p)

    def add_rail_gun_slug(self, shooter, target):
        s = armory.Slug(shooter, target)
        self._projectiles.append(s)

    def add_rocket(self, shooter, target):
        r = armory.Rocket(shooter, target)
        self._projectiles.append(r)

    def add_bolt(self, shooter, target):
        b = armory.Bolt(shooter, target)
        self._projectiles.append(b)

    def get_bot_at_position(self, pos):
        for bot in self.bots:
            if pos.get_distance(bot.position) <= bot.bounding_radius:
                if bot.is_alive:
                    return bot

    def exorcise_any_possessed_bot(self):
        """when called will release any possessed bot from user control"""
        if self._selected_bot:
            if self._selected_bot.is_possessed:
                self._selected_bot.exorcise()
            else:
                self._selected_bot = None

    def change_weapon_of_possessed_bot(self, weapon_type):
        if self._selected_bot.is_possessed:
            self._selected_bot.weapon_sys.change_weapon(weapon_type)

    def rotate_facing_toward_pos(self, pos):
        if self._selected_bot and self._selected_bot.is_possessed:
            self._selected_bot.rotate_facing_toward_position(pos)

    def on_click_left_mouse_button(self, pos):
        if self._selected_bot and self._selected_bot.is_possessed:
            self._selected_bot.weapon_sys.shoot_at(pos)

    def on_click_right_mouse_button(self, pos):
        bot = self.get_bot_at_position(pos)

        # if there is no selected bot just return
        if bot is None and self._selected_bot is None:
            return

        # if the cursor is over a different bot to the existing selection,
        # change selection
        if bot and bot != self._selected_bot:
            if self._selected_bot:
                self._selected_bot.exorcise()
            self._selected_bot = bot
            return

        # if the user clicks on a selected bot twice it becomes possessed
        # (under the players control)
        if bot and bot == self._selected_bot:
            self._selected_bot.take_possession()

            # clear any current goals
            self._selected_bot.brain.remove_all_sub_goals()

        # if the bot is possessed then a right click moves the bot to the cursor position
        if self._selected_bot.is_possessed:
            # if the shift key is pressed down at the same time as clicking then
            # the movement command will be queued
            if pygame.key.get_mods() & pygame.KMOD_SHIFT:
                self._selected_bot.brain.queue_goal_move_to_position(pos)
            else:
                # clear any current goals
                self._selected_bot.brain.remove_all_sub_goals()
                self._selected_bot.brain.add_goal_move_to_position(pos)

    def draw(self, screen):
        white = (255, 255, 255)  # fixme: color
        screen.fill(white)
        self.draw_triggers(screen)
        self.draw_walls(screen)
        self.draw_doors(screen)
        self.draw_spawn_points(screen)
        self.draw_graph(screen)
        self.draw_grave_markers(screen)
        self.draw_bots(screen)
        self.draw_projectiles(screen)
        self.draw_selected_bot(screen)

    def draw_graph(self, screen):
        font_info = pygame.font.Font(None, 14)
        dark_grey = (100, 100, 100)
        grey = (200, 200, 200)  # fixme: color
        invalid_node_color = (200, 100, 100)  # fixme: color
        if UserOptions.showGraph:
            # nothing to paint if there are no nodes
            nav_graph = self.raven_map.nav_graph
            if nav_graph.num_nodes() == 0:
                return
            # draw nodes
            draw_circle = pygame.draw.circle
            draw_line = pygame.draw.line
            for node in nav_graph.nodes:
                width = 0 if node.index != graph.INVALID_NODE_INDEX else 1
                color = grey if node.index != graph.INVALID_NODE_INDEX else invalid_node_color
                cx, cy = node.position
                draw_circle(screen, color, (int(cx), int(cy)), 3, width)

                if node.index != graph.INVALID_NODE_INDEX:
                    for edge in nav_graph.edges[node.index]:
                        sx, sy = node.position
                        ex, ey = nav_graph.get_node(edge.idx_to).position
                        draw_line(screen, color, (sx, sy), (ex, ey), 1)

                        # todo: render info per line?
                        if edge.flags != NavGraphEdge.Flags.NORMAL:
                            edge_info = "c:{0} f:{1}".format(edge.cost, NavGraphEdge.Flags.flag_to_string[edge.flags])
                            if hasattr(edge,
                                       "do_intersecting_entity") and edge.do_intersecting_entity and edge.do_intersecting_entity != -1:
                                edge_info += " e:{0}".format(edge.do_intersecting_entity)
                            label = font_info.render(str(edge_info), True, dark_grey)
                            y_offset = -5 if ex - sx > 0 else 5
                            center = (0.75 * sx + 0.25 * ex, 0.75 * sy + 0.25 * ey + y_offset)
                            label_rect = label.get_rect(center=center)
                            screen.blit(label, label_rect)

            if UserOptions.showNodeIndices:
                font = pygame.font.Font(None, 12)
                for node in nav_graph.nodes:
                    # color = dark_grey if node.index != graph.INVALID_NODE_INDEX else invalid_node_color
                    color = dark_grey
                    text = font.render(str(node.index), True, color)  # fixme: cache label
                    screen.blit(text, text.get_rect(center=node.position))

        if UserOptions.showDebugInfo:
            dark_grey = 150, 150, 150
            for cell in self.raven_map.cell_space._cells:
                pygame.draw.rect(screen, dark_grey, cell.rect, 1)
            dark_green = 0, 130, 0
            screen_rect = screen.get_rect()
            pygame.draw.circle(screen, dark_green, screen_rect.center,
                               int(self.raven_map.cell_space_neighbourhood_range), 1)

    def draw_walls(self, screen):
        black = (0, 0, 0)  # fixme color
        draw_line = pygame.draw.line
        for wall in self.raven_map.walls:
            p1 = wall.start.as_xy_tuple(int)
            p2 = wall.end.as_xy_tuple(int)
            draw_line(screen, black, p1, p2, 2)
            if UserOptions.showDebugInfo:
                mid = ((wall.start + wall.end) / 2)
                n = mid + wall.normal * 5
                start = mid.as_xy_tuple(int)
                end = n.as_xy_tuple(int)
                draw_line(screen, black, start, end, 2)

    def draw_bots(self, screen):
        # render all the bots unless the user has selected the option to only
        # render those bots that are in the fov of the selected bot
        if self._selected_bot and UserOptions.showOnlyShowBotsInTargetsFOV:
            bots_to_render = self.get_all_bots_in_fov(self._selected_bot)
            if self._selected_bot:
                bots_to_render.append(self._selected_bot)
        else:
            # render all bots that are alive
            bots_to_render = [b for b in self.bots if b.health > 0]  # alive bots

        for _bot in bots_to_render:
            #   //when a bot is hit by a projectile this value is set to a constant user
            #   //defined value which dictates how long the bot should have a thick red
            #   //circle drawn around it (to indicate it's been hit) The circle is drawn
            #   //as long as this value is positive. (see Render)
            _bot.num_updates_hit_persistent -= 1

            if _bot.is_dead or _bot.is_spawning:
                continue

            points = []
            scale = _bot.scale.x
            position = _bot.position
            facing = _bot.facing
            side = facing.perp

            # body
            shoulder_front = 10
            shoulder_back = 7
            thickness = 3
            points.append((position + facing * scale * thickness + side * scale * shoulder_front).as_xy_tuple(int))
            points.append((position - facing * scale * thickness + side * scale * shoulder_back).as_xy_tuple(int))
            points.append((position - facing * scale * thickness - side * scale * shoulder_back).as_xy_tuple(int))
            points.append((position + facing * scale * thickness - side * scale * shoulder_front).as_xy_tuple(int))
            blue = (0, 0, 255)  # fixme color
            pygame.draw.aalines(screen, blue, True, points, 2)

            # head
            brown = (133, 90, 0)  # fixme color
            pygame.draw.circle(screen, blue, position.as_xy_tuple(int), int(6.0 * scale + 0.5))
            pygame.draw.circle(screen, brown, position.as_xy_tuple(int), int(6.0 * scale))

            if UserOptions.showDebugInfo:
                # feelers
                for feeler in _bot.steering.feelers:
                    pygame.draw.line(screen, blue, position, feeler.as_xy_tuple(int))

                # bounding radius
                pink = 255, 0, 255
                pygame.draw.circle(screen, pink, position.as_xy_tuple(int), int(_bot.bounding_radius + 0.5), 1)

            # weapons
            if UserOptions.doAttack:
                self.draw_weapon(screen, _bot)

            # hit circle
            if _bot.hit:
                red = (255, 0, 0)
                pygame.draw.circle(screen, red, position.as_xy_tuple(int), int(_bot.bounding_radius), 5)

                if _bot.num_updates_hit_persistent <= 0:
                    _bot.hit = False

            green = (0, 200, 0)  # fixme color
            # id
            if UserOptions.showBotIDs:
                # fixme cache label
                id_label = self._bot_font.render(str(int(_bot.ID)), True, green)
                screen.blit(id_label, (int(position.x - 10), int(position.y - 20)))

            # health
            if UserOptions.showBotHealth:
                # fixme cache health?
                id_label = self._bot_font.render("H:" + str(_bot.health), True, green)
                screen.blit(id_label, (int(position.x - 40), int(position.y - 10)))

            # score
            if UserOptions.showScore:
                # fixme cache health?
                id_label = self._bot_font.render("Scr:" + str(_bot.score), True, green)
                screen.blit(id_label, (int(position.x - 40), int(position.y + 0)))

            if UserOptions.showNextNode:
                self._draw_next_path_node(screen, _bot)

    def draw_doors(self, screen):
        blue = (0, 0, 200)
        pygame_draw_line = pygame.draw.line
        for door in self.raven_map.doors:
            pygame_draw_line(screen, blue, door._p1.as_xy_tuple(int), door._p2.as_xy_tuple(int), 3)
            # todo: draw door id too
        if UserOptions.showDebugInfo:
            font = pygame.font.Font(None, 12)
            for door in self.raven_map.doors:
                label = font.render(str(door.ID), True, blue)
                label_pos = (door.position + Vec2(5, 5)).as_xy_tuple(int)
                screen.blit(label, label_pos)
            for door_button in self.raven_map.trigger_system.triggers:
                if isinstance(door_button, TriggerOnButtonSendMsg):
                    label = font.render(str(door_button.ID), True, blue)
                    screen.blit(label, door_button.position.as_xy_tuple(int))

    def draw_triggers(self, screen):
        triggers = self.raven_map.trigger_system.triggers
        black = 0, 0, 0

        # health giver
        red = 255, 0, 0
        sz = 5
        # position of rects is set below
        rect_horizontal = pygame.Rect(0, 0, 2 * sz, 2)
        rect_vertical = pygame.Rect(0, 0, 2, 2 * sz)
        rect = pygame.Rect(0, 0, 2 * sz, 2 * sz)
        for trigger in (_t for _t in triggers if isinstance(_t, TriggerHealthGiver)):
            if trigger.active:
                # fixme: extract health draw method
                px, py = trigger.position.as_xy_tuple(int)
                rect.center = (px, py)
                pygame.draw.rect(screen, black, rect, 1)
                rect_horizontal.center = (px, py)
                pygame.draw.rect(screen, red, rect_horizontal)
                rect_vertical.center = (px, py)
                pygame.draw.rect(screen, red, rect_vertical)

        # weapon giver
        blue = 0, 0, 200
        brown = 150, 90, 0
        for trigger in (_t for _t in triggers if isinstance(_t, TriggerWeaponGiver)):
            if trigger.active:
                px, py = trigger.position.as_xy_tuple(int)
                if trigger.entity_type == object_enumerations.type_rail_gun:
                    # fixme: extract rail gun draw method!!
                    self.draw_rail_gun(screen, px, py, 90.0)
                elif trigger.entity_type == object_enumerations.type_shotgun:
                    # fixme: extract shot gun draw method
                    self.draw_shot_gun(screen, px, py, 90.0)
                elif trigger.entity_type == object_enumerations.type_rocket_launcher:
                    # fixme: extract rocket launcher draw method
                    vertices = [(0, -3), (1, -2), (1, -0), (2, 2), (-2, 2), (-1, 0), (-1, -2), (0, -3)]
                    s = 3
                    px, py = trigger.position.as_xy_tuple(int)
                    points = [(_px * s + px, _py * s + py) for _px, _py in vertices]
                    pygame.draw.polygon(screen, red, points, 1)

        # on button send message
        orange = 255, 170, 40
        for trigger in (_t for _t in triggers if isinstance(_t, TriggerOnButtonSendMsg)):
            sz = 2 * trigger.bounding_radius
            rect = pygame.Rect(0, 0, sz, sz)
            rect.center = trigger.position.as_xy_tuple(int)
            pygame.draw.rect(screen, orange, rect, 1)

    def draw_shot_gun(self, screen, px, py, angle):
        sz = 4
        brown = 150, 90, 0
        black = 0, 0, 0
        pygame.draw.circle(screen, brown, (px - sz, py), sz)
        pygame.draw.circle(screen, brown, (px + sz, py), sz)
        pygame.draw.circle(screen, black, (px - sz, py), sz - 1)
        pygame.draw.circle(screen, black, (px + sz, py), sz - 1)

    def draw_rail_gun(self, screen, px, py, angle):
        blue = 0, 0, 200
        direction = Vec2(11.0, 0.0)
        direction.rotate(angle)
        pygame.draw.circle(screen, blue, (px, py), 4)
        pygame.draw.line(screen, blue, (px, py), (px + direction.x, py + direction.y), 3)

    def draw_spawn_points(self, screen):
        grey = 200, 200, 200
        draw_circle = pygame.draw.circle
        for sp in self.raven_map.spawn_points:
            draw_circle(screen, grey, sp.as_xy_tuple(int), 8)

    def draw_projectiles(self, screen):
        for p in self._projectiles:
            if isinstance(p, armory.Pellet):
                if p.is_visible_to_player() and p.has_impacted:
                    brown = 175, 100, 40
                    pygame.draw.circle(screen, brown, p.impact_point.as_xy_tuple(int), 3)
                    yellow = 255, 220, 0
                    pygame.draw.line(screen, yellow, p.origin.as_xy_tuple(int), p.impact_point.as_xy_tuple(int))
            elif isinstance(p, armory.Bolt):
                dark_green = 0, 200, 0
                start = p.position.as_xy_tuple(int)
                stop = (p.position - p.velocity).as_xy_tuple(int)
                pygame.draw.line(screen, dark_green, start, stop, 4)
            elif isinstance(p, armory.Slug):
                if p.is_visible_to_player() and p.has_impacted:
                    green = (0, 255, 0)
                    pygame.draw.line(screen, green, p.origin.as_xy_tuple(int), p.impact_point.as_xy_tuple(int))
            elif isinstance(p, armory.Rocket):
                red = 255, 0, 0
                orange = 255, 180, 0
                pos = p.position.as_xy_tuple(int)
                pygame.draw.circle(screen, orange, pos, 4)
                pygame.draw.circle(screen, red, pos, 2)
                if p.has_impacted:
                    radius = int(p.current_blast_radius + 0.5) + 2
                    pygame.draw.circle(screen, orange, pos, radius, 2)

    def draw_selected_bot(self, screen):
        if self._selected_bot:
            the_bot = self._selected_bot
            # render a red circle around the selected bot (blue if possessed)
            red = (255, 0, 0)  # fixme colors
            blue = (0, 0, 255)  # fixme colors
            if the_bot.is_possessed:
                pygame.draw.circle(screen, blue, the_bot.position.as_xy_tuple(int), int(the_bot.bounding_radius + 1), 1)
            else:
                pygame.draw.circle(screen, red, the_bot.position.as_xy_tuple(int), int(the_bot.bounding_radius + 1), 1)

            if UserOptions.showOpponentsSensedBySelectedBot:
                # the_bot.sensory_memory
                orange = 255, 170, 0
                for op in self._selected_bot.sensory_memory.get_list_of_recently_sensed_opponents():
                    h = op.bounding_radius * 2
                    r = pygame.Rect(0, 0, h, h)
                    r.center = op.position.as_xy_tuple(int)
                    pygame.draw.rect(screen, orange, r, 2)

            # render a square around the bots target
            target_bot = the_bot.get_target_bot()
            if UserOptions.showTargetOfSelectedBot and target_bot:
                r = pygame.Rect(0, 0, target_bot.bounding_radius * 2, target_bot.bounding_radius * 2)
                r.center = target_bot.position.as_xy_tuple(int)
                pygame.draw.rect(screen, red, r, 4)

            if UserOptions.showPathOfSelectedBot:
                self._draw_goal(screen, the_bot.brain)

            if UserOptions.showGoalsOfSelectedBot:
                _pos = self._selected_bot.position + Vec2(-75, 0)
                self._draw_at(screen, _pos, self._selected_bot.brain)

            y_step = 10
            if UserOptions.showGoalAppraisals:
                orange = 255, 120, 40
                p = self._selected_bot.position.clone()
                p.x += 10
                p.y += y_step

                label = self._bot_font.render("Evaluators:", True, orange)
                screen.blit(label, p.as_xy_tuple(int))
                p.x += 15
                p.y += y_step
                for _eval in self._selected_bot.brain._evaluators:
                    name = _eval.__class__.__name__.replace("GoalEvaluator", "")
                    if isinstance(_eval, raven_goal_evaluators.GetWeaponGoalEvaluator):
                        name = "Get" + str(object_enumerations.name_of_type[_eval.weapon_type])

                    result = "{0:03.2f} ({1:03.2f}) {2}".format(_eval.last_desirability, _eval.character_bias, name)
                    label = self._bot_font.render(result, True, orange)
                    screen.blit(label, p.as_xy_tuple(int))
                    p.y += y_step

            if UserOptions.showWeaponAppraisals:
                black = 0, 0, 0
                p = self._selected_bot.position.clone()
                p.x += 15
                p.y -= y_step
                for w in self._selected_bot.weapon_sys._weapon_map.values():
                    if w:
                        result = "{0:03.2f} {1}".format(w.last_desirability_score,
                                                        object_enumerations.name_of_type[w.weapon_type])
                        label = self._bot_font.render(result, True, black)
                        screen.blit(label, p.as_xy_tuple(int))
                        p.y -= y_step

            if self._selected_bot.is_possessed and pygame.key.get_pressed()[pygame.K_q]:
                red = 255, 0, 0
                label = self._bot_font.render("Queueing", True, red)
                mx, my = pygame.mouse.get_pos()
                screen.blit(label, (mx, my))

    def _draw_next_path_node(self, screen, the_bot):
        logger.debug("%s", the_bot.brain)
        black = 0, 0, 0
        green = 0, 200, 0
        red = 200, 0, 0
        blue = 0, 0, 255
        depth = 0
        goals = [the_bot.brain]
        while goals:
            goal = goals.pop(0)
            if isinstance(goal, CompositeGoal):
                goals.extend(goal._sub_goals)

            indent = "".join([" "] * depth)
            logger.debug(indent + "%s", goal)
            depth += 1

            if isinstance(goal, raven_goals.GoalSeekToPosition):
                pos = goal._position.as_xy_tuple(int)
                dest = goal._owner.position.as_xy_tuple(int)
                pygame.draw.line(screen, blue, pos, dest)
                if goal.is_active:
                    pygame.draw.circle(screen, black, pos, 5)
                    pygame.draw.circle(screen, green, pos, 3)
                elif goal.is_inactive:
                    pygame.draw.circle(screen, black, pos, 5)
                    pygame.draw.circle(screen, red, pos, 3)
            elif isinstance(goal, raven_goals.GoalTraverseEdge):
                if goal.is_active:
                    dest = goal.destination_pos.as_xy_tuple(int)
                    pos = goal._owner.position.as_xy_tuple(int)
                    pygame.draw.line(screen, blue, pos, dest)
                    pygame.draw.circle(screen, black, dest, 4)
                    pygame.draw.circle(screen, green, dest, 3)

    def _draw_goal(self, screen, goal):
        blue = (0, 0, 255)
        black = (0, 0, 0)
        green = (0, 170, 0)
        red = (255, 0, 0)
        yellow = (255, 255, 0)
        orange = 200, 100, 0
        if isinstance(goal, raven_goals.GoalThink):
            # forward the request to the sub-goals
            for g in goal._sub_goals:
                self._draw_goal(screen, g)  # fixme: maybe convert to a loop instead of recursion?
        elif isinstance(goal, raven_goals.GoalAttackTarget):
            # forward the request to the sub-goals
            for g in goal._sub_goals:
                self._draw_goal(screen, g)
        elif isinstance(goal, raven_goals.GoalDodgeSideToSide):
            t = goal.strafe_target.as_xy_tuple(int)
            pos = goal._owner.position.as_xy_tuple(int)
            pygame.draw.line(screen, orange, pos, t, 1)
            pygame.draw.circle(screen, orange, t, 3)
        elif isinstance(goal, raven_goals.GoalExplore):
            # forward the request to the sub-goals
            for g in goal._sub_goals:
                self._draw_goal(screen, g)
        elif isinstance(goal, raven_goals.GoalFollowPath):
            # render all the path way-points remaining on the path
            for it in goal._path:
                source = it.source
                dest = it.destination
                self._draw_arrow(screen, source, dest, black)

                pygame.draw.circle(screen, black, dest.as_xy_tuple(int), 4)
                pygame.draw.circle(screen, red, dest.as_xy_tuple(int), 3)

            # forward the request to the sub-goals
            for g in goal._sub_goals:
                self._draw_goal(screen, g)
        elif isinstance(goal, raven_goals.GoalGetItem):
            # forward the request to the sub-goals
            for g in goal._sub_goals:
                self._draw_goal(screen, g)
        elif isinstance(goal, raven_goals.GoalHuntTarget):
            if goal._owner.targeting_sys.is_target_present:
                position = goal._owner.targeting_sys.get_last_recorded_position()
                if position:
                    pos = position.as_xy_tuple(int)
                    pygame.draw.circle(screen, green, pos, 3)
                    pygame.draw.circle(screen, red, pos, 2)
                else:
                    print("no last recorded position for target")

            # forward the request to the sub-goals
            for g in goal._sub_goals:
                self._draw_goal(screen, g)
        elif isinstance(goal, raven_goals.GoalMoveToPosition):
            # forward the request to the sub-goals
            for g in goal._sub_goals:
                self._draw_goal(screen, g)

            # draw the bullseye
            pos = goal._destination.as_xy_tuple(int)
            pygame.draw.circle(screen, black, pos, 6)
            pygame.draw.circle(screen, blue, pos, 5)
            pygame.draw.circle(screen, red, pos, 4)
            pygame.draw.circle(screen, yellow, pos, 2)
        elif isinstance(goal, raven_goals.GoalNegotiateDoor):
            # forward the request to the sub-goals
            for g in goal._sub_goals:
                self._draw_goal(screen, g)
        elif isinstance(goal, raven_goals.GoalSeekToPosition):
            pos = goal._position.as_xy_tuple(int)
            if goal.is_active:
                pygame.draw.circle(screen, black, pos, 5)
                pygame.draw.circle(screen, green, pos, 3)
            elif goal.is_inactive:
                pygame.draw.circle(screen, black, pos, 5)
                pygame.draw.circle(screen, red, pos, 3)
        elif isinstance(goal, raven_goals.GoalTraverseEdge):
            if goal.is_active:
                dest = goal.destination_pos.as_xy_tuple(int)
                pos = goal._owner.position.as_xy_tuple(int)
                pygame.draw.line(screen, blue, pos, dest)
                pygame.draw.circle(screen, black, dest, 4)
                pygame.draw.circle(screen, green, dest, 3)
        elif isinstance(goal, raven_goals.GoalWander):
            pass

    def _draw_arrow(self, screen, source, to, color, width=1):
        to_as_int = to.as_xy_tuple(int)
        pygame.draw.line(screen, color, source.as_xy_tuple(int), to_as_int, width)
        v = (to - source).normalized
        v *= 10
        perp = v.perp.normalized * 5
        left = to - v + perp
        right = to - v - perp
        pygame.draw.line(screen, color, left.as_xy_tuple(int), to_as_int, width)
        pygame.draw.line(screen, color, right.as_xy_tuple(int), to_as_int, width)

    def notify_all_bots_of_removal(self, bot):
        for _bot in self.bots:
            messagedispatcher.dispatch_message(SEND_MSG_IMMEDIATELY, bot.ID, _bot.ID, msg_user_has_removed_bot, bot)

    def get_pos_of_closest_switch(self, bot_pos, door_id):
        # first we need to get the ids of the switches attached to this door
        filtered_doors = [_d for _d in self.raven_map.doors if _d.ID == door_id]
        assert len(filtered_doors) == 1, "more than one door found for id " + str(door_id)
        door = filtered_doors[0]

        closest = 0
        closest_dist = sys.maxsize
        #   now test to see which one is closest and visible
        for sw_id in door.switches:
            trigger = entityregistry.get_entity_from_ID(sw_id)

            if self.is_los_okay(bot_pos, trigger.position):
                dist = bot_pos.get_distance_sq(trigger.position)

                if dist < closest_dist:
                    closest_dist = dist
                    closest = trigger.position
        logger.debug("bot at %s, door %s using door trigger at %s for door %s", bot_pos, door.position, closest,
                     door_id)
        return closest

    def is_los_okay(self, pos1, pos2):
        """returns true if the ray between A and B is unobstructed."""
        return not self.do_wall_obstruct_line_segment(pos1, pos2, self.raven_map.walls)

    @staticmethod
    def do_wall_obstruct_line_segment(pos1, pos2, walls):
        for wall in walls:
            # do a line segment intersection test
            if line_intersection_2d(pos1, pos2, wall.start, wall.end):
                return True
        return False

    def is_path_obstructed(self, a, b, radius):
        """
        returns true if a bot cannot move from A to B without bumping into
        world geometry. It achieves this by stepping from A to B in steps of
        size BoundingRadius and testing for intersection with world geometry at
        each point.
        """
        to_b = (b - a).normalized
        cur_pos = a.clone()
        step = to_b * 0.5 * radius
        while cur_pos.get_distance_sq(b) > radius * radius:
            # advance cur_pos one step
            cur_pos += step

            # test all walls against the new position
            if do_lines_intersect_circle(self.raven_map.walls, cur_pos, radius):
                return True

        return False

    def draw_weapon(self, screen, bot):
        w = bot.weapon_sys.current_weapon
        color = 0, 0, 0

        p = bot.position + bot.facing * 4
        half_w = bot.facing.perp * 2
        h = bot.facing * 8
        p1 = p - half_w
        p2 = p1 + half_w * 2
        p3 = p2 + h
        p4 = p3 - half_w * 2

        pointlist = [
            p1.as_xy_tuple(int),
            p2.as_xy_tuple(int),
            p3.as_xy_tuple(int),
            p4.as_xy_tuple(int),
        ]
        if w.weapon_type == object_enumerations.type_blaster:
            color = 0, 255, 0
        elif w.weapon_type == object_enumerations.type_rail_gun:
            color = 0, 0, 0
        elif w.weapon_type == object_enumerations.type_shotgun:
            color = 133, 90, 0
        elif w.weapon_type == object_enumerations.type_rocket_launcher:
            color = 255, 0, 0

        pygame.draw.polygon(screen, color, pointlist, 1)

    def _draw_at(self, screen, _pos, goal):
        _pos.y += 10
        color = None
        if goal.is_completed:
            color = 0, 255, 0
        elif goal.is_inactive:
            color = 0, 0, 255
        elif goal.has_failed:
            color = 255, 0, 0
        elif goal.is_active:
            color = 0, 0, 255

        if color:
            text = goal.__class__.__name__
            text = text.replace("Goal", "")
            if isinstance(goal, raven_goals.GoalGetItem):
                text += ": " + name_of_type[goal._item_to_get]

            label = self._bot_font.render(text, True, color)
            screen.blit(label, _pos.as_xy_tuple(int))

        if isinstance(goal, raven_goals.CompositeGoal):
            _pos.x += 10
            for g in reversed(goal._sub_goals):
                self._draw_at(screen, _pos, g)  # fixme: maybe convert to a loop instead of recursion?
            _pos.x -= 10

    def get_all_bots_in_fov(self, bot):
        visible_bots = []
        for _b in self.bots:
            # make sure time is not wasted checking against the same bot  or against a bot that is dead or re-spawning
            if _b == bot or not _b.is_alive:
                continue

            # first of all test to see if this bot is within the FOV
            if is_second_in_fov_of_first(bot.position, bot.facing, _b.position, bot.field_of_view):
                # cast a ray from between the bots to test visibility. If the bot is visible add it to the list
                if not self.do_wall_obstruct_line_segment(bot.position, _b.position, self.walls):
                    visible_bots.append(_b)

        return visible_bots

    def draw_grave_markers(self, screen):
        vertices = [
            Vec2(-4, 5),
            Vec2(-4, -3),
            Vec2(-3, -5),
            Vec2(-1, -6),
            Vec2(1, -6),
            Vec2(3, -5),
            Vec2(4, -3),
            Vec2(4, 5),
            Vec2(-4, 5)
        ]

        color = 133, 90, 0
        rip_label = self._rip_font.render("RIP", True, color)
        rip_rect = rip_label.get_rect()
        for g in self.grave_markers.grave_list:
            moved_vertices = [2 * _v + g.position for _v in vertices]
            pygame.draw.lines(screen, color, True, moved_vertices)
            rip_rect.midbottom = g.position.as_xy_tuple(int)
            screen.blit(rip_label, rip_rect)
            if g.args:
                for _a in g.args:
                    rip_rect.midtop = rip_rect.midbottom
                    info_label = self._rip_font.render(str(_a), True, color)
                    screen.blit(info_label, rip_rect)


class BrushType(object):
    NORMAL = 0
    OBSTACLE = 1
    WATER = 2
    MUD = 3
    SOURCE = 4
    TARGET = 5
