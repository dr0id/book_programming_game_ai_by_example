# -*- coding: utf-8 -*-
from __future__ import print_function, division

# this numbers should be the same as used in the maps!
msg_blank = 0
msg_path_ready = 1
msg_no_path_available = 2
msg_take_that_MF = 3
msg_you_got_me = 4
msg_goal_queue_empty = 5  # unused
msg_open_sesame = 6
msg_gun_shot_sound = 7
msg_user_has_removed_bot = 8
