# -*- coding: utf-8 -*-
from __future__ import print_function, division


num_cells_x = 24
num_cells_y = 24
# num_cells_x = 48 * 4
# num_cells_y = 48 * 4

cell_size = 19
# cell_size = 2

info_window_height = 20
tool_bar_height = 28
menu_bar_height = 20

client_w = num_cells_x * cell_size
client_h = num_cells_y * cell_size

screen_w = client_w
screen_h = client_h + menu_bar_height + tool_bar_height + info_window_height

frame_rate = 60
