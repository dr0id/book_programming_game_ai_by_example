# -*- coding: utf-8 -*-
from __future__ import print_function, division

import logging
import random

import pygame

import params
from common.goals import CompositeGoal, Goal
from common.graph import NavGraphEdge
from common.steeringbehaviors import SteeringBehavior
from raven import object_enumerations, raven_messages
from raven.raven_goal_evaluators import GetHealthGoalEvaluator, ExploreGoalEvaluator, AttackTargetGoalEvaluator, \
    GetWeaponGoalEvaluator

logger = logging.getLogger(__name__)


# logger.setLevel(logging.ERROR)


class GoalTypes(object):
    Think = 0
    Explore = 1
    ArriveAtPosition = 2
    SeekToPosition = 3
    FollowPath = 4
    TraverseEdge = 5
    MoveToPosition = 6
    GetHealth = 7
    GetShotgun = 8
    GetRocketLauncher = 9
    GetRailgun = 10
    Wander = 11
    NegotiateDoor = 12
    AttackTarget = 13
    HuntTarget = 14
    Strafe = 15
    AdjustRange = 16
    SayPhrase = 17

    @staticmethod
    def from_item_type(item_type):
        if item_type == object_enumerations.type_health:
            return GoalTypes.GetHealth
        elif item_type == object_enumerations.type_shotgun:
            return GoalTypes.GetShotgun
        elif item_type == object_enumerations.type_rail_gun:
            return GoalTypes.GetRailgun
        elif item_type == object_enumerations.type_rocket_launcher:
            return GoalTypes.GetRocketLauncher
        else:
            raise Exception("cannot determine goal type from item type '{0}'".format(item_type))


class GoalSeekToPosition(Goal):

    def __init__(self, owner, target):
        Goal.__init__(self, owner, GoalTypes.SeekToPosition)
        self._position = target
        self.time_to_reach_pos = 0
        self.start_time = 0

    def activate(self):
        logger.debug("%s activate", self.__class__.__name__)
        logger.info("%s status %s active", self.__class__.__name__, Goal.Active)
        self._status = Goal.Active

        # record the time the bot starts this goal
        self.start_time = pygame.time.get_ticks() / 1000.0  # convert to seconds

        # this value is used to determine if the bot becomes stuck
        self.time_to_reach_pos = self._owner.calculate_time_to_reach_position(self._position)

        # factor in a margin of error for any reactive behavior
        margin_of_error = 1.0  # in seconds

        self.time_to_reach_pos += margin_of_error

        self._owner.steering.target = self._position.clone()  # clone to avoid shared vectors

        self._owner.steering.switch_on(SteeringBehavior.SEEK)

    def process(self):
        logger.debug("%s process", self.__class__.__name__)
        # if status is inactive, call activate()
        self.activate_if_inactive()

        # test to see if the bot has become stuck
        if self.is_stuck:
            logger.info("%s status %s failed", self.__class__.__name__, Goal.Failed)
            self._status = Goal.Failed
        else:
            # test to see if the bot has reached the waypoint. If so terminate the goal.
            if self._owner.is_at_position(self._position):
                logger.info("%s status %s completed (bot %s %s is at %s", self.__class__.__name__, Goal.Completed,
                            self._owner.ID, self._owner.position, self._position)
                self._status = Goal.Completed

        return self._status

    def terminate(self):
        logger.debug("%s terminate", self.__class__.__name__)
        self._owner.steering.switch_off(SteeringBehavior.SEEK)
        self._owner.steering.switch_off(SteeringBehavior.ARRIVE)
        logger.info("%s status %s completed", self.__class__.__name__, Goal.Completed)
        self._status = Goal.Completed

    @property
    def is_stuck(self):
        time_taken = (pygame.time.get_ticks() / 1000.0) - self.start_time
        if time_taken > self.time_to_reach_pos:
            logger.info("%s bot %s is stuck", self.__class__.__name__, self._owner.ID)
            return True
        return False


class GoalTraverseEdge(Goal):

    def __init__(self, owner, edge, is_last_edge):
        Goal.__init__(self, owner, GoalTypes.TraverseEdge)
        self.is_last_edge = is_last_edge
        self.edge = edge
        self._time_expected = 0.0
        self._start_time = 0.0
        self.destination_pos = self.edge.destination

    def activate(self):
        logger.debug("%s activate", self.__class__.__name__)
        logger.info("%s status %s active", self.__class__.__name__, Goal.Active)
        self._status = Goal.Active

        #   //the edge behavior flag may specify a type of movement that necessitates a
        #   //change in the bot's max possible speed as it follows this edge
        if self.edge.behavior == NavGraphEdge.Flags.SWIM:
            self._owner.max_speed = params.Bot_MaxSwimmingSpeed
        elif self.edge.behavior == NavGraphEdge.Flags.CRAWL:
            self._owner.max_speed = params.Bot_MaxCrawlingSpeed

        # record the time the bot starts this goal
        self._start_time = pygame.time.get_ticks() / 1000.0

        #   //calculate the expected time required to reach the this waypoint. This value
        #   //is used to determine if the bot becomes stuck
        self._time_expected = self._owner.calculate_time_to_reach_position(self.destination_pos)

        #  //factor in a margin of error for any reactive behavior
        margin_of_error = 2.0  # in seconds

        self._time_expected += margin_of_error

        # set the steering target
        self._owner.steering.target = self.destination_pos.clone()

        # set the appropriate steering behavior. If this the last edge in the path
        # the bot should arrive at the position it point to, else it should seek
        if self.is_last_edge:
            self._owner.steering.switch_on(SteeringBehavior.ARRIVE)
        else:
            self._owner.steering.switch_on(SteeringBehavior.SEEK)

    def process(self):
        logger.debug("%s process", self.__class__.__name__)
        # if status is inactive, call activate()
        self.activate_if_inactive()

        # if the bot has become stuck return failure
        if self.is_stuck:
            logger.info("%s status %s failed", self.__class__.__name__, Goal.Failed)
            self._status = Goal.Failed
        else:
            # if the bot has reached the end of the edge return completed
            if self._owner.is_at_position(self.destination_pos):
                logger.info("%s status %s completed", self.__class__.__name__, Goal.Completed)
                self._status = Goal.Completed

        return self._status

    def terminate(self):
        logger.debug("%s terminate", self.__class__.__name__)
        self._owner.steering.switch_off(SteeringBehavior.SEEK)
        self._owner.steering.switch_off(SteeringBehavior.ARRIVE)
        self._owner.max_speed = params.Bot_MaxSpeed

    @property
    def is_stuck(self):
        """
        returns true if the bot has taken longer than expected to reach the
        currently active waypoint
        """
        time_taken = (pygame.time.get_ticks() / 1000.0) - self._start_time
        if time_taken > self._time_expected:
            logger.info("%s bot %s is stuck", self.__class__.__name__, self._owner.ID)
            return True
        return False


class GoalNegotiateDoor(CompositeGoal):

    def __init__(self, owner, edge, is_last_edge):
        CompositeGoal.__init__(self, owner, GoalTypes.NegotiateDoor)
        self.edge = edge
        self.is_last_edge = is_last_edge

    def activate(self):
        logger.debug("%s activate", self.__class__.__name__)
        # if this goal is reactivated then there may be some existing sub-goals that must be remove
        self.remove_all_sub_goals()

        # get the position of the closes navigable switch
        pos_switch = self._owner.world.get_pos_of_closest_switch(self._owner.position, self.edge.door_id)

        # because goals are *pushed* onto the front of the sub-goals list they must be added in reverse order.

        # first the goal to traverse the edge that passes through the door
        self.add_sub_goal(GoalTraverseEdge(self._owner, self.edge, self.is_last_edge))

        # next the goal that will move the bot to the beginning of the edge that passes through the door
        node = self._owner.world.raven_map.nav_graph.nodes[self.edge.idx_from]
        self.add_sub_goal(GoalMoveToPosition(self._owner, node.position))

        # finally, the goal that will direct the bot to the location of the switch
        self.add_sub_goal(GoalMoveToPosition(self._owner, pos_switch))

    def process(self):
        logger.debug("%s process", self.__class__.__name__)
        # if status is inactive, call activate()
        self.activate_if_inactive()

        # process the sub goals
        self._status = self.process_sub_goals()
        logger.info("%s set status %s %s", self.__class__.__name__, self._status, Goal.state_to_string[self._status])

        return self._status


class GoalFollowPath(CompositeGoal):

    def __init__(self, owner, path_to_follow):
        CompositeGoal.__init__(self, owner, GoalTypes.FollowPath)
        self._path = path_to_follow

    def activate(self):
        logger.debug("%s activate", self.__class__.__name__)
        logger.info("%s status %s active", self.__class__.__name__, Goal.Active)
        self._status = Goal.Active

        # get a reference to the next edge and remove edge
        edge = self._path.pop(0)

        #   some edges specify that the bot should use a specific behavior when
        #   following them. This switch statement queries the edge behavior flag and
        #   adds the appropriate goals/s to the sub-goal list.
        if edge.behavior == NavGraphEdge.Flags.NORMAL:
            self.add_sub_goal(
                GoalTraverseEdge(self._owner, edge, len(self._path) == 0))
        elif edge.behavior == NavGraphEdge.Flags.GOES_THROUGH_DOOR:
            # also add a goal that is able to handle opening the door
            self.add_sub_goal(GoalNegotiateDoor(self._owner, edge, len(self._path) == 0))
        elif edge.behavior == NavGraphEdge.Flags.JUMP:
            pass  # add sub goal to jump along the edge
        elif edge.behavior == NavGraphEdge.Flags.GRAPPLE:
            pass  # add sub goal to grapple along the edge
        else:
            raise Exception("GoalFollowPath.activate:  unrecognized edge type")

    def process(self):
        logger.debug("%s process", self.__class__.__name__)
        # if status is inactive, call activate()
        self.activate_if_inactive()

        self._status = self.process_sub_goals()
        logger.info("%s set status %s %s", self.__class__.__name__, self._status, Goal.state_to_string[self._status])

        # if there ar no sub-goals present check to see if the path still has edges
        # remaining. If it does then call activate to grab the next edge
        if self._status == Goal.Completed and len(self._path) != 0:
            self.activate()

        return self._status


class GoalMoveToPosition(CompositeGoal):

    def __init__(self, owner, pos):
        CompositeGoal.__init__(self, owner, GoalTypes.MoveToPosition)
        self._destination = pos

    def activate(self):
        logger.debug("%s activate", self.__class__.__name__)
        self._status = Goal.Active
        logger.info("%s set status %s %s", self.__class__.__name__, self._status, Goal.state_to_string[self._status])

        # make sure the sub-goal list is clear
        self.remove_all_sub_goals()

        #   requests a path to the target position from the path planner. Because, for
        #   demonstration purposes, the Raven path planner uses time-slicing when
        #   processing the path requests the bot may have to wait a few update cycles
        #   before a path is calculated. Consequently, for appearances sake, it just
        #   seeks directly to the target position whilst it's awaiting notification
        #   that the path planning request has succeeded/failed
        if self._owner.path_planner.request_path_to_position(self._destination):
            self.add_sub_goal(GoalSeekToPosition(self._owner, self._destination))

    def process(self):
        logger.debug("%s process", self.__class__.__name__)
        # if status is inactive, call Activate()
        self.activate_if_inactive()

        # process the sub-goals
        self._status = self.process_sub_goals()
        logger.info("%s set status %s %s", self.__class__.__name__, self._status, Goal.state_to_string[self._status])

        # if any of the sub-goals have failed then this goal re-plans
        self.reactivate_if_failed()

        return self._status

    def handle_message(self, msg):
        # first, pass the message down the gaol hierarchy
        handled = self.forward_to_front_most_sub_goal(msg)

        # if the msg was not handled, test to see if this goal can handle it
        if handled is False:
            if msg.message == raven_messages.msg_path_ready:
                # clear any existing goals
                self.remove_all_sub_goals()

                self.add_sub_goal(GoalFollowPath(self._owner, self._owner.path_planner.get_path()))

                return True  # msg handled
            elif msg.message == raven_messages.msg_no_path_available:
                self._status = Goal.Failed
                logger.info("%s set status %s %s", self.__class__.__name__, self._status,
                            Goal.state_to_string[self._status])
                return True  # msg handled
            else:
                return False


class GoalExplore(CompositeGoal):
    def __init__(self, owner):
        CompositeGoal.__init__(self, owner, GoalTypes.Explore)
        self._destination = None

    def activate(self):
        logger.debug("%s activate", self.__class__.__name__)
        self._status = Goal.Active
        logger.info("%s set status %s %s", self.__class__.__name__, self._status, Goal.state_to_string[self._status])

        # if this goal is reactivated thn there may be some existing sub-goals that mus be removed
        self.remove_all_sub_goals()

        if self._destination is None:
            # grab a random location
            self._destination = self._owner.world.raven_map.get_random_node_location()

        # and request a path to that position
        self._owner.path_planner.request_path_to_position(self._destination)

        # the bot may have to wait a few update cycles before a path is calculated
        # so for appearances sake it simple ARRIVES at the destination until a path hase been found
        self.add_sub_goal(GoalSeekToPosition(self._owner, self._destination))

    def process(self):
        logger.debug("%s process", self.__class__.__name__)
        # if status is inactive, call activate()
        self.activate_if_inactive()

        # process the subgoals
        self._status = self.process_sub_goals()
        logger.info("%s set status %s %s", self.__class__.__name__, self._status, Goal.state_to_string[self._status])

        return self._status

    def handle_message(self, msg):
        # first, pass the message down the goal hierarchy
        handled = self.forward_to_front_most_sub_goal(msg)

        if handled is False:
            if msg.message == raven_messages.msg_path_ready:
                # clear any existing goals
                self.remove_all_sub_goals()

                self.add_sub_goal(GoalFollowPath(self._owner, self._owner.path_planner.get_path()))

                return True  # msg handled
            elif msg.message == raven_messages.msg_no_path_available:
                self._status = Goal.Failed
                logger.info("%s set status %s %s", self.__class__.__name__, self._status,
                            Goal.state_to_string[self._status])
                return True  # msg handled
            else:
                return False

        return True  # handled by sub goals


class GoalWander(Goal):
    def __init__(self, owner):
        Goal.__init__(self, owner, GoalTypes.Wander)

    def activate(self):
        logger.debug("%s activate", self.__class__.__name__)
        self._status = Goal.Active
        logger.info("%s set status %s %s", self.__class__.__name__, self._status, Goal.state_to_string[self._status])
        self._owner.steering.switch_on(SteeringBehavior.WANDER)

    def process(self):
        logger.debug("%s process", self.__class__.__name__)
        # if status is inactive, call activate
        self.activate_if_inactive()
        return self._status

    def terminate(self):
        logger.debug("%s terminate", self.__class__.__name__)
        self._owner.steering.switch_off(SteeringBehavior.WANDER)


class GoalGetItem(CompositeGoal):
    def __init__(self, owner, item):
        CompositeGoal.__init__(self, owner, GoalTypes.from_item_type(item))
        self._item_to_get = item
        self._giver_trigger = None
        # true if a path to the item has been formulated
        self._following_path = False

    def _has_item_been_stolen(self):
        """
        Returns true if the bot sees that the item is is heading for has been
        picked up by an opponent
        """
        if self._giver_trigger and \
                not self._giver_trigger.active and \
                self._owner.has_los_to(self._giver_trigger.position):
            return True
        return False

    def activate(self):
        logger.debug("%s activate", self.__class__.__name__)
        self._status = Goal.Active
        logger.info("%s set status %s %s", self.__class__.__name__, self._status, Goal.state_to_string[self._status])

        self._giver_trigger = None

        # request a path to the item
        self._owner.path_planner.request_path_to_item(self._item_to_get)

        # the bot may have to wait a few update cycles before a path is calculated
        # so for appearances sake it just wander
        self.add_sub_goal(GoalWander(self._owner))

    def process(self):
        logger.debug("%s process", self.__class__.__name__)
        self.activate_if_inactive()

        if self._has_item_been_stolen():
            self.terminate()
        else:
            # process the sub-goals
            self._status = self.process_sub_goals()
            logger.info("%s set status %s %s", self.__class__.__name__, self._status,
                        Goal.state_to_string[self._status])

        return self._status

    def terminate(self):
        logger.debug("%s terminate", self.__class__.__name__)
        self._status = Goal.Completed
        logger.info("%s set status %s %s", self.__class__.__name__, self._status, Goal.state_to_string[self._status])
        CompositeGoal.terminate(self)

    def handle_message(self, msg):
        # first, pass the message down the goal hierarchy
        handled = self.forward_to_front_most_sub_goal(msg)

        # if the msg was not handled, test to see if this goal can handle it
        if handled is False:
            if msg.message == raven_messages.msg_path_ready:
                # clear any exiting goals
                self.remove_all_sub_goals()

                self.add_sub_goal(GoalFollowPath(self._owner, self._owner.path_planner.get_path()))

                # get the pointer to the item
                self._giver_trigger = msg.extra_info
                return True  # msg handled
            elif msg.message == raven_messages.msg_no_path_available:
                self._status = Goal.Failed
                logger.info("%s set status %s %s", self.__class__.__name__, self._status,
                            Goal.state_to_string[self._status])
                return True  # msg handled
            else:
                return False

        # handled by sub-goals
        return True


class GoalDodgeSideToSide(Goal):
    def __init__(self, owner):
        Goal.__init__(self, owner, GoalTypes.Strafe)
        self._clock_wise = True if random.random() >= 0.5 else False
        self.strafe_target = None

    def activate(self):
        logger.debug("%s activate", self.__class__.__name__)
        # print(self._owner.ID, "activate", self.__class__.__name__)
        self._status = Goal.Active
        logger.info("%s set status %s %s", self.__class__.__name__, self._status, Goal.state_to_string[self._status])

        self._owner.steering.switch_on(SteeringBehavior.SEEK)

        # if self._owner.ID == 401:
        #     print("pos", self._owner.position, "facing", self._owner.facing, "heading", self._owner.heading)
        if self._clock_wise:
            # if self._owner.can_step_left(self.strafe_target):
            can_step_right, self.strafe_target = self._owner.can_step_right()
            if can_step_right:
                # if self._owner.ID == 401: print("can step right", self.strafe_target)
                self._owner.steering.target.set_values(self.strafe_target)
            else:
                # if self._owner.ID == 401: print("right -> left")
                self._clock_wise = not self._clock_wise
                self._status = Goal.Inactive
                logger.info("%s set status %s %s", self.__class__.__name__, self._status,
                            Goal.state_to_string[self._status])
        else:
            # if self._owner.can_step_right(self.strafe_target):
            can_step_left, self.strafe_target = self._owner.can_step_left()
            if can_step_left:
                # if self._owner.ID == 401: print("can step left", self.strafe_target)
                self._owner.steering.target.set_values(self.strafe_target)
            else:
                # if self._owner.ID == 401: print("left -> right")
                self._clock_wise = not self._clock_wise
                self._status = Goal.Inactive
                logger.info("%s set status %s %s", self.__class__.__name__, self._status,
                            Goal.state_to_string[self._status])
        # if self._owner.ID == 401:
        #     print("pos", self._owner.position, "facing", self._owner.facing, "heading", self._owner.heading)

    def process(self):
        logger.debug("%s process", self.__class__.__name__)
        # print(self._owner.ID, "process", self.__class__.__name__)
        # if state is inactive, call activate
        self.activate_if_inactive()

        # if target goes out of view terminate
        if not self._owner.targeting_sys.is_target_within_fov:
            self._status = Goal.Completed
            logger.info("%s set status %s %s", self.__class__.__name__, self._status,
                        Goal.state_to_string[self._status])

        else:
            # else if bot reaches the target position set status to inactive so the goal
            # is reactivated on the next update step
            if self._owner.is_at_position(self.strafe_target):
                self._status = Goal.Inactive
                logger.info("%s set status %s %s", self.__class__.__name__, self._status,
                            Goal.state_to_string[self._status])

        return self._status

    def terminate(self):
        logger.debug("%s terminate", self.__class__.__name__)
        # print(self._owner.ID, "terminate", self.__class__.__name__)
        self._owner.steering.switch_off(SteeringBehavior.SEEK)


class GoalHuntTarget(CompositeGoal):
    def __init__(self, owner):
        CompositeGoal.__init__(self, owner, GoalTypes.HuntTarget)
        # this value is set to true if the last visible position of the target
        # bot has been searched without success
        self._lvp_tried = False

    def activate(self):
        logger.debug("%s activate", self.__class__.__name__)
        # if this goal is reactivated then there may be some existing sub-goals that must be reomved
        self.remove_all_sub_goals()

        # it is possible for the target to dies whilst this goal is active so we
        # must test to make sure the bot always has an active target
        if self._owner.targeting_sys.is_target_present:
            # grab local copy of the last recorded position (LRP) of the target
            lrp = self._owner.targeting_sys.get_last_recorded_position()

            # if the bos has reached the LRP and it still hasn't found the target
            # it starts to search by using the explore goal to move to random map locations
            if lrp is None or lrp.is_zero() or self._owner.is_at_position(lrp):
                self.add_sub_goal(GoalExplore(self._owner))
            else:
                # move to the LRP
                self.add_sub_goal(GoalMoveToPosition(self._owner, lrp))
        else:
            # if there is no active target then this goal can be removed from the queue
            self._status = Goal.Completed
            logger.info("%s set status %s %s", self.__class__.__name__, self._status,
                        Goal.state_to_string[self._status])

    def process(self):
        logger.debug("%s process", self.__class__.__name__)
        # if status is inactive, call activate()
        self.activate_if_inactive()

        self._status = self.process_sub_goals()
        logger.info("%s set status %s %s", self.__class__.__name__, self._status, Goal.state_to_string[self._status])

        # if target is in view this goal is satisfied
        if self._owner.targeting_sys.is_target_within_fov:
            self._status = Goal.Completed
            logger.info("%s set status %s %s", self.__class__.__name__, self._status,
                        Goal.state_to_string[self._status])

        return self._status


class GoalAttackTarget(CompositeGoal):
    def __init__(self, owner):
        CompositeGoal.__init__(self, owner, GoalTypes.AttackTarget)

    def activate(self):
        logger.debug("%s activate", self.__class__.__name__)
        # print(self._owner.ID, "activate", self.__class__.__name__)
        self._status = Goal.Active
        logger.info("%s set status %s %s", self.__class__.__name__, self._status, Goal.state_to_string[self._status])

        # if this goal is reactivated then there may be some existing sub-goals that mus be removed
        self.remove_all_sub_goals()

        # it is possible for a bots target to die whilst this goal is active so we
        # must test to make sure the bot always has an active target
        if not self._owner.targeting_sys.is_target_present:
            self._status = Goal.Completed
            logger.info("%s set status %s %s", self.__class__.__name__, self._status,
                        Goal.state_to_string[self._status])
            return

        # if the bot is able to shoot the target ( there is LOS between bot and
        # target, then select a tactic to follow while shooting
        if self._owner.targeting_sys.is_target_shootable:
            # if the bot has space to strafe then do so
            can_step_left, dummy = self._owner.can_step_left()
            can_step_right, dummy = self._owner.can_step_right()
            if can_step_left or can_step_right:
                self.add_sub_goal(GoalDodgeSideToSide(self._owner))
            else:
                # if not able to strafe, head directly at the targets position
                self.add_sub_goal(GoalSeekToPosition(self._owner, self._owner.get_target_bot().position))
        else:
            # if the target is not visible, go hunt it
            self.add_sub_goal(GoalHuntTarget(self._owner))

    def process(self):
        logger.debug("%s process", self.__class__.__name__)
        # print(self._owner.ID, "process", self.__class__.__name__)
        # if status is inactive, call activate()
        self.activate_if_inactive()

        # process the sub-goals
        self._status = self.process_sub_goals()
        logger.info("%s set status %s %s", self.__class__.__name__, self._status, Goal.state_to_string[self._status])

        self.reactivate_if_failed()
        return self._status

    def terminate(self):
        logger.debug("%s terminate", self.__class__.__name__)
        # print(self._owner.ID, "terminate", self.__class__.__name__)
        self._status = Goal.Completed
        logger.info("%s set status %s %s", self.__class__.__name__, self._status, Goal.state_to_string[self._status])
        CompositeGoal.terminate(self)


class GoalThink(CompositeGoal):

    def __init__(self, owner, attack_bias=None):
        CompositeGoal.__init__(self, owner, GoalTypes.Think)
        #   these biases could be loaded in from a script on a per bot basis
        #   but for now we'll just give them some random values
        # low_range_of_bias = 0.5
        # high_range_of_bias = 1.5
        # random.random() + 0.5 gives a value in the range [0.5, 1.5)

        health_bias = random.random() + 0.5
        shotgun_bias = random.random() + 0.5
        rocket_launcher_bias = random.random() + 0.5
        railgun_bias = random.random() + 0.5
        explore_bias = random.random() + 0.5
        attack_bias = random.random() + 0.5 if attack_bias is None else attack_bias

        print("owner", self._owner.ID,
              "health bias", health_bias,
              "shotgun_bias", shotgun_bias,
              "rocket_launcher_bias", rocket_launcher_bias,
              "railgun_bias", railgun_bias,
              "explore_bias", explore_bias,
              "attack_bias", attack_bias
              )

        # create the evaluator objects
        self._evaluators = []
        self._evaluators.append(GetHealthGoalEvaluator(health_bias))
        self._evaluators.append(ExploreGoalEvaluator(explore_bias))
        self._evaluators.append(AttackTargetGoalEvaluator(attack_bias))
        shotgun_evaluator = GetWeaponGoalEvaluator(shotgun_bias, object_enumerations.type_shotgun,
                                                   params.Bot_ShotgunGoalTweaker)
        self._evaluators.append(shotgun_evaluator)
        railgun_evaluator = GetWeaponGoalEvaluator(railgun_bias, object_enumerations.type_rail_gun,
                                                   params.Bot_RailgunGoalTweaker)
        self._evaluators.append(railgun_evaluator)
        rocket_launcher_evaluator = GetWeaponGoalEvaluator(rocket_launcher_bias,
                                                           object_enumerations.type_rocket_launcher,
                                                           params.Bot_RocketLauncherTweaker)
        self._evaluators.append(rocket_launcher_evaluator)

    def activate(self):
        logger.debug("%s activate", self.__class__.__name__)
        if not self._owner.is_possessed:
            self.arbitrate()

        self._status = Goal.Active
        logger.info("%s set status %s %s", self.__class__.__name__, self._status, Goal.state_to_string[self._status])

    def process(self):
        logger.debug("%s process", self.__class__.__name__)
        self.activate_if_inactive()

        sub_goal_status = self.process_sub_goals()
        if sub_goal_status == Goal.Completed or sub_goal_status == Goal.Failed:
            if not self._owner.is_possessed:
                self._status = Goal.Inactive
                logger.info("%s set status %s %s", self.__class__.__name__, self._status,
                            Goal.state_to_string[self._status])

        return self._status

    def arbitrate(self):
        """
        this method iterates through each goal option to determine which one has
        the highest desirability.
        """
        best = 0
        most_desirable = None

        # iterate through all the evaluators to see which produces the highest score
        for evaluator in self._evaluators:
            desirability = evaluator.calculate_desirability(self._owner)

            if desirability >= best:
                best = desirability
                most_desirable = evaluator

        assert most_desirable is not None, "should have selected a goal"
        logger.debug("%s bot %s arbitrate: %s", self.__class__.__name__, self._owner.ID,
                     most_desirable.__class__.__name__)
        most_desirable.set_goal(self._owner)

    def not_present(self, goal_type):
        """
        returns true if the goal type passed as a parameter is the same as this
        goal or any of its sub-goals
        """
        if self._sub_goals:
            return self._sub_goals[0].goal_type != goal_type

        return True

    def add_goal_move_to_position(self, pos):
        logger.info("add_goal_move_to_position %s: %s", self._owner.ID, pos)
        self.add_sub_goal(GoalMoveToPosition(self._owner, pos))

    def add_goal_explore(self):
        if self.not_present(GoalTypes.Explore):
            logger.info("add_goal_explore %s", self._owner.ID)
            self.remove_all_sub_goals()
            self.add_sub_goal(GoalExplore(self._owner))

    def add_goal_get_item(self, item_type):
        if self.not_present(GoalTypes.from_item_type(item_type)):
            logger.info("add_goal_get_item %s: %s", self._owner.ID, object_enumerations.name_of_type[item_type])
            self.remove_all_sub_goals()
            self.add_sub_goal(GoalGetItem(self._owner, item_type))

    def add_goal_attack_target(self):
        if self.not_present(GoalTypes.AttackTarget):
            logger.info("add_goal_attack_target %s", self._owner.ID)
            self.remove_all_sub_goals()
            self.add_sub_goal(GoalAttackTarget(self._owner))

    def queue_goal_move_to_position(self, pos):
        logger.info("queue_goal_move_to_position %s: %s", self._owner.ID, pos)
        self._sub_goals.append(GoalMoveToPosition(self._owner, pos))
