# -*- coding: utf-8 -*-
from __future__ import print_function, division


type_wall = 0
type_bot = 1
type_unused = 2
type_waypoint = 3
type_health = 4
type_spawn_point = 5
type_rail_gun = 6
type_rocket_launcher = 7
type_shotgun = 8
type_blaster = 9
type_obstacle = 10
type_sliding_door = 11
type_door_trigger = 12

name_of_type = {
    type_wall: "Wall",
    type_bot: "Bot",
    type_unused: "Knife",  # unused, I guess it was planned to have this as default weapon
    type_waypoint: "Waypoint",
    type_health: "Health",
    type_spawn_point: "Spawn Point",
    type_rail_gun: "Railgun",
    type_rocket_launcher: "Rocket launcher",
    type_shotgun: "Shotgun",
    type_blaster: "Blaster",
    type_obstacle: "Obstacle",
    type_sliding_door: "Sliding Door",
    type_door_trigger: "Trigger",

}

unknown_object_type_name = "UNKNOWN OBJECT TYPE"
