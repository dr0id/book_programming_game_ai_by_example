# -*- coding: utf-8 -*-
from __future__ import print_function, division

import logging

import params
from common import messagedispatcher
from common.messagedispatcher import SEND_MSG_IMMEDIATELY
from common.space_2d.vectors import Vec2 as Vec
from common.triggers import Trigger, TriggerRespawning, TriggerLimitedLifetime
from raven import constants, raven_messages, object_enumerations
from raven.object_enumerations import name_of_type

logger = logging.getLogger(__name__)


class TriggerOnButtonSendMsg(Trigger):

    def __init__(self, new_id, receiver, message_id, position, radius):
        Trigger.__init__(self, position, 1.0, radius, new_id)
        self._receiver = receiver
        self._message_id = message_id

        _half = Vec(radius, radius)
        self.add_rectangular_trigger_region(self.position - _half, self.position + _half)

    def handle_message(self, message):
        return False

    def try_entity(self, entity):
        if self.is_touching_trigger(entity.position, entity.bounding_radius):
            logger.info("trigger %s: receiver %s, msg: %s", self.ID, self._receiver, self._message_id)
            messagedispatcher.dispatch_message(SEND_MSG_IMMEDIATELY,
                                               self.ID,
                                               self._receiver,
                                               self._message_id,
                                               None)

    def update(self):
        pass  # override base implementation


class TriggerHealthGiver(TriggerRespawning):

    def __init__(self, health_given, pos, scale, radius, trigger_id):
        TriggerRespawning.__init__(self, pos, scale, radius, trigger_id)
        #   //the amount of health an entity receives when it runs over this trigger
        self._health_given = health_given

        #   //create this trigger's region of influence
        self.add_circular_trigger_region(pos, params.DefaultGiverTriggerRange)
        self.set_respawn_delay(params.Health_RespawnDelay)
        self.entity_type = object_enumerations.type_health

    def try_entity(self, bot):
        if self.active and self.is_touching_trigger(bot.position, bot.bounding_radius):
            logger.info("trigger %s: increase health of bot %s about %s", self.ID, bot.ID, self._health_given)
            bot.increase_health(self._health_given)
            self.deactivate()


class TriggerWeaponGiver(TriggerRespawning):

    def __init__(self, trigger_id, pos, radius, entity_type):
        TriggerRespawning.__init__(self, pos, 1.0, radius, trigger_id)
        self.entity_type = entity_type

    def try_entity(self, entity):
        if self.active and self.is_touching_trigger(entity.position, entity.bounding_radius):
            logger.info("trigger %s: give weapon to bot %s: %s", self.ID, entity.ID, name_of_type[self.entity_type])
            entity.weapon_sys.add_weapon(self.entity_type)
            self.deactivate()


class TriggerSoundNotify(TriggerLimitedLifetime):

    def __init__(self, source, sound_range):
        life_time = constants.frame_rate / params.Bot_TriggerUpdateFreq
        TriggerLimitedLifetime.__init__(self, life_time, source.position, source.scale, sound_range)
        self.source = source
        self.add_circular_trigger_region(source.position, sound_range)

    def try_entity(self, entity):
        if self.is_touching_trigger(entity.position, entity.bounding_radius):
            logger.info("sound source %s: noticed by %s msg %s", self.source.ID, entity.ID,
                        raven_messages.msg_gun_shot_sound)
            messagedispatcher.dispatch_message(SEND_MSG_IMMEDIATELY, self.source.ID, entity.ID,
                                               raven_messages.msg_gun_shot_sound, self.source)
