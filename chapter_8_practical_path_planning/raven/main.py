# -*- coding: utf-8 -*-
from __future__ import print_function, division

import os
import sys

import pygame

import params
from common import gui
from common.space_2d.vectors import Vec2
from raven import constants, object_enumerations
from raven.raven_game import RavenGame
from raven.useroptions import UserOptions
import logging

logging.basicConfig(level=logging.WARNING,
                    format="%(asctime)s %(levelname)s:%(name)s:%(message)s %(filename)s:%(lineno)s")


class App(object):

    def __init__(self, info_message=""):
        self.running = True
        self.screen = None
        self.fill_color = (255, 0, 255)
        self._map_extension = "*.map"
        self._info_message = info_message

        # raven game
        self.raven_game = RavenGame()

        # resources
        self.image_path = os.path.join(os.path.dirname(__file__), "images")
        self.map_path = os.path.join(os.curdir, "maps")

        # gui, created in init()
        self.caption = "Raven"
        self.panel = None  # to get correct mouse coordinates
        self.gui_root = None
        self._info_bar = None
        self._info_bar_text = ""

        # gui, menu items to keep track
        self._mi_toggle_pause = gui.CheckableMenuItem("Toggle Pause [ 'P' ]", self.on_toggle_pause)

        self._mi_show_graph = gui.CheckableMenuItem("Show NavGraph", self.on_show_nav_graph)
        self._mi_show_node_indices = gui.CheckableMenuItem("Show Node Indices", self.on_show_node_indices)
        self._mi_smooth_paths_quick = gui.CheckableMenuItem("Smooth Paths (quick)", self.on_smooth_paths_quick)
        self._mi_smooth_paths_precise = gui.CheckableMenuItem("Smooth Paths (precise)", self.on_smooth_paths_precise)
        self._mi_show_wall_normals = gui.CheckableMenuItem("Show debug info", self.on_show_debug_info)

        self._mi_show_bots_ids = gui.CheckableMenuItem("Show Ids", self.on_show_bot_ids)
        self._mi_show_bots_health = gui.CheckableMenuItem("Show Health", self.on_show_bot_health)
        self._mi_show_bots_scores = gui.CheckableMenuItem("Show Scores", self.on_show_bot_scores)

        self._mi_show_target = gui.CheckableMenuItem("Show Target (boxed in red)", self.on_show_target_boxes)
        self._mi_show_sensed_opponents = gui.CheckableMenuItem("Show Sensed Opponents (boxed in orange)",
                                                               self.on_show_sensed_opponents)
        self._mi_show_opponents_in_fov = gui.CheckableMenuItem("Only Show Opponents in FOV",
                                                               self.on_show_opponents_in_fov)
        self._mi_show_goal_queue = gui.CheckableMenuItem("Show Goal Queue", self.on_show_goal_queue)
        self._mi_show_path = gui.CheckableMenuItem("Show Path", self.on_show_path)
        self._mi_show_weapon_appraisals = gui.CheckableMenuItem("Show weapons appraisals", self.on_show_w_appraisals)
        self._mi_show_goal_appraisals = gui.CheckableMenuItem("Show goal appraisals", self.on_show_goal_appraisals)
        self._menu_is_open = False

        # a pygame clock to control the frame rate
        self.clock = None

    def init_pygame(self):
        os.environ['SDL_VIDEO_WINDOW_POS'] = 'center'
        pygame.init()
        pygame.display.set_caption(self.caption)
        icon = pygame.image.load(os.path.join(self.image_path, "icon2.png"))
        pygame.display.set_icon(icon)

    def init_screen(self, client_size):
        self.screen = pygame.display.set_mode(client_size, pygame.RESIZABLE)

    def init(self):
        self.init_pygame()

        self.raven_game._bot_font = pygame.font.Font(None, 16)
        self.raven_game._rip_font = pygame.font.Font(None, 12)
        self.raven_game.load_map(params.StartMap)

        client_size = int(self.raven_game.raven_map.size_x), int(self.raven_game.raven_map.size_y)

        screen_size = self.init_gui(client_size)
        self.init_screen(screen_size)
        self.show_info_box()
        self._check_menu_item_appropriately()
        self.clock = pygame.time.Clock()

        # self._info_bar_text = "Right click bot once to select, twice to possess (x to release)."

        self._check_menu_item_appropriately()

    def show_info_box(self):
        self.raven_game.is_paused = True
        info_message = self._info_message
        info_postfix = "\n" \
                       "Use the mouse.\n" \
                       "Right click once select bot.\n" \
                       "Right click twice steer bot.\n" \
                       "\n" \
                       "Open this info anytime from Game -> Info"
        msg_box = gui.MessageBox("Info", info_message + info_postfix, self.on_init_msg_box_closed)
        msg_box.show()

    def init_gui(self, client_size):

        gui.StatusBar.height = 30
        screen_size = client_size[0], constants.menu_bar_height + gui.StatusBar.height + client_size[1]

        menu_bar = gui.MenuBar((0, 0), (screen_size[0], constants.menu_bar_height))

        game_menu = gui.MenuItem("Game", on_open_cb=self._on_menu_open, on_close_cb=self._on_menu_close)
        game_menu.append(gui.MenuItem("Load Map", self.on_load_map))
        game_menu.append(gui.MenuItem("Add Bot [ csr up ]", self.on_add_bot))
        game_menu.append(gui.MenuItem("Remove Bot [ csr down ]", self.on_remove_bot))
        game_menu.append(self._mi_toggle_pause)
        game_menu.append(gui.MenuItem("Info", self.on_info))
        game_menu.append(gui.MenuItem("Exit", self.on_exit))

        navigation_menu = gui.MenuItem("Navigation", on_open_cb=self._on_menu_open, on_close_cb=self._on_menu_close)
        navigation_menu.append(self._mi_show_graph)
        navigation_menu.append(self._mi_show_node_indices)
        navigation_menu.append(self._mi_smooth_paths_quick)
        navigation_menu.append(self._mi_smooth_paths_precise)
        navigation_menu.append(self._mi_show_wall_normals)

        bot_menu = gui.MenuItem("General Bot Info", on_open_cb=self._on_menu_open, on_close_cb=self._on_menu_close)
        bot_menu.append(self._mi_show_bots_ids)
        bot_menu.append(self._mi_show_bots_health)
        bot_menu.append(self._mi_show_bots_scores)

        selected_bot_menu = gui.MenuItem("Selected Bot Info", on_open_cb=self._on_menu_open,
                                         on_close_cb=self._on_menu_close)
        selected_bot_menu.append(self._mi_show_target)
        selected_bot_menu.append(self._mi_show_sensed_opponents)
        selected_bot_menu.append(self._mi_show_opponents_in_fov)
        selected_bot_menu.append(self._mi_show_goal_queue)
        selected_bot_menu.append(self._mi_show_path)
        selected_bot_menu.append(self._mi_show_weapon_appraisals)
        selected_bot_menu.append(self._mi_show_goal_appraisals)

        menu_bar.append(game_menu)
        menu_bar.append(navigation_menu)
        menu_bar.append(bot_menu)
        menu_bar.append(selected_bot_menu)

        self._info_bar = gui.StatusBar((0, screen_size[1] - gui.StatusBar.height),
                                       (screen_size[0], gui.StatusBar.height))
        self._info_bar.set_text("just a test text")

        self.panel = gui.GfxPanel((0, menu_bar.rect.bottom),
                                  (screen_size[0], self._info_bar.rect.top - menu_bar.rect.bottom),
                                  self.raven_game.draw)

        # add in draw order
        self.gui_root = gui.GuiRoot(screen_size)
        self.gui_root.append(self.panel)
        self.gui_root.append(self._info_bar)
        self.gui_root.append(menu_bar)
        self.gui_root.init()

        return screen_size

    def run(self):

        self.init()

        while self.running:
            # if __debug__:
            #     try:
            #         self.check_events()
            #         self.update()
            #         self.draw()
            #     except Exception as ex:
            #         print(ex)
            # else:
            self.check_events()
            self.update()
            self.draw()

        pygame.quit()
        sys.exit()

    def check_events(self):
        if self.raven_game.is_paused:
            events = [pygame.event.wait()]
        else:
            events = pygame.event.get()

        for event in events:

            if gui.HANDLED == self.gui_root.handle_event(event):
                self._check_menu_item_appropriately()
                continue

            if event.type == pygame.QUIT:
                self.running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self.running = False
                elif event.key == pygame.K_p:
                    self.raven_game.is_paused = not self.raven_game.is_paused
                elif event.key == pygame.K_UP:
                    self.raven_game.add_bots(1)
                elif event.key == pygame.K_DOWN:
                    self.raven_game.remove_bot()
                elif event.key == pygame.K_x:
                    self.raven_game.exorcise_any_possessed_bot()
                elif event.key == pygame.K_1:
                    self.raven_game.change_weapon_of_possessed_bot(object_enumerations.type_blaster)
                elif event.key == pygame.K_2:
                    self.raven_game.change_weapon_of_possessed_bot(object_enumerations.type_shotgun)
                elif event.key == pygame.K_3:
                    self.raven_game.change_weapon_of_possessed_bot(object_enumerations.type_rocket_launcher)
                elif event.key == pygame.K_4:
                    self.raven_game.change_weapon_of_possessed_bot(object_enumerations.type_rail_gun)
                self._check_menu_item_appropriately()
            elif event.type == pygame.VIDEORESIZE:
                self.init_screen(event.size)
            elif event.type == pygame.MOUSEBUTTONDOWN:
                x, y = event.pos
                x -= self.panel.rect.left
                y -= self.panel.rect.top
                pos = Vec2(x, y)
                if event.button == 1:  # left mouse button
                    self.raven_game.on_click_left_mouse_button(pos)
                elif event.button == 3:  # right mouse button
                    self.raven_game.on_click_right_mouse_button(pos)
            elif event.type == pygame.MOUSEMOTION:
                x, y = event.pos
                x -= self.panel.rect.left
                y -= self.panel.rect.top
                pos = Vec2(x, y)
                self.raven_game.rotate_facing_toward_pos(pos)

    def update(self):
        self.clock.tick(constants.frame_rate)
        if not self._menu_is_open:
            self.raven_game.update()
        if UserOptions.showNumSearches:
            self._info_bar_text = "Num current searches: {0}".format(self.raven_game.path_manager.get_num_active_searches())
        self._info_bar.set_text(self._info_bar_text)

    def draw(self):
        self.screen.fill(self.fill_color)

        # game
        # self.raven_game.draw(self.screen)

        # gui
        self.gui_root.draw(self.screen)

        pygame.display.flip()

    def on_exit(self, sender):
        self.running = False

    def on_info(self, sender):
        self.show_info_box()

    def on_load_map(self, sender):
        open_dialog = gui.OpenDialog("load map", "./maps", self.on_map_selected)
        open_dialog.show()

    def on_map_selected(self, dialog):
        path_to_map = dialog.filename
        if not self.raven_game.load_map(path_to_map):
            print("could not load map {0}", path_to_map)

    def on_add_bot(self, sender):
        self.raven_game.add_bots(1)

    def on_remove_bot(self, sender):
        self.raven_game.remove_bot()

    def on_toggle_pause(self, sender):
        self.raven_game.is_paused = sender.is_checked

    def on_show_nav_graph(self, sender):
        UserOptions.showGraph = sender.is_checked

    def on_show_node_indices(self, sender):
        UserOptions.showNodeIndices = sender.is_checked

    def on_smooth_paths_quick(self, sender):
        UserOptions.smoothPathsQuick = sender.is_checked
        if UserOptions.smoothPathsPrecise:
            UserOptions.smoothPathsPrecise = False

    def on_smooth_paths_precise(self, sender):
        UserOptions.smoothPathsPrecise = sender.is_checked
        if UserOptions.smoothPathsQuick:
            UserOptions.smoothPathsQuick = False

    def on_show_debug_info(self, sender):
        UserOptions.showDebugInfo = sender.is_checked

    def on_show_bot_ids(self, sender):
        UserOptions.showBotIDs = sender.is_checked

    def on_show_bot_health(self, sender):
        UserOptions.showBotHealth = sender.is_checked

    def on_show_bot_scores(self, sender):
        UserOptions.showScore = sender.is_checked

    def on_show_target_boxes(self, sender):
        UserOptions.showTargetOfSelectedBot = sender.is_checked

    def on_show_sensed_opponents(self, sender):
        UserOptions.showOpponentsSensedBySelectedBot = sender.is_checked

    def on_show_opponents_in_fov(self, sender):
        UserOptions.showOnlyShowBotsInTargetsFOV = sender.is_checked

    def on_show_goal_queue(self, sender):
        UserOptions.showGoalsOfSelectedBot = sender.is_checked

    def on_show_path(self, sender):
        UserOptions.showPathOfSelectedBot = sender.is_checked

    def on_show_w_appraisals(self, sender):
        UserOptions.showWeaponAppraisals = sender.is_checked

    def on_show_goal_appraisals(self, sender):
        UserOptions.showGoalAppraisals = sender.is_checked

    def on_init_msg_box_closed(self, *args):
        self.raven_game.is_paused = False
        self._check_menu_item_appropriately()

    def _on_menu_open(self, menu_item):
        self._menu_is_open = True

    def _on_menu_close(self, menu_item):
        self._menu_is_open = False

    def _check_menu_item_appropriately(self):
        self._mi_toggle_pause.is_checked = self.raven_game.is_paused

        self._mi_show_graph.is_checked = UserOptions.showGraph
        self._mi_show_node_indices.is_checked = UserOptions.showNodeIndices
        self._mi_smooth_paths_quick.is_checked = UserOptions.smoothPathsQuick
        self._mi_smooth_paths_precise.is_checked = UserOptions.smoothPathsPrecise
        self._mi_show_wall_normals.is_checked = UserOptions.showDebugInfo

        self._mi_show_bots_ids.is_checked = UserOptions.showBotIDs
        self._mi_show_bots_health.is_checked = UserOptions.showBotHealth
        self._mi_show_bots_scores.is_checked = UserOptions.showScore

        self._mi_show_target.is_checked = UserOptions.showTargetOfSelectedBot
        self._mi_show_sensed_opponents.is_checked = UserOptions.showOpponentsSensedBySelectedBot
        self._mi_show_opponents_in_fov.is_checked = UserOptions.showOnlyShowBotsInTargetsFOV
        self._mi_show_goal_queue.is_checked = UserOptions.showGoalsOfSelectedBot
        self._mi_show_path.is_checked = UserOptions.showPathOfSelectedBot
        self._mi_show_weapon_appraisals.is_checked = UserOptions.showWeaponAppraisals
        self._mi_show_goal_appraisals.is_checked = UserOptions.showGoalAppraisals
