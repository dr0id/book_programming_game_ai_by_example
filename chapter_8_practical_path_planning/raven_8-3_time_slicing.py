#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function, division

import params
from raven import main
from raven.useroptions import UserOptions

if __name__ == "__main__":

    # set corresponding options for this demo
    UserOptions.showGraph = True
    UserOptions.showNodeIndices = False
    UserOptions.showPathOfSelectedBot = True
    UserOptions.showTargetOfSelectedBot = False
    UserOptions.showOpponentsSensedBySelectedBot = False
    UserOptions.showOnlyShowBotsInTargetsFOV = False
    UserOptions.showGoalsOfSelectedBot = False
    UserOptions.showGoalAppraisals = False
    UserOptions.showWeaponAppraisals = False
    UserOptions.smoothPathsQuick = False
    UserOptions.smoothPathsPrecise = False
    UserOptions.showBotIDs = False
    UserOptions.showBotHealth = False
    UserOptions.showScore = False
    UserOptions.showDebugInfo = False
    UserOptions.doAttack = False
    UserOptions.showNextNode = True
    UserOptions.showNumSearches = True

    params.StartMap = "maps/Raven_DM1_Fine.map"
    params.NumBots = 1

    # start the application
    app = main.App()
    app.caption = "Raven 8.3 - time slicing"
    app.run()
