
Raven implementation in python
==============================

The following files are the ones to run.
General advise:

  - press (p) to pause/resume the game
  - select a bot by right click on it for the first time
  - take control of a bot by right click on a selected bot again (it will stop)
      - left click now shoots the selected weapon
      - right click the position where the bot should move to
  - use 1-4 to change the weapon (if collected)
  - 'x' to deselect

raven.py
--------
This is the complete implementation. Use the mouse to change things through the
menus.

raven_8-1_coarse_graph.py
-------------------------
This example shows the coarse paths navigation. Some parameters are pre-set.

raven_8-2_path_smoothing.py
---------------------------
This example shows how paths are smoothed out (so the agent does not follow
each edge of the graph. Some parameters are pre-set.

raven_8-3_time_slicing.py
-------------------------
This example shows how multiple path searches are executed at the same time.
As the title already suggests this is done by slicing the calculations into slices 
and executing them over multiple 'frames'.


raven_8-4_bots_getting_stuck.py
-------------------------------
This example shows how the bot are getting stuck (let it run for a while).
The time that is used to check if a but is stuck is heavily increased to
demonstrate that they get stuck.






