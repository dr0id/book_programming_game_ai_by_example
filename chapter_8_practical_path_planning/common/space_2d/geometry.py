# -*- coding: utf-8 -*-
"""
//  Desc:   v simple inverted (y increases down screen) axis aligned bounding
//          box class

"""
from __future__ import print_function, division

import sys

from common.space_2d.transform import point_to_local_2d
from common.space_2d.vectors import Vec2

# __all__ = ["InvertedAABBox2D"]

# for testing purposes
_test_with_pygame = True

try:
    if _test_with_pygame is False:
        raise ImportError

    import pygame


    def _create_pygame_rect(topleft, bottomright):
        # using cheap int rounding..
        rect = pygame.Rect(int(topleft.x + 0.5), int(topleft.y + 0.5), int(bottomright.x - topleft.x + 0.5),
                           int(bottomright.y - topleft.y + 0.5))
        rect.normalize()
        return rect


    # book compatible implementation using a pygame.Rect internally
    class _InvertedAABBox2DRectBook(object):
        def __init__(self, topleft, bottomright):
            self._rect = pygame.Rect(topleft.x,
                                     topleft.y,
                                     topleft.x - bottomright.x,
                                     bottomright.y - topleft.y)

        @property
        def top(self):
            return self._rect.top

        @property
        def bottom(self):
            return self._rect.bottom

        @property
        def left(self):
            return self._rect.left

        @property
        def right(self):
            return self._rect.right

        @property
        def center(self):
            return Vec2((self.left + self.right) / 2.0, (self.top + self.bottom) / 2.0)

        @property
        def topleft(self):
            return Vec2(*self._rect.topleft)

        @property
        def bottomright(self):
            return Vec2(*self._rect.bottomright)

        @property
        def rect_tuple(self):
            return tuple(self._rect)

        def is_overlapped_with(self, other):
            return self._rect.colliderect(tuple(other)) == 1

        def __getitem__(self, item):
            return [self.left, self.top, self.right - self.left, self.bottom - self.top][item]


    InvertedAABBox2D = _create_pygame_rect
    # InvertedAABBox2D = _InvertedAABBox2DRectBook

except ImportError:

    # pygame.Rect compatible implementation
    class _InvertedAABBox2DRectMimic(object):
        def __init__(self, topleft, bottomright):
            self._bottomright = bottomright
            self._topleft = topleft
            self._center = (topleft + bottomright) / 2.0

        @property
        def top(self):
            return self._topleft.y

        @property
        def bottom(self):
            return self._bottomright.y

        @property
        def left(self):
            return self._topleft.x

        @property
        def right(self):
            return self._bottomright.x

        @property
        def center(self):
            return self._center.x, self._center.y  # return a tuple to be compatible to pygame.Rect

        @property
        def topleft(self):
            return self._topleft.x, self._topleft.y  # return a tuple to be compatible to pygame.Rect

        @property
        def bottomright(self):
            return self._bottomright.x, self._bottomright.y  # return a tuple to be compatible to pygame.Rect

        @property
        def rect_tuple(self):
            return self._topleft.x, \
                   self._topleft.y, \
                   self._bottomright.x - self._topleft.x, \
                   self._bottomright.y - self._topleft.y

        def __getitem__(self, item):
            return self.rect_tuple[item]

        # noinspection SpellCheckingInspection
        def colliderect(self, other):
            return not (other.top > self._bottomright.y or
                        other.bottom < self._topleft.y or
                        other.left > self._bottomright.x or
                        other.right < self._topleft.x)

    # book compatible implementation
    class _InvertedAABBox2DBook(object):
        def __init__(self, topleft, bottomright):
            self._bottomright = bottomright
            self._topleft = topleft
            self._center = (topleft + bottomright) / 2.0

        @property
        def top(self):
            return self._topleft.y

        @property
        def bottom(self):
            return self._bottomright.y

        @property
        def left(self):
            return self._topleft.x

        @property
        def right(self):
            return self._bottomright.x

        @property
        def center(self):
            return self._center.clone()

        @property
        def topleft(self):
            return self._topleft.clone()

        @property
        def bottomright(self):
            return self._bottomright.clone()

        @property
        def rect_tuple(self):
            return self._topleft.x, \
                   self._topleft.y, \
                   self._bottomright.x - self._topleft.x, \
                   self._bottomright.y - self._topleft.y

        def is_overlapped_with(self, other):
            return not (other.top > self._bottomright.y or
                        other.bottom < self._topleft.y or
                        other.left > self._bottomright.x or
                        other.right < self._topleft.x)

        def __getitem__(self, item):
            return [self.left, self.top, self.right - self.left, self.bottom - self.top][item]


    InvertedAABBox2D = _InvertedAABBox2DRectMimic
    # InvertedAABBox2D = _InvertedAABBox2DBook


def get_line_segment_circle_closest_intersection_point(a, b, pos, radius):
    to_b_norm = (b - a).normalized

    # move the circle into the local space defined by the vector b-a with origin at a
    # local_pos = point_to_local_space(pos, to_b_norm, to_b_norm.perp, a)
    local_pos = point_to_local_2d(pos, to_b_norm, to_b_norm.perp, a)

    ip_found = False
    impact_point = None

    #   //if the local position + the radius is negative then the circle lays behind
    #   //point A so there is no intersection possible. If the local x pos minus the
    #   //radius is greater than length A-B then the circle cannot intersect the
    #   //line segment
    if abs(local_pos.y) < radius:
        #         //now to do a line/circle intersection test. The center of the
        #         //circle is represented by A, B. The intersection points are
        #         //given by the formulae x = A +/-sqrt(r^2-B^2), y=0. We only
        #         //need to look at the smallest positive value of x.
        _a = local_pos.x
        _b = local_pos.y

        ip = _a - (radius * radius - _b * _b) ** 0.5
        if ip <= 0:
            ip = _a + (radius * radius - _b * _b) ** 0.5
        ip_found = True

        impact_point = a + to_b_norm * ip

    return ip_found, impact_point


def line_intersection_2d(a, b, c, d):
    r_top = (a.y - c.y) * (d.x - c.x) - (a.x - c.x) * (d.y - c.y)
    s_top = (a.y - c.y) * (b.x - a.x) - (a.x - c.x) * (b.y - a.y)
    bot = (b.x - a.x) * (d.y - c.y) - (b.y - a.y) * (d.x - c.x)

    if bot == 0:  # //parallel
        return False

    inv_bot = 1.0 / bot
    r = r_top * inv_bot
    s = s_top * inv_bot

    if (r > 0) and (r < 1) and (s > 0) and (s < 1):
        # lines intersect
        return True

    # lines do not intersect
    return False


def line_intersection_point_2d(a, b, c, d):
    r_top = (a.y - c.y) * (d.x - c.x) - (a.x - c.x) * (d.y - c.y)
    r_bot = (b.x - a.x) * (d.y - c.y) - (b.y - a.y) * (d.x - c.x)
    s_top = (a.y - c.y) * (b.x - a.x) - (a.x - c.x) * (b.y - a.y)
    s_bot = (b.x - a.x) * (d.y - c.y) - (b.y - a.y) * (d.x - c.x)

    if r_bot == 0 or s_bot == 0:  # //parallel
        return False, None, None

    r = r_top / r_bot
    s = s_top / s_bot

    if (r > 0) and (r < 1) and (s > 0) and (s < 1):
        # lines intersect

        _dist = a.get_distance(b) * r
        _point = a + r * (b - a)
        return True, _point, _dist

    # lines do not intersect
    return False, None, None


def dist_to_line_segment_sq(a, b, p):
    """
    given a line segment AB and a point P, this function calculates the
    perpendicular distance between them.
    Avoiding sqrt.
    """
    # if the angle is obtuse between pa and ab is obtuse then the closest vertex must be A
    dot_a = (p.x - a.x) * (b.x - a.x) + (p.y - a.y) * (b.y - a.y)

    if dot_a <= 0:
        return a.get_distance_sq(p)

    # if the angle is obtuse between pb and ab is obtuse then the closest vertex must be B
    dot_b = (p.x - b.x) * (a.x - b.x) + (p.y - b.y) * (a.y - b.y)

    if dot_b <= 0:
        return b.get_distance_sq(p)

    # calculate the point along AB that is the closest to p
    point = a + ((b - a) * dot_a) / (dot_a + dot_b)

    # calculate the distance p-point
    return p.get_distance_sq(point)


def line_segment_circle_intersection(a, b, p, r):
    """
    returns true if the line segemnt AB intersects with a circle at
    position P with radius radius
    """
    # first determine the distance from the center o the circle to
    # the line segment (working in distance squared space)
    dist_to_line_sq = dist_to_line_segment_sq(a, b, p)

    if dist_to_line_sq < r * r:
        return True
    else:
        return False


def do_lines_intersect_circle(walls, cur_pos, radius):
    # test against the walls
    for wall in walls:
        # do a line segment intersection test
        # if line_segment_circle_intersection(wall.start, wall.end, cur_pos, radius):
        #     return True
        # optimized by inlining method 'line_segment_circle_intersection'
        dist_to_line_sq = dist_to_line_segment_sq(wall.start, wall.end, cur_pos)

        if dist_to_line_sq < radius * radius:
            return True
        # else:
        #     return False

    return False


def find_closest_point_of_intersection_with_walls(a, b, walls):
    distance = sys.maxsize
    this_impact_point = None
    for wall in walls:
        is_intersecting, impact_point, dist = line_intersection_point_2d(a, b, wall.start, wall.end)
        if is_intersecting:
            if dist < distance:
                distance = dist
                this_impact_point = impact_point

    if distance < sys.maxsize:
        return True, this_impact_point, distance

    return False, this_impact_point, distance


if __name__ == '__main__':

    def _test_rect_like(r, b, other_not_overlapping, other_overlapping):
        assert tuple(r) == tuple(b)
        assert r.top == b.top
        assert r.bottom == b.bottom
        assert r.left == b.left
        assert r.right == b.right
        assert r.center == tuple(int(i) for i in b.center)
        assert r.bottomright == b.bottomright
        assert r.topleft == b.topleft
        assert r.colliderect(pygame.Rect(10, 10, 5, 5)) == b.colliderect(other_not_overlapping)
        assert r.colliderect(pygame.Rect(1, 1, 5, 5)) == b.colliderect(other_overlapping)
        print('rect like all done')


    def _test_book_like(b, other_no_overlap, other_overlapping):
        assert tuple([1, 2, 2, 3]) == tuple(b)
        assert 2 == b.top
        assert 5 == b.bottom
        assert 1 == b.left
        assert 3 == b.right
        assert 2 == b.center.x
        assert 3.5 == b.center.y
        assert 3 == b.bottomright.x
        assert 5 == b.bottomright.y
        assert 1 == b.topleft.x
        assert 2 == b.topleft.y

        assert b.is_overlapped_with(other_no_overlap) is False
        assert b.is_overlapped_with(other_overlapping) is True
        print('book like all done')


    import pygame

    if _test_with_pygame:
        print("testing with pygame")
        o1p = _create_pygame_rect(Vec2(10, 10), Vec2(15, 15)),
        o2p = _create_pygame_rect(Vec2(1, 1), Vec2(5, 5))
        _test_rect_like(pygame.Rect(1, 2, 2, 3), _create_pygame_rect(Vec2(1, 2), Vec2(3, 5)), o1p, o2p)

        o3p = _InvertedAABBox2DRectBook(Vec2(10, 10), Vec2(15, 15))
        o4p = _InvertedAABBox2DRectBook(Vec2(1, 1), Vec2(5, 5))
        _test_book_like(_InvertedAABBox2DRectBook(Vec2(1, 2), Vec2(3, 5)), o3p, o4p)
    else:
        print("testing book implementation")
        o2 = _InvertedAABBox2DRectMimic(Vec2(1, 1), Vec2(5, 5))
        o1 = _InvertedAABBox2DRectMimic(Vec2(10, 10), Vec2(15, 15))
        _test_rect_like(pygame.Rect(1, 2, 2, 3), _InvertedAABBox2DRectMimic(Vec2(1, 2), Vec2(3, 5)), o1, o2)

        o3 = _InvertedAABBox2DBook(Vec2(10, 10), Vec2(15, 15))
        o4 = _InvertedAABBox2DBook(Vec2(1, 1), Vec2(5, 5))
        _test_book_like(_InvertedAABBox2DBook(Vec2(1, 2), Vec2(3, 5)), o3, o4)
