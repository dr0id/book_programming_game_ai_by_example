# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'transform.py' is part of HG_book_programming_ai_by_example
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Module about coordinate transformations.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

import logging

from common.space_2d.vectors import Vec2

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 

logger = logging.getLogger(__name__)
logger.debug("importing...")

EPSILON = 1e-9


def point_to_local_2d(global_point, x_axis, y_axis, position):
    # m11 m12 m13       r11 r12 t13       xax xay -pox
    # m21 m22 m23  =>   r21 r22 t23  =>   yax yay -poy
    # m31 m32 m33       m31 m32 1.0       m31 m32 1.0
    assert 1.0 + EPSILON > x_axis.length > 1.0 - EPSILON
    assert 1.0 + EPSILON > y_axis.length > 1.0 - EPSILON
    gx = global_point.x - position.x
    gy = global_point.y - position.y
    return Vec2(x_axis.x * gx + x_axis.y * gy,
                y_axis.x * gx + y_axis.y * gy)


def point_to_world_2d(local_point, x_axis, y_axis, global_pos):
    # m11 m12 m13       r11 r12 t13       xax yax pox
    # m21 m22 m23  =>   r21 r22 t23  =>   xay yay poy
    # m31 m32 m33       m31 m32 1.0       m31 m32 1.0
    assert 1.0 + EPSILON > x_axis.length > 1.0 - EPSILON
    assert 1.0 + EPSILON > y_axis.length > 1.0 - EPSILON
    return Vec2(x_axis.x * local_point.x + y_axis.x * local_point.y + global_pos.x,
                x_axis.y * local_point.x + y_axis.y * local_point.y + global_pos.y)


def vector_to_world_2d(local_vector, x_axis, y_axis):
    # m11 m12 m13       r11 r12 t13       xax yax pox
    # m21 m22 m23  =>   r21 r22 t23  =>   xay yay poy
    # m31 m32 m33       m31 m32 1.0       m31 m32 1.0
    assert 1.0 + EPSILON > x_axis.length > 1.0 - EPSILON
    assert 1.0 + EPSILON > y_axis.length > 1.0 - EPSILON
    return Vec2(x_axis.x * local_vector.x + y_axis.x * local_vector.y,
                x_axis.y * local_vector.x + y_axis.y * local_vector.y)


def vector_to_local_2d(global_vector, x_axis, y_axis):
    # m11 m12 m13       r11 r12 t13       xax yax pox
    # m21 m22 m23  =>   r21 r22 t23  =>   xay yay poy
    # m31 m32 m33       m31 m32 1.0       m31 m32 1.0
    assert 1.0 + EPSILON > x_axis.length > 1.0 - EPSILON
    assert 1.0 + EPSILON > y_axis.length > 1.0 - EPSILON
    return Vec2(x_axis.x * global_vector.x + x_axis.y * global_vector.y,
                y_axis.x * global_vector.x + y_axis.y * global_vector.y)


logger.debug("imported")
