# -*- coding: utf-8 -*-
from __future__ import print_function, division


# ------------------------------------------------------------------------------

class Smoother(object):

    def __init__(self, zero_value, size=3):
        self._zero_value = zero_value
        self._size = size
        self._next_idx = 0
        self._values = [0] * int(size)

    def update(self, recent_value):
        self._values[self._next_idx] = recent_value
        self._next_idx += 1
        
        if self._next_idx >= self._size:
            self._next_idx = 0
            
        return sum(self._values, self._zero_value ) / self._size
        
        
