# -*- coding: utf-8 -*-
from __future__ import print_function, division

import logging
import math
import random
import sys
from random import random as rand_float

from common.space_2d.vectors import EPSILON
from common.space_2d.vectors import Vec2 as Vec

logger = logging.getLogger(__name__)


class SteeringParameters:

    def __init__(self):
        self.NumAgents = 300

        self.NumObstacles = 7
        self.MinObstacleRadius = 10
        self.MaxObstacleRadius = 50

        # number of horizontal cells used for spatial partitioning
        self.NumCellsX = 7
        # number of vertical cells used for spatial partitioning
        self.NumCellsY = 7

        # how many samples the smoother will use to average a value
        self.NumSamplesForSmoothing = 10

        # this is used to multiply the steering force AND all the multipliers
        # found in SteeringBehavior
        self.SteeringForceTweaker = 200.0
        self.DecelerationTweaker = 0.3

        self.SteeringForce = 2.0
        self.MaxSpeed = 150.0
        self.VehicleMass = 1.0
        self.VehicleScale = 3.0
        self.MaxTurnRatePerSecond = 3.1415926535897931

        # use these values to tweak the amount that each steering force
        # contributes to the total steering force
        self.SeparationWeight = 1.0
        self.AlignmentWeight = 1.0
        self.CohesionWeight = 2.0
        self.ObstacleAvoidanceWeight = 100.0
        self.WallAvoidanceWeight = 10.0
        self.WanderWeight = 1.0
        self.SeekWeight = 1.0
        self.FleeWeight = 1.0
        self.ArriveWeight = 1.0
        self.PursuitWeight = 1.0
        self.OffsetPursuitWeight = 1.0
        self.InterposeWeight = 1.0
        self.HideWeight = 1.0
        self.EvadeWeight = 0.01
        self.FollowPathWeight = 0.05

        # how close a neighbour must be before an agent perceives it (considers it
        # to be within its neighborhood)
        self.ViewDistance = 50.0

        # used in obstacle avoidance
        self.MinDetectionBoxLength = 40.0

        # used in wall avoidance
        self.WallDetectionFeelerLength = 40.0

        # these are the probabilities that a steering behavior will be used
        # when the Prioritized Dither calculate method is used to sum
        # combined behaviors
        self.prWallAvoidance = 0.5
        self.prObstacleAvoidance = 0.5
        self.prSeparation = 0.2
        self.prAlignment = 0.3
        self.prCohesion = 0.6
        self.prWander = 0.8
        self.prSeek = 0.8
        self.prFlee = 0.6
        self.prEvade = 1.0
        self.prHide = 0.8
        self.prArrive = 0.5

        # the of the constraining circle for the wander behavior
        self.WanderRad = 1.2
        # distance the wander circle is projected in front of the agent
        self.WanderDist = 2.0
        # the maximum amount of displacement along the circle each frame
        self.WanderJitterPerSec = 80.0
        # used in path following
        self.WaypointSeekDist = 20


class SteeringBehavior(object):
    # todo: implement on/off methods to avoid import of this enum values?
    # behavior types
    # NONE = 0 # does not make much sense
    SEEK = 1 << 0
    FLEE = 1 << 1
    ARRIVE = 1 << 2
    WANDER = 1 << 3
    COHESION = 1 << 4
    SEPARATION = 1 << 5
    ALIGNMENT = 1 << 6
    OBSTACLEAVOIDANCE = 1 << 7
    WALLAVOIDANCE = 1 << 8
    FOLLOWPATH = 1 << 9
    PURSUIT = 1 << 10
    EVADE = 1 << 11
    INTERPOSE = 1 << 12
    HIDE = 1 << 13
    FLOCK = 1 << 14
    OFFSETPURSUIT = 1 << 15

    behavior_to_string = {
        SEEK: "seek",
        FLEE: "flee",
        ARRIVE: "arrive",
        WANDER: "wander",
        COHESION: "cohesion",
        SEPARATION: "separation",
        ALIGNMENT: "alignment",
        OBSTACLEAVOIDANCE: "obstacle avoidance",
        WALLAVOIDANCE: "wall avoidance",
        FOLLOWPATH: "follow path",
        PURSUIT: "pursuit",
        EVADE: "evade",
        INTERPOSE: "interpose",
        HIDE: "hide",
        FLOCK: "flock",
        OFFSETPURSUIT: "offset pursuit"
    }

    # summing method
    WEIGHTEDAVERAGE = 1 << 1
    PRIORITIZED = 1 << 2
    DITHERED = 1 << 3

    SLOW = 3
    NORMAL = 2
    FAST = 1

    def __init__(self, vehicle, world, parameters=SteeringParameters()):
        self._vehicle = vehicle
        self._world = world

        self.target = None
        self.deceleration = self.NORMAL
        self._cellspaceon = False  # fixme: what is this for? Is it used?
        self.summing_method = self.PRIORITIZED
        self._steering_force = Vec(0.0, 0.0)
        # flags
        self._behavior_status = 0
        # ---- params ---- #
        self._params = parameters
        self._viewdistance = parameters.ViewDistance
        self._min_dboxlength = parameters.MinDetectionBoxLength
        self.dboxlength = parameters.MinDetectionBoxLength

        # ---- weights ---- #
        self._weightcohesion = parameters.CohesionWeight
        self._weightalignment = parameters.AlignmentWeight
        self._weightseparation = parameters.SeparationWeight
        self._weightobstacleavoidance = parameters.ObstacleAvoidanceWeight
        self._weightwallavoidance = parameters.WallAvoidanceWeight
        self._weightseek = parameters.SeekWeight
        self._weightflee = parameters.FleeWeight
        self._weightarrive = parameters.ArriveWeight
        self._weightoffsetpursuit = parameters.OffsetPursuitWeight
        self._weightinterpose = parameters.InterposeWeight
        self._weighthide = parameters.HideWeight
        self._weightevade = parameters.EvadeWeight
        self._weightfollowpath = parameters.FollowPathWeight
        self._weightpursuit = parameters.PursuitWeight
        self._weightwander = parameters.WanderWeight

        self._prwallavoidance = parameters.prWallAvoidance
        self._probstacleavoidance = parameters.prObstacleAvoidance
        self._prseparation = parameters.prSeparation
        self._pralignment = parameters.prAlignment
        self._prcohesion = parameters.prCohesion
        self._prwander = parameters.prWander
        self._prseek = parameters.prSeek
        self._prflee = parameters.prFlee
        self._prevade = parameters.prEvade
        self._prhide = parameters.prHide
        self._prarrive = parameters.prArrive

        # ---- params pursuit ---- #
        self.targeted_pursuit_position = None  # set by _pursuit else None!
        self.use_turn_arround_time = False
        self.turn_around_coefficient = -0.5  #
        # ---- params evade ---- #
        self.targeted_evader_position = None  # set by _pursuit else None!
        self.evade_thread_range = None
        # ---- parame interpose ---- #
        self.target_agent1 = None
        self.target_agent2 = None
        self.interpose_point = None
        # ---- params wander ---- #
        self.wander_distance = parameters.WanderDist
        self.wander_jitter = parameters.WanderJitterPerSec
        self.wander_radius = parameters.WanderRad
        theta = random.random() * 2.0 * math.pi
        self._wander_target = Vec(self.wander_radius * math.sin(theta), self.wander_radius * math.cos(theta))
        # ---- params arrive ---- #
        # this is needed because deceleration is a int between 1-3
        self.deceleration_tweaker = parameters.DecelerationTweaker
        # ---- params flee ---- #
        self.flee_panic_distance = None
        # ---- params wall avoidance ---- #
        self.feelers = []  # [Vec]
        # ---- params path following ---- #
        self.waypointseekdist = parameters.WaypointSeekDist
        self.path = None
        # ---- params group behaviors ---- #
        self.group_vehicles = None

    # ---- public ---- #

    def switch_on(self, behavior_type):
        logger.debug("switch on steering behavior %s", self.behavior_to_string[behavior_type])
        self._behavior_status |= behavior_type
        if behavior_type == self.FLOCK:
            self._behavior_status |= self.COHESION | self.SEPARATION | self.ALIGNMENT | self.WANDER

    def switch_off(self, behavior_type):
        logger.debug("switch off steering behavior %s", self.behavior_to_string[behavior_type])
        if self.is_on(behavior_type):
            self._behavior_status ^= behavior_type
        # reset special variables
        if behavior_type == self.PURSUIT:
            self.targeted_pursuit_position = None
        if behavior_type == self.EVADE:
            self.targeted_evader_position = None
        if behavior_type == self.INTERPOSE:
            self.interpose_point = None
        if behavior_type == self.FLOCK:
            if self.is_on(self.COHESION):
                self._behavior_status ^= self.COHESION
            if self.is_on(self.SEPARATION):
                self._behavior_status ^= self.SEPARATION
            if self.is_on(self.ALIGNMENT):
                self._behavior_status ^= self.ALIGNMENT

    def is_on(self, behavior_type):
        return bool(self._behavior_status & behavior_type)

    def calculate(self):
        self._steering_force.zero()  # reset

        if self.COHESION & self._behavior_status or self.SEPARATION & self._behavior_status or self.ALIGNMENT & self._behavior_status:
            assert self.group_vehicles is not None
            self._vehicle.world.tag_vehicles_within_view_range(self._vehicle, self.group_vehicles, self._viewdistance)

        if self.summing_method == self.WEIGHTEDAVERAGE:
            self._steering_force = self._calculate_weighted_sum()
        elif self.summing_method == self.PRIORITIZED:
            self._steering_force = self._calculate_prioritized()
        elif self.summing_method == self.DITHERED:
            self._steering_force = self._calculate_dithered()
        else:
            self._steering_force.zero()
            print("Warning no valid summing method")

        return self._steering_force

    # def forward_component(self):
    # return self._vehicle.heading.dot(self._steering_force)

    # def side_component(self):
    # return self._vehicle.side.dot(self._steering_force)

    # def set_target(self, target):
    # self.target = target

    # ---- summing methods ---- #

    def _calculate_weighted_sum(self):
        self._steering_force.zero()  # reset

        if self.SEEK & self._behavior_status:
            assert self.target is not None
            self._steering_force += self._seek(self.target) * self._weightseek
        if self.FLEE & self._behavior_status:
            assert self.target is not None
            self._steering_force += self._flee(self.target) * self._weightflee
        if self.ARRIVE & self._behavior_status:
            assert self.target is not None
            self._steering_force += self._arrive(self.target, self.deceleration) * self._weightarrive
        if self.INTERPOSE & self._behavior_status:
            assert self.target_agent1 is not None
            assert self.target_agent2 is not None
            self._steering_force += self._interpose(self.target_agent1, self.target_agent2) * self._weightinterpose
        if self.WANDER & self._behavior_status:
            self._steering_force += self._wander() * self._weightwander
        if self.PURSUIT & self._behavior_status:
            assert self.target_agent1 is not None
            self._steering_force += self._pursuit(self.target_agent1) * self._weightpursuit
        if self.EVADE & self._behavior_status:
            assert self.target_agent1 is not None
            self._steering_force += self._evade(self.target_agent1, self.evade_thread_range) * self._weightevade
        if self.OBSTACLEAVOIDANCE & self._behavior_status:
            self._steering_force += self._obstacle_avoidance(self._vehicle.obstacles) * self._weightobstacleavoidance
        if self.WALLAVOIDANCE & self._behavior_status:
            self._steering_force += self._wall_avoidance(self._vehicle.world.walls) * self._weightwallavoidance
        if self.HIDE & self._behavior_status:
            assert self.target_agent1 is not None
            self._steering_force += self._hide(self.target_agent1, self._vehicle.obstacles) * self._weighthide
        if self.FOLLOWPATH & self._behavior_status:
            assert self.path is not None
            self._steering_force += self._follow_path() * self._weightfollowpath
        if self.OFFSETPURSUIT & self._behavior_status:
            assert self.target_agent1 is not None
            assert self.offset is not None
            self._steering_force += self._offset_pursuit(self.target_agent1, self.offset) * self._weightoffsetpursuit

        if self.COHESION & self._behavior_status:
            # uses previously tagged vehicles
            self._steering_force += self._cohesion(self.group_vehicles) * self._weightcohesion  # * 0.01

        if self.SEPARATION & self._behavior_status:
            # uses previously tagged vehicles
            self._steering_force += self._separation(self.group_vehicles) * self._weightseparation  # * 5

        if self.ALIGNMENT & self._behavior_status:
            # uses previously tagged vehicles
            self._steering_force += self._alignment(self.group_vehicles) * self._weightalignment

        self._steering_force.truncate(self._vehicle.max_force)
        return self._steering_force

    def _calculate_prioritized(self):
        # NOTE: the order of the if statements is the prioritation
        self._steering_force.zero()  # reset
        force = Vec(0.0, 0.0)

        if self.WALLAVOIDANCE & self._behavior_status:
            force = self._wall_avoidance(self._vehicle.world.walls) * self._weightwallavoidance
            if self._exceed_accumulate_force(force):
                return self._steering_force

        if self.OBSTACLEAVOIDANCE & self._behavior_status:
            force = self._obstacle_avoidance(self._vehicle.obstacles) * self._weightobstacleavoidance
            if self._exceed_accumulate_force(force):
                return self._steering_force

        if self.EVADE & self._behavior_status:
            assert self.target_agent1 is not None
            force = self._evade(self.target_agent1, self.evade_thread_range) * self._weightevade
            if self._exceed_accumulate_force(force):
                return self._steering_force

        if self.FLEE & self._behavior_status:
            assert self.target is not None
            force = self._flee(self.target) * self._weightflee
            if self._exceed_accumulate_force(force):
                return self._steering_force

        if self.SEPARATION & self._behavior_status:
            # uses previously tagged vehicles
            force = self._separation(self.group_vehicles) * self._weightseparation
            if self._exceed_accumulate_force(force):
                return self._steering_force

        if self.ALIGNMENT & self._behavior_status:
            # uses previously tagged vehicles
            force = self._alignment(self.group_vehicles) * self._weightalignment
            if self._exceed_accumulate_force(force):
                return self._steering_force

        if self.COHESION & self._behavior_status:
            # uses previously tagged vehicles
            force = self._cohesion(self.group_vehicles) * self._weightcohesion
            if self._exceed_accumulate_force(force):
                return self._steering_force

        if self.SEEK & self._behavior_status:
            assert self.target is not None
            force = self._seek(self.target) * self._weightseek
            if self._exceed_accumulate_force(force):
                return self._steering_force
        if self.ARRIVE & self._behavior_status:
            assert self.target is not None
            force = self._arrive(self.target, self.deceleration) * self._weightarrive
            if self._exceed_accumulate_force(force):
                return self._steering_force
        if self.WANDER & self._behavior_status:
            force = self._wander() * self._weightwander
            if self._exceed_accumulate_force(force):
                return self._steering_force
        if self.PURSUIT & self._behavior_status:
            assert self.target_agent1 is not None
            force = self._pursuit(self.target_agent1) * self._weightpursuit
            if self._exceed_accumulate_force(force):
                return self._steering_force
        if self.OFFSETPURSUIT & self._behavior_status:
            assert self.target_agent1 is not None
            assert self.offset is not None
            force = self._offset_pursuit(self.target_agent1, self.offset) * self._weightoffsetpursuit
            if self._exceed_accumulate_force(force):
                return self._steering_force
        if self.INTERPOSE & self._behavior_status:
            assert self.target_agent1 is not None
            assert self.target_agent2 is not None
            force = self._interpose(self.target_agent1, self.target_agent2) * self._weightinterpose
            if self._exceed_accumulate_force(force):
                return self._steering_force
        if self.HIDE & self._behavior_status:
            assert self.target_agent1 is not None
            force = self._hide(self.target_agent1, self._vehicle.obstacles) * self._weighthide
            if self._exceed_accumulate_force(force):
                return self._steering_force
        if self.FOLLOWPATH & self._behavior_status:
            assert self.path is not None
            force = self._follow_path() * self._weightfollowpath
            if self._exceed_accumulate_force(force):
                return self._steering_force

        return self._steering_force

    def _exceed_accumulate_force(self, force):
        # //calculate how much steering force the vehicle has used so far
        magnitude_so_far = self._steering_force.length
        # //calculate how much steering force remains to be used by this vehicle
        magnitude_remaining = self._vehicle.max_force - magnitude_so_far
        # //return true if there is no more force left to use
        if magnitude_remaining <= 0.0:
            return True
        # //calculate the magnitude of the force we want to add
        magnitude_to_add = force.length
        # //if the magnitude of the sum of ForceToAdd and the running total
        # //does not exceed the maximum force available to this vehicle, just
        # //add together. Otherwise add as much of the ForceToAdd vector is
        # //possible without going over the max.
        if magnitude_to_add < magnitude_remaining:
            self._steering_force += force
        else:
            # //add it to the steering force
            self._steering_force += force.normalized * magnitude_remaining
        # return false;
        return False

    def _calculate_dithered(self):
        self._steering_force.zero()  # reset

        if self.WALLAVOIDANCE & self._behavior_status and rand_float() < self._prwallavoidance:
            self._steering_force += self._wall_avoidance(
                self._vehicle.world.walls) * self._weightwallavoidance / self._prwallavoidance
            if not self._steering_force.is_zero():
                self._steering_force.truncate(self._vehicle.max_force)
                return self._steering_force

        if self.OBSTACLEAVOIDANCE & self._behavior_status and rand_float() < self._probstacleavoidance:
            self._steering_force += self._obstacle_avoidance(
                self._vehicle.obstacles) * self._weightobstacleavoidance / self._probstacleavoidance
            if not self._steering_force.is_zero():
                self._steering_force.truncate(self._vehicle.max_force)
                return self._steering_force

        if self.SEPARATION & self._behavior_status and rand_float() < self._prseparation:
            # uses previously tagged vehicles
            self._steering_force += self._separation(self.group_vehicles) * self._weightseparation / self._prseparation
            if not self._steering_force.is_zero():
                self._steering_force.truncate(self._vehicle.max_force)
                return self._steering_force

        if self.FLEE & self._behavior_status and rand_float() < self._prflee:
            assert self.target is not None
            self._steering_force += self._flee(self.target) * self._weightflee / self._prflee
            if not self._steering_force.is_zero():
                self._steering_force.truncate(self._vehicle.max_force)
                return self._steering_force

        if self.EVADE & self._behavior_status and rand_float() < self._prevade:
            assert self.target_agent1 is not None
            self._steering_force += self._evade(self.target_agent1,
                                                self.evade_thread_range) * self._weightevade / self._prevade
            if not self._steering_force.is_zero():
                self._steering_force.truncate(self._vehicle.max_force)
                return self._steering_force

        if self.ALIGNMENT & self._behavior_status and rand_float() < self._pralignment:
            # uses previously tagged vehicles
            self._steering_force += self._alignment(self.group_vehicles) * self._weightalignment / self._pralignment
            if not self._steering_force.is_zero():
                self._steering_force.truncate(self._vehicle.max_force)
                return self._steering_force

        if self.COHESION & self._behavior_status and rand_float() < self._prcohesion:
            # uses previously tagged vehicles
            self._steering_force += self._cohesion(self.group_vehicles) * self._weightcohesion / self._prcohesion
            if not self._steering_force.is_zero():
                self._steering_force.truncate(self._vehicle.max_force)
                return self._steering_force

        if self.WANDER & self._behavior_status and rand_float() < self._prwander:
            self._steering_force += self._wander() * self._weightwander / self._prwander
            if not self._steering_force.is_zero():
                self._steering_force.truncate(self._vehicle.max_force)
                return self._steering_force

        # if self.INTERPOSE & self._behavior_status and rand_float() < :
        # assert self.target_agent1 is not None
        # assert self.target_agent2 is not None
        # self._steering_force += self._interpose(self.target_agent1, self.target_agent2) * self._weightinterpose

        # if self.PURSUIT & self._behavior_status and rand_float() < :
        # assert self.target_agent1 is not None
        # self._steering_force += self._pursuit(self.target_agent1) * self._weightpursuit

        if self.SEEK & self._behavior_status and rand_float() < self._prseek:
            assert self.target is not None
            self._steering_force += self._seek(self.target) * self._weightseek / self._prseek
            if not self._steering_force.is_zero():
                self._steering_force.truncate(self._vehicle.max_force)
                return self._steering_force

        if self.ARRIVE & self._behavior_status and rand_float() < self._prarrive:
            assert self.target is not None
            self._steering_force += self._arrive(self.target, self.deceleration) * self._weightarrive / self._prarrive
            if not self._steering_force.is_zero():
                self._steering_force.truncate(self._vehicle.max_force)
                return self._steering_force

        if self.HIDE & self._behavior_status and rand_float() < self._prhide:
            self._steering_force += self._hide(self.target_agent1,
                                               self._vehicle.obstacles) * self._weighthide / self._prhide
            if not self._steering_force.is_zero():
                self._steering_force.truncate(self._vehicle.max_force)
                return self._steering_force

        # if self.FOLLOWPATH & self._behavior_status and rand_float() < :
        # assert self.path is not None
        # self._steering_force += self._follow_path() * self._weightfollowpath
        # if self.OFFSETPURSUIT & self._behavior_status and rand_float() < :
        # assert self.target_agent1 is not None
        # assert self.offset is not None
        # self._steering_force += self._offset_pursuit(self.target_agent1, self.offset) * self._weightoffsetpursuit

        return self._steering_force

    # ---- behavior helper methods ---- #

    def _seek(self, target_pos):
        desired_vel = (target_pos - self._vehicle.position).normalized * \
                      self._vehicle.max_speed
        return desired_vel - self._vehicle.velocity

    def _flee(self, target_pos):
        if self.flee_panic_distance:
            if self._vehicle.position.get_distance_sq(target_pos) > self.flee_panic_distance * self.flee_panic_distance:
                return Vec(0.0, 0.0)
        desired_vel = (self._vehicle.position - target_pos).normalized * \
                      self._vehicle.max_speed
        return desired_vel - self._vehicle.velocity

    def _arrive(self, target_pos, deceleration):
        to_target = target_pos - self._vehicle.position
        dist = to_target.length

        if dist > 1e-9:  # todo tolerance?
            speed = dist / (deceleration * self.deceleration_tweaker)
            # make sure velocity does not exceed max
            if speed > self._vehicle.max_speed:
                speed = self._vehicle.max_speed
            desired_velocity = to_target * speed / dist
            return desired_velocity - self._vehicle.velocity
        return Vec(0, 0)

    def _wander(self):
        self._wander_target += Vec(2 * random.random() - 1.0, 2 * random.random() - 1.0) \
                               * self.wander_jitter  # * 0.001
        self._wander_target.normalize()
        self._wander_target *= self.wander_radius

        target_local = Vec(self.wander_distance, 0) + self._wander_target
        target_world = point_to_world_2d(target_local, self._vehicle.heading, self._vehicle.side,
                                         self._vehicle.position)
        return target_world - self._vehicle.position

    def _pursuit(self, evader):
        to_evader = evader.position - self._vehicle.position
        relative_heading = self._vehicle.heading.dot(evader.heading)
        if to_evader.dot(self._vehicle.heading) > 0 and relative_heading < -0.95:
            self.targeted_pursuit_position = evader.position
            return self._seek(evader.position)

        look_ahead_time = to_evader.length / (self._vehicle.max_speed + evader.velocity.length)
        if self.use_turn_arround_time:
            look_ahead_time += self._turn_arround_time(self._vehicle, evader.position)
        self.targeted_pursuit_position = evader.position + evader.velocity * look_ahead_time
        return self._seek(self.targeted_pursuit_position)

    def _turn_arround_time(self, agent, target_position):
        to_target = (target_position - agent.position).normalized
        dot = agent.heading.dot(to_target)
        return (dot - 1.0) * self.turn_around_coefficient

    def _evade(self, pursuer, thread_range):
        to_pursuer = pursuer.position - self._vehicle.position

        if thread_range and to_pursuer.length_sq > thread_range * thread_range:
            return Vec(0.0, 0.0)

        look_ahead_time = to_pursuer.length / (self._vehicle.max_speed + pursuer.velocity.length)
        self.targeted_evader_position = pursuer.position + pursuer.velocity * look_ahead_time
        return self._flee(self.targeted_evader_position)

    def _obstacle_avoidance(self, obstacles):
        self.dboxlength = self._min_dboxlength + (
                self._vehicle.velocity.length / self._vehicle.max_speed) * self._min_dboxlength

        # self._vehicle.world.tag_obstacles_within_view_range(self._vehicle, obstacles, self.dboxlength)
        _obstacles = self._vehicle.world.get_obstacles_within_view_range(self._vehicle, obstacles, self.dboxlength)

        closest_intersecting_obstacle = None
        dist_to_closest_IP = sys.maxsize
        local_pos_of_closest_obstacle = Vec(0.0, 0.0)

        for obstacle in _obstacles:
            # for obstacle in self._vehicle.world.obstacles:
            # if obstacle.tag:
            circle_x, circle_y = point_to_local_2d(obstacle.position, self._vehicle.heading, self._vehicle.side,
                                                   self._vehicle.position).as_xy_tuple()
            # obstacles behind the vehicle are ignored
            if circle_x >= 0:
                expanded_radius = self._vehicle.bounding_radius + obstacle.bounding_radius
                # intersecting the detection box
                if abs(circle_y) < expanded_radius:
                    # find cicle line intersection points

                    sqrt_part = math.sqrt(expanded_radius * expanded_radius + circle_y * circle_y)

                    ip = circle_x - sqrt_part

                    if ip <= 0:
                        ip = circle_x + sqrt_part

                    # see if this is the closes so far
                    if ip < dist_to_closest_IP:
                        dist_to_closest_IP = ip
                        closest_intersecting_obstacle = obstacle
                        local_pos_of_closest_obstacle = Vec(circle_x, circle_y)

        # calculte steering force away from obstacle
        steering_force = Vec(0.0, 0.0)
        if closest_intersecting_obstacle:
            # the closer, the stronger the steering force should be
            multiplier = 1.0 + (self.dboxlength - local_pos_of_closest_obstacle.x) / self.dboxlength
            # lateral force
            steering_force.y = (
                                       closest_intersecting_obstacle.bounding_radius - local_pos_of_closest_obstacle.y) * multiplier
            self.break_weight = 0.2
            # apply a breaking force proportional to the distance to obstacle
            steering_force.x = (
                                       closest_intersecting_obstacle.bounding_radius - local_pos_of_closest_obstacle.x) * self.break_weight

            # convert the steering vector to global space
        return vector_to_world_2d(steering_force, self._vehicle.heading, self._vehicle.side)

    def _wall_avoidance(self, walls):
        self.feelers = self._vehicle.create_feelers()
        dist_to_IP = 0
        dist_to_closest_IP = sys.maxsize
        closest_wall = None
        closest_point = Vec(0.0, 0.0)
        point = Vec(0.0, 0.0)
        steering_force = Vec(0.0, 0.0)

        for feeler in self.feelers:
            for wall in walls:
                intersection, dist_to_IP, point = line_intersection_2d(self._vehicle.position, feeler, wall.start,
                                                                       wall.end)
                if intersection:
                    if dist_to_IP < dist_to_closest_IP:
                        dist_to_closest_IP = dist_to_IP
                        closest_wall = wall
                        closest_point = point

            if closest_wall:
                overshoot = feeler - closest_point
                # create force away from wall
                steering_force = closest_wall.normal * overshoot.length

        return steering_force

    def _interpose(self, agent1, agent2):
        midpoint = (agent1.position + agent2.position) * 0.5
        time_to_reach_midpoint = self._vehicle.position.get_distance(midpoint) / self._vehicle.max_speed

        a_pos = agent1.position + agent1.velocity * time_to_reach_midpoint
        b_pos = agent2.position + agent2.velocity * time_to_reach_midpoint

        self.interpose_point = (a_pos + b_pos) * 0.5

        return self._arrive(self.interpose_point, self.FAST)

    def _hide(self, target, obstacles):
        dist_to_closest = sys.maxsize
        best_hiding_spot = Vec(0.0, 0.0)

        for obstacle in obstacles:
            # getting hiding spot
            distance_from_boundary = 30.0
            dist_away = obstacle.bounding_radius + distance_from_boundary
            to_ob = (obstacle.position - target.position).normalized
            hiding_spot = (to_ob * dist_away) + obstacle.position

            dist = self._vehicle.position.get_distance(hiding_spot)
            if dist < dist_to_closest:
                dist_to_closest = dist
                best_hiding_spot = hiding_spot

        if dist_to_closest == sys.maxsize:
            return self._evade(target)

        return self._arrive(best_hiding_spot, self.FAST)

    def _follow_path(self):
        if self.path.current_waypoint.get_distance_sq(
                self._vehicle.position) < self.waypointseekdist * self.waypointseekdist:
            self.path.set_next_waypoint()

        if self.path.is_finished:
            return self._arrive(self.path.current_waypoint, self.NORMAL)
        else:
            return self._seek(self.path.current_waypoint)

    def _offset_pursuit(self, leader, offset):
        world_offset_point = point_to_world_2d(offset, leader.heading, leader.side, leader.position)

        to_offset = world_offset_point - self._vehicle.position

        look_ahead_time = to_offset.length / (self._vehicle.max_speed + leader.velocity.length)

        return self._arrive(world_offset_point + leader.velocity * look_ahead_time, self.FAST)

    def _separation(self, vehicles):
        # uses previously tagged vehicles
        steering_force = Vec(0.0, 0.0)
        for vehicle in vehicles:
            # if vehicle is not self._vehicle and vehicle.tag is True:
            if vehicle.tag is True and vehicle is not self._vehicle:
                to_agent = self._vehicle.position - vehicle.position
                # scale force inversily proportional to the agents distance
                # steering_force += to_agent.normalized / to_agent.length
                length = float(to_agent.length)
                if length > 0:
                    force = (to_agent / length) / length
                    # print length, force.length
                    steering_force += force
        return steering_force

    def _alignment(self, vehicles):
        # uses previously tagged vehicles
        average_heading = Vec(0.0, 0.0)
        count = 0
        for vehicle in vehicles:
            # if vehicle is not self._vehicle and vehicle.tag is True:
            if vehicle.tag is True and vehicle is not self._vehicle:
                average_heading += vehicle.heading
                count += 1

        if count > 0:
            average_heading /= float(count)
            # substract vehicles heading to get steering force
            return average_heading - self._vehicle.heading
        return Vec(0.0, 0.0)

    def _cohesion(self, vehicles):
        # uses previously tagged vehicles
        center_of_mass = Vec(0.0, 0.0)
        count = 0
        for vehicle in vehicles:
            # todo input should be the relevant vehicles and then use sum methods and similar for speed
            # if vehicle is not self._vehicle and vehicle.tag is True:
            if vehicle.tag is True and vehicle is not self._vehicle:
                center_of_mass += vehicle.position
                count += 1

        if count > 0:
            center_of_mass /= float(count)
            return self._seek(center_of_mass)
        return center_of_mass


def line_intersection_2d(A, B, C, D):
    rTop = (A.y - C.y) * (D.x - C.x) - (A.x - C.x) * (D.y - C.y)
    rBot = (B.x - A.x) * (D.y - C.y) - (B.y - A.y) * (D.x - C.x)

    sTop = (A.y - C.y) * (B.x - A.x) - (A.x - C.x) * (B.y - A.y)
    sBot = (B.x - A.x) * (D.y - C.y) - (B.y - A.y) * (D.x - C.x)

    if (rBot == 0) or (sBot == 0):
        # lines are parallel
        return False, None, None

    r = rTop / rBot
    s = sTop / sBot

    if (r > 0) and (r < 1) and (s > 0) and (s < 1):
        dist = A.get_distance(B) * r
        point = A + r * (B - A)
        return True, dist, point
    else:
        return False, 0, None


def point_to_world_2d(local_point, x_axis, y_axis, global_pos):
    # m11 m12 m13       r11 r12 t13       xax yax pox
    # m21 m22 m23  =>   r21 r22 t23  =>   xay yay poy
    # m31 m32 m33       m31 m32 1.0       m31 m32 1.0
    assert x_axis.length < 1.0 + EPSILON and x_axis.length > 1.0 - EPSILON
    assert y_axis.length < 1.0 + EPSILON and y_axis.length > 1.0 - EPSILON
    return Vec(x_axis.x * local_point.x + y_axis.x * local_point.y + global_pos.x,
               x_axis.y * local_point.x + y_axis.y * local_point.y + global_pos.y)


def point_to_local_2d(global_point, x_axis, y_axis, position):
    # m11 m12 m13       r11 r12 t13       xax xay -pox
    # m21 m22 m23  =>   r21 r22 t23  =>   yax yay -poy
    # m31 m32 m33       m31 m32 1.0       m31 m32 1.0
    assert x_axis.length < 1.0 + EPSILON and x_axis.length > 1.0 - EPSILON
    assert y_axis.length < 1.0 + EPSILON and y_axis.length > 1.0 - EPSILON
    gx = global_point.x - position.x
    gy = global_point.y - position.y
    return Vec(x_axis.x * gx + x_axis.y * gy,
               y_axis.x * gx + y_axis.y * gy)


def vector_to_world_2d(local_vector, x_axis, y_axis):
    # m11 m12 m13       r11 r12 t13       xax yax pox
    # m21 m22 m23  =>   r21 r22 t23  =>   xay yay poy
    # m31 m32 m33       m31 m32 1.0       m31 m32 1.0
    assert x_axis.length < 1.0 + EPSILON and x_axis.length > 1.0 - EPSILON
    assert y_axis.length < 1.0 + EPSILON and y_axis.length > 1.0 - EPSILON
    return Vec(x_axis.x * local_vector.x + y_axis.x * local_vector.y,
               x_axis.y * local_vector.x + y_axis.y * local_vector.y)


def vector_to_local_2d(global_vector, x_axis, y_axis):
    # m11 m12 m13       r11 r12 t13       xax yax pox
    # m21 m22 m23  =>   r21 r22 t23  =>   xay yay poy
    # m31 m32 m33       m31 m32 1.0       m31 m32 1.0
    assert x_axis.length < 1.0 + EPSILON and x_axis.length > 1.0 - EPSILON
    assert y_axis.length < 1.0 + EPSILON and y_axis.length > 1.0 - EPSILON
    return Vec(x_axis.x * global_vector.x + x_axis.y * global_vector.y,
               y_axis.x * global_vector.x + y_axis.y * global_vector.y)


if __name__ == '__main__':
    x_axis = Vec(1, 1).normalized
    y_axis = Vec(-1, 1).normalized
    position = Vec(10, 0)
    local_point = Vec(45, 77)
    print("local", local_point)
    p = point_to_world_2d(local_point, x_axis, y_axis, position)
    # p = point_to_world_2d(local_point, x_axis, y_axis, Vec(0.0, 0.0))
    print("world", p)
    # p = point_to_local_2d(Vec(0, 1.41421356237), x_axis, y_axis, Vec(0,0))
    p = point_to_local_2d(p, x_axis, y_axis, position)
    # p = point_to_local_2d(p, x_axis, y_axis, Vec(0.0, 0.0))
    print("local again", p)
    print(p == local_point)

    global_point = Vec(20.0, 33.0)
    print("global", global_point)
    p = point_to_local_2d(global_point, x_axis, y_axis, position)
    print("local", p)
    p = point_to_world_2d(p, x_axis, y_axis, position)
    print("global again", p)
    print(p == global_point)
