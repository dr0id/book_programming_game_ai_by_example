# -*- coding: utf-8 -*-
from __future__ import print_function, division

import glob
import os
import os.path
import string

import pygame

# QUIT	     none
# ACTIVEEVENT	     gain, state
# KEYDOWN	     unicode, key, mod
# KEYUP	     key, mod
# MOUSEMOTION	     pos, rel, buttons
# MOUSEBUTTONUP    pos, button
# MOUSEBUTTONDOWN  pos, button
# JOYAXISMOTION    joy, axis, value
# JOYBALLMOTION    joy, ball, rel
# JOYHATMOTION     joy, hat, value
# JOYBUTTONUP      joy, button
# JOYBUTTONDOWN    joy, button
# VIDEORESIZE      size, w, h
# VIDEOEXPOSE      none
# USEREVENT        code


HANDLED = True
UNHANDLED = False


class _Color(object):
    black = (0, 0, 0)
    background = (240, 240, 240)
    yellow = (240, 240, 0)
    orange = (240, 200, 0)
    white = (255, 255, 255)
    border_dark = (105, 105, 105)
    border_light_dark = (160, 160, 160)
    border_light = (227, 227, 227)
    blue = (0, 0, 230)


class Anchors(object):
    NoAnchor = 0
    Top = 1 << 0
    Right = 1 << 1
    Bottom = 1 << 2
    Left = 1 << 3
    TopLeft = Top | Left
    TopRight = Top | Right
    BottomLeft = Bottom | Left
    BottomRight = Bottom | Right
    LeftAndRight = Left | Right
    TopAndBottom = Top | Bottom
    All = Top | Bottom | Left | Right


class _Node(object):
    color = _Color()
    root = None
    default_font = None
    anchors = Anchors.TopLeft  # bitwise combination of Anchors
    rect = pygame.Rect(0, 0, 0, 0)  # absolute coordinates (e.g. window/root coordinates)

    def __init__(self):
        self.children = []
        self.visible = True
        self.parent = None

    def init(self):
        for child in self.children:
            child.init()

    def append(self, child):
        self.children.append(child)
        child.parent = self

    def draw(self, surf):
        if self.visible:
            for child in self.children:
                if child.visible:
                    child.draw(surf)

    def handle_event(self, event):
        if self.visible:
            for child in reversed(self.children):
                if child.visible:  # check not needed because each child does this too?
                    if HANDLED == child.handle_event(event):
                        return HANDLED
        return UNHANDLED

    def resize(self, old_parent_rect):
        old_rect = self.rect.copy()
        t, b = self._calc(self.anchors & Anchors.Top,
                          self.anchors & Anchors.Bottom,
                          self.rect.top, self.rect.bottom,
                          old_parent_rect.top, old_parent_rect.bottom,
                          self.parent.rect.top, self.parent.rect.bottom)
        l, r = self._calc(self.anchors & Anchors.Left,
                          self.anchors & Anchors.Right,
                          self.rect.left, self.rect.right,
                          old_parent_rect.left, old_parent_rect.right,
                          self.parent.rect.left, self.parent.rect.right)
        self.rect = pygame.Rect(l, t, r - l, b - t)

        for child in self.children:
            child.resize(old_rect)

    def _calc(self, top_anchor, bottom_anchor, top, bottom, old_parent_rect_top, old_parent_rect_bottom,
              parent_rect_top, parent_rect_bottom):
        if top_anchor and bottom_anchor:
            # resize, maintain distance to top and bottom
            top_dy = top - old_parent_rect_top
            bottom_dy = old_parent_rect_bottom - bottom
            return parent_rect_top + top_dy, parent_rect_bottom - bottom_dy
        elif top_anchor:
            # maintain distance to top
            top_dy = top - old_parent_rect_top
            h = bottom - top
            return parent_rect_top + top_dy, parent_rect_top + top_dy + h
        elif bottom_anchor:
            # maintain distance to bottom
            bottom_dy = old_parent_rect_bottom - bottom
            h = bottom - top
            return parent_rect_bottom - bottom_dy - h, parent_rect_bottom - bottom_dy
        else:
            # move half resize size, maintain relative distance
            oh = old_parent_rect_bottom - old_parent_rect_top
            nh = parent_rect_bottom - parent_rect_top
            dh = oh - nh
            return top - dh // 2, bottom - dh // 2


class GuiRoot(_Node):

    def __init__(self, size):
        _Node.__init__(self)
        if _Node.root is not None:
            raise Exception("GuiRoot can only be instantiated once!")
        _Node.root = self
        self.parent = self
        self.rect = pygame.Rect((0, 0), size)

        # self.keyb_focus = None

    def init(self):
        # init font?
        if self.default_font is None:
            _Node.default_font = pygame.font.Font(None, 18)
        _Node.init(self)

    def print_structure(self):
        stack = [(0, self)]
        while stack:
            level, node = stack.pop(-1)
            print("\t" * level + "{0} {1} rect: {2}".format(node.visible, node, node.rect))
            for child in node.children:
                stack.append((level + 1, child))

    def handle_event(self, event):

        if event.type == pygame.VIDEORESIZE:
            self._resize(event.size)
            return UNHANDLED

        return _Node.handle_event(self, event)

    def _resize(self, size):
        old_rect = self.rect.copy()
        self.rect.size = size
        for child in self.children:
            child.resize(old_rect)


class Panel(_Node):

    def __init__(self, pos, size):
        _Node.__init__(self)
        self.rect = pygame.Rect(pos, size)
        self.anchors = Anchors.All

    def init(self):
        _Node.init(self)

        pos_x = self.rect.left + 2  # fixme: 2 is magic number for border or similar'
        for child in self.children:
            child.rect.left = pos_x

            child.rect.centery = self.rect.centery
            pos_x += child.rect.width + 1

    def draw(self, surf):
        surf.fill(self.color.background, self.rect)
        # pygame.draw.line(surf, self.color.white, self.rect.topleft, self.rect.topright, 1)
        # pygame.draw.line(surf, (82, 82, 82), (self.rect.left, self.rect.bottom - 1),  # todo: magic color
        #                  (self.rect.right, self.rect.bottom - 1), 1)

        _Node.draw(self, surf)


class GfxPanel(_Node):
    def __init__(self, pos, size, draw_cb_to_use):
        _Node.__init__(self)
        self.rect = pygame.Rect(pos, size)
        self.anchors = Anchors.All
        self.draw_cb = draw_cb_to_use  # callback: draw(surf)

    def draw(self, surf):
        if self.draw_cb:
            self.draw_cb(surf.subsurface(self.rect))

        _Node.draw(self, surf)


class Bar(_Node):

    def __init__(self, pos, size):
        _Node.__init__(self)
        self.rect = pygame.Rect(pos, size)
        self.anchors = Anchors.NoAnchor

    def init(self):
        _Node.init(self)

        pos_x = self.rect.left + 2
        for child in self.children:
            child.rect.left = pos_x

            child.rect.centery = self.rect.centery
            pos_x += child.rect.width + 1

    def draw(self, surf):
        surf.fill(self.color.background, self.rect)
        pygame.draw.line(surf, self.color.white, self.rect.topleft, self.rect.topright, 1)
        pygame.draw.line(surf, (82, 82, 82), (self.rect.left, self.rect.bottom - 1),  # todo: magic color
                         (self.rect.right, self.rect.bottom - 1), 1)

        _Node.draw(self, surf)


class StatusBar(Bar):

    def __init__(self, pos, size):
        Bar.__init__(self, pos, size)
        self.text = ""
        self._text_changed = True
        self.rendered_text = None
        self.anchors = Anchors.BottomLeft | Anchors.BottomRight

    def set_text(self, text):
        self.text = text
        self._text_changed = True

    def init(self):
        Bar.init(self)

    def draw(self, surf):
        Bar.draw(self, surf)

        if self._text_changed:
            self._text_changed = False
            self.rendered_text = self.default_font.render(self.text, True, self.color.black, self.color.background)

        surf.blit(self.rendered_text, (self.rect.left + 5, self.rect.top + 2))


class MenuBar(Bar):

    def __init__(self, pos, size):
        Bar.__init__(self, pos, size)
        self.is_open = False
        self.anchors = Anchors.TopLeft | Anchors.TopRight

    def init(self):
        _Node.init(self)

        pos_x = 0
        for child in self.children:
            child.rect.left = pos_x

            child.rect.top = self.rect.top + 2
            pos_x += child.rect.width
            child.update_position()

    def reset(self):
        pass

    def handle_event(self, event):
        handled = UNHANDLED
        if self.visible:
            if event.type == pygame.MOUSEMOTION:
                pos = event.pos
                self.is_open = False
                for child in self.children:
                    if child.is_open:
                        self.is_open = True
                        break
                if self.is_open:
                    for child in self.children:
                        if child.rect.collidepoint(*pos):
                            if child.is_open:
                                pass
                            else:
                                for ch in self.children:
                                    ch.on_close()
                                child.on_open()
                                break
            elif event.type == pygame.MOUSEBUTTONDOWN:
                pos = event.pos
                items = list(self.children)
                hit = False
                while items:
                    item = items.pop()
                    items.extend(item.children)

                    if item.rect.collidepoint(*pos):
                        hit = True
                if not hit:
                    for child in self.children:
                        child.reset()

            for child in self.children:
                if HANDLED == child.handle_event(event):
                    # return HANDLED
                    handled = HANDLED
        return handled


class MenuItem(_Node):

    def __init__(self, text, callback=None, on_open_cb=None, on_close_cb=None):
        _Node.__init__(self)
        self.text = text
        self.visible = True
        self.rendered_text = None
        self.rect = None
        self.is_open = False
        self.hover = False

        self.callback = callback
        if callback is None:
            self.callback = self._try_open

        self.on_open_cb = on_open_cb
        self.on_close_cb = on_close_cb

    def init(self):
        self.rendered_text = self.default_font.render(self.text, True, self.color.black)
        self.rect = self.rendered_text.get_rect()
        self.rect.width += self.rect.height + self.rect.height // 2
        _Node.init(self)
        for child in self.children:
            child.visible = False

    def update_position(self, parent_is_menu_item=False):

        pos_y = self.rect.top

        rect_width = None
        if self.children:
            rect_width = self.children[0].rect.unionall(self.children).width

        if not parent_is_menu_item:
            pos_y += self.rect.height

        for child in self.children:
            if parent_is_menu_item:
                child.rect.left = self.rect.right
            else:
                child.rect.left = self.rect.left

            child.rect.width = rect_width if rect_width is not None else child.rect.width
            child.rect.top = pos_y
            pos_y += child.rect.height
            child.update_position(True)

    def draw(self, surf):
        if self.visible:
            if self.hover or self.is_open:
                surf.fill(self.color.orange, self.rect)
            else:
                surf.fill(self.color.background, self.rect)
            left = self.rect.left  # + self.rect.height
            surf.blit(self.rendered_text, (left + self.rect.height // 2, self.rect.top))
            pygame.draw.line(surf, self.color.white, (left, self.rect.top),
                             (left, self.rect.top + self.rect.height - 1), 1)
            pygame.draw.line(surf, (227, 227, 227), (left + 1, self.rect.top),  # todo: magic color
                             (left + 1, self.rect.top + self.rect.height - 1), 1)
            # if self.hover or self.is_open:
            # pygame.draw.rect(surf, self.color.orange, self.rect, 1)

            if self.is_open:
                if self.children:
                    rect = self.children[0].rect.unionall(self.children)
                    surf.fill(self.color.background, rect)

            _Node.draw(self, surf)

    def _try_open(self, sender):
        if self.is_open:
            self.on_close()
        else:
            self.on_open()

    def append(self, item):
        self.callback = self._try_open
        _Node.append(self, item)

    def on_open(self):
        self.is_open = True
        for child in self.children:
            child.visible = True
        if self.on_open_cb:
            self.on_open_cb(self)

    def on_close(self):
        self.is_open = False
        for child in self.children:
            child.visible = False
        if self.on_close_cb:
            self.on_close_cb(self)

    def reset(self):
        self.on_close()
        if self.parent.is_open:
            self.parent.reset()
        for child in self.children:
            if child.is_open:
                child.reset()

    def on_click(self):
        self.callback(self)
        if not self.children:
            self.reset()
        # TODO: close all open menues

    def handle_event(self, event):
        handled = UNHANDLED
        if self.visible:
            if event.type == pygame.MOUSEMOTION:
                pos = event.pos
                if self.rect.collidepoint(*pos):
                    self.hover = True
                    # return HANDLED
                    handled = HANDLED
                else:
                    self.hover = False
            elif event.type == pygame.MOUSEBUTTONDOWN:
                pos = event.pos
                if self.rect.collidepoint(*pos):
                    if event.button == 1:  # left mouse button
                        self.on_click()
                        # return HANDLED
                        handled = HANDLED

            for child in self.children:
                if HANDLED == child.handle_event(event):
                    # return HANDLED
                    handled = HANDLED
        return handled


class CheckableMenuItem(MenuItem):

    def __init__(self, text, callback=None, checked=False, on_open_cb=None, on_close_cb=None):
        MenuItem.__init__(self, text, callback, on_open_cb, on_close_cb)
        self.is_checked = checked

    def on_click(self):
        self.is_checked = not self.is_checked
        MenuItem.on_click(self)

    def draw(self, surf):
        # MenuItem.draw(self, surf)
        if self.visible:
            if self.hover or self.is_open:
                surf.fill(self.color.orange, self.rect)
            else:
                surf.fill(self.color.background, self.rect)
            left = self.rect.left + self.rect.height
            surf.blit(self.rendered_text, (left + self.rect.height // 2, self.rect.top))
            pygame.draw.line(surf, self.color.white, (left, self.rect.top),
                             (left, self.rect.top + self.rect.height - 1), 1)
            pygame.draw.line(surf, (227, 227, 227), (left + 1, self.rect.top),  # todo: magic color
                             (left + 1, self.rect.top + self.rect.height - 1), 1)
            # if self.hover or self.is_open:
            # pygame.draw.rect(surf, self.color.orange, self.rect, 1)

            if self.is_open:
                if self.children:
                    rect = self.children[0].rect.unionall(self.children)
                    surf.fill(self.color.background, rect)

            _Node.draw(self, surf)

        if self.is_checked:
            x, y = self.rect.topleft
            # fixme: make a icon draw lib?
            pygame.draw.polygon(surf, (0, 0, 200),
                                ((x + 2, y + 7), (x + 5, y + 9), (x + 10, y + 2), (x + 5, y + 12), (x + 2, y + 7)), 0)


class ImageToggleButton(_Node):

    def __init__(self, pos, image, callback):
        _Node.__init__(self)
        self.pos = pos
        self.rect = pygame.Rect(pos, (22, 22))
        self.image = image
        self.callback = callback
        self.is_pressed = False
        self.hover = False
        self.tag = None

    def handle_event(self, event):
        handled = UNHANDLED
        if self.visible:
            if event.type == pygame.MOUSEMOTION:
                pos = event.pos
                if self.rect.collidepoint(*pos):
                    self.hover = True
                    # return HANDLED
                else:
                    self.hover = False
            elif event.type == pygame.MOUSEBUTTONDOWN:
                pos = event.pos
                if self.rect.collidepoint(*pos):
                    if event.button == 1:  # left mouse button
                        self.on_click()
                        # return HANDLED
                        handled = HANDLED

            for child in self.children:
                if HANDLED == child.handle_event(event):
                    # return HANDLED
                    handled = HANDLED
        return handled

    def on_click(self):
        self.is_pressed = not self.is_pressed
        if self.is_pressed:
            self.callback(self)

    def reset(self):
        self.is_pressed = False

    def draw(self, surf):
        if self.visible:
            surf.fill(self.color.background, self.rect)
            x, y = self.rect.topleft
            w, h = self.rect.size
            h -= 1
            if self.is_pressed:
                pygame.draw.line(surf, self.color.border_dark, (x, y), (x + w, y), 1)
                pygame.draw.line(surf, self.color.border_dark, (x, y), (x, y + h), 1)
                pygame.draw.line(surf, self.color.border_light_dark, (x + 1, y + 1), (x + w, y + 1), 1)
                pygame.draw.line(surf, self.color.border_light_dark, (x + 1, y + 1), (x + 1, y + h), 1)

                pygame.draw.line(surf, self.color.border_light, (x + w - 1, y + h - 1), (x + w - 1, y + 1), 1)
                pygame.draw.line(surf, self.color.border_light, (x + w - 1, y + h - 1), (x + 1, y + h - 1), 1)
                pygame.draw.line(surf, self.color.white, (x + w, y + h), (x + w, y), 1)
                pygame.draw.line(surf, self.color.white, (x + w, y + h), (x, y + h), 1)

            else:
                pygame.draw.line(surf, self.color.white, (x, y), (x + w, y), 1)
                pygame.draw.line(surf, self.color.white, (x, y), (x, y + h), 1)
                pygame.draw.line(surf, self.color.border_light, (x + 1, y + 1), (x + w, y + 1), 1)
                pygame.draw.line(surf, self.color.border_light, (x + 1, y + 1), (x + 1, y + h), 1)

                pygame.draw.line(surf, self.color.border_light_dark, (x + w - 1, y + h - 1), (x + w - 1, y + 1), 1)
                pygame.draw.line(surf, self.color.border_light_dark, (x + w - 1, y + h - 1), (x + 1, y + h - 1), 1)
                pygame.draw.line(surf, self.color.border_dark, (x + w, y + h), (x + w, y), 1)
                pygame.draw.line(surf, self.color.border_dark, (x + w, y + h), (x, y + h), 1)
            surf.blit(self.image, (x + 3, y + 3))


class Button(_Node):

    def __init__(self, pos, text, callback):
        _Node.__init__(self)
        self._callback = callback
        self._pos = pos
        self._text = text
        self._rendered_text = None
        self.rect = pygame.Rect(pos, (0, 0))
        self.hover = False
        self.is_pressed = False

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, value):
        self._text = value
        self.init()

    def init(self):
        self._rendered_text = self.default_font.render(self._text, True, self.color.black)
        w, h = self._rendered_text.get_rect().size
        self.rect.width = w + 16
        self.rect.height = h + 8

    def draw(self, surf):
        if self.visible:
            if self.hover:
                # pygame.draw.rect(surf, self.color.orange, self.rect, 1)
                surf.fill(self.color.orange, self.rect)
            else:
                surf.fill(self.color.background, self.rect)
            x, y = self.rect.topleft
            w, h = self.rect.size
            h -= 1
            if self.is_pressed:
                pygame.draw.line(surf, self.color.border_dark, (x, y), (x + w, y), 1)
                pygame.draw.line(surf, self.color.border_dark, (x, y), (x, y + h), 1)
                pygame.draw.line(surf, self.color.border_light_dark, (x + 1, y + 1), (x + w, y + 1), 1)
                pygame.draw.line(surf, self.color.border_light_dark, (x + 1, y + 1), (x + 1, y + h), 1)

                pygame.draw.line(surf, self.color.border_light, (x + w - 1, y + h - 1), (x + w - 1, y + 1), 1)
                pygame.draw.line(surf, self.color.border_light, (x + w - 1, y + h - 1), (x + 1, y + h - 1), 1)
                pygame.draw.line(surf, self.color.white, (x + w, y + h), (x + w, y), 1)
                pygame.draw.line(surf, self.color.white, (x + w, y + h), (x, y + h), 1)
            else:
                pygame.draw.line(surf, self.color.white, (x, y), (x + w, y), 1)
                pygame.draw.line(surf, self.color.white, (x, y), (x, y + h), 1)
                pygame.draw.line(surf, self.color.border_light, (x + 1, y + 1), (x + w, y + 1), 1)
                pygame.draw.line(surf, self.color.border_light, (x + 1, y + 1), (x + 1, y + h), 1)

                pygame.draw.line(surf, self.color.border_light_dark, (x + w - 1, y + h - 1), (x + w - 1, y + 1), 1)
                pygame.draw.line(surf, self.color.border_light_dark, (x + w - 1, y + h - 1), (x + 1, y + h - 1), 1)
                pygame.draw.line(surf, self.color.border_dark, (x + w, y + h), (x + w, y), 1)
                pygame.draw.line(surf, self.color.border_dark, (x + w, y + h), (x, y + h), 1)
            surf.blit(self._rendered_text, self._rendered_text.get_rect(center=self.rect.center))

    def handle_event(self, event):
        handled = UNHANDLED
        if self.visible:
            if event.type == pygame.MOUSEMOTION:
                pos = event.pos
                if self.rect.collidepoint(*pos):
                    self.hover = True
                    # return HANDLED
                else:
                    self.hover = False
            elif event.type == pygame.MOUSEBUTTONDOWN:
                self.is_pressed = False
                pos = event.pos
                if self.rect.collidepoint(*pos):
                    if event.button == 1:  # left mouse button
                        self.is_pressed = True

            elif event.type == pygame.MOUSEBUTTONUP:
                pos = event.pos
                if self.is_pressed:
                    if event.button == 1:  # left mouse button
                        self.is_pressed = False
                        if self.rect.collidepoint(*pos):
                            self.on_click()
                        return HANDLED

            for child in self.children:
                if HANDLED == child.handle_event(event):
                    # return HANDLED
                    handled = HANDLED
        return handled

    def on_click(self):
        self._callback(self)


class _Dialog(_Node):

    def __init__(self, caption, closed_cb=None):
        _Node.__init__(self)
        self.rect = None
        self._caption = caption
        self._rendered_caption = None
        self.anchors = Anchors.NoAnchor
        self.close_cb = closed_cb

    def close(self):
        # self.root.children.pop(-1)
        # might not be anymore the topmost window!
        self.root.children.remove(self)
        if self.close_cb:
            self.close_cb()

    def show(self):
        # children are in draw order
        self.root.append(self)
        self.init()

    def init(self):
        _Node.init(self)
        self._rendered_caption = self.default_font.render(self._caption, True, self.color.black, self.color.background)

    def draw(self, surf):
        if self.visible:

            if self.rect is None:
                self.rect = surf.get_rect()
                self.rect.inflate_ip(-self.rect.width // 3, -self.rect.height // 3)

            surf.fill(self.color.background, self.rect)

            surf.blit(self._rendered_caption, self.rect.move(3, 5).topleft)
            _Node.draw(self, surf)

            # draw a border
            x, y = self.rect.topleft
            w, h = self.rect.size
            h -= 1
            pygame.draw.line(surf, self.color.white, (x, y), (x + w, y), 1)
            pygame.draw.line(surf, self.color.white, (x, y), (x, y + h), 1)
            pygame.draw.line(surf, self.color.border_light, (x + 1, y + 1), (x + w, y + 1), 1)
            pygame.draw.line(surf, self.color.border_light, (x + 1, y + 1), (x + 1, y + h), 1)

            pygame.draw.line(surf, self.color.border_light_dark, (x + w - 1, y + h - 1), (x + w - 1, y + 1), 1)
            pygame.draw.line(surf, self.color.border_dark, (x + w, y + h), (x + w, y), 1)
            pygame.draw.line(surf, self.color.border_light_dark, (x + w - 1, y + h - 1), (x + 1, y + h - 1), 1)
            pygame.draw.line(surf, self.color.border_dark, (x + w, y + h), (x, y + h), 1)

            h = 20
            pygame.draw.line(surf, self.color.border_light_dark, (x + w - 1, y + h - 1), (x + 1, y + h - 1), 1)
            pygame.draw.line(surf, self.color.border_dark, (x + w, y + h), (x, y + h), 1)

    def handle_event(self, event):
        if self.visible:
            # not sure if this is the appropriate way to close any dialog
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self.close()
                    return HANDLED
            for child in reversed(self.children):
                if child.visible:
                    if HANDLED == child.handle_event(event):
                        return HANDLED
            return HANDLED  # this is a modal dialog
        return UNHANDLED


class MessageBox(_Dialog):

    def __init__(self, caption, message, closed_cb=None):
        _Dialog.__init__(self, caption, closed_cb)
        self._message = message
        self._rendered_message = None
        self.rect = pygame.Rect(0, 0, 100, 100)

        self.append(Button((0, 0), "Ok", self.on_ok_button))

    def on_ok_button(self, sender):
        self.close()

    def init(self):
        _Dialog.init(self)
        renders = []
        height = 0
        width = 0
        for text in self._message.split("\n"):
            render = self.default_font.render(text, True, self.color.black, self.color.background)
            height += render.get_rect().height
            w = render.get_rect().width
            width = w if w > width else width
            renders.append(render)
        self._rendered_message = pygame.Surface((width, height))
        self._rendered_message.fill(self.color.background)
        pos = 0
        for render in renders:
            self._rendered_message.blit(render, (0, pos))
            pos += render.get_rect().height
        self.rect.height = height + 100
        self.rect.width = width + 40
        self.rect.center = self.parent.rect.center
        self.children[0].rect.centerx = self.rect.centerx
        self.children[0].rect.bottom = self.rect.bottom - 5

    def draw(self, surf):
        if self.visible:
            _Dialog.draw(self, surf)
            surf.blit(self._rendered_message, self.rect.move(5, 30).topleft)

    def handle_event(self, event):
        if self.visible:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE or event.key == pygame.K_RETURN:
                    self.close()
                    return HANDLED
            for child in reversed(self.children):
                if child.visible:
                    if HANDLED == child.handle_event(event):
                        return HANDLED
            return HANDLED  # this is a modal dialog
        return UNHANDLED


class YesNoMessageBox(MessageBox):

    def __init__(self, caption, message, yes_callback=None, no_callback=None):
        MessageBox.__init__(self, caption, message)
        self._yes_callback = yes_callback
        self._no_callback = no_callback
        self.append(Button((0, 0), "No", self.on_no_button))

    def on_no_button(self, sender):
        self.close()
        if self._no_callback:
            self._no_callback(self)

    def on_ok_button(self, sender):
        self.close()
        if self._yes_callback:
            self._yes_callback(self)

    def init(self):
        MessageBox.init(self)
        self.children[1].rect.bottomright = (self.rect.right - 5, self.rect.bottom - 5)
        self.children[0].text = "Yes"  # yes button
        self.children[0].rect.bottomright = (self.children[1].rect.left - 5, self.rect.bottom - 5)


class OpenDialog(_Dialog):

    def __init__(self, caption, initial_path, success_callback, extension_filter="*.*"):
        _Dialog.__init__(self, caption)
        self.filename = None
        self.rect = None
        self._view_rect = None
        self._initial_path = initial_path
        self._extension_filter = extension_filter
        self._success_callback = success_callback

        # gui
        self._ok_button = Button((0, 0), "Ok", self.on_ok_button)
        self.append(self._ok_button)
        self._cancel_button = Button((0, 0), "Cancel", self.on_cancel_button)
        self.append(self._cancel_button)
        self._list_view = ListView()
        self._list_view.activate_item_callback = self.on_ok_button
        self.append(self._list_view)

    def on_ok_button(self, sender):
        if self._list_view.selected is not None:
            self.filename = os.path.join(self._initial_path, self._list_view.items[self._list_view.selected])
            self._success_callback(self)
            self.close()
        else:
            msg_box = MessageBox("Warning", "Nothing selected!")
            msg_box.show()

    def on_cancel_button(self, sender):
        self.close()

    def init(self):
        files = glob.glob(os.path.join(self._initial_path, self._extension_filter))
        files.sort()
        for file in files:
            self._list_view.items.append(os.path.basename(file))
        _Dialog.init(self)

    def draw(self, surf):
        if self.visible:
            if self.rect is None:
                self.rect = surf.get_rect()
                self.rect.inflate_ip(-self.rect.width // 3, -self.rect.height // 3)
                self._cancel_button.rect.bottomright = (self.rect.right - 5, self.rect.bottom - 5)
                # self._ok_button.rect.topleft = (self.rect.left + 50, self.rect.bottom - 30)
                self._ok_button.rect.bottomright = (self._cancel_button.rect.left - 5, self.rect.bottom - 5)
                self._view_rect = self.rect.inflate(-self.rect.width // 8, -self.rect.height // 8)
                self._view_rect.center = self.rect.center
                self._view_rect.height = self._view_rect.height - 30
                self._view_rect.top += 15
                self._list_view.rect = pygame.Rect(self._view_rect)

            _Dialog.draw(self, surf)


class ItemList(list):

    def __init__(self, item_added_callback):
        list.__init__(self)
        self._item_added_callback = item_added_callback

    def append(self, item):
        list.append(self, item)
        self._item_added_callback(self, item)


class ListView(_Node):
    _double_click_time = 250

    def __init__(self):
        _Node.__init__(self)
        self.items = ItemList(self._on_new_item)
        self.rect = None
        self.selected = None

        self._hovered = None
        self._rendered_items = []
        self._item_rects = []
        self.selection_changed_callback = None
        self.activate_item_callback = None
        self._last_mb_down = 0

    def _on_new_item(self, sender, item):
        self._update_items()

    def _update_items(self):
        if self.default_font is not None:
            self._rendered_items = []
            self._item_rects = []
            pos = 1
            for idx, text in enumerate(self.items):
                rendered_text = self.default_font.render(text, True, self.color.black, self.color.background)
                if self.selected == idx:
                    rendered_text = self.default_font.render(text, True, self.color.black, self.color.orange)
                self._rendered_items.append(rendered_text)
                self._item_rects.append(rendered_text.get_rect(topleft=(5, pos)))
                pos += self._item_rects[-1].height + 3

    def init(self):
        self._update_items()
        _Node.init(self)

    def draw(self, surf):
        if self.visible:
            surf.set_clip(self.rect)
            surf.fill(self.color.background, self.rect)
            for idx, item in enumerate(self._rendered_items):
                item_rect = self._item_rects[idx].move(self.rect.left, self.rect.top)
                surf.blit(item, item_rect)
                if idx == self._hovered:
                    pygame.draw.rect(surf, self.color.orange, item_rect, 2)
            pygame.draw.rect(surf, self.color.border_light_dark, self.rect, 1)

            _Node.draw(self, surf)
            surf.set_clip(None)

    def handle_event(self, event):
        if self.visible:
            if event.type == pygame.MOUSEBUTTONDOWN:
                now = pygame.time.get_ticks()
                delta = now - self._last_mb_down
                self._last_mb_down = now
                if event.button == 1:  # left mouse button
                    pos = pygame.Rect(event.pos, (0, 0))
                    pos.move_ip((-self.rect.left, -self.rect.top))
                    idx = pos.collidelist(self._item_rects)
                    if delta < self._double_click_time:
                        if idx == self.selected:
                            if self.activate_item_callback:
                                self.activate_item_callback(self)
                    else:
                        if idx > -1:
                            self.selected = idx
                            if self.selection_changed_callback is not None:
                                self.selection_changed_callback(self)
                            self._update_items()
                            return HANDLED
            elif event.type == pygame.MOUSEMOTION:
                pos = pygame.Rect(event.pos, (0, 0))
                pos.move_ip((-self.rect.left, -self.rect.top))
                idx = pos.collidelist(self._item_rects)
                if idx > -1:
                    self._hovered = idx
                else:
                    self._hovered = None

            return _Node.handle_event(self, event)
        return UNHANDLED


class SaveDialog(_Dialog):

    def __init__(self, caption, initial_path, callback, extension_filter="*.*"):
        _Dialog.__init__(self, caption)
        self.rect = pygame.Rect(0, 0, 0, 0)
        self.append(Button((0, 0), "Ok", self.on_ok_button))
        self.append(Button((0, 0), "Cancel", self.on_cancel_button))
        self._initial_path = initial_path
        self._callback = callback
        self._extension_filter = extension_filter

        self._list_view = ListView()
        self._list_view.selection_changed_callback = self.on_selection_changed
        self._list_view.activate_item_callback = self.on_activate_item
        self.append(self._list_view)
        self._text_box = TextBox()
        self.append(self._text_box)

    def init(self):
        _Dialog.init(self)
        self.rect = pygame.Rect(self.parent.rect)
        self.rect.inflate_ip(-self.parent.rect.width // 4, -self.parent.rect.height // 4)
        self.rect.center = self.parent.rect.center
        self.children[1].rect.right = self.rect.right - 5
        self.children[1].rect.bottom = self.rect.bottom - 5
        self.children[0].rect.right = self.children[1].rect.left - 5
        self.children[0].rect.bottom = self.rect.bottom - 5
        self._text_box.rect.bottom = self.rect.bottom - 5 - self.children[1].rect.height - 5
        self._text_box.rect.width = self.rect.width - self.rect.width // 8
        self._text_box.rect.centerx = self.rect.centerx
        self._text_box.text = ""
        files = glob.glob(os.path.join(self._initial_path, self._extension_filter))
        files.sort()
        for file in files:
            self._list_view.items.append(os.path.basename(file))
        self._list_view.rect = self.rect.inflate(-self.rect.width // 8, -self.rect.height // 8)
        self._list_view.rect.center = self.rect.center
        self._list_view.rect.height = self._list_view.rect.height - 50
        self._list_view.rect.top += 15

    def on_ok_button(self, sender):
        name = self._text_box.text
        suffix = ".map"
        if not name.endswith(suffix):
            name += suffix
        self.filename = os.path.join(self._initial_path, name)
        if os.path.exists(self.filename):
            msgbox = YesNoMessageBox("Warning: File exists", "Overwrite file \n {0}?".format(name), self._on_write_file)
            msgbox.show()
        else:
            self._on_write_file(None)

    def _on_write_file(self, sender):
        self.close()
        self._callback(self)

    def on_cancel_button(self, sender):
        self.close()

    def close(self):
        self._text_box.cleanup()
        _Dialog.close(self)

    def on_activate_item(self, sender):
        self.on_selection_changed(sender)
        self.on_ok_button(sender)

    def on_selection_changed(self, sender):
        self._text_box.text = ""
        if self._list_view.selected is not None:
            self._text_box.text = self._list_view.items[self._list_view.selected]


class TextBox(_Node):

    def __init__(self):
        _Node.__init__(self)

        self._rendered_cur_pos = 0
        self._rendered_text = pygame.Surface((0, 0))
        self.rect = pygame.Rect(0, 0, 0, 20)
        self._is_init = False
        self.allowed_character = string.ascii_letters + string.digits + "_-."
        self._cur_on = True
        self._text = ""
        self._cur_pos = len(self._text)

    def init(self):
        _Node.init(self)
        self._is_init = True
        pygame.time.set_timer(pygame.USEREVENT, 500)
        pygame.key.set_repeat(350, 50)
        self.text = ""  # make cursor visible

    def cleanup(self):
        pygame.time.set_timer(pygame.USEREVENT, 0)
        pygame.key.set_repeat()

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, value):
        self._text = value
        self._update_rendered_text()
        self._cur_pos = len(self._text)
        self._updat_cur_pos()

    def _update_rendered_text(self):
        if self._is_init:
            self._rendered_text = self.default_font.render(self._text, True, self.color.black, self.color.white)

    def _updat_cur_pos(self):
        if self._is_init:
            self._rendered_cur_pos = self.rect.left + 2 + self.default_font.size(self._text[:self._cur_pos])[0]

    def draw(self, surf):
        if self.visible:
            surf.fill(self.color.white, self.rect)
            surf.set_clip(self.rect)

            surf.blit(self._rendered_text, self.rect.move(2, 2))

            # draw cursor
            if self._cur_on:
                pygame.draw.line(surf, self.color.black, (self._rendered_cur_pos, self.rect.top),
                                 (self._rendered_cur_pos, self.rect.bottom), 1)

            _Node.draw(self, surf)
            surf.set_clip(None)
            pygame.draw.rect(surf, self.color.border_light_dark, self.rect, 1)

    def handle_event(self, event):
        handled = UNHANDLED
        if self.visible:
            if event.type == pygame.USEREVENT:
                self._cur_on = not self._cur_on
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    self._cur_pos -= 1 if self._cur_pos > 0 else 0
                    handled = HANDLED
                elif event.key == pygame.K_RIGHT:
                    self._cur_pos += 1 if self._cur_pos < len(self._text) else 0
                    handled = HANDLED
                elif event.key == pygame.K_DELETE:
                    self._text = self._text[:self._cur_pos] + self._text[self._cur_pos + 1:]
                    self._update_rendered_text()
                    handled = HANDLED
                elif event.key == pygame.K_BACKSPACE:
                    if self._cur_pos > 0:
                        self._cur_pos -= 1
                        self._text = self._text[:self._cur_pos] + self._text[self._cur_pos + 1:]
                        self._update_rendered_text()
                        handled = HANDLED
                elif event.key == pygame.K_END:
                    self._cur_pos = len(self._text)
                    handled = HANDLED
                elif event.key == pygame.K_HOME:
                    self._cur_pos = 0
                    handled = HANDLED
                elif len(event.unicode) > 0 and event.unicode in self.allowed_character:
                    self._text = self._text[:self._cur_pos] + event.unicode + self._text[self._cur_pos:]
                    self._text = self._text.lower()
                    self._cur_pos += 1
                    self._update_rendered_text()
                    handled = HANDLED
                self._updat_cur_pos()

            if handled == UNHANDLED:
                return _Node.handle_event(self, event)
        return handled
