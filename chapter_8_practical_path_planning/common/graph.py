# -*- coding: utf-8 -*-
from __future__ import print_function, division

import logging
import array
import collections
import random
from heapq import heappush, heappop, heapify

from common.space_2d.vectors import Vec2 as Vec

logger = logging.getLogger(__name__)
# todo more logs
INVALID_NODE_INDEX = -1


class GraphNode(object):
    __slots__ = ['index']

    def __init__(self, idx=INVALID_NODE_INDEX):
        """
        index:
        //every node has an index. A valid index is >= 0
        """
        self.index = idx

    def __str__(self):
        return "<{0}(index: {1})>".format(self.__class__.__name__, self.index)


class NavGraphNode(GraphNode):
    """
    //  Graph node for use in creating a navigation graph. This node contains
    //  the position of the node and a pointer to a BaseGameEntity... useful
    //  if you want your nodes to represent health packs, gold mines and the like
    """
    __slots__ = ['position', 'extra_info']

    def __init__(self, pos_x, pos_y, idx=INVALID_NODE_INDEX):  # fixme: pass in a Vec instead of x,y
        """
        position_x, position_y
        //the node's position

        extra_info:
        //often you will require a navgraph node to contain additional information.
        //For example a node might represent a pickup such as armor in which
        //case m_ExtraInfo could be an enumerated value denoting the pickup type,
        //thereby enabling a search algorithm to search a graph for specific items.
        //Going one step further, m_ExtraInfo could be a pointer to the instance of
        //the item type the node is twinned with. This would allow a search algorithm
        //to test the status of the pickup during the search.
        """
        GraphNode.__init__(self, idx)
        self.position = Vec(pos_x, pos_y)
        self.extra_info = None

    def __str__(self):
        return "<{0}(index: {1}, position: ({2},{3}))>".format(self.__class__.__name__, self.index, self.position.x,
                                                               self.position.y)


class GraphEdge(object):
    __slots__ = ['idx_from', 'idx_to', 'cost']

    def __init__(self, idx_from, idx_to, cost=1.0):
        # def __init__(self, idx_from=INVALID_NODE_INDEX, idx_to=INVALID_NODE_INDEX, cost=1.0):
        self.idx_from = idx_from
        self.idx_to = idx_to
        self.cost = cost

    def __eq__(self, other):
        return (other.idx_from == self.idx_from and
                other.idx_to == self.idx_to and
                other.cost == self.cost)

    def __ne__(self, other):
        return not (self == other)

    def __str__(self):
        return "<{0}(from: {1}, to: {2}, cost: {3})>".format(self.__class__.__name__, self.idx_from, self.idx_to,
                                                             self.cost)


class _Flags(object):
    """
    //examples of typical flags
    """
    NORMAL = 0
    SWIM = 1 << 0
    CRAWL = 1 << 1
    CREEP = 1 << 2
    JUMP = 1 << 3
    FLY = 1 << 4
    GRAPPLE = 1 << 5
    GOES_THROUGH_DOOR = 1 << 6

    flag_to_string = {
        NORMAL: "normal",
        SWIM: "swim",
        CRAWL: "drawl",
        CREEP: "creep",
        JUMP: "jump",
        FLY: "fly",
        GRAPPLE: "grapple",
        GOES_THROUGH_DOOR: "door",
    }


class NavGraphEdge(GraphEdge):
    __slots__ = ['flags', 'do_intersecting_entity']

    Flags = _Flags

    def __init__(self, idx_from, idx_to, cost=1.0, flags=0, intersecting_entity=-1):
        """
        do_intersecting_entity:
        //if this edge intersects with an object (such as a door or lift), then
        //this is that object's ID.

        """
        GraphEdge.__init__(self, idx_from, idx_to, cost)
        self.flags = flags
        self.do_intersecting_entity = intersecting_entity

    def __str__(self):
        return "<{0}(from: {1}, to: {2}, cost: {3}, flags: {4}, intersecting identity: {5})>".format(
            self.__class__.__name__, self.idx_from, self.idx_to, self.cost, self.flags, self.do_intersecting_entity)

    @staticmethod
    def clone(other):
        return NavGraphEdge(other.idx_from, other.idx_to, other.cost, other.flags, other.do_intersecting_entity)


class SparseGraph(object):

    def __init__(self, node_type, edge_type, is_digraph):
        self.node_type = node_type
        self.edge_type = edge_type
        # //is this a directed graph?
        self.is_digraph = is_digraph

        # //the nodes that comprise this graph
        self.nodes = []

        # //a vector of adjacency edge lists. (each node index keys into the
        # //list of edges associated with that node)
        self.edges = []

        # //the index of the next node to be added
        self._next_node_index = 0

    def is_edge_unique(self, idx_from, idx_to):
        """
        //returns true if an edge is not already present in the graph. Used
        //when adding edges to make sure no duplicates are created.
        """
        for edge in self.edges[idx_from]:
            if edge.idx_to == idx_to:
                return False
        return True

    def cull_invalid_edges(self):
        """
        //iterates through all the edges in the graph and removes any that point
        //to an invalidated node
        """
        for edges in self.edges:
            for edge in list(edges):
                if self.nodes[edge.idx_to].index == INVALID_NODE_INDEX or \
                        self.nodes[edge.idx_from].index == INVALID_NODE_INDEX:
                    logger.debug("culling edge: %s -> %s (cost: %s)", edge.idx_from, edge.idx_to, edge.cost)
                    edges.remove(edge)

    def get_node(self, idx):
        """
        Returns the node at the given index.
        """
        assert idx < len(self.nodes) and idx >= 0, "SparceGraph.get_node: invalid index: " + str(idx)
        return self.nodes[idx]

    def get_edge(self, idx_from, idx_to):
        """
        Method for obtaining a reference to an edge.
        """
        assert idx_from < len(self.nodes) and idx_from >= 0 and \
               self.nodes[idx_from].index != INVALID_NODE_INDEX, "SparseGraph.get_edge: invalid idx_from"
        assert idx_to < len(self.nodes) and idx_to >= 0 and \
               self.nodes[idx_to].index != INVALID_NODE_INDEX, "SparseGraph.get_edge: invalid idx_to"

        for edge in self.edges[idx_from]:
            if edge.idx_to == idx_to:
                return edge

        assert False, "SparseGraph.get_edge: edge does not exist!"

    def get_next_free_node_index(self):
        """
        Retrieves the next free node index.
        """
        return self._next_node_index

    def add_node(self, node):
        """
        Adds a node to the graph and returns its index.
        //  Given a node this method first checks to see if the node has been added
        //  previously but is now innactive. If it is, it is reactivated.
        //
        //  If the node has not been added previously, it is checked to make sure its
        //  index matches the next node index before being added to the graph
        """
        assert isinstance(node, self.node_type), "wrong node type"
        if node.index < len(self.nodes):
            # //make sure the client is not trying to add a node with the same ID as
            # //a currently active node
            assert self.nodes[node.index].index == INVALID_NODE_INDEX, "Attemping to add a node with a duplicate ID"

            self.nodes[node.index] = node

            return self._next_node_index
            # return node.index

        else:
            # //make sure the new node has been indexed correctly
            assert node.index == self._next_node_index, "invalid index {0}".format(node.index)

            self.nodes.append(node)
            self.edges.append([])

            self._next_node_index += 1

            return node.index

    def remove_node(self, node_idx):
        """
        Removes a node by setting its index to invalid_node_index.
        //  Removes a node from the graph and removes any links to neighbouring
        //  nodes
        """
        assert node_idx < len(self.nodes), "invalid node index"

        # set this node's index to INVALID_NODE_INDEX
        self.nodes[node_idx].index = INVALID_NODE_INDEX

        # //if the graph is not directed remove all edges leading to this node and then
        # //clear the edges leading from the node
        if not self.is_digraph:
            # visit each neighbour and erase any edges leading to this node
            for edge_from_node in self.edges[node_idx]:
                for edge_to_node in list(self.edges[edge_from_node.idx_to]):
                    if edge_to_node.idx_to == node_idx:
                        self.edges[edge_from_node.idx_to].remove(edge_to_node)
                        break
            # //finally, clear this node's edges
            self.edges[node_idx] = []
        else:
            # //if a digraph remove the edges the slow way
            self.cull_invalid_edges()

    def add_edge(self, edge):
        """
        //Use this to add an edge to the graph. The method will ensure that the
        //edge passed as a parameter is valid before adding it to the graph. If the
        //graph is a digraph then a similar edge connecting the nodes in the opposite
        //direction will be automatically added.
        """
        # //first make sure the from and to nodes exist within the graph
        assert edge.idx_from < self._next_node_index and edge.idx_to < self._next_node_index, "invalid node index"
        assert isinstance(edge, self.edge_type), "wrong edge type"

        # //make sure both nodes are active before adding the edge
        if self.nodes[edge.idx_to].index != INVALID_NODE_INDEX and \
                self.nodes[edge.idx_from].index != INVALID_NODE_INDEX:
            # //add the edge, first making sure it is unique
            if self.is_edge_unique(edge.idx_from, edge.idx_to):
                self.edges[edge.idx_from].append(edge)

            # //if the graph is undirected we must add another connection in the opposite
            # //direction
            if not self.is_digraph:
                # //check to make sure the edge is unique before adding
                if self.is_edge_unique(edge.idx_to, edge.idx_from):
                    new_edge = self.edge_type.clone(edge)
                    new_edge.idx_from = edge.idx_to
                    new_edge.idx_to = edge.idx_from
                    self.edges[edge.idx_to].append(new_edge)

    def remove_edge(self, idx_from, idx_to):
        """
        //removes the edge connecting from and to from the graph (if present). If
        //a digraph then the edge connecting the nodes in the opposite direction
        //will also be removed.
        """
        assert idx_from < len(self.nodes) and idx_to < len(self.nodes), "invalid node index"

        if not self.is_digraph:
            for edge in self.edges[idx_to]:
                if edge.idx_to == idx_from:
                    self.edges[idx_to].remove(edge)
                    break

            for edge in self.edges[idx_from]:
                if edge.idx_to == idx_to:
                    self.edges[idx_from].remove(edge)
                    break

    def set_edge_cost(self, idx_from, idx_to, cost):
        """
        Sets the cost of an edge.
        """
        assert idx_from < len(self.nodes) and idx_to < len(self.nodes), "invalid index"

        for edge in self.edges[idx_from]:
            if edge.idx_to == idx_to:
                edge.cost = cost
                break

    def num_nodes(self):
        """
        Returns the number of active + inactive nodes present in the graph.
        """
        return len(self.nodes)

    def num_active_nodes(self):
        """
        Returns the number of active nodes present in the graph (this method's
        performance can be improved greatly by caching the value).
        """
        sum_count = sum((1 for node in self.nodes if node.index != INVALID_NODE_INDEX))
        return sum_count

    def num_edges(self):
        """
        Returns the total number of edges present in the graph.
        """
        tot = 0
        for edge in self.edges:
            tot += len(edge)
        assert tot == sum([len(x) for x in self.edges])
        return tot

    def is_empty(self):
        """
        Returns true if the graph contains no nodes.
        """
        return (len(self.nodes) == 0)

    def is_node_present(self, idx):
        """
        Returns true if a node with the given index is present in the graph.
        """
        if idx >= len(self.nodes) or self.nodes[idx].index <= INVALID_NODE_INDEX:
            return False
        else:
            return True

    def is_edge_present(self, idx_from, idx_to):
        """
        //returns true if an edge connecting the nodes 'to' and 'from'
        //is present in the graph
        """
        if self.is_node_present(idx_from) and self.is_node_present(idx_to):
            if len(self.edges) > idx_from:
                for edge in self.edges[idx_from]:
                    if edge.idx_to == idx_to:
                        return True
        return False

    def save(self, filename):
        raise NotImplementedError()

    def load(self, filename):
        raise NotImplementedError()

    def clear(self):
        """
        Clears the graph ready for new node insertion.
        """
        self._next_node_index = 0
        self.nodes = []
        self.edges = []

    def remove_edges(self):
        new_edges = []
        for edge in self.edges:
            new_edges.append([])
        self.edges = new_edges


# helps legibility
_VISITED = 0
_UNVISITED = 1
_NO_PARENT_ASSIGNED = 2


class GraphSearchDFS(object):

    def __init__(self, graph, source, target=-1):
        # //a reference to the graph to be searched
        self._graph = graph

        # //this records the indexes of all the nodes that are visited as the
        # //search progresses
        self._visited = array.array('i', [_UNVISITED] * graph.num_nodes())  # node indices as int

        # //this holds the route taken to the target. Given a node index, the value
        # //at that index is the node's parent. ie if the path to the target is
        # //3-8-27, then m_Route[8] will hold 3 and m_Route[27] will hold 8.
        self._route = [_NO_PARENT_ASSIGNED] * graph.num_nodes()  # node indices as int

        # //As the search progresses, this will hold all the edges the algorithm has
        # //examined. THIS IS NOT NECESSARY FOR THE SEARCH, IT IS HERE PURELY
        # //TO PROVIDE THE USER WITH SOME VISUAL FEEDBACK
        self._spanning_tree = []  # edges

        # the source and target node indices
        self._source = source
        self._target = target

        # true if a path to the target has been found
        self._found = self._search()

    # this method performs the DFS search
    def _search(self):
        # create a stack of edges
        stack = collections.deque()

        # create a dummy edge and put on the stack
        dummy = GraphEdge(self._source, self._source)

        stack.append(dummy)

        while stack:
            # grab and remove the next edge
            next = stack.pop()  # last item

            # make a note of the parent of the node this edge points to
            self._route[next.idx_to] = next.idx_from

            # put it on the tree (making sure the dummy edge is not placed on the tree)
            if next != dummy:
                self._spanning_tree.append(next)

            # and mark it visited
            self._visited[next.idx_to] = _VISITED

            # if the target has been found the method can return success
            if next.idx_to == self._target:
                return True

            # push the edges leading from the node this edge points to  onto
            # the stack (provided the edge does not point to a prviously visited node)
            for edge in self._graph.edges[next.idx_to]:
                if self._visited[edge.idx_to] == _UNVISITED:
                    stack.append(edge)

        # no path to target
        return False

    def get_search_tree(self):
        """
        //returns a vector containing pointers to all the edges the search has examined
        """
        return self._spanning_tree

    def found(self):
        """
        Returns true if the target node has been located
        """
        return self._found

    def get_path_to_target(self):
        """
        //returns a vector of node indexes that comprise the shortest path
        //from the source to the target
        """
        path = []
        # //just return an empty path if no path to target found or if
        # //no target has been specified
        if not self._found or self._target < 0:
            return path

        nd = self._target
        path.append(nd)
        while nd != self._source:
            nd = self._route[nd]
            path.append(nd)

        path.reverse()
        return path


class GraphSearchBFS(object):

    def __init__(self, graph, source, target=-1):
        # //a reference to the graph to be searched
        self._graph = graph

        # //this records the indexes of all the nodes that are visited as the
        # //search progresses
        # TODO: maybe use array instead of list
        self._visited = array.array('i', [_UNVISITED] * graph.num_nodes())  # node indices as int

        # //this holds the route taken to the target. Given a node index, the value
        # //at that index is the node's parent. ie if the path to the target is
        # //3-8-27, then m_Route[8] will hold 3 and m_Route[27] will hold 8.
        self._route = [_NO_PARENT_ASSIGNED] * graph.num_nodes()  # node indices as int

        # //As the search progresses, this will hold all the edges the algorithm has
        # //examined. THIS IS NOT NECESSARY FOR THE SEARCH, IT IS HERE PURELY
        # //TO PROVIDE THE USER WITH SOME VISUAL FEEDBACK
        self._spanning_tree = []  # edges

        # the source and target node indices
        self._source = source
        self._target = target

        # true if a path to the target has been found
        self._found = self._search()

    # this method performs the BFS search
    def _search(self):
        # create a queue of edges
        queue = collections.deque()

        # create a dummy edge and put on the queue
        dummy = GraphEdge(self._source, self._source)

        queue.append(dummy)

        # //mark the source node as visited
        self._visited[self._source] = _VISITED

        while queue:
            # grab and remove the next edge
            next = queue.popleft()  # first item

            # make a note of the parent of the node this edge points to
            self._route[next.idx_to] = next.idx_from

            # put it on the tree (making sure the dummy edge is not placed on the tree)
            if next != dummy:
                self._spanning_tree.append(next)

            # if the target has been found the method can return success
            if next.idx_to == self._target:
                return True

            # push the edges leading from the node  at the end of this edge onto
            # the queue (provided the edge does not point to a prviously visited node)
            for edge in self._graph.edges[next.idx_to]:
                if self._visited[edge.idx_to] == _UNVISITED:
                    queue.append(edge)
                    # and mark it visited
                    self._visited[edge.idx_to] = _VISITED

        # no path to target
        return False

    def get_search_tree(self):
        """
        //returns a vector containing pointers to all the edges the search has examined
        """
        return self._spanning_tree

    def found(self):
        """
        Returns true if the target node has been located
        """
        return self._found

    def get_path_to_target(self):
        """
        //returns a vector of node indexes that comprise the shortest path
        //from the source to the target
        """
        path = []
        # //just return an empty path if no path to target found or if
        # //no target has been specified
        if not self._found or self._target < 0:
            return path

        nd = self._target
        path.append(nd)
        while nd != self._source:
            nd = self._route[nd]
            path.append(nd)

        path.reverse()
        return path


# ---------------
# 'heap' is a heap at all indices >= startpos, except possibly for pos.  pos
# is the index of a leaf with a possibly out-of-order value.  Restore the
# heap invariant.
def _siftdown(heap, startpos, pos):
    newitem = heap[pos]
    # Follow the path to the root, moving parents down until finding a place
    # newitem fits.
    while pos > startpos:
        parentpos = (pos - 1) >> 1
        parent = heap[parentpos]
        if newitem < parent:
            heap[pos] = parent
            pos = parentpos
            continue
        break
    heap[pos] = newitem


# The child indices of heap index pos are already heaps, and we want to make
# a heap at index pos too.  We do this by bubbling the smaller child of
# pos up (and so on with that child's children, etc) until hitting a leaf,
# then using _siftdown to move the oddball originally at index pos into place.
#
# We *could* break out of the loop as soon as we find a pos where newitem <=
# both its children, but turns out that's not a good idea, and despite that
# many books write the algorithm that way.  During a heap pop, the last array
# element is sifted in, and that tends to be large, so that comparing it
# against values starting from the root usually doesn't pay (= usually doesn't
# get us out of the loop early).  See Knuth, Volume 3, where this is
# explained and quantified in an exercise.
#
# Cutting the # of comparisons is important, since these routines have no
# way to extract "the priority" from an array element, so that intelligence
# is likely to be hiding in custom __cmp__ methods, or in array elements
# storing (priority, record) tuples.  Comparisons are thus potentially
# expensive.
#
# On random arrays of length 1000, making this change cut the number of
# comparisons made by heapify() a little, and those made by exhaustive
# heappop() a lot, in accord with theory.  Here are typical results from 3
# runs (3 just to demonstrate how small the variance is):
#
# Compares needed by heapify     Compares needed by 1000 heappops
# --------------------------     --------------------------------
# 1837 cut to 1663               14996 cut to 8680
# 1855 cut to 1659               14966 cut to 8678
# 1847 cut to 1660               15024 cut to 8703
#
# Building the heap by using heappush() 1000 times instead required
# 2198, 2148, and 2219 compares:  heapify() is more efficient, when
# you can use it.
#
# The total compares needed by list.sort() on the same lists were 8627,
# 8627, and 8632 (this should be compared to the sum of heapify() and
# heappop() compares):  list.sort() is (unsurprisingly!) more efficient
# for sorting.

def _siftup(heap, pos):
    endpos = len(heap)
    startpos = pos
    newitem = heap[pos]
    # Bubble up the smaller child until hitting a leaf.
    childpos = 2 * pos + 1  # leftmost child position
    while childpos < endpos:
        # Set childpos to index of smaller child.
        rightpos = childpos + 1
        if rightpos < endpos and not heap[childpos] < heap[rightpos]:
            childpos = rightpos
        # Move the smaller child up.
        heap[pos] = heap[childpos]
        pos = childpos
        childpos = 2 * pos + 1
    # The leaf at pos is empty now.  Put newitem there, and bubble it up
    # to its final resting place (by sifting its parents down).
    heap[pos] = newitem
    _siftdown(heap, startpos, pos)


# ---------------


class GraphSearchDijkstra(object):
    """
    //  Given a graph, source and optional target this class solves for
    //  single source shortest paths (without a target being specified) or
    //  shortest path from source to target.
    //
    //  The algorithm used is a priority queue implementation of Dijkstra's.
    //  note how similar this is to the algorithm used in Graph_MinSpanningTree.
    //  The main difference is in the calculation of the priority in the line:
    //
    //  double NewCost = m_CostToThisNode[best] + pE->Cost;
    """

    def __init__(self, graph, source, target=-1):
        # //a reference to the graph to be searched
        self._graph = graph

        # //this vector contains the edges that comprise the shortest path tree -
        # //a directed subtree of the graph that encapsulates the best paths from
        # //every node on the SPT to the source node.
        self._shortest_path_tree = [None] * graph.num_nodes()  # edges

        # //this is indexed into by node index and holds the total cost of the best
        # //path found so far to the given node. For example, m_CostToThisNode[5]
        # //will hold the total cost of all the edges that comprise the best path
        # //to node 5, found so far in the search (if node 5 is present and has
        # //been visited)
        self._cost_to_this_node = [0] * graph.num_nodes()  # floats

        # //this is an indexed (by node) vector of 'parent' edges leading to nodes
        # //connected to the SPT but that have not been added to the SPT yet. This is
        # //a little like the stack or queue used in BST and DST searches.
        self._search_frontier = [None] * graph.num_nodes()  # edges

        # the source and target node indices
        self._source = source
        self._target = target

        # the Dijkstra search algorithm
        self._search()

    def _search(self):
        # //create an indexed priority queue that sorts smallest to largest
        # //(front to back).Note that the maximum number of elements the iPQ
        # //may contain is N. This is because no node can be represented on the
        # //queue more than once.
        # pq = [(c, n) for n, c in enumerate(self._cost_to_this_node)]
        pq = []
        # heapify(pq)

        # put the source node on the queue
        heappush(pq, [self._cost_to_this_node[self._source], self._source])

        while pq:
            # self.print_info(pq)
            # //get lowest cost node from the queue. Don't forget, the return value
            # //is a *node index*, not the node itself. This node is the node not already
            # //on the SPT that is the closest to the source node
            next_cost, next_closest_node_index = heappop(pq)

            # move this edge from the frontier to the shortest path tree
            self._shortest_path_tree[next_closest_node_index] = self._search_frontier[next_closest_node_index]

            # if the target has been found exit
            if next_closest_node_index == self._target:
                return

            # now to relax the edges
            # for each edge connected to the next closest node
            for edge in self._graph.edges[next_closest_node_index]:
                # the total cost to the node this edge points to is the cost to the
                # current node plus the cost of the edge connecting them
                # new_cost = next_cost + edge.cost
                new_cost = self._cost_to_this_node[next_closest_node_index] + edge.cost

                # //if this edge has never been on the frontier make a note of the cost
                # //to get to the node it points to, then add the edge to the frontier
                # //and the destination node to the PQ.
                if self._search_frontier[edge.idx_to] is None:
                    self._cost_to_this_node[edge.idx_to] = new_cost
                    heappush(pq, [new_cost, edge.idx_to])
                    self._search_frontier[edge.idx_to] = edge

                    # //else test to see if the cost to reach the destination node via the
                    # //current node is cheaper than the cheapest cost found so far. If
                    # //this path is cheaper, we assign the new cost to the destination
                    # //node, update its entry in the PQ to reflect the change and add the
                    # //edge to the frontier
                elif new_cost < self._cost_to_this_node[edge.idx_to] and \
                        self._shortest_path_tree[edge.idx_to] is None:

                    cost = self._cost_to_this_node[edge.idx_to]
                    # pq.remove([cost, edge.idx_to])
                    pos = pq.index([cost, edge.idx_to])
                    pq[pos][0] = new_cost
                    self._cost_to_this_node[edge.idx_to] = new_cost

                    # //because the cost is less than it was previously, the PQ must be
                    # //re-sorted to account for this.
                    heapify(pq)
                    # this is faster than heapify and dos the same thing
                    # _siftup(pq, pos)

                    self._search_frontier[edge.idx_to] = edge

    def print_info(self, pq):
        return
        edges = self.get_SPT()
        print(('pq    : ', pq))
        print(('source: ', self._source, 'target: ', self._target))
        print(('STP :  ', [None if e is None else (idx, e.idx_from, e.idx_to) for idx, e in enumerate(edges)]))
        print(('cost:  ', [(idx, c) for idx, c in enumerate(self._cost_to_this_node)]))
        print((
            'front: ',
            [None if f is None else (idx, f.idx_from, f.idx_to) for idx, f in enumerate(self._search_frontier)]))
        print(('-' * 10))

    def get_SPT(self):
        """
        //returns the vector of edges that defines the SPT. If a target was given
        //in the constructor then this will be an SPT comprising of all the nodes
        //examined before the target was found, else it will contain all the nodes
        //in the graph.
        """
        return self._shortest_path_tree

    def get_path_to_target(self):
        """
        //returns a vector of node indexes that comprise the shortest path
        //from the source to the target. It calculates the path by working
        //backwards through the SPT from the target node.
        """
        """
        //returns a vector of node indexes that comprise the shortest path
        //from the source to the target
        """
        path = []
        # //just return an empty path if no path to target found or if
        # //no target has been specified
        if self._target < 0:
            return path

        nd = self._target
        path.append(nd)
        while (nd != self._source) and (self._shortest_path_tree[nd] is not None):
            nd = self._shortest_path_tree[nd].idx_from
            path.append(nd)

        path.reverse()
        return path

    def get_cost_to_target(self):
        """
        Returns the total cost to the target
        """
        return self._cost_to_this_node[self._target]

    def get_cost_to_node(self, node_index):
        """
        Returns the total cost to the given node.
        """
        return self._cost_to_this_node[node_index]


class HeuristicEuclidian(object):
    """
    //the euclidian heuristic (straight-line distance)
    """

    @staticmethod
    def calculate(graph, node_index1, node_index2):
        nd1 = graph.get_node(node_index1)
        nd2 = graph.get_node(node_index2)
        assert graph.node_type == NavGraphNode
        dx = nd1.position.x - nd2.position.x
        dy = nd1.position.y - nd2.position.y
        # distance
        return (dx ** 2 + dy ** 2) ** 0.5


class HeuristicNoisyEuclidian(object):
    """
    //this uses the euclidian distance but adds in an amount of noise to the
    //result. You can use this heuristic to provide imperfect paths. This can
    //be handy if you find that you frequently have lots of agents all following
    //each other in single file to get from one place to another
    """

    @staticmethod
    def calculate(graph, node_index1, node_index2):
        nd1 = graph.get_node(node_index1)
        nd2 = graph.get_node(node_index2)
        assert graph.node_type == NavGraphNode
        dx = nd1.position_x - nd2.position_x
        dy = nd1.position_y - nd2.position_y
        # distance
        factor = random.random() * 0.2 + 0.9  # 0.9 <= x < 1.1
        return ((dx ** 2 + dy ** 2) ** 0.5) * factor


class HeuristicDijkstra(object):
    """
    //you can use this class to turn the A* algorithm into Dijkstra's search.
    //this is because Dijkstra's is equivalent to an A* search using a heuristic
    //value that is always equal to zero.
    """

    @staticmethod
    def calculate(graph, node_index1, node_index2):
        return 0


class GraphSearchAStar(object):
    """
    //  this searchs a graph using the distance between the target node and the
    //  currently considered node as a heuristic.
    //
    //  This search is more commonly known as A* (pronounced Ay-Star)
    """

    def __init__(self, graph, source, target, heuristic=None):

        self._graph = graph

        # //indexed into my node. Contains the 'real' accumulative cost to that node
        self._g_costs = [0] * graph.num_nodes()

        # //indexed into by node. Contains the cost from adding m_GCosts[n] to
        # //the heuristic cost from n to the target node. This is the vector the
        # //iPQ indexes into.
        self._f_costs = [0] * graph.num_nodes()

        # //this vector contains the edges that comprise the shortest path tree -
        # //a directed subtree of the graph that encapsulates the best paths from
        # //every node on the SPT to the source node.
        self._shortest_path_tree = [None] * graph.num_nodes()  # edges

        # //this is an indexed (by node) vector of 'parent' edges leading to nodes
        # //connected to the SPT but that have not been added to the SPT yet. This is
        # //a little like the stack or queue used in BST and DST searches.
        self._search_frontier = [None] * graph.num_nodes()  # edges

        self._heuristic = heuristic
        if self._heuristic is None:
            self._heuristic = HeuristicDijkstra

        # the source and target node indices
        self._source = source
        self._target = target

        # the A* search algorithm
        self._search()

    def _search(self):
        # //create an indexed priority queue of nodes. The nodes with the
        # //lowest overall F cost (G+H) are positioned at the front.
        pq = []

        # put the source node on the queue

        heappush(pq, [self._f_costs[self._source], self._source])

        # while queue not empty
        while pq:
            # get lowest cost node from the queue
            new_cost, next_closest_node_index = heappop(pq)

            # move this edge from the frontier to the shortest path tree
            self._shortest_path_tree[next_closest_node_index] = self._search_frontier[next_closest_node_index]

            # if the target has been found exit
            if next_closest_node_index == self._target:
                return

            # now to test all the edges attached to this node:
            for edge in self._graph.edges[next_closest_node_index]:

                # //calculate the heuristic cost from this node to the target (H)
                h_cost = self._heuristic.calculate(self._graph, self._target, edge.idx_to)

                # //calculate the 'real' cost to this node from the source (G)
                g_cost = self._g_costs[next_closest_node_index] + edge.cost

                # //if the node has not been added to the frontier, add it and update
                # //the G and F costs
                if self._search_frontier[edge.idx_to] is None:
                    self._f_costs[edge.idx_to] = g_cost + h_cost
                    self._g_costs[edge.idx_to] = g_cost

                    heappush(pq, [self._f_costs[edge.idx_to], edge.idx_to])

                    self._search_frontier[edge.idx_to] = edge

                # //if this node is already on the frontier but the cost to get here
                # //is cheaper than has been found previously, update the node
                # //costs and frontier accordingly.
                elif (g_cost < self._g_costs[edge.idx_to]) and (self._shortest_path_tree[edge.idx_to] is None):

                    old_cost = self._f_costs[edge.idx_to]
                    self._f_costs[edge.idx_to] = g_cost + h_cost
                    self._g_costs[edge.idx_to] = g_cost

                    idx = pq.index([old_cost, edge.idx_to])
                    pq[idx][0] = self._f_costs[edge.idx_to]

                    # //because the cost is less than it was previously, the PQ must be
                    # //re-sorted to account for this.
                    heapify(pq)
                    # this is faster than heapify and dos the same thing
                    # _siftup(pq, idx)

                    self._search_frontier[edge.idx_to] = edge

    def get_SPT(self):
        """
        //returns the vector of edges that defines the SPT. If a target was given
        //in the constructor then this will be an SPT comprising of all the nodes
        //examined before the target was found, else it will contain all the nodes
        //in the graph.
        """
        return self._shortest_path_tree

    def get_path_to_target(self):
        """
        //returns a vector of node indexes that comprise the shortest path
        //from the source to the target. It calculates the path by working
        //backwards through the SPT from the target node.
        """
        """
        //returns a vector of node indexes that comprise the shortest path
        //from the source to the target
        """
        path = []
        # //just return an empty path if no path to target found or if
        # //no target has been specified
        if self._target < 0:
            return path

        nd = self._target
        path.append(nd)
        while (nd != self._source) and (self._shortest_path_tree[nd] is not None):
            nd = self._shortest_path_tree[nd].idx_from
            path.append(nd)

        path.reverse()
        return path

    def get_cost_to_target(self):
        """
        Returns the total cost to the target
        """
        return self._g_costs[self._target]

    def get_cost_to_node(self, node_index):
        """
        Returns the total cost to the given node.
        """
        return self._g_costs[node_index]


class HandyGraphFunctions(object):

    @staticmethod
    def is_neighbour_valid(node_x, node_y, num_cells_x, num_cells_y):
        """
        Returns true if x,y is a valid position in the map

        :param node_x: x coordinate of the cell the node is in
        :param node_y: y coordinate of the cell the node is in
        :param num_cells_x: number of cells in x direction
        :param num_cells_y: number of cells in y direction
        :return: bool
        """
        return not (node_x < 0 or node_x >= num_cells_x or node_y < 0 or node_y >= num_cells_y)

    @staticmethod
    def add_all_neighbours_to_grid_node(_graph, row, col, num_cells_x, num_cells_y):
        """
        Use to add the eight neighboring edges of a graph node that is positioned in a grid layout.

        :param _graph: graph to operate on
        :param row: row of the grid
        :param col: column of the grid
        :param num_cells_x: number of cells of the grid in x direction
        :param num_cells_y: number of cells of the grid in y direction
        """
        for j, i in [(-1, -1), (0, -1), (1, -1), (-1, 0), (1, 0), (-1, 1), (0, 1), (1, 1)]:
            node_x = col + j
            node_y = row + i

            if HandyGraphFunctions.is_neighbour_valid(node_x, node_y, num_cells_x, num_cells_y):
                node_pos = _graph.get_node(row * num_cells_x + col).position
                neighbour_pos = _graph.get_node(node_y * num_cells_x + node_x).position

                dist = node_pos.get_distance(neighbour_pos)

                new_edge = NavGraphEdge(row * num_cells_x + col, node_y * num_cells_x + node_x, dist)

                _graph.add_edge(new_edge)

                # if graph is not a di-graph then an edge needs to be added going
                # in the other direction
                if not _graph.is_digraph:
                    new_edge = NavGraphEdge(node_y * num_cells_x + node_x, row * num_cells_x + col, dist)
                    _graph.add_edge(new_edge)

    @staticmethod
    def create_grid(_graph, cy_size, cx_size, num_cells_x, num_cells_y):
        """
        //  creates a graph based on a grid layout. This function requires the
        //  dimensions of the environment and the number of cells required horizontally
        //  and vertically
        """
        cell_w = 1.0 * cx_size / num_cells_x
        cell_h = 1.0 * cy_size / num_cells_y

        # assert cell_w == constants.cell_size, "cell_w '{0}' is not the same as '{1}'".format(cell_w, constants.cell_size)
        # assert cell_h == constants.cell_size, "cell_h '{0}' is not the same as '{1}'".format(cell_h, constants.cell_size)

        mid_x = cell_w / 2.0
        mid_y = cell_h / 2.0

        for row in range(num_cells_y):
            for col in range(num_cells_x):
                n = NavGraphNode(mid_x + col * cell_w, mid_y + row * cell_h, _graph.get_next_free_node_index())
                _graph.add_node(n)

        # //now to calculate the edges. (A position in a 2d array [x][y] is the
        # //same as [y*NumCellsX + x] in a 1d array). Each cell has up to eight
        # //neighbours.
        for row in range(num_cells_y):
            for col in range(num_cells_x):
                HandyGraphFunctions.add_all_neighbours_to_grid_node(_graph, row, col, num_cells_x, num_cells_y)

    @staticmethod
    def weight_nav_graph_node_edges(_graph, node_idx, weight):
        """
        Given a cost value and an index to a valid node this function examines
        all a node's edges, calculates their length, and multiplies
        the value with the weight. Useful for setting terrain costs.

        :param self:
        :param _graph:
        :param node_idx:
        :param weight:
        :return:
        """
        # make sure the node is present
        assert node_idx < _graph.num_nodes(), "node index invalid"

        # set the cost for each edge
        for edge in _graph.edges[node_idx]:
            # calculate the distance between nodes
            node_from = _graph.nodes[edge.idx_from]
            node_to = _graph.nodes[edge.idx_to]
            dist = node_from.position.get_distance(node_to.position)

            # set edge cost
            _graph.set_edge_cost(edge.idx_from, edge.idx_to, dist * weight)

            # if not a digraph, set the cost of the parallel edge to the same
            if not _graph.is_digraph:
                _graph.set_edge_cost(edge.idx_to, edge.idx_from, dist * weight)

    @staticmethod
    def create_all_pairs_table(graph):
        """
        Creates a lookup table encoding the shortest path info between each node in a graph to every other.

        If there is not path then the value is None.

        :param graph: graph to use
        :return: look up table of shortest paths: next_node = shortest_paths[idx_from][idx_to]
        """
        no_path = None

        shortest_paths = []
        for i in range(graph.num_nodes()):
            row = [no_path] * graph.num_nodes()
            shortest_paths.append(row)

        for source in range(graph.num_nodes()):
            # calculate the SPT for this node
            search = GraphSearchDijkstra(graph, source)
            spt = search.get_SPT()

            #     //now we have the SPT it's easy to work backwards through it to find
            #     //the shortest paths from each node to this source node
            for target in range(graph.num_nodes()):
                if source == target:
                    shortest_paths[source][target] = target
                else:
                    nd = target
                    while nd != source and spt[nd] != 0:
                        shortest_paths[spt[nd].idx_from][target] = nd
                        nd = spt[nd].idx_from

        return shortest_paths

    @staticmethod
    def create_all_pairs_costs_table(graph):
        """
        creates a lookup table of the cost associated from traveling from one node to every other

        :param graph: graph to use
        :return: look up table for the cost of the path: cost = path_costs[idx_from][idx_to]
        """
        path_costs = []
        for i in range(graph.num_nodes()):
            path_costs.append([0.0] * graph.num_nodes())

        for source_idx in range(graph.num_nodes()):

            # do the search
            search = GraphSearchDijkstra(graph, source_idx)

            # iterate through every node in the graph and grab the cost to travel to that node
            for target_idx in range(graph.num_nodes()):
                if source_idx != target_idx:
                    path_costs[source_idx][target_idx] = search.get_cost_to_node(target_idx)

        return path_costs

    @staticmethod
    def calculate_average_graph_edge_length(graph):
        """
        determines the average length of the edges in a navgraph (using the
        distance between the source & target node positions (not the cost of the
        edge as represented in the graph, which may account for all sorts of
        other factors such as terrain type, gradients etc)

        :param graph: graph to use
        :return: average edge length as a float
        """
        total_length = 0.0
        num_edges_counted = 0.0

        for node in graph.nodes:
            for edge in graph.edges[node.index]:
                num_edges_counted += 1
                to_node = graph.get_node(edge.idx_to)
                from_node = graph.get_node(edge.idx_from)
                total_length += from_node.position.get_distance(to_node.position)

        return total_length / num_edges_counted

    @staticmethod
    def get_costliest_graph_edge(graph):
        """
        Returns the cost of the costliest edge in the graph.

        :param graph: graph to use.
        :return: costliest edge
        """
        greatest = None

        for node in graph.nodes:
            for edge in graph.edges[node.index]:
                if greatest is None or edge.cost > greatest.cost:
                    greatest = edge

        return greatest
