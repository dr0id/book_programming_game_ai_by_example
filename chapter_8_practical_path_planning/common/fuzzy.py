# -*- coding: utf-8 -*-
from __future__ import print_function, division
import sys


class _DeFuzzifyMethodEnum(object):
    max_av = 0
    centroid = 1


class FuzzySet(object):

    def __init__(self, representative_value):
        # this is the maximum of the sets membership function. For instance, if
        # the set is triangular then this will be the peak point of the triangle.
        # if the set has a plateau then this value wil be the mid point of the
        # plateau. This value is set in the constructor to avoid run time
        # calculation of mid point values.
        self.representative_value = representative_value
        # this will hold the degree of membership of a given value in this set
        self._dom = 0.0

    def calculate_dom(self, val):
        """ return the degree of membership in this set of the given value. NOTE,
        #   this does not set m_dDOM to the DOM of the value passed as the parameter.
        #   This is because the centroid defuzzification method also uses this method
        #   to determine the DOMs of the values it uses as its sample points."""
        raise NotImplementedError()

    def or_with_dom(self, val):
        """if this fuzzy set is part of a consequent FLV, and it is fired by a rule
           then this method sets the DOM (in this context, the DOM represents a
           confidence level)to the maximum of the parameter value or the set's
           existing m_dDOM value"""
        if val > self._dom:
            self._dom = val

    def clear_dom(self):
        self._dom = 0

    @property
    def dom(self):
        return self._dom

    @dom.setter
    def dom(self, value):
        assert (value <= 1) and (value >= 0), "FuzzySet.dom: invalid value"
        self._dom = value


class FuzzySetLeftShoulder(FuzzySet):

    def __init__(self, peak, left_offset, right_offset):
        FuzzySet.__init__(self, ((peak - left_offset) + peak) / 2.0)
        self._right_offset = right_offset
        self._left_offset = left_offset
        self._peak = peak

    def calculate_dom(self, val):
        # test for the case where the left or right offsets are zero
        # (to prevent divide by zero errors below)
        if (self._right_offset == 0.0 and self._peak == val) or \
                (self._left_offset == 0.0 and self._peak == val):
            return 1.0
        elif self._peak <= val < (self._peak + self._right_offset):
            # find dom if right of center
            grad = 1.0 / -self._right_offset
            return grad * (val - self._peak) + 1.0
        elif self._peak > val >= (self._peak - self._left_offset):
            # find dom if left of center
            return 1.0
        else:
            # out of range of this FLV, return zero
            return 0.0


class FuzzySetRightShoulder(FuzzySet):

    def __init__(self, peak, left_offset, right_offset):
        FuzzySet.__init__(self, ((peak + right_offset) + peak) / 2.0)
        self._peak = peak
        self._left_offset = left_offset
        self._right_offset = right_offset

    def calculate_dom(self, val):
        # test for the case where the left or right offsets are zero
        # (to prevent divide by zero errors below)
        if (self._right_offset == 0.0 and self._peak == val) or \
                (self._left_offset == 0.0 and self._peak == val):
            return 1.0
        elif self._peak >= val > (self._peak - self._left_offset):
            # find DOM if left of center
            grad = 1.0 / self._left_offset
            return grad * (val - (self._peak - self._left_offset))
        elif self._peak < val <= self._peak + self._right_offset:
            # find DOM if right of center and less than center + right offset
            return 1.0
        else:
            return 0.0


class FuzzySetTriangle(FuzzySet):

    def __init__(self, mid, left, right):
        FuzzySet.__init__(self, mid)
        self._peak = mid
        self._left_offset = left
        self._right_offset = right

    def calculate_dom(self, val):
        # test for the case where the left or right offsets are zero
        # (to prevent divide by zero errors below)
        if (self._right_offset == 0.0 and self._peak == val) or \
                (self._left_offset == 0.0 and self._peak == val):  # todo: floating point precision comparison??
            return 1.0
        elif self._peak >= val >= (self._peak - self._left_offset):
            # find DOM if left of center
            grad = 1.0 / self._left_offset
            return grad * (val - (self._peak - self._left_offset))
        elif self._peak < val < (self._peak + self._right_offset):
            # find DOM if right of center
            grad = 1.0 / -self._right_offset
            return grad * (val - self._peak) + 1.0
        else:
            # out of range of this FLV, return zero
            return 0.0


class FuzzySetSingleton(FuzzySet):

    def __init__(self, mid, left, right):
        FuzzySet.__init__(self, mid)
        self._peak = mid
        self._left_offset = left
        self._right_offset = right

    def calculate_dom(self, val):
        if (self._peak - self._left_offset) <= val <= (self._peak + self._right_offset):
            return 1.0
        else:
            # out of range of this FLV, return zero
            return 0.0


class FuzzyVariable(object):
    def __init__(self):
        # the minimum and maximum value of the range of this variable
        self._min_range = 0.0
        self._max_range = 0.0
        # a map of the fuzzy sets that comprise this variable
        self._members = {}  # {str:FuzzySet}

    def _adjust_range_to_fit(self, min_val, max_val):
        """this method is called with the upper and lower bound of a set each time a
            new set is added to adjust the upper and lower range values accordingly
        """
        if min_val < self._min_range:
            self._min_range = min_val
        if max_val > self._max_range:
            self._max_range = max_val

    #   //the following methods create instances of the sets named in the method
    #   //name and add them to the member set map. Each time a set of any type is
    #   //added the m_dMinRange and m_dMaxRange are adjusted accordingly. All of the
    #   //methods return a proxy class representing the newly created instance. This
    #   //proxy set can be used as an operand when creating the rule base.
    #   FzSet  AddLeftShoulderSet(std::string name, double minBound, double peak, double maxBound);
    def add_left_shoulder_set(self, name, min_bound, peak, max_bound):
        self._members[name] = FuzzySetLeftShoulder(peak, peak - min_bound, max_bound - peak)

        # adjust range if necessary
        self._adjust_range_to_fit(min_bound, max_bound)
        return FzSet(self._members[name])

    def add_right_shoulder_set(self, name, min_bound, peak, max_bound):
        self._members[name] = FuzzySetRightShoulder(peak, peak - min_bound, max_bound - peak)

        # adjust range if necessary
        self._adjust_range_to_fit(min_bound, max_bound)
        return FzSet(self._members[name])

    def add_triangular_set(self, name, min_bound, peak, max_bound):
        """Adds a triangular shaped fuzzy set to the variable."""
        self._members[name] = FuzzySetTriangle(peak, peak - min_bound, max_bound - peak)

        # adjust range if necessary
        self._adjust_range_to_fit(min_bound, max_bound)
        return FzSet(self._members[name])

    def add_singleton_set(self, name, min_bound, peak, max_bound):
        self._members[name] = FuzzySetSingleton(peak, peak - min_bound, max_bound - peak)

        # adjust range if necessary
        self._adjust_range_to_fit(min_bound, max_bound)
        return FzSet(self._members[name])

    def fuzzify(self, val):
        """Takes a crisp value and calculates its degree of membership for each set
        in the variable"""
        # make sure the value is within the bounds of this variable
        assert self._min_range <= val <= self._max_range, \
            "fuzzify: value '{0}' out of range ({1}, {2})".format(val, self._min_range, self._max_range)

        # for each set in the flv calculate the DOM for the given value
        for cur_set in self._members.values():
            cur_set.dom = cur_set.calculate_dom(val)

    def defuzzify_max_av(self):
        """Defuzzifies the value by averaging the maxima of the sets that have fired.

            OUTPU = sum ( maxima * DOM) / sum(DOMs)"""
        bottom = 0.0
        top = 0.0

        for cur_set in self._members.values():
            bottom += cur_set.dom
            top += cur_set.representative_value * cur_set.dom

        # make sure bottom is not equal to zero
        if bottom == 0.0:
            return 0.0

        return top / bottom

    def defuzzify_centroid(self, num_samples):
        """Defuzzify the variable using the centroid method"""
        # calculate the step size
        step_size = (self._max_range - self._min_range) / float(num_samples)

        total_area = 0.0
        sum_of_moments = 0.0

        #  step through the range of this variable in increments equal to StepSize
        #  adding up the contribution (lower of CalculateDOM or the actual DOM of this
        #  variable's fuzzified value) for each subset. This gives an approximation of
        #  the total area of the fuzzy manifold.(This is similar to how the area under
        #  a curve is calculated using calculus... the heights of lots of 'slices' are
        #  summed to give the total area.)
        #
        #  in addition the moment of each slice is calculated and summed. Dividing
        #  the total area by the sum of the moments gives the centroid. (Just like
        #  calculating the center of mass of an object)
        for samp in range(1, num_samples + 1):
            # for each set get the contribution to the area. This is the lower of the
            # value returned from calculate_dom or the actual DOM of the fuzzified
            # value itself
            for cur_set in self._members.values():
                _samp_dom_value = cur_set.calculate_dom(self._min_range + samp * step_size)
                contribution = _samp_dom_value if _samp_dom_value < cur_set.dom else cur_set.dom

                total_area += contribution
                sum_of_moments += (self._min_range + samp * step_size) * contribution

        # make sure that total area is not equal to zero
        if total_area == 0.0:
            return 0.0

        return sum_of_moments / total_area


class FuzzyRule(object):
    def __init__(self, ant, con):
        assert isinstance(ant, FuzzyTerm), "ant isn't FuzzyTerm"
        assert isinstance(con, FuzzyTerm), "con isn't FuzzyTerm"
        self._ant = ant  # antecedent (usually a composite of several fuzzy sets and operators)
        self._con = con  # consequence (usually a single fuzzy set, but can be several ANDed together)

    def set_confidence_of_consequent_to_zero(self):
        self._con.clear_dom()

    def calculate(self):
        """This method updates the DOM (the confidence) of the consequent term with
        the DOM of the antecedent term."""
        self._con.or_with_dom(self._ant.dom)


class FuzzyModule(object):
    # you must pass one of these values to the defuzzify method. This module
    # only supports the MaxAv and centroid methods.
    DeFuzzifyMethod = _DeFuzzifyMethodEnum

    def __init__(self):
        # a map of all the fuzzy variables this module uses
        self._variables = {}  # {str: fuzzy_variable}
        # a vector containing all the fuzzy rules
        self._rules = []

    def _set_confidences_of_consequents_to_zero(self):
        """Zeros the DOMs of the consequents of each rule. Used by Defuzzify()."""
        for rule in self._rules:
            rule.set_confidence_of_consequent_to_zero()

    def create_FLV(self, var_name):
        """Creates a new 'empty' fuzzy variable and returns a reference to it."""
        variable = FuzzyVariable()
        self._variables[var_name] = variable
        return variable

    def add_rule(self, antecedent, consequent):
        """Adds a rule to the module."""
        self._rules.append(FuzzyRule(antecedent, consequent))

    def fuzzify(self, name_of_flv, val):
        """This method calls the fuzzify method of the named FLV"""
        assert name_of_flv in self._variables.keys(), "key {0} not found in fuzzify".format(name_of_flv)
        self._variables[name_of_flv].fuzzify(val)

    def de_fuzzify(self, key, method=_DeFuzzifyMethodEnum.max_av, num_samples=15):
        """
        Given a fuzzy variable and a defuzification method this returns a crisp value.

        :param key: The name of the variable.
        :param method: The method to use for the deffuzification.
        :param num_samples: when calculating the centroid of the fuzzy manifold this value is used to determine how
                many cross -sections should be sampled
        :return: the defuzzified, crisp value
        """
        # first make sure the key exists
        assert key in self._variables.keys(), "key {0} not found in de_fuzzify".format(key)

        # clear the DOMs of all the consequents of all the rules
        self._set_confidences_of_consequents_to_zero()

        # process the rules
        for rule in self._rules:
            rule.calculate()

        # now defuzzify the resultant conclusion using the specified method
        if method == _DeFuzzifyMethodEnum.centroid:
            return self._variables[key].defuzzify_centroid(num_samples)
        elif method == _DeFuzzifyMethodEnum.max_av:
            return self._variables[key].defuzzify_max_av()
        else:
            raise Exception("Unknown defuzzify method " + str(method))

        return 0


class FuzzyTerm(object):
    def clone(self):
        raise NotImplementedError()

    @property
    def dom(self):
        """retrieves the degree of membership of the term"""
        raise NotImplementedError

    def clear_dom(self):
        """clears the degree of membership of the term"""
        raise NotImplementedError()

    def or_with_dom(self, val):
        """method for updating the DOM of a consequent when a rule fires"""
        raise NotImplementedError()


class FuzzyOperators(object):
    class FzAnd(FuzzyTerm):
        """The AND operator returns the minimum DOM of the sets it is operating on."""
        def __init__(self, op1, op2, op3=None, op4=None):
            self._terms = [op1, op2]
            if op3:
                self._terms.append(op3)
            if op4:
                self._terms.append(op4)

        def clone(self):
            return FuzzyOperators.FzAnd(*self._terms)

        @property
        def dom(self):
            smallest = sys.maxsize
            for cur_term in self._terms:
                if cur_term.dom < smallest:
                    smallest = cur_term.dom
            return smallest
            # return min((_t.dom for _t in self._terms))

        def clear_dom(self):
            for cur_term in self._terms:
                cur_term.clear_dom()

        def or_with_dom(self, val):
            for cur_term in self._terms:
                cur_term.or_with_dom(val)

    class FzOr(FuzzyTerm):
        """The OR operator returns the maximum DOM of the sets it it operating on."""
        def __init__(self, op1, op2, op3=None, op4=None):
            self._terms = [op1, op2]
            if op3:
                self._terms.append(op3)
            if op4:
                self._terms.append(op4)

        def clone(self):
            return FuzzyOperators.FzOr(*self._terms)

        @property
        def dom(self):
            largest = -sys.maxsize
            for cur_term in self._terms:
                if cur_term.dom > largest:
                    largest = cur_term.dom
            return largest

        def clear_dom(self):
            raise NotImplementedError("Not used, invalid context")

        def or_with_dom(self, val):
            raise NotImplementedError("Not used, invalid context")


class FzSet(FuzzyTerm):
    def __init__(self, fuzzy_set):
        assert isinstance(fuzzy_set, FuzzySet), "fuzzy_set should be of type FuzzySet"
        self._fuzzy_set = fuzzy_set

    @property
    def dom(self):
        return self._fuzzy_set.dom

    def clone(self):
        return FzSet(self._fuzzy_set)

    def clear_dom(self):
        self._fuzzy_set.clear_dom()

    def or_with_dom(self, val):
        self._fuzzy_set.or_with_dom(val)


class FuzzyHedges(object):
    class FzVery(FuzzyTerm):

        def __init__(self, ft):
            assert isinstance(ft, FzSet), "should be FzSet"
            self._set = ft._fuzzy_set

        def clone(self):
            return FuzzyHedges.FzVery(self)

        @property
        def dom(self):
            return self._set.dom * self._set.dom

        def clear_dom(self):
            self._set.clear_dom()

        def or_with_dom(self, val):
            self._set.or_with_dom(val * val)

    class FzFairly(FuzzyTerm):

        def __init__(self, ft):
            assert isinstance(ft, FzSet), "should be FzSet"
            self._set = ft._fuzzy_set

        def clone(self):
            return FuzzyHedges.FzFairly(self)

        @property
        def dom(self):
            return self._set.dom ** 0.5  # sqrt

        def clear_dom(self):
            self._set.clear_dom()

        def or_with_dom(self, val):
            self._set.or_with_dom(val ** 0.5)
