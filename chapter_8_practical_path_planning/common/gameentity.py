# -*- coding: utf-8 -*-

"""
This mode contains the base entities that are used in the games.
"""
from __future__ import print_function, division

from math import acos as math_acos

from common.smoother import Smoother
from common.space_2d.vectors import EPSILON
from common.space_2d.vectors import PI_DIV_180
from common.space_2d.vectors import Vec2 as Vec
from common.steeringbehaviors import SteeringBehavior


class BaseGameEntity(object):
    """
    The base game entity. Provides the basic properties that every entity should have.
    """
    _next_valid_id = 0

    def __init__(self, position, scale, bounding_radius, new_id):
        self._id = -1
        self.position = position  # vec
        self.scale = scale
        self.bounding_radius = bounding_radius
        self.tag = False

        assert new_id >= BaseGameEntity._next_valid_id, "invalid id '{0}' <= {1}".format(new_id,
                                                                                         BaseGameEntity._next_valid_id)
        self._id = new_id
        BaseGameEntity._next_valid_id = self._id + 1
        self.entity_type = -1

    ID = property(lambda self: self._id, "unique id")

    def update(self):
        pass

    def handle_message(self, message):
        return False

    @staticmethod
    def get_next_valid_id():
        return BaseGameEntity._next_valid_id

    @staticmethod
    def reset_next_valid_id():
        BaseGameEntity._next_valid_id = 0


class MovingEntity(BaseGameEntity):
    """
    The base moving entity having additional properties needed for moving an entity
    with a (simple) physical simulation.
    """

    def __init__(self,
                 position,
                 bounding_radius,
                 velocity,
                 max_speed,
                 heading,
                 mass,
                 scale,
                 max_turn_rate,
                 max_force):
        super(MovingEntity, self).__init__(position, scale, bounding_radius, BaseGameEntity.get_next_valid_id())
        # vector types
        self.velocity = velocity
        self.heading = heading.normalized
        self.side = self.heading.perp
        # scalar types
        self.mass = mass
        self.max_speed = max_speed
        self.max_force = max_force
        self.max_turn_rate = max_turn_rate

    def update(self, dt):
        pass

    def draw(self):
        pass

    def rotate_heading_to_face_position(self, target):
        to_target = (target - self.position).normalized
        dot = self.heading.dot(to_target)

        # correct inaccuracy by clamping into range [-1, 1] to be valid for acos
        dot = dot if dot < 1 else 1
        dot = dot if dot > -1 else -1

        angle = math_acos(dot)

        # clamp the amount to turn to the max turn rate
        if angle > self.max_turn_rate:
            angle = self.max_turn_rate

        # rotate the heading and velocity
        self.heading.rotate(angle / PI_DIV_180)
        self.velocity.rotate(angle / PI_DIV_180)

        # update/recreate the side also
        self.side = self.heading.perp

    def set_heading(self, new_heading):
        self.heading = new_heading
        self.side = self.heading.perp

    def enforce_non_penetration(self, entities):
        for entity in entities:
            if entity is not self:
                to_entity = self.position - entity.position
                dist_from_each_other = to_entity.length
                amount_of_overlap = entity.bounding_radius + self.bounding_radius - dist_from_each_other
                if amount_of_overlap > 0:
                    entity.position -= (to_entity / dist_from_each_other) * amount_of_overlap


class Vehicle(MovingEntity):
    """
    The base vehicle entity that uses steering behaviors to move around the world.
    """

    def __init__(self, world, position, bounding_radius, velocity, max_speed, heading, mass, scale, max_turn_rate,
                 max_force, params, obstacles=[]):
        super(Vehicle, self).__init__(position,
                                      bounding_radius,
                                      velocity,
                                      max_speed,
                                      heading,
                                      mass,
                                      scale,
                                      max_turn_rate,
                                      max_force)
        self.world = world
        self.steering = SteeringBehavior(self, world)
        self._heading_smoother = Smoother(Vec(0, 0), params.NumSamplesForSmoothing)
        self.smoothed_heading = None
        self.is_smoothing_on = False
        self._time_elapsed = 0.0
        self.color = (0, 0, 200)  # rgb
        self.obstacles = obstacles
        self.is_non_penetration_on = False
        self._params = params

    # def toggle_smoothing(self):
    # self.is_smoothing_on = not self.is_smoothing_on

    def update(self, elapsed):
        steering_force = self.steering.calculate()
        acceleration = steering_force / self.mass
        self.velocity += acceleration * elapsed
        # make sure it does not exceed max speed
        self.velocity.truncate(self.max_speed)
        self.position += self.velocity * elapsed
        # update heading if it has a velocity
        if self.velocity.length_sq > EPSILON:
            self.heading = self.velocity.normalized
            self.side = self.heading.perp

        if self.is_non_penetration_on:
            self.enforce_non_penetration(self.world.vehicles)

        # treat the screen as a toroid
        self.position.wrap_around(self.world.size)

    def enforce_non_penetration(self, entities):
        for entity in entities:
            if entity is not self:
                to_entity = self.position - entity.position
                dist_from_each_other = to_entity.length
                amount_of_overlap = entity.bounding_radius + self.bounding_radius - dist_from_each_other
                if amount_of_overlap > 0:
                    entity.position -= (to_entity / dist_from_each_other) * amount_of_overlap

    def create_feelers(self):
        """
        Returns feelers in local coordinates.
        """
        feelers = []
        feeler_length = self._params.WallDetectionFeelerLength
        feelers.append(self.position + self.heading * feeler_length)
        feelers.append(
            self.position + (self.heading + self.side) * feeler_length * 0.5 * 0.70710678118654752440084436210485)
        feelers.append(
            self.position + (self.heading - self.side) * feeler_length * 0.5 * 0.70710678118654752440084436210485)
        return feelers
