# -*- coding: utf-8 -*-
from __future__ import print_function, division
from raven.raven_graphtimesliced import TARGET_FOUND, TARGET_NOT_FOUND


class PathManager(object):
    """Class to manage a number of graph searcher and to distribute
    the calculation of each search over several update steps"""

    def __init__(self, num_search_cycles_per_update):
        # container of all the active search requests
        self._search_requests = []

        # this is the total number of search cycles allocated to the manager
        # each update step these are divided equally amongst all registered path requests
        self._num_search_cycles_per_update = num_search_cycles_per_update

    def update_searches(self):
        """Every time this is called the total amount of search cycles available  will
        be shared out equally between all the active path requests. If a search
        completes successfully or fails the method will notify the relevant"""
        # this method iterates through all the active path planning requests updating
        # their searches until the user specified total number of search cycles has been satisfied
        # if a path is found or the search is unsuccessful the rlevant agent is notified accordingly by telegram
        num_cycles_remaining = self._num_search_cycles_per_update
        current_idx = 0
        while num_cycles_remaining > 0 and self._search_requests:
            num_cycles_remaining -= 1
            current_planner = self._search_requests[current_idx]
            result = current_planner.cycle_once()

            # if the search has terminated remove from the list
            if result == TARGET_FOUND or result == TARGET_NOT_FOUND:
                self._search_requests.remove(current_planner)
            else:
                # more on to the next
                current_idx += 1

            # the iterator may now be pointing to the end of the list
            # it must be reset to the beginning
            if current_idx >= len(self._search_requests):
                current_idx = 0

    def register(self, path_planner):
        """A path planner should call this method to register a search with the manager.
        (the method checks to ensure the path planner is registered only once"""
        if path_planner not in self._search_requests:
            self._search_requests.append(path_planner)

    def unregister(self, path_planner):
        """Remove a path planner"""
        if path_planner in self._search_requests:
            self._search_requests.remove(path_planner)

    def get_num_active_searches(self):
        return len(self._search_requests)


