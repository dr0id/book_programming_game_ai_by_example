**"Programming Game AI by Example"** python code examples
==================================================

![screenshots](screenshots.png "Screenshots")

This repository contains code examples for the book **Programming Game AI by Example** 
written in python. 

Example code for:

- state driven agents
- steering behaviors
- graph theory and path search algorithms (depth first search, breadth first search, edge relaxation, shortest path trees, Dijkstra's search, A*
- hierarchical goal based agents
- fuzzy logic


The Book
--------

More info about the book: http://www.ai-junkie.com/books/index.html

Here I want to give my thanks to the author because I learned many things 
and had fun implementing those examples.


Missing chapters
----------------
Not all chapters of the book have accompanying code. 
That's why some chapters are missing in the repo. Chapter 8 contains the code 
for chapters 8 through 10 of the book.

I really suggest to read the book and try to understand its contents.

If you find any issue with the code let me know through the issue tracker. Or leave a comment on https://www.pygame.org/project/4965


Code style
----------
The code is heavily based on the available C++ examples and might not be as 
pythonic as it could be. There are parts that have been adapted to the 
language due to the language features. There might also be some changes in 
other parts of the code where I felt it was needed.

C++ code examples can be found here: http://www.jblearning.com/Catalog/9781556220784/student/


Dependencies:
-------------

  - python (tested with 2.7 and 3.7.3)
  - pygame (tested with 1.9.6 and 2.0.0.dev10)


How to run
----------
All runnable files begin with 'demo_' (e.g. 'demo_soccer.py') except for chapter 8 (see readme.txt there). 

  - Windows: 
    
    either double click on the file (e.g. demo_soccer.py) or type in a console: python demo_soccer.py


  - Others: 
   
    type in a console: python demo_soccer.py


License
-------

_New BSD license_

See 'license.text' for details.


